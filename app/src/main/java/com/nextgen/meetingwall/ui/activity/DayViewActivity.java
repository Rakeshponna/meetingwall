package com.nextgen.meetingwall.ui.activity;

/**
 * Created by cas on 20-03-2017.
 */

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.nextgen.meetingwall.R;
import com.nextgen.meetingwall.enums.TimeEnum;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Sherif
 *
 * Copyright 2011
 *
 * Sample Day Viewer that will show entries of each hour with ability to
 * add events and stuff
 * You should find a way to keep a container that will keep track of added events
 *
 */
public class DayViewActivity extends ListActivity {
    /**
     * Called when the activity is first created.
     */
    private static int HOURS_PER_DAY = 24;
    String myTime = "08:00";
    String newTime;
    Context mContext = this;

    ArrayList<String> timeList=new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getListView().setBackgroundColor(Color.rgb(12, 12, 12));
        getListView().setDividerHeight(0);
        setListAdapter(new ListAdapter() {

            @Override
            public boolean areAllItemsEnabled() {
                // TODO Auto-generated method stub
                return false;
            }

            @Override
            public boolean isEnabled(int arg0) {
                // TODO Auto-generated method stub
                return false;
            }

            @Override
            public int getCount() {
                // TODO Auto-generated method stub
                return HOURS_PER_DAY;
            }

            @Override
            public Object getItem(int arg0) {
                // TODO Auto-generated method stub
                return null;
            }

            @Override
            public long getItemId(int arg0) {
                // TODO Auto-generated method stub
                return 0;
            }

            @Override
            public int getItemViewType(int arg0) {
                // TODO Auto-generated method stub
                return 0;
            }

            @Override
            public View getView(int position, View arg1, ViewGroup arg2) {
                // TODO Auto-generated method stub
                LayoutInflater inflater = getLayoutInflater();
                View listItem = (View) inflater.inflate(R.layout.list_item, getListView(), false);
                TextView hourTV = (TextView) listItem.findViewById(R.id.hourTV);
                TextView amTV = (TextView) listItem.findViewById(R.id.amTV);
                hourTV.setTextColor(Color.BLUE);
                amTV.setTextColor(Color.BLUE);
                final LinearLayout eventsLL = (LinearLayout) listItem.findViewById(R.id.eventsLL);

                SimpleDateFormat df = new SimpleDateFormat("HH:mm");


                Date d = null;

                try {
                    if(timeList.size()==0){
                        d = df.parse(myTime);
                    }else{
                        try{
                            d = df.parse(timeList.get((position)-1));
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    }

                    Calendar cal = Calendar.getInstance();
                    cal.setTime(d);
                    cal.add(Calendar.MINUTE, 15);
                     newTime = df.format(cal.getTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                timeList.add(newTime);
                hourTV.setText(newTime); //String.valueOf((position + 8) % 24)
                //I set am/pm for each entry ... you could specify which entries
                if (((position >= 0) && (position <= 2)) || ((position >= 15) && (position <= 23)))
                    amTV.setText("am");
                else
                    amTV.setText("pm");
                eventsLL.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        // TODO Auto-generated method stub
                        AlertDialog.Builder alert = new AlertDialog.Builder(mContext);

                        alert.setTitle("New Event");
                        alert.setMessage("Event:");

                        // Set an EditText view to get user input
                        final EditText input = new EditText(mContext);
                        alert.setView(input);

                        alert.setPositiveButton("Add", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                TextView A = new TextView(mContext);
                                A.setText(input.getText());
                                A.setTextColor(Color.BLACK);
                                eventsLL.addView(A);
                            }
                        });

                        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                            }
                        });
                        alert.show();
                    }

                });
                return listItem;
            }

            @Override
            public int getViewTypeCount() {
                // TODO Auto-generated method stub
                return 1;
            }

            @Override
            public boolean hasStableIds() {
                // TODO Auto-generated method stub
                return false;
            }

            @Override
            public boolean isEmpty() {
                // TODO Auto-generated method stub
                return false;
            }

            @Override
            public void registerDataSetObserver(DataSetObserver arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void unregisterDataSetObserver(DataSetObserver arg0) {
                // TODO Auto-generated method stub

            }

        });
    }
}