package com.nextgen.meetingwall.ui.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.nextgen.meetingwall.R;
import com.nextgen.meetingwall.adapter.ContactListAdapter;
import com.nextgen.meetingwall.listeners.OnCustomItemSelectListener;
import com.nextgen.meetingwall.model.ModuleListMetadataArray;
import com.nextgen.meetingwall.network.Network;
import com.nextgen.meetingwall.network.RequestCallback;
import com.nextgen.meetingwall.network.json.MeetingEmployeesResponse;
import com.nextgen.meetingwall.network.json.MeetingLocationsResponse;
import com.nextgen.meetingwall.network.json.MeetingWraper;
import com.nextgen.meetingwall.stateprogressbar.StateProgressBar;
import com.nextgen.meetingwall.ui.dialog.DialogFactory;
import com.nextgen.meetingwall.utils.Constants;
import com.nextgen.meetingwall.utils.Utils;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cc.cloudist.acplibrary.ACProgressFlower;

/**
 * Created by cas on 27-03-2017.
 */

public class ContactsListActivity extends AppCompatActivity implements OnCustomItemSelectListener, View.OnClickListener {

    protected String[] descriptionData = {"Setup", "Agenda", "Essentials", "Review"};
    @BindView(R.id.usage_stateprogressbar)
    protected StateProgressBar stateprogressbar;

    @BindView(R.id.Contacts_listview)
            protected ListView ContactsListview;
    @BindView(R.id.Selected_Meeting_locationTime_name)
            protected TextView SelectedMeetingTimeLocationTextView;

    @BindView(R.id.Selected_contacts_TextView)
            protected TextView SelectedContactsTextView;
    @BindView(R.id.side_index)
    protected  LinearLayout indexLayout;
    @OnClick(R.id.Deselecte_all_TextView)
    void Deselect(){
        SelectedListItems.clear();
  //      indexLayout.removeAllViews();

        for(int i=0; i<meetingBeenArrayList.size(); i++){
            meetingBeenArrayList.get(i).setSelected(false);
        }
        Contactlistadapter.notifyDataSetChanged();
        UpdateUI();
    }

    @OnClick(R.id.Add_Participants)
            void AddParticipants(){

    }

    ContactListAdapter Contactlistadapter;
    ArrayList<MeetingEmployeesResponse.RowsBeen> meetingBeenArrayList = new ArrayList<>();
    Map<String, Integer> mapIndex;
    ArrayList<String> SelectedListItems =new ArrayList<>();
    ArrayList<MeetingEmployeesResponse.RowsBeen>SelectedDataArray=new ArrayList<MeetingEmployeesResponse.RowsBeen>();

    private static final String TAG = "Network";
    private final Network network = new Network(this);
    int FirstInt=0;
    int RowsInt=10;int SortOrderInt=1;
    int PageInt=1;
    private ACProgressFlower dialog;

    ArrayList<ModuleListMetadataArray> mMeetingListMetaDataArray = new ArrayList<>();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_contacts_list);
        ButterKnife.bind(this);
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            getWindow().setStatusBarColor(getResources()
                    .getColor(R.color.colorPrimary));
        }
        LinearLayout backLayout = (LinearLayout) findViewById(R.id.back_to_layout);
        Button doneButton       = (Button) findViewById(R.id.select_button);

        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(ContactsListActivity.this, MainActivity.class);
                intent.putExtra(Constants.SELECTED_DAY, SelectedDataArray);
                setResult(RESULT_OK, intent);
                finish();
                overridePendingTransition(0, R.anim.push_down_in);
            }
        });
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(0, R.anim.push_down_in);
            }
        });




        stateprogressbar.setStateDescriptionData(descriptionData);
  //      prepareMeetingsData();
        prepareMeetingsEmployees();

        Contactlistadapter = new ContactListAdapter(this, meetingBeenArrayList,  this);
        ContactsListview.setAdapter(Contactlistadapter);
        ContactsListview.setFocusable(false);
     //   Utils.setListViewHeightBasedOnChildren(ContactsListview);
        Contactlistadapter.notifyDataSetChanged();


        SelectedMeetingTimeLocationTextView.setText("29th March 2017 from 9:00 Am to 9:30 Am @ Mystic River,Hyderabad");
    }

    private void prepareMeetingsEmployees() {
        dialog = new ACProgressFlower.Builder(this)
                .text(getResources().getString(R.string.label_loading))
                .build();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        network.MeetingEmployees(new RequestCallback<ArrayList<ModuleListMetadataArray>>() {
            @Override
            public void onSuccess(ArrayList<ModuleListMetadataArray> response) {

                Log.e(TAG, "Success response::" + response);
                onSuccesEmployees(response);

            }

            @Override
            public void onFailed(String message) {

                Log.e(TAG, "Failed response::" + message);
            }
        });


    }
    private void onSuccesEmployees(final ArrayList<ModuleListMetadataArray> jobsList) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                mMeetingListMetaDataArray.clear();
                if (jobsList.size() == 0) {
                    dialog(getResources().getString(R.string.label_server_data_notfound));
                } else {
                    for (int i = 0; i < jobsList.size(); i++) {

                        mMeetingListMetaDataArray.add(jobsList.get(i));
                    }

                    ListDataMeetingEmployees();
                }
            }
        });
    }

    private void ListDataMeetingEmployees(){
        MeetingWraper wrapper = new MeetingWraper();
        wrapper.setFirstInt(FirstInt);
        wrapper.setRowsInt(RowsInt);
        wrapper.setSortOrderInt(SortOrderInt);
        //   wrapper.setPageInt(PageInt);
        //    wrapper.setFiltersObject(new FiltersObjectBeen());
        //    wrapper.setGlobalFilter(null);
        wrapper.setDisplayColsArray(mMeetingListMetaDataArray);

        network.ListDataMeetingEmployees(wrapper,new RequestCallback<MeetingEmployeesResponse>() {
            @Override
            public void onSuccess(MeetingEmployeesResponse response) {

                Log.e(TAG, "Success response::" + response);
                onSuccesEmployeesResponce(response);
            }

            @Override
            public void onFailed(String message) {

                Log.e(TAG, "Failed response::" + message);
            }
        });


    }
    private void onSuccesEmployeesResponce(final MeetingEmployeesResponse response){
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                meetingBeenArrayList.clear();

                if (response.getJqGridData().getRowsArray().size() > 0) {
                    for (int i = 0; i < response.getJqGridData().getRowsArray().size(); i++) {
                        response.getJqGridData().getRowsArray().get(i).setSelected(false);
                        meetingBeenArrayList.add(response.getJqGridData().getRowsArray().get(i));
                    }


                    getIndexList(meetingBeenArrayList);
                    displayIndex();
                    Collections.sort(meetingBeenArrayList, new Comparator<MeetingEmployeesResponse.RowsBeen>() {
                        public int compare(MeetingEmployeesResponse.RowsBeen v1, MeetingEmployeesResponse.RowsBeen v2) {
                            return v1.getEmployeeFirstName().compareTo(v2.getEmployeeFirstName());
                        }
                    });

              //      Utils.setListViewHeightBasedOnChildren(ContactsListview);
                    Contactlistadapter.notifyDataSetChanged();
                    dialog.cancel();
                }
            }
        });

    }

    private void dialog(final String message) {
        DialogFactory.showAConfirmationDialog(this, message, 0,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
    }


    @Override
    public void onItemClick(int position) {
        if(!SelectedListItems.contains(String.valueOf(position))){
            SelectedListItems.add(String.valueOf(position));
            meetingBeenArrayList.get(position).setSelected(true);
            SelectedDataArray.add( meetingBeenArrayList.get(position));
        }else{
            SelectedListItems.remove(String.valueOf(position));
            meetingBeenArrayList.get(position).setSelected(false);
        }
        Contactlistadapter.notifyDataSetChanged();
        UpdateUI();
    }

    private void UpdateUI() {
            SelectedContactsTextView.setText(""+SelectedListItems.size()+" Selected | ");
    }


    @Override
    public void onSelectedItemClick(int position, int type) {

    }
    private void getIndexList(ArrayList<MeetingEmployeesResponse.RowsBeen> fruits) {
        mapIndex = new LinkedHashMap<String, Integer>();
        for (int i = 0; i < fruits.size(); i++) {
            String fruit = String.valueOf(fruits.get(i).getEmployeeFirstName());
            String index = fruit.substring(0, 1);

            if (mapIndex.get(index) == null)
                mapIndex.put(index, i);
        }
    }

    private void displayIndex() {


        TextView textView;
        List<String> indexList = new ArrayList<String>(mapIndex.keySet());
        for (String index : indexList) {
            textView = (TextView) getLayoutInflater().inflate(
                    R.layout.side_index_item, null);
            textView.setText(index);
            textView.setOnClickListener(this);
            indexLayout.addView(textView);
        }
    }

    public void onClick(View view) {
        TextView selectedIndex = (TextView) view;
        ContactsListview.setSelection(mapIndex.get(selectedIndex.getText()));
    }


}

