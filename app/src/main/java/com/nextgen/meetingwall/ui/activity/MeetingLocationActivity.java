package com.nextgen.meetingwall.ui.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nextgen.meetingwall.R;
import com.nextgen.meetingwall.adapter.CountryLocationListAdapter;
import com.nextgen.meetingwall.adapter.MeetingLocationAdapter;
import com.nextgen.meetingwall.model.ModuleListMetadataArray;
import com.nextgen.meetingwall.network.Network;
import com.nextgen.meetingwall.network.RequestCallback;
import com.nextgen.meetingwall.network.json.MeetingLocationsResponse;
import com.nextgen.meetingwall.network.json.MeetingWraper;
import com.nextgen.meetingwall.stateprogressbar.StateProgressBar;
import com.nextgen.meetingwall.ui.dialog.DialogFactory;
import com.nextgen.meetingwall.utils.Constants;
import com.nextgen.meetingwall.utils.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import cc.cloudist.acplibrary.ACProgressFlower;

/*
 * Created by cas on 24-03-2017.
 */

@SuppressWarnings("deprecation")
public class MeetingLocationActivity extends AppCompatActivity {

    protected String[] descriptionData = {"Setup", "Agenda", "Essentials", "Review"};

    @BindView(R.id.Country_location_listview)
    protected ListView MeetingLocationListView;
    @BindView(R.id.Meeting_location_ListView)
    protected ListView MeetingPlacesListView;
    @BindView(R.id.usage_stateprogressbar)
    protected StateProgressBar stateprogressbar;
    @BindView(R.id.Selected_location_name)
    protected TextView SelectedLocationTextView;
    @BindView(R.id.errorView)
    protected RelativeLayout errorView;
    @BindView(R.id.errorMessageText)
    protected TextView errorMessageText;
    private ACProgressFlower dialog;

    private static final String TAG = "Network";
    private final Network network = new Network(this);
    protected int FirstInt = 0;
    protected int RowsInt = 10;
    protected int SortOrderInt = 1;

    private ArrayList<ModuleListMetadataArray> mMeetingListMetaDataArray = new ArrayList<>();
    private ArrayList<MeetingLocationsResponse.RowsBeen> mMeetingLocationsArrayList = new ArrayList<>();
    private ArrayList<MeetingLocationsResponse.RowsBeen> mMeetingPlacesArrayList = new ArrayList<>();
    private ArrayList<MeetingLocationsResponse.RowsBeen> mMeetingDataArrayList = new ArrayList<>();
    private ArrayList<MeetingLocationsResponse.RowsBeen> SelectedDataArray = new ArrayList<>();

    private CountryLocationListAdapter mLocationAdapter;
    private MeetingLocationAdapter mPlacesAdapter;
    private MeetingLocationsResponse.RowsBeen SelectedLocationDat;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_location);

        ButterKnife.bind(this);
        stateprogressbar.setStateDescriptionData(descriptionData);
        errorView.setVisibility(View.GONE);
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            getWindow().setStatusBarColor(getResources()
                    .getColor(R.color.colorPrimary));
        }

        LinearLayout backLayout = (LinearLayout) findViewById(R.id.back_to_layout);
        TextView backText = (TextView) findViewById(R.id.back_text_header);
        Button doneButton = (Button) findViewById(R.id.select_button);
        backText.setText(getResources().getString(R.string.Nav_NewMeeting));


        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(SelectedDataArray.size()==0){
                    displayErrorMessage(getString(R.string.label_location_select));
                }else{
                    Intent intent = new Intent(MeetingLocationActivity.this, MainActivity.class);
                    intent.putExtra(Constants.SELECTED_DAY, SelectedDataArray);
                    setResult(RESULT_OK, intent);
                    finish();
                    overridePendingTransition(0, R.anim.push_down_in);
                }
            }
        });

        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(0, R.anim.push_down_in);

            }
        });

        MeetingPlacesListView.setVisibility(View.GONE);
        prepareMeetingsPlaces();

        mLocationAdapter = new CountryLocationListAdapter(this, mMeetingLocationsArrayList);
        MeetingLocationListView.setAdapter(mLocationAdapter);
        MeetingLocationListView.setFocusable(false);
        mLocationAdapter.notifyDataSetChanged();


        MeetingLocationListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position,
                                    long arg3) {
                mMeetingPlacesArrayList.clear();
                for (int i = 0; i < mMeetingDataArrayList.size(); i++) {
                    if (mMeetingDataArrayList.get(i).getLocationpkId() == mMeetingLocationsArrayList.get(position).getLocationpkId()){
                        mMeetingPlacesArrayList.add(mMeetingDataArrayList.get(i));
                    }
                }
                MeetingPlacesListView.setVisibility(View.VISIBLE);
                mPlacesAdapter.notifyDataSetChanged();
                Utils.setListViewHeightBasedOnChildren(MeetingPlacesListView);
            }
        });

        MeetingPlacesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position,
                                    long arg3) {
                SelectedLocationDat = mMeetingPlacesArrayList.get(position);
                SelectedDataArray.add(SelectedLocationDat);
            }
        });
        mPlacesAdapter = new MeetingLocationAdapter(this, mMeetingPlacesArrayList);
        MeetingPlacesListView.setAdapter(mPlacesAdapter);
        MeetingPlacesListView.setFocusable(false);
    }

    private void prepareMeetingsPlaces() {
        dialog = new ACProgressFlower.Builder(this)
                .text(getResources().getString(R.string.label_loading))
                .build();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        network.MeetingPlaces(new RequestCallback<ArrayList<ModuleListMetadataArray>>() {
            @Override
            public void onSuccess(ArrayList<ModuleListMetadataArray> response) {
                Log.e(TAG, "Success response::" + response);
                onSuccesPlaces(response);
            }

            @Override
            public void onFailed(String message) {
                Log.e(TAG, "Failed response::" + message);
            }
        });


    }

    private void onSuccesPlaces(final ArrayList<ModuleListMetadataArray> jobsList) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                mMeetingListMetaDataArray.clear();
                if (jobsList.size() == 0) {
                    dialog(getResources().getString(R.string.label_server_data_notfound));
                } else {
                    for (int i = 0; i < jobsList.size(); i++) {
                        mMeetingListMetaDataArray.add(jobsList.get(i));
                    }
                    ListDataMeetingPlaces();
                }
            }
        });
    }


    private void ListDataMeetingPlaces() {
        MeetingWraper wrapper = new MeetingWraper();
        wrapper.setFirstInt(FirstInt);
        wrapper.setRowsInt(RowsInt);
        wrapper.setSortOrderInt(SortOrderInt);
        //   wrapper.setPageInt(PageInt);
        //    wrapper.setFiltersObject(new FiltersObjectBeen());
        //    wrapper.setGlobalFilter(null);
        wrapper.setDisplayColsArray(mMeetingListMetaDataArray);

        network.ListDataMeetingPlaces(wrapper, new RequestCallback<MeetingLocationsResponse>() {
            @Override
            public void onSuccess(MeetingLocationsResponse response) {
                Log.e(TAG, "Success response::" + response);
                onSuccesPlacesResponce(response);
            }

            @Override
            public void onFailed(String message) {
                Log.e(TAG, "Failed response::" + message);
            }
        });


    }

    private void onSuccesPlacesResponce(final MeetingLocationsResponse response) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mMeetingPlacesArrayList.clear();
                ArrayList<Integer> mLocationpkId = new ArrayList<>();
                if (response.getJqGridData().getRowsArray().size() > 0) {
                    for (int i = 0; i < response.getJqGridData().getRowsArray().size(); i++) {
                        mMeetingDataArrayList.add(response.getJqGridData().getRowsArray().get(i));
                        if (!mLocationpkId.contains(response.getJqGridData().getRowsArray().get(i).getLocationpkId())) {
                            mLocationpkId.add(response.getJqGridData().getRowsArray().get(i).getLocationpkId());
                            mMeetingLocationsArrayList.add(response.getJqGridData().getRowsArray().get(i));
                        }
                        if (response.getJqGridData().getRowsArray().get(i).getLocationpkId() == mMeetingLocationsArrayList.get(0).getLocationpkId()) {
                            mMeetingPlacesArrayList.add(response.getJqGridData().getRowsArray().get(i));
                        }
                    }

                    mPlacesAdapter.notifyDataSetChanged();
                    mLocationAdapter.notifyDataSetChanged();
                    Utils.setListViewHeightBasedOnChildren(MeetingPlacesListView);
                    Utils.setListViewHeightBasedOnChildren(MeetingLocationListView);
                    dialog.cancel();
                }
            }
        });

    }

    private void dialog(final String message) {
        DialogFactory.showAConfirmationDialog(this, message, 0,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
    }

    public void displayErrorMessage(String message) {
        errorMessageText.setText(message);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) errorMessageText.getLayoutParams();
        if (errorMessageText.getLineCount() == 3) {
            params.height = getResources().getDimensionPixelSize(R.dimen.search_screen_layout_height);
            errorMessageText.setLayoutParams(params);
        } else {
            params.height = getResources().getDimensionPixelSize(R.dimen.signIn_page_reset_mail_view);
            errorMessageText.setLayoutParams(params);
        }
        if (errorView.getAlpha() == 0) {
            errorView.setTranslationY(-60);
            stateprogressbar.setVisibility(View.GONE);
            errorView.setVisibility(View.VISIBLE);
            errorView.setBackgroundResource(R.color.error_message_background);
            errorView.animate().translationY(0)
                    .alpha(1)
                    .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    errorView.animate()
                            .alpha(0)
                            .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
                    errorView.setVisibility(View.GONE);
                    stateprogressbar.setVisibility(View.VISIBLE);
                }
            }, 2000);

        }
    }

}