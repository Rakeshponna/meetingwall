package com.nextgen.meetingwall.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.materialcalendarview.CalendarDay;
import com.materialcalendarview.MaterialCalendarView;
import com.materialcalendarview.OnDateSelectedListener;
import com.materialcalendarview.been.CalendarBeen;
import com.materialcalendarview.decorators.EventDecorator;
import com.materialcalendarview.decorators.MySelectorDecorator;
import com.materialcalendarview.decorators.OneDayDecorator;
import com.nextgen.meetingwall.R;
import com.nextgen.meetingwall.adapter.CalendarMeetingDetailsAdapter;
import com.nextgen.meetingwall.adapter.CalendarNationalitylocationAdapter;
import com.nextgen.meetingwall.adapter.CalendarRoomlocationAdapter;
import com.nextgen.meetingwall.adapter.MeeetingTimeSlotAdapter;
import com.nextgen.meetingwall.listeners.OnCustomItemSelectListener;
import com.nextgen.meetingwall.model.CalendarMeetingsBeen;
import com.nextgen.meetingwall.model.OthermeetingdetailsBeen;
import com.nextgen.meetingwall.model.RoomLocationBeen;
import com.nextgen.meetingwall.model.SearchLocationBeen;
import com.nextgen.meetingwall.model.TimeslotBeen;
import com.nextgen.meetingwall.stateprogressbar.StateProgressBar;
import com.nextgen.meetingwall.utils.Constants;
import com.nextgen.meetingwall.utils.Utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.Executors;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;

/**
 * Shows off the most basic usage
 */
@SuppressWarnings("ALL")
public class CalendarActivityDecorated extends AppCompatActivity implements OnDateSelectedListener,OnCustomItemSelectListener {
    private Fragment fragment;
    private final OneDayDecorator oneDayDecorator = new OneDayDecorator();
    private MeeetingTimeSlotAdapter mAdapter;
    private ArrayList<TimeslotBeen> meetingBeenList = new ArrayList<>();
    private ArrayList<OthermeetingdetailsBeen> list = new ArrayList<>();
    private ArrayList<String> Datelist = new ArrayList<>();
    private ArrayList<String> meetingStatuslist = new ArrayList<>();
    private ArrayList<String> Daylist = new ArrayList<>();
    private ArrayList<String> DesignReviewlist = new ArrayList<>();
    private ArrayList<String> Timelist = new ArrayList<>();
    private ArrayList<String> SearchlocationsArray = new ArrayList<>();
    private ArrayList<String> MeetingNamesArray = new ArrayList<>();
    private ArrayList<String> MeetingTimeArray = new ArrayList<>();
    private ArrayList<String> MeetingLocationArray = new ArrayList<>();
    private ArrayList<String> meetingLocationlistTime = new ArrayList<>();

    private ArrayList<ArrayList<RoomLocationBeen>> LocationlistRoomArray = new ArrayList<>();
    private ArrayList<RoomLocationBeen> AlaskalocationsArray = new ArrayList<>();
    private ArrayList<RoomLocationBeen> HydlocationsArray = new ArrayList<>();
    private ArrayList<RoomLocationBeen> LondlocationsArray = new ArrayList<>();
    private ArrayList<RoomLocationBeen> SydlocationsArray = new ArrayList<>();
    private ArrayList<SearchLocationBeen> SearchlocationArrayList = new ArrayList<>();
    private List<CalendarBeen> PresentDatesArray = new ArrayList<>();
    private ArrayList<String> meetingList = new ArrayList<>();
    private ArrayList<CalendarBeen> PreviousDatesArray= new ArrayList<>();
    private ArrayList<CalendarBeen> UpComingDatesArray= new ArrayList<>();
    private ArrayList<CalendarMeetingsBeen> meetingBeenArrayList = new ArrayList<>();
    private CalendarMeetingDetailsAdapter forumAnswerListAdapter = null;


    protected  String SelectedDate;
    private float mActionBarHeight;
    private ActionBar mActionBar;
    protected String[] descriptionData = {"Setup", "Agenda", "Essentials", "Review"};
    protected boolean onItemclick =false;
    private  Boolean SearchLocationClick=false;
    private  Boolean firsttimeClick =false;
    int NationalityLostposition=0;
    private LayoutInflater inflater;
    private ListView NationalityListview,RoomListview;
    private View currentSelectedView;

    @BindView(R.id.calendarView)
    MaterialCalendarView widget;
    @BindView(R.id.usage_stateprogressbar)
    protected StateProgressBar stateprogressbar;
    @BindView(R.id.meeting_listview)
    protected ListView mMyRefListView = null;
    @BindView(R.id.Meeting_Location_Name_Textview)
    protected TextView MeetingLocationTextview;
    @BindView(R.id.infalte_search_list_layout)
    protected LinearLayout InflateSrachLocation;



    @Nullable
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_calendarbasic);
        ButterKnife.bind(this);
        stateprogressbar.setStateDescriptionData(descriptionData);
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            getWindow().setStatusBarColor(getResources()
                    .getColor(R.color.colorPrimary));
        }

        LinearLayout backLayout = (LinearLayout) findViewById(R.id.back_to_layout);
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(0, R.anim.push_down_in);

            }
        });

        Button doneButton = (Button) findViewById(R.id.select_button);
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onItemclick){
                    SelectedDate= String.valueOf(widget.getSelectedDate().getDate());
                }else{
                    if(SelectedDate==null){
                        //            DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
                        Calendar cal = Calendar.getInstance();
                        SelectedDate=String.valueOf((cal.getTime()));
                    }
                }
                Intent intent = new Intent(CalendarActivityDecorated.this, MainActivity.class);
                intent.putExtra(Constants.SELECTED_DAY, SelectedDate);
                setResult(RESULT_OK, intent);
                finish();
                overridePendingTransition(0, R.anim.push_down_in);



            }
        });
        prepareData();
        widget.setOnDateChangedListener(this);
        widget.setShowOtherDates(MaterialCalendarView.SHOW_ALL);

        Calendar instance = Calendar.getInstance();
        widget.setSelectedDate(instance.getTime());

        Calendar instance1 = Calendar.getInstance();
        instance1.set(instance1.get(Calendar.YEAR), Calendar.JANUARY, 1);

        Calendar instance2 = Calendar.getInstance();
        instance2.set(instance2.get(Calendar.YEAR), Calendar.DECEMBER, 31);

        widget.state().edit()
                .setMinimumDate(instance1.getTime())
                .setMaximumDate(instance2.getTime())
                .commit();

        widget.addDecorators(
                new MySelectorDecorator(this),
                oneDayDecorator
        );
        PresentEventsData();
        PreviousEventDates();
        UpcommingEventsDates();
        new ApiSimulator().executeOnExecutor(Executors.newSingleThreadExecutor());
        prepareMeetingsData();








    }

    @OnClick(R.id.Search_location_Layout)
    void Locationserch(){
        if(!firsttimeClick){
            firsttimeClick=true;
            inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View SearchLocationView = inflater.inflate(R.layout.multipule_listview, null);
            NationalityListview = ButterKnife.findById(SearchLocationView, R.id.Nationality_ListView);
            RoomListview = ButterKnife.findById(SearchLocationView, R.id.Room_listView);
            InflateSrachLocation.addView(SearchLocationView);
            InflateSrachLocation.setVisibility(View.VISIBLE);

            prepareLocationsData();
            SearchLocations(NationalityListview);
            UpdateRoomList(NationalityLostposition);
        }else{
            if(!SearchLocationClick){
                SearchLocationClick=true;
                InflateSrachLocation.setVisibility(View.VISIBLE);
            }else{
                InflateSrachLocation.setVisibility(View.GONE);
                SearchLocationClick=false;
            }
        }


    }

    @OnTouch(R.id.Calendar_Parent_ScrollView)
    protected boolean OnTouch(){
        InflateSrachLocation.setVisibility(View.GONE);
        SearchLocationClick=false;
        return false;
    }
    @Override
    public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
        //If you change a decorate, you need to invalidate decorators
        oneDayDecorator.setDate(date.getDate());
        SelectedDate= String.valueOf(date.getDate());
        Log.e("Selected Date ::", "" + date.getDate());
        widget.invalidateDecorators();
        if (meetingList.contains(String.valueOf(date.getDate()))) {
            Toast.makeText(this, "" + date.getDate(), Toast.LENGTH_LONG).show();


        }


    }


    /**
     * Simulate an API call to show how to add decorators
     */
    ArrayList<CalendarDay> Presentdates;
    ArrayList<CalendarDay> PreviesDates;
    ArrayList<CalendarDay> Upcommingdates;
    private class ApiSimulator extends AsyncTask<Void, Void, List<CalendarDay>> {

        @Override
        protected List<CalendarDay> doInBackground(@NonNull Void... voids) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Calendar calendar = Calendar.getInstance();

            //     calendar.add(Calendar.MONTH, -2);
            Presentdates = new ArrayList<>();
            for (int i = 0; i < PresentDatesArray.size(); i++) {
                calendar.set(PresentDatesArray.get(i).getYear(), PresentDatesArray.get(i).getMonth(), PresentDatesArray.get(i).getDay());
                CalendarDay day = CalendarDay.from(calendar);
                Presentdates.add(day);
                Log.e("Event Date ::", "" + Presentdates);

                //      calendar.add(Calendar.DATE, 10);

            }

            PreviesDates = new ArrayList<>();

                for (int i = 0; i < 5; i++) {
                    calendar.set(PreviousDatesArray.get(i).getYear(), PreviousDatesArray.get(i).getMonth(), PreviousDatesArray.get(i).getDay());
                    CalendarDay day = CalendarDay.from(calendar);
                    PreviesDates.add(day);
                }

            Upcommingdates = new ArrayList<>();

                for (int i = 0; i < 5; i++) {
                    calendar.set(UpComingDatesArray.get(i).getYear(), UpComingDatesArray.get(i).getMonth(), UpComingDatesArray.get(i).getDay());
                    CalendarDay day = CalendarDay.from(calendar);
                    Upcommingdates.add(day);
                }

            return Presentdates;
        }

        @Override
        protected void onPostExecute(@NonNull List<CalendarDay> calendarDays) {
            super.onPostExecute(calendarDays);

            if (isFinishing()) {
                return;
            }
            widget.addDecorator(new EventDecorator(getResources().getColor(R.color.currentday_meeting_dot_color), calendarDays));
            widget.addDecorator(new EventDecorator(getResources().getColor(R.color.previousday_meeting_dot_color), PreviesDates));
            widget.addDecorator(new EventDecorator(getResources().getColor(R.color.upcommingday_meeting_dot_color), Upcommingdates));
        }
    }


    private void PresentEventsData() {

        CalendarBeen been = new CalendarBeen();
        PresentDatesArray = new ArrayList<>();
        if (PresentDatesArray.size() == 0) {
            ArrayList<Integer> yearlist = new ArrayList<>();
            ArrayList<Integer> monthlist = new ArrayList<>();
            ArrayList<Integer> Daylist = new ArrayList<>();
            Daylist.add(18);
            Daylist.add(21);
            Daylist.add(24);
            Daylist.add(27);
            Daylist.add(4);
            Daylist.add(5);
            monthlist.add(3);
            monthlist.add(3);
            monthlist.add(3);
            monthlist.add(3);
            monthlist.add(3);
            monthlist.add(3);
            yearlist.add(2017);
            yearlist.add(2017);
            yearlist.add(2017);
            yearlist.add(2017);
            yearlist.add(2017);
            yearlist.add(2017);

            for (int i = 0; i < 5; i++) {

                been = new CalendarBeen();
                been.setYear(yearlist.get(i));
                been.setMonth(monthlist.get(i));
                been.setDay(Daylist.get(i));
                PresentDatesArray.add(been);

            }


        }
    }


    private void PreviousEventDates(){

        CalendarBeen been = new CalendarBeen();
        PreviousDatesArray = new ArrayList<>();
        if (PreviousDatesArray.size() == 0) {
            ArrayList<Integer> yearlist = new ArrayList<>();
            ArrayList<Integer> monthlist = new ArrayList<>();
            ArrayList<Integer> Daylist = new ArrayList<>();
            Daylist.add(19);
            Daylist.add(22);
            Daylist.add(25);
            Daylist.add(28);
            Daylist.add(30);
            Daylist.add(31);
            monthlist.add(3);
            monthlist.add(3);
            monthlist.add(3);
            monthlist.add(3);
            monthlist.add(3);
            monthlist.add(3);
            yearlist.add(2017);
            yearlist.add(2017);
            yearlist.add(2017);
            yearlist.add(2017);
            yearlist.add(2017);
            yearlist.add(2017);

            for (int i = 0; i < 5; i++) {

                been = new CalendarBeen();
                been.setYear(yearlist.get(i));
                been.setMonth(monthlist.get(i));
                been.setDay(Daylist.get(i));
                PreviousDatesArray.add(been);

            }


        }
    }

    private void UpcommingEventsDates(){
        CalendarBeen been = new CalendarBeen();
        UpComingDatesArray = new ArrayList<>();
        if (UpComingDatesArray.size() == 0) {
            ArrayList<Integer> yearlist = new ArrayList<>();
            ArrayList<Integer> monthlist = new ArrayList<>();
            ArrayList<Integer> Daylist = new ArrayList<>();
            Daylist.add(20);
            Daylist.add(23);
            Daylist.add(26);
            Daylist.add(29);
            Daylist.add(1);
            Daylist.add(3);
            monthlist.add(3);
            monthlist.add(3);
            monthlist.add(3);
            monthlist.add(3);
            monthlist.add(3);
            monthlist.add(3);
            yearlist.add(2017);
            yearlist.add(2017);
            yearlist.add(2017);
            yearlist.add(2017);
            yearlist.add(2017);
            yearlist.add(2017);

            for (int i = 0; i < 5; i++) {

                been = new CalendarBeen();
                been.setYear(yearlist.get(i));
                been.setMonth(monthlist.get(i));
                been.setDay(Daylist.get(i));
                UpComingDatesArray.add(been);

            }


        }
    }
    private void prepareData() {

        meetingList.add("Sat Mar 18 00:00:00 GMT+05:30 2017");
        meetingList.add("Mon Mar 20 00:00:00 GMT+05:30 2017");
        meetingList.add("Sat Mar 25 00:00:00 GMT+05:30 2017");
        meetingList.add("Tue Mar 28 00:00:00 GMT+05:30 2017");
        meetingList.add("Thu Mar 30 00:00:00 GMT+05:30 2017");
    }

    public void prepareMeetingsData() {

        CalendarMeetingsBeen been = new CalendarMeetingsBeen();



        been = new CalendarMeetingsBeen();
        been.setYear(2017);
        been.setMonth(3);
        been.setDay(18);
        been.setMeetingTime("10:15AM");
        been.setMeetingName("Android Design Meeting");

        meetingBeenArrayList.add(been);

        been = new CalendarMeetingsBeen();
        been.setYear(2017);
        been.setMonth(3);
        been.setDay(20);
        been.setMeetingTime("10:45AM");
        been.setMeetingName("MeetingWall Review Meeting");

        meetingBeenArrayList.add(been);
        been = new CalendarMeetingsBeen();
        been.setYear(2017);
        been.setMonth(3);
        been.setDay(22);
        been.setMeetingTime("12:15PM");
        been.setMeetingName("UI Design Meeting");


        meetingBeenArrayList.add(been);

        //  meetingBeenArrayList.add(been);
        forumAnswerListAdapter = new CalendarMeetingDetailsAdapter(this, meetingBeenArrayList, this);
        mMyRefListView.setAdapter(forumAnswerListAdapter);
        mMyRefListView.setFocusable(false);
        Utils.setListViewHeightBasedOnChildren(mMyRefListView);
        forumAnswerListAdapter.notifyDataSetChanged();


    }
    @Override
    public void onItemClick(int position) {

        onItemclick=true;
        widget.setSelectedDate(CalendarDay.from(meetingBeenArrayList.get(position).getYear(), meetingBeenArrayList.get(position).getMonth(), meetingBeenArrayList.get(position).getDay()));
        Log.e("Event Selected Date ::", "" + widget.getSelectedDate().getDate());

        forumAnswerListAdapter.notifyDataSetChanged();
        Utils.setListViewHeightBasedOnChildren(mMyRefListView);

    }

    @Override
    public void onSelectedItemClick(int position, int type) {

    }



    public void     prepareLocationsData() {
        meetingStatuslist.add("08:15 Am TO 08:30 Am");meetingStatuslist.add("08:15 Am TO 08:30 Am");meetingStatuslist.add("08:30 Am TO 08:45 Am");meetingStatuslist.add("like");meetingStatuslist.add("dislike");meetingStatuslist.add("like");

        meetingStatuslist.add("Feb,15");meetingStatuslist.add("Feb,14");meetingStatuslist.add("Feb,14");meetingStatuslist.add("Feb,13");meetingStatuslist.add("Feb,12");meetingStatuslist.add("Feb,11");

        meetingStatuslist.add("11:15 Am TO 11:45 Am");meetingStatuslist.add("11:15 Am TO 11:45 Am");

        DesignReviewlist.add("ABC STANDUP");DesignReviewlist.add("ABC ProjectReview");DesignReviewlist.add("ABC STANDUP");DesignReviewlist.add("Friday");

        DesignReviewlist.add("Design Review");DesignReviewlist.add("Design");DesignReviewlist.add("Review");DesignReviewlist.add("Android Review");DesignReviewlist.add("Meeting Review");DesignReviewlist.add("Design Review");

        DesignReviewlist.add("10:12-15:22");DesignReviewlist.add("11:12-15:30");DesignReviewlist.add("10:15-15:20");DesignReviewlist.add("ABC BRAINSTROMING");DesignReviewlist.add("11:12-15:22");DesignReviewlist.add("10:12-10:30");


        Timelist.add("02");Timelist.add("02");Timelist.add("02");Timelist.add("02");

        Timelist.add("02");Timelist.add("02");Timelist.add("02");Timelist.add("02 Review");Timelist.add("02");Timelist.add("02");

        Timelist.add("02");Timelist.add("02");Timelist.add("05");Timelist.add("05");Timelist.add("05");Timelist.add("10:12-10:30");

        Daylist.add("2");Daylist.add("2");Daylist.add("3");Daylist.add("2");

        Daylist.add("2");Daylist.add("2");Daylist.add("2");Daylist.add("2");Daylist.add("2");Daylist.add("2");

        Daylist.add("2");Daylist.add("2");Daylist.add("5");Daylist.add("5");Daylist.add("5");Daylist.add("1");

        MeetingNamesArray.add("XYZ Design Review");MeetingNamesArray.add("Design Review");MeetingNamesArray.add("Android Design");MeetingNamesArray.add("UI Meentig");

        MeetingNamesArray.add("Mobile App");MeetingNamesArray.add("UI Design Review");

        MeetingTimeArray.add("11:15 Am TO 11:45 Am");MeetingTimeArray.add("11:15 Am TO 11:45 Am");MeetingTimeArray.add("11:15 Am TO 11:45 Am");MeetingTimeArray.add("11:15 Am TO 11:45 Am");

        MeetingTimeArray.add("11:15 Am TO 11:45 Am");MeetingTimeArray.add("11:15 Am TO 11:45 Am");

        MeetingLocationArray.add("Royal Arena, Hyderabad");MeetingLocationArray.add("Mystic Revier, Hyderabad");MeetingLocationArray.add("Taj krishna, Hyderabad");MeetingLocationArray.add("Asok, Hyderabad");

        MeetingLocationArray.add("Swageth, Hyderabad");MeetingLocationArray.add("Royal Arena, Hyderabad");

        meetingLocationlistTime.add("08:30 TO 08:45");meetingLocationlistTime.add("08:30 TO 08:45");meetingLocationlistTime.add("08:30 TO 08:45");meetingLocationlistTime.add("08:30 TO 08:45");meetingLocationlistTime.add("08:30 TO 08:45");meetingLocationlistTime.add("08:30 TO 08:45");

        meetingLocationlistTime.add("08:30 TO 08:45");meetingLocationlistTime.add("08:30 TO 08:45");


        SearchlocationsArray.add("Alaska,Us");SearchlocationsArray.add("Hyderabad,India");SearchlocationsArray.add("London,Uk");SearchlocationsArray.add("Sydney,Australia");SearchlocationsArray.add("08:30 TO 08:45");SearchlocationsArray.add("08:30 TO 08:45");

        SearchlocationsArray.add("08:30 TO 08:45");SearchlocationsArray.add("08:30 TO 08:45");



        RoomLocationBeen roomLocationBeen = new RoomLocationBeen();
        roomLocationBeen.setRoomLocation("All Locations");
        roomLocationBeen.setRoomSelected(false);
        AlaskalocationsArray.add(roomLocationBeen);
        roomLocationBeen = new RoomLocationBeen();
        roomLocationBeen.setRoomLocation("Mystic Revier");
        roomLocationBeen.setRoomSelected(false);
        AlaskalocationsArray.add(roomLocationBeen);
        roomLocationBeen = new RoomLocationBeen();
        roomLocationBeen.setRoomLocation("Pearl Meadows");
        roomLocationBeen.setRoomSelected(false);
        AlaskalocationsArray.add(roomLocationBeen);
        roomLocationBeen = new RoomLocationBeen();
        roomLocationBeen.setRoomLocation("Royal Arena");
        roomLocationBeen.setRoomSelected(false);
        AlaskalocationsArray.add(roomLocationBeen);
        roomLocationBeen = new RoomLocationBeen();
        roomLocationBeen.setRoomLocation("Taj Krishna");
        roomLocationBeen.setRoomSelected(false);
        HydlocationsArray.add(roomLocationBeen);
        roomLocationBeen = new RoomLocationBeen();
        roomLocationBeen.setRoomLocation("Mystic");
        roomLocationBeen.setRoomSelected(false);
        HydlocationsArray.add(roomLocationBeen);
        roomLocationBeen = new RoomLocationBeen();
        roomLocationBeen.setRoomLocation("Revier Hotel");
        roomLocationBeen.setRoomSelected(false);
        HydlocationsArray.add(roomLocationBeen);
        roomLocationBeen = new RoomLocationBeen();
        roomLocationBeen.setRoomLocation("Royal");
        roomLocationBeen.setRoomSelected(false);
        HydlocationsArray.add(roomLocationBeen);
        roomLocationBeen = new RoomLocationBeen();
        roomLocationBeen.setRoomLocation("Krishna");
        roomLocationBeen.setRoomSelected(false);
        LondlocationsArray.add(roomLocationBeen);
        roomLocationBeen = new RoomLocationBeen();
        roomLocationBeen.setRoomLocation("Revier");
        roomLocationBeen.setRoomSelected(false);
        LondlocationsArray.add(roomLocationBeen);
        roomLocationBeen = new RoomLocationBeen();
        roomLocationBeen.setRoomLocation("Pearl ");
        roomLocationBeen.setRoomSelected(false);
        LondlocationsArray.add(roomLocationBeen);
        roomLocationBeen = new RoomLocationBeen();
        roomLocationBeen.setRoomLocation("Meadows");
        roomLocationBeen.setRoomSelected(false);
        LondlocationsArray.add(roomLocationBeen);
        roomLocationBeen = new RoomLocationBeen();
        roomLocationBeen.setRoomLocation(" Krishna");
        roomLocationBeen.setRoomSelected(false);
        SydlocationsArray.add(roomLocationBeen);
        roomLocationBeen = new RoomLocationBeen();
        roomLocationBeen.setRoomLocation("Mystic Meadows");
        roomLocationBeen.setRoomSelected(false);
        SydlocationsArray.add(roomLocationBeen);
        roomLocationBeen = new RoomLocationBeen();
        roomLocationBeen.setRoomLocation("Meadows Revier Hotel");
        roomLocationBeen.setRoomSelected(false);
        SydlocationsArray.add(roomLocationBeen);
        roomLocationBeen = new RoomLocationBeen();
        roomLocationBeen.setRoomLocation("Royal Meadows");
        roomLocationBeen.setRoomSelected(false);
        SydlocationsArray.add(roomLocationBeen);

        LocationlistRoomArray.add(AlaskalocationsArray);
        LocationlistRoomArray.add(HydlocationsArray);
        LocationlistRoomArray.add(LondlocationsArray);
        LocationlistRoomArray.add(SydlocationsArray);
    }

    public void SearchLocations(final ListView nationalityListview){

        for (int j = 0; j < 4; j++) {
            SearchLocationBeen searchLocationBeen = new SearchLocationBeen();
            searchLocationBeen.setCountryLocation(SearchlocationsArray.get(j));
            searchLocationBeen. setHotelListArray(LocationlistRoomArray.get(j));

            SearchlocationArrayList.add(searchLocationBeen);
        }

        final CalendarNationalitylocationAdapter mAdapter = new CalendarNationalitylocationAdapter(this,  SearchlocationArrayList);
        nationalityListview.setAdapter(mAdapter);
        nationalityListview.setFocusable(false);
        mAdapter.notifyDataSetChanged();

        //   Utils.setListViewHeightBasedOnChildren(NationalityListview);

        nationalityListview.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position,
                                    long arg3)
            {
                UpdateRoomList(position);
                if(position!=0){
                    nationalityListview.getChildAt(0).setBackgroundResource(R.color.black_light_color);
                }else{
                    nationalityListview.getChildAt(0).setBackgroundResource(R.drawable.listviewselecterbackgroud);
                }


            }
        });



    }

    public void UpdateRoomList(final int nationalityLostposition){

        final CalendarRoomlocationAdapter RoomAdapter = new CalendarRoomlocationAdapter(this,  SearchlocationArrayList, nationalityLostposition);
        RoomListview.setAdapter(RoomAdapter);
        RoomListview.setFocusable(false);
        //      Utils.setListViewHeightBasedOnChildren(RoomListview);
        RoomListview.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position,
                                    long arg3)
            {

                for (int i=0; i<LocationlistRoomArray.size(); i++) {
                    for(int j=0; j<LocationlistRoomArray.get(i).size(); j++){
                        LocationlistRoomArray.get(i).get(j).setRoomSelected(false);
                    }
                }
                LocationlistRoomArray.get(nationalityLostposition).get(position).setRoomSelected(true);
                SearchlocationArrayList.get(nationalityLostposition).setHotelListArray(LocationlistRoomArray.get(nationalityLostposition));
                RoomAdapter.notifyDataSetChanged();
                String locationName   =   LocationlistRoomArray.get(nationalityLostposition).get(position).getRoomLocation();
                String NationalLocation =  SearchlocationArrayList.get(nationalityLostposition).getCountryLocation();

                MeetingLocationTextview.setText(locationName+" in "+NationalLocation);
            }
        });
    }









}
