package com.nextgen.meetingwall.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.nextgen.meetingwall.R;
import com.nextgen.meetingwall.adapter.SelectedContactListAdapter;
import com.nextgen.meetingwall.listeners.OnCustomItemSelectListener;
import com.nextgen.meetingwall.network.Network;
import com.nextgen.meetingwall.network.RequestCallback;
import com.nextgen.meetingwall.network.json.CreateLocatationsWraper;
import com.nextgen.meetingwall.network.json.CreateParticipantsResponce;
import com.nextgen.meetingwall.network.json.CreateParticipantsWraper;
import com.nextgen.meetingwall.network.json.MeetingEmployeesResponse;
import com.nextgen.meetingwall.network.json.MeetingLocationsResponse;
import com.nextgen.meetingwall.network.json.PickListDataResponce;
import com.nextgen.meetingwall.network.json.PickListDataWraper;
import com.nextgen.meetingwall.network.json.SaveUpdateModuleDataResponce;
import com.nextgen.meetingwall.network.json.SaveUpdateModuleDataWraper;
import com.nextgen.meetingwall.ui.activity.MainActivity;
import com.nextgen.meetingwall.utils.SharedPreference;
import com.nextgen.meetingwall.utils.Utils;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;


import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cc.cloudist.acplibrary.ACProgressFlower;

/**
 * Created by cas on 14-03-2017.
 */

@SuppressWarnings("ALL")
public class NewMeetingFragment extends Fragment implements OnCustomItemSelectListener {


    @BindView(R.id.Location)
    TextInputLayout inputLayoutLocation;
    @BindView(R.id.Location1)
    TextInputLayout inputLayoutLocation1;
    @BindView(R.id.Location2)
    TextInputLayout inputLayoutLocation2;
    @BindView(R.id.Location3)
    TextInputLayout inputLayoutLocation3;
    @BindView(R.id.meeting_invitees_listview)
    protected ListView mMyRefListView = null;
    @BindView(R.id.Meeting_Name_editText)
    protected EditText MeetingNameEditText;
    @BindView(R.id.material_spinner1)
    protected MaterialBetterSpinner materialBetterSpinner;
    @BindView(R.id.dateselection_editText)
    protected EditText mSelectDateEdittext;
    @BindView(R.id.Location_EditText)
    protected EditText mSelectLocationEdittext;
    @BindView(R.id.Location_EditText_one)
    protected EditText mSelectLocationEdittextOne;
    @BindView(R.id.Location_EditText_two)
    protected EditText mSelectLocationEdittextTwo;
    @BindView(R.id.Location_EditText_three)
    protected EditText mSelectLocationEdittextThree;
    @BindView(R.id.time_slat_editText)
    protected EditText mSelectTimeEdittext;
    @BindView(R.id.Availabulity_Layout)
    protected LinearLayout mAvailabulityLinearLayout;
    @BindView(R.id.Invitees_hint_Layout)
    protected LinearLayout mInviteesHintLinearLayout;
    @BindView(R.id.Time_slat_hint_Layout)
    protected LinearLayout mTimeSlatHintLinearLayout;
    @BindView(R.id.Add_Location_ImageView)
    protected ImageView AddLocationImageView;


    private SharedPreference sharedPreference;

    private Date    parsedDate;
    private String  mSelecteddate;
    private String  mIntentSelecteTime;
    private String  mselectedFinalDate ;
    private String  mSelectedTimestring ;
    private String  mSelectedStartTime;
    private String  mSelectedEndTime;
    private Network network ;
    private ACProgressFlower dialog;
    private SelectedContactListAdapter selectedContactListAdapter = null;
    private static final String TAG = "NewMeeting";

    private ArrayList<MeetingEmployeesResponse.RowsBeen> mMeetingEmployeRowsBeenArrayList = new ArrayList<>();
    private ArrayList<MeetingLocationsResponse.RowsBeen> mMeetingLocationsRowsBeenArrayList = new ArrayList<>();
    private ArrayList<Serializable> serializableLocationsArrayList= new ArrayList<>();
    private ArrayList<String> getLocationArray;
    private ArrayList<Object> mSaveUpdateModuleDataArrayList = new ArrayList<>();
    private ArrayList<PickListDataResponce.PicklistDataRowsBeen> SpinnerpicklistdataArray = new ArrayList<PickListDataResponce.PicklistDataRowsBeen>();

    public String saveUpdate_pkId;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.newmeeting_setup, container, false);
        ButterKnife.bind(this, view);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.Nav_NewMeeting);
        network = new Network((AppCompatActivity) getActivity());
        ListDataMeetingTypes();


        sharedPreference = new SharedPreference(getActivity());
       return view;
    }

    @OnClick(R.id.Add_Location_ImageView)
    void AddLocation(){
        ((MainActivity)getActivity()).LocationOnClick();
    }
    @OnClick(R.id.Location_EditText)
    void LocationClick(){
        String SS = String.valueOf(mSelectLocationEdittext.getText());
        if(!String.valueOf(mSelectLocationEdittext.getText()).equals("")){
            ((MainActivity)getActivity()).EdditTextLocationOnClick(getLocationArray.get(0));
        }else{
            ((MainActivity)getActivity()).LocationOnClick();
        }
    }
    @OnClick(R.id.btn_save_continue)
    void SaveMethod(){

        if(MeetingNameEditText.getText().toString().isEmpty()){
            ((MainActivity)getActivity()).displayErrorMessage(getString(R.string.label_meeting_name));
        }else if(materialBetterSpinner.getText().toString().isEmpty()){
            ((MainActivity)getActivity()).displayErrorMessage(getString(R.string.label_meeting_type));
        }else if(mSelectDateEdittext.getText().toString().isEmpty()){
            ((MainActivity)getActivity()).displayErrorMessage(getString(R.string.label_meeting_date));
        }else if(mSelectTimeEdittext.getText().toString().isEmpty()){
            ((MainActivity)getActivity()).displayErrorMessage(getString(R.string.label_meeting_time));
        }else if(mSelectLocationEdittext.getText().toString().isEmpty()){
            ((MainActivity)getActivity()).displayErrorMessage(getString(R.string.label_meeting_location));
        }else if(mMeetingEmployeRowsBeenArrayList.size() == 0){
            ((MainActivity)getActivity()).displayErrorMessage(getString(R.string.label_meeting_invitees));
        }else{
            if(Utils.isConnectingToInternet(getActivity())){
                dialog = new ACProgressFlower.Builder(getActivity())
                        .text(getResources().getString(R.string.label_loading))
                        .build();
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();

                TimeCalate();
         ///       ((MainActivity)getActivity()).displayErrorMessage("An Error Occured ");

            }else{
                ((MainActivity)getActivity()).displayErrorMessage(getString(R.string.label_connection));
            }

        }
    }
    @OnClick(R.id.dateselection_editText)
    void DateSelect(){
        ((MainActivity)getActivity()).BasicActivity();
    }
    @OnClick(R.id.time_slat_editText)
    void timeslat(){
        ((MainActivity)getActivity()).TimeSloat(mIntentSelecteTime);
    }
    @OnClick(R.id.invitees_adding_button)
    void InvitesAddClick(){
        ((MainActivity)getActivity()).InviteesAddingOnClick();
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onItemClick(int position) {
        mMeetingEmployeRowsBeenArrayList.remove(position);
        if (mMeetingEmployeRowsBeenArrayList.size() != 0) {
            mAvailabulityLinearLayout.setVisibility(View.VISIBLE);
        } else {
            mAvailabulityLinearLayout.setVisibility(View.GONE);
        }
        selectedContactListAdapter.notifyDataSetChanged();
        Utils.setListViewHeightBasedOnChildren(mMyRefListView);
    }

    @Override
    public void onSelectedItemClick(int position, int type) {

    }

    public void dateselection(String SelectedDate) {

        try {
            mselectedFinalDate = SelectedDate;
            DateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
            parsedDate = format.parse(SelectedDate);
            SimpleDateFormat print = new SimpleDateFormat("EEEE d MMMM yyyy");
            System.out.println(print.format(parsedDate));
            mSelecteddate = String.valueOf(print.format(parsedDate));
            System.out.println("Selecte date :: " + mSelecteddate);
            mSelectDateEdittext.setText(mSelecteddate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void LocationSelected(ArrayList< MeetingLocationsResponse.RowsBeen> Location) {
        mMeetingLocationsRowsBeenArrayList.add(Location.get(0));

        if(String.valueOf(mSelectLocationEdittext.getText()).equalsIgnoreCase("")){
            mSelectLocationEdittext.setText(mMeetingLocationsRowsBeenArrayList.get(0).getName());
        }else if(String.valueOf(mSelectLocationEdittextOne.getText()).equalsIgnoreCase("")){
            mSelectLocationEdittextOne.setText(mMeetingLocationsRowsBeenArrayList.get(1).getName());
            inputLayoutLocation1.setVisibility(View.VISIBLE);
        }else if(String.valueOf(mSelectLocationEdittextTwo.getText()).equalsIgnoreCase("")){
            mSelectLocationEdittextTwo.setText(mMeetingLocationsRowsBeenArrayList.get(2).getName());
            inputLayoutLocation2.setVisibility(View.VISIBLE);
        }else if(String.valueOf(mSelectLocationEdittextThree.getText()).equalsIgnoreCase("")){
            mSelectLocationEdittextThree.setText(mMeetingLocationsRowsBeenArrayList.get(3).getName());
            inputLayoutLocation3.setVisibility(View.VISIBLE);
            AddLocationImageView.setVisibility(View.GONE);
        }

    }

    public void TimeSelected(String Time) {
        mIntentSelecteTime = Time;
        mSelectedTimestring = Time;
        mSelectTimeEdittext.setText(Time);
        mTimeSlatHintLinearLayout.setVisibility(View.GONE);
    }

    public void ContactsSelected(ArrayList<MeetingEmployeesResponse.RowsBeen> Time) {
        mMeetingEmployeRowsBeenArrayList.addAll(Time);
        if (mMeetingEmployeRowsBeenArrayList.size() != 0) {
            mAvailabulityLinearLayout.setVisibility(View.VISIBLE);
            mInviteesHintLinearLayout.setVisibility(View.GONE);
        } else {
            mAvailabulityLinearLayout.setVisibility(View.GONE);
            mInviteesHintLinearLayout.setVisibility(View.GONE);
        }
        selectedContactListAdapter = new SelectedContactListAdapter(getActivity(), mMeetingEmployeRowsBeenArrayList, this);
        mMyRefListView.setAdapter(selectedContactListAdapter);
        mMyRefListView.setFocusable(false);
        Utils.setListViewHeightBasedOnChildren(mMyRefListView);
        selectedContactListAdapter.notifyDataSetChanged();
    }

    public void TimeCalate() {
        if (mSelectedTimestring != null) {
            String[] Time = mSelectedTimestring.split("-");
            String StartTime = Time[0];
            String EndTime = Time[1];
            String[] Date = mselectedFinalDate.split(" ");
            String Date1 = Date[0];
            String Date2 = Date[1];
            String Date3 = Date[2];
            String Date4 = Date[5];
            String TimeS = StartTime.replace(" AM ", "");
                   TimeS = StartTime.replace(" am ", "");

            String SDatee = Date1 + " " + Date2 + " " + Date3 + " " + TimeS + ":00" + " " + Date4;
            String TimeE = EndTime.replace(" AM", "");
                   TimeE = EndTime.replace(" am", "");
            String TimeEE = TimeE.replace(" ", "");
            String EDatee = Date1 + " " + Date2 + " " + Date3 + " " + TimeEE + ":00" + " " + Date4;
            DateFormat format = new SimpleDateFormat("EEE MMM dd hh:mm:ss yyyy");
            Date StartparsedDate = null;
            Date EndparsedDate = null;
            try {
                StartparsedDate = format.parse(SDatee);
                EndparsedDate = format.parse(EDatee);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(StartparsedDate);
                Date Starttime = calendar.getTime();
                DateFormat gmtFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
                TimeZone gmtTime = TimeZone.getTimeZone("GMT");
                gmtFormat.setTimeZone(gmtTime);
                mSelectedStartTime = String.valueOf((gmtFormat.format(Starttime)));
                calendar.setTime(EndparsedDate);
                Date Endtime = calendar.getTime();
                mSelectedEndTime = String.valueOf((gmtFormat.format(Endtime)));
                mSelectedStartTime = mSelectedStartTime + ".000Z";
                mSelectedEndTime = mSelectedEndTime + ".000Z";

            } catch (ParseException e) {
                e.printStackTrace();
            }



            CreateMeeting();
        }

    }

    private void ListDataMeetingTypes() {
        PickListDataWraper wrapper = new PickListDataWraper();
        wrapper.setFirstInt(0);
        wrapper.setRowsInt(10);
        wrapper.setSortOrderInt(1);
        wrapper.setPickListId(7);

        network.PickListData(wrapper, new RequestCallback<PickListDataResponce>() {
            @Override
            public void onSuccess(PickListDataResponce response) {

                Log.e(TAG, "Success response::" + response);
                onSuccesLocationResponce(response);
            }

            @Override
            public void onFailed(String message) {

                Log.e(TAG, "Failed response::" + message);
            }
        });

    }

    private void onSuccesLocationResponce(final PickListDataResponce response) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (response.getJqGridData().getRowsArray().size() > 0) {
                    for (int i = 0; i < response.getJqGridData().getRowsArray().size(); i++) {
                        SpinnerpicklistdataArray.add(response.getJqGridData().getRowsArray().get(i));
                    }
                    ArrayAdapter<PickListDataResponce.PicklistDataRowsBeen> adapter = new ArrayAdapter<PickListDataResponce.PicklistDataRowsBeen>(getActivity(), R.layout.custom_spinner_dropdown_item, SpinnerpicklistdataArray);
                    materialBetterSpinner.setAdapter(adapter);
                }
            }
        });

    }

    public void CreateMeeting() {
        int Ids = 0;
        if (materialBetterSpinner.getText().toString() != null) {
            for (int i = 0; i < SpinnerpicklistdataArray.size(); i++) {
                if (SpinnerpicklistdataArray.get(i).getPicklistDataType().equalsIgnoreCase(materialBetterSpinner.getText().toString())) {
                    Ids = SpinnerpicklistdataArray.get(i).getPicklistDataId1();
                }
            }
        }
        SaveUpdateModuleDataWraper.FieldsdataArray fieldsdataArray = new SaveUpdateModuleDataWraper.FieldsdataArray();
        fieldsdataArray.setName("Name");
        fieldsdataArray.setDb_field_name("field_name");
        fieldsdataArray.setType("Text");
        fieldsdataArray.setBlock_pk_id(43);
        fieldsdataArray.setValue(MeetingNameEditText.getText().toString());
        mSaveUpdateModuleDataArrayList.add(fieldsdataArray);

        ArrayList<SaveUpdateModuleDataWraper.Type> Array = new ArrayList<>();
        SaveUpdateModuleDataWraper.Type type = new SaveUpdateModuleDataWraper.Type();
        type.setName("Type");
        type.setDb_field_name("field_module_meetings_r0id");
        type.setType("Pick List");
        type.setBlock_pk_id(43);
        SaveUpdateModuleDataWraper.Type.TypeValueOfValue fieldsdataArray4 = new SaveUpdateModuleDataWraper.Type.TypeValueOfValue();
        fieldsdataArray4.setTypeValue(materialBetterSpinner.getText().toString());
        fieldsdataArray4.setTypeValuePkId(Ids);
        type.setTypeValueOfValue(fieldsdataArray4);
        Array.add(type);
        mSaveUpdateModuleDataArrayList.add(Array.get(0));

        SaveUpdateModuleDataWraper.FieldsdataArray fieldsdataArray1 = new SaveUpdateModuleDataWraper.FieldsdataArray();
        fieldsdataArray1.setName("Start Time");
        fieldsdataArray1.setDb_field_name("field_start_time");
        fieldsdataArray1.setType("Time Stamp");
        fieldsdataArray1.setBlock_pk_id(43);
        fieldsdataArray1.setValue(mSelectedStartTime);
        mSaveUpdateModuleDataArrayList.add(fieldsdataArray1);

        SaveUpdateModuleDataWraper.FieldsdataArray fieldsdataArray2 = new SaveUpdateModuleDataWraper.FieldsdataArray();
        fieldsdataArray2.setName("End Time");
        fieldsdataArray2.setDb_field_name("field_end_time");
        fieldsdataArray2.setType("Time Stamp");
        fieldsdataArray2.setBlock_pk_id(43);
        fieldsdataArray2.setValue(mSelectedEndTime);
        mSaveUpdateModuleDataArrayList.add(fieldsdataArray2);


        ArrayList<SaveUpdateModuleDataWraper.Location> LocationArray = new ArrayList<>();
        SaveUpdateModuleDataWraper.Location Locationtype = new SaveUpdateModuleDataWraper.Location();
        Locationtype.setName("Location");
        Locationtype.setDb_field_name("field_location");
        Locationtype.setType("Relation");
        Locationtype.setBlock_pk_id(43);
        Locationtype.setLocationValueOfValue(mMeetingLocationsRowsBeenArrayList);
        LocationArray.add(Locationtype);
        mSaveUpdateModuleDataArrayList.add(LocationArray.get(0));

        SaveUpdateModuleDataWraper saveUpdateModuleDataWraper = new SaveUpdateModuleDataWraper();
        saveUpdateModuleDataWraper.setModule("Meetings");
        saveUpdateModuleDataWraper.setAction("Create");
        saveUpdateModuleDataWraper.setDisplayColsArray(mSaveUpdateModuleDataArrayList);

        network.SaveUpdateModuleData(saveUpdateModuleDataWraper, new RequestCallback<SaveUpdateModuleDataResponce>() {
            @Override
            public void onSuccess(SaveUpdateModuleDataResponce response) {
                Log.e(TAG, "Success response::" + response);
                onSuccesSaveUpdateResponce(response);
            }

            @Override
            public void onFailed(String message) {
                Log.e(TAG, "Failed response::" + message);
            }
        });
    }

    private void onSuccesSaveUpdateResponce(final SaveUpdateModuleDataResponce response) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (response.isSuccess()) {
                    saveUpdate_pkId = response.getDataObject().getSaveUpdate_pkId();
                    CreateParticipants(saveUpdate_pkId);
                    sharedPreference.addMeetingSaveData(response.getDataObject().getSaveUpdate_pkId(),MeetingNameEditText.getText().toString(),mSelectedStartTime,mSelectedEndTime);

                } else {
                    dialog.cancel();
                    Toast.makeText(getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public void CreateParticipants(String saveUpdate_pkId) {

        CreateParticipantsWraper createParticipantsWraper = new CreateParticipantsWraper();
        createParticipantsWraper.setFromModuleString("Meetings");
        createParticipantsWraper.setFromDbTableString("module_meetings");
        createParticipantsWraper.setFromRelationInt(0);
        createParticipantsWraper.setFromFieldIdInt(506);
        createParticipantsWraper.setFromBlockIdInt(43);
        createParticipantsWraper.setFromBlockString("Basic Information");
        createParticipantsWraper.setFromFieldString("Participants");
        createParticipantsWraper.setFromDbFieldString("field_participants_id");
        createParticipantsWraper.setToModuleIconString("fa-users");
        createParticipantsWraper.setToModuleString("Employees");
        createParticipantsWraper.setToTableString("app_employee");
        createParticipantsWraper.setToRelationInt(0);
        createParticipantsWraper.setToFieldIdInt(507);
        createParticipantsWraper.setToBlockIdInt(8);
        createParticipantsWraper.setToBlockString("Basic Information");
        createParticipantsWraper.setToFieldString("Meetings");
        createParticipantsWraper.setToDbFieldString("field_meetings_id");
        createParticipantsWraper.setXrefTableNameString("module_meetings_xref_employee");
        createParticipantsWraper.setToIsUserInt(1);
        createParticipantsWraper.setPkIdString(saveUpdate_pkId);
        createParticipantsWraper.setDataArray(mMeetingEmployeRowsBeenArrayList);
        mSaveUpdateModuleDataArrayList.add(createParticipantsWraper);


        network.createParticipants(createParticipantsWraper, new RequestCallback<CreateParticipantsResponce>() {
            @Override
            public void onSuccess(CreateParticipantsResponce response) {
                    Log.e(TAG, "Success response::" + response);
                    onSuccescreateParticipantsResponce(response);
            }

            @Override
            public void onFailed(String message) {
                Log.e(TAG, "Failed response::" + message);
            }
        });
    }

    public void onSuccescreateParticipantsResponce(CreateParticipantsResponce response){
        if (response.getSuccess()) {
            CreateLocations(saveUpdate_pkId);
      //      dialog.cancel();
            //          ((MainActivity)getActivity()).displaySuccesMessage(getString(R.string.label_meeting_success));
         //   ((MainActivity) getActivity()).SaveandContinuebtnOnClick();
        } else {
            dialog.cancel();
            Toast.makeText(getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }
    public void CreateLocations(String saveUpdate_pkId) {

        CreateLocatationsWraper createLocatationsWraper = new CreateLocatationsWraper();
        createLocatationsWraper.setFromModuleString("Meetings");
        createLocatationsWraper.setFromDbTableString("module_meetings");
        createLocatationsWraper.setFromRelationInt(1);
        createLocatationsWraper.setFromFieldIdInt(556);
        createLocatationsWraper.setFromBlockIdInt(43);
        createLocatationsWraper.setFromBlockString("Basic Information");
        createLocatationsWraper.setFromFieldString("Place");
        createLocatationsWraper.setFromDbFieldString("pk_id");
        createLocatationsWraper.setToModuleString("Meeting Places");
        createLocatationsWraper.setToTableString("module_meeting_places");
        createLocatationsWraper.setToRelationInt(0);
        createLocatationsWraper.setToFieldIdInt(557);
        createLocatationsWraper.setToBlockIdInt(46);
        createLocatationsWraper.setToBlockString("Basic Information");
        createLocatationsWraper.setToFieldString("Meeting");
        createLocatationsWraper.setToDbFieldString("field_meeting");
        createLocatationsWraper.setXrefTableNameString("module_meeting_places");
        createLocatationsWraper.setToRecordIdentifier("field_name");
        createLocatationsWraper.setToIsUserInt(0);
        createLocatationsWraper.setPkIdString(saveUpdate_pkId);
        createLocatationsWraper.setDataArray( mMeetingLocationsRowsBeenArrayList);
        mSaveUpdateModuleDataArrayList.add(createLocatationsWraper);


        network.createLocations(createLocatationsWraper, new RequestCallback<CreateParticipantsResponce>() {
            @Override
            public void onSuccess(CreateParticipantsResponce response) {
                Log.e(TAG, "Success response::" + response);
                    onSuccescreateLocationsResponce(response);
            }

            @Override
            public void onFailed(String message) {
                Log.e(TAG, "Failed response::" + message);
            }
        });
    }


    public void onSuccescreateLocationsResponce(CreateParticipantsResponce response){
        if (response.getSuccess()) {
            dialog.cancel();
  //          ((MainActivity)getActivity()).displaySuccesMessage(getString(R.string.label_meeting_success));
            ((MainActivity) getActivity()).SaveandContinuebtnOnClick(saveUpdate_pkId);
        } else {
            dialog.cancel();
            Toast.makeText(getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }




}
