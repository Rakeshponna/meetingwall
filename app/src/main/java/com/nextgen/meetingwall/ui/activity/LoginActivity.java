package com.nextgen.meetingwall.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.nextgen.meetingwall.R;
import com.nextgen.meetingwall.data.Storage;
import com.nextgen.meetingwall.model.UserLoginBeen;
import com.nextgen.meetingwall.network.Network;
import com.nextgen.meetingwall.network.RequestCallback;
import com.nextgen.meetingwall.network.json.LoginResponse;
import com.nextgen.meetingwall.utils.Constants;
import com.nextgen.meetingwall.utils.FormValidator;
import com.nextgen.meetingwall.utils.Helper;
import com.nextgen.meetingwall.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;
import cc.cloudist.acplibrary.ACProgressFlower;

/**
 * Created by ASHUTOSH.N on 24-01-17.
 */
@SuppressWarnings("ALL")
public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.input_layout_email)
    TextInputLayout inputLayoutEmail;
    @BindView(R.id.input_layout_password)
    TextInputLayout inputLayoutPassword;
    @BindView(R.id.input_email)
    EditText emailText;
    @BindView(R.id.input_password)
    EditText passwordText;
    @BindView(R.id.btn_email_clear)
    Button emailClearButton;
    @BindView(R.id.btn_password_clear)
    Button PasswordClearButton;




    @OnTouch(R.id.Loginpage_linearlayout)
    protected boolean touch(){
        Helper.hideKeyboard(LoginActivity.this);
        return false;
    }

    @OnClick(R.id.link_signup)
    protected void OnSignupclick(){
        // Start the Signup activity
        Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
        // startActivityForResult(intent, REQUEST_SIGNUP);
        startActivity(intent);
        overridePendingTransition(R.anim.enter, R.anim.stable);
    }
    @OnClick(R.id.link_forgotPassword)
    protected void Onforgotclick(){
        // Start the Signup activity
        Intent intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
        // startActivityForResult(intent, REQUEST_SIGNUP);
        startActivity(intent);
        overridePendingTransition(R.anim.enter,R.anim.stable);
    }

    private static final String TAG = "LoginActivity";
    private static final int REQUEST_SIGNUP = 0;
    private final Network network = new Network(this);
    private String ipAdress;
     ACProgressFlower dialog;
    private Storage storage;
    @OnClick(R.id.btn_signin)
    public void onClickLoginButton() {
        Helper.hideKeyboard(this);
       // Helper.hideKeyboard(LoginActivity.this);
     //   login();
        if (validate()) {
            if (Utils.isConnectingToInternet(this)) {
                 dialog = new ACProgressFlower.Builder(this)
                        .text(getResources().getString(R.string.label_loading))
                        .build();
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();

                UserLoginBeen userLoginBeen = new UserLoginBeen();
                userLoginBeen.setEmail(emailText.getText().toString().trim());
                userLoginBeen.setPassword(passwordText.getText().toString().trim());


                network.loginDetails(userLoginBeen
                        , new RequestCallback<LoginResponse>() {
                            @Override
                            public void onSuccess(LoginResponse response) {
                                onSuccesLogin(response);
                                Log.d("Success Responce ::",""+response);
                            }

                            @Override
                            public void onFailed(String message) {
                                hideProgressDialog();
                                onFailure(message);
                            }
                        });
            } else {
                dialog = new ACProgressFlower.Builder(this)
                        .text(getResources().getString(R.string.label_connection))
                        .build();
                dialog.setCanceledOnTouchOutside(true);
                dialog.show();

            }
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setContentView(R.layout.activitylogin);
        ButterKnife.bind(this);
        getSupportActionBar().hide();




        //set on text change listener for edittext
        emailText.addTextChangedListener(textWatcher());
        passwordText.addTextChangedListener(textWatcher());
        //set event for clear button
        emailClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emailText.setText(""); //clear edittext
                Helper.hideKeyboard(LoginActivity.this);
            }
        });
        PasswordClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                passwordText.setText(""); //clear edittext
                Helper.hideKeyboard(LoginActivity.this);
            }
        });

    }

    private boolean validate() {
        boolean email = FormValidator.validateEmail(emailText, R.string.label_err_msg_email, inputLayoutEmail);
        boolean password = FormValidator.notEmptyEditText(passwordText, R.string.label_err_msg_password, inputLayoutPassword);
        return email && password;
    }

    public void login() {
        Log.d(TAG, "Login");

        if (!validateLogin()) {
            //   onLoginFailed();
            return;
        }

  //      signInButton.setEnabled(false);
        final ACProgressFlower dialog = new ACProgressFlower.Builder(this)
                .text("Authenticating...")
                .build();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();


        String email = emailText.getText().toString();
        String password = passwordText.getText().toString();

        // TODO: Implement your own authentication logic here.

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        // On complete call either onLoginSuccess or onLoginFailed
                        onLoginSuccess();
                        // onLoginFailed();

                        dialog.dismiss();
                    }
                }, 3000);
    }

    public void onLoginFailed() {
        Utils.showToast(getString(R.string.label_failure), LoginActivity.this);
   //     signInButton.setEnabled(true);
    }

    public void onLoginSuccess() {
   //     signInButton.setEnabled(true);
        Utils.showToast(getString(R.string.label_success), LoginActivity.this);
        Intent intent = new Intent(LoginActivity.this, DashBoardActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.zoom_in,0);
        finish();
    }

    private boolean validateLogin() {
        boolean email = FormValidator.validateEmail(emailText, R.string.label_err_msg_email, inputLayoutEmail);
        boolean password = FormValidator.passwordValidate(passwordText, R.string.label_err_msg_password, inputLayoutPassword);
        return email && password;
    }

    private void onSuccesLogin(final LoginResponse response) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialog.dismiss();
                try { //"User Not registered"
                    if (response.isSuccess()) {
                        intentCall();
                        Toast.makeText(getApplicationContext(), response.getMessage(), Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(),response.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void intentCall() {
        Intent i = new Intent(LoginActivity.this, ImageUploadActivity.class);
     //    i.putExtra(Constants.ISSIGNUP_PAGE, true);
        startActivity(i);
        finish();
    }
    private void hideProgressDialog() {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialog.cancel();
            }
        });
    }
    private void onFailure(final String rep) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), rep, Toast.LENGTH_LONG).show();
            }
        });
    }
    private TextWatcher textWatcher() {
        return new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!emailText.getText().toString().equals("")) { //if edittext include text
                    emailClearButton.setVisibility(View.VISIBLE);
                } else { //not include text
                    emailClearButton.setVisibility(View.GONE);
                } if (!passwordText.getText().toString().equals("")) {
                    PasswordClearButton.setVisibility(View.VISIBLE);
                } else { //not include text
                    PasswordClearButton.setVisibility(View.GONE);
                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
    }

}
