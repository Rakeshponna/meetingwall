package com.nextgen.meetingwall.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import android.widget.LinearLayout;
import android.widget.ListView;

import com.nextgen.meetingwall.R;
import com.nextgen.meetingwall.adapter.DashBoardGridViewAdapter;
import com.nextgen.meetingwall.adapter.DashBoardListAdapter;
import com.nextgen.meetingwall.model.DesignReviewMeetingBeen;
import com.nextgen.meetingwall.utils.Constants;
import com.nextgen.meetingwall.utils.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressWarnings("ALL")
public class DashBoardActivity extends AppCompatActivity {

    @BindView(R.id.grid_view_image_text)
    protected GridView mGridView;
    private static final String TAG = "DashBoardActivity";
    private DashBoardGridViewAdapter mDashBoardGridViewAdapter;
    private String[] gridViewString = {" Create \n Meeting", " Create \n Project", " Create \n Contact"};
    private int[] cout;
    private LayoutInflater inflater;

    private int[] gridViewImageId = {R.drawable.create_meeting, R.drawable.create_project, R.drawable.create_contact};
 private int[] ListViewImageId = {R.drawable.timer, R.drawable.like, R.drawable.dislike};


    @BindView(R.id.meetings_recycler_view)
    protected  ListView MeetingsRecyclerView=null;
    private DashBoardListAdapter mAdapter;
    private ArrayList<DesignReviewMeetingBeen> meetingBeenArrayList = new ArrayList<>();
    ArrayList<String> Datelist = new ArrayList<>();ArrayList<String> meetingStatuslist = new ArrayList<>();
    ArrayList<String> Daylist = new ArrayList<>();
    ArrayList<String> DesignReviewlist = new ArrayList<>();
    ArrayList<String> Timelist = new ArrayList<>();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_dashboard);
        ButterKnife.bind(this);
        getSupportActionBar().hide();

        mDashBoardGridViewAdapter = new DashBoardGridViewAdapter(DashBoardActivity.this, gridViewString, gridViewImageId);
        mGridView = (GridView) findViewById(R.id.grid_view_image_text);
        mGridView.setAdapter(mDashBoardGridViewAdapter);
        mGridView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                int pos =position;
                if(position==0){
                    Intent intent = new Intent(DashBoardActivity.this, MainActivity.class);
                    intent.putExtra(Constants.FRAGMENT_ID, R.string.Nav_NewMeeting);
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.stable);
                }else if(position==1){
                    Intent intent = new Intent(DashBoardActivity.this, MainActivity.class);
                    intent.putExtra(Constants.FRAGMENT_ID, R.string.Nav_NewContact);
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.stable);
                }else if(position==2){
                    Intent intent = new Intent(DashBoardActivity.this, MainActivity.class);
                    intent.putExtra(Constants.FRAGMENT_ID, R.string.Nav_NewContact);
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.stable);
                }

            }
        });
        prepareMeetingsData();
        for (int i = 0; i < 5; i++) {
            DesignReviewMeetingBeen Been = new DesignReviewMeetingBeen();

            Been.setProfileImage(meetingStatuslist.get(i));
            Been.setDate(Datelist.get(i));
            Been.setDay(Daylist.get(i));
            Been.setDesignReview(DesignReviewlist.get(i));
            Been.setTime(Timelist.get(i));
            meetingBeenArrayList.add(Been);
        }
        mAdapter = new DashBoardListAdapter(this,  meetingBeenArrayList);
        MeetingsRecyclerView.setAdapter(mAdapter);
        MeetingsRecyclerView.setFocusable(false);
        mAdapter.notifyDataSetChanged();
        Utils.setListViewHeightBasedOnChildren(MeetingsRecyclerView);

        MeetingsRecyclerView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                int pos =position;

                    Intent intent = new Intent(DashBoardActivity.this, MainActivity.class);
                    intent.putExtra(Constants.FRAGMENT_ID, R.string.Meeting_preview_fragment);
                    startActivity(intent);

                overridePendingTransition(R.anim.slideright, R.anim.slideleft);
            }
        });



    }
    private void prepareMeetingsData() {
        meetingStatuslist.add("time");meetingStatuslist.add("like");meetingStatuslist.add("dislike");meetingStatuslist.add("like");meetingStatuslist.add("dislike");meetingStatuslist.add("like");

        Datelist.add("Feb,15");Datelist.add("Feb,14");Datelist.add("Feb,14");Datelist.add("Feb,13");Datelist.add("Feb,12");Datelist.add("Feb,11");

        Daylist.add("Wednesday");Daylist.add("Tuesday");Daylist.add("Monday");Daylist.add("Sunday");Daylist.add("Saturday");Daylist.add("Friday");

        DesignReviewlist.add("Design Review");DesignReviewlist.add("Design");DesignReviewlist.add("Review");DesignReviewlist.add("Android Review");DesignReviewlist.add("Meeting Review");DesignReviewlist.add("Design Review");

        Timelist.add("10:12-15:22");Timelist.add("11:12-15:30");Timelist.add("10:15-15:20");Timelist.add("12:12-13:22");Timelist.add("11:12-15:22");Timelist.add("10:12-10:30");
    }

    @Override
    public void onBackPressed() {

             super.onBackPressed();

    }
}
