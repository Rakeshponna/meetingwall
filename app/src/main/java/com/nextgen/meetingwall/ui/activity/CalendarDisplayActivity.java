package com.nextgen.meetingwall.ui.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.calendarlistview.library.DatePickerController;
import com.calendarlistview.library.DayPickerView;
import com.calendarlistview.library.SimpleMonthAdapter;
import com.nextgen.meetingwall.R;
import com.nextgen.meetingwall.utils.Constants;
import com.nextgen.meetingwall.utils.Utils;

import java.util.Calendar;

public class CalendarDisplayActivity extends AppCompatActivity implements DatePickerController {
    private DayPickerView dayPickerView;


    private TextView sundayText, mondayText, tuesdayText, wednesdayText, thursdayText, fridayText, saturdayText;




    private static final String TAG = "CalendarDisplayActivity";
    private String singleTripSelectedDate = "";
    private String roundTripSelectedDepartureDate = "";
    private String roundTripSelectedReturnDate = "";
    private int singleDepartureDay = -1;
    private int singleDepartureMonth = -1;
    private int singleDepartureYear = -1;
    private int departureDay = -1;
    private int departureMonth = -1;
    private int departureYear = -1;
    private boolean isDepartureSelected = false;
    private int tripType;
    private boolean isHotelDateSelected = false;
    private String disabledDates = "";
    private boolean isArabicSelected = false;
    private Typeface titleFace, textBold, textFace;

    private String previousDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar_display_view);




        sundayText = (TextView) findViewById(R.id.sunday_text);
        mondayText = (TextView) findViewById(R.id.monday_text);
        tuesdayText = (TextView) findViewById(R.id.tuesday_text);
        wednesdayText = (TextView) findViewById(R.id.wednesday_text);
        thursdayText = (TextView) findViewById(R.id.thursday_text);
        fridayText = (TextView) findViewById(R.id.friday_text);
        saturdayText = (TextView) findViewById(R.id.saturday_text);

        Bundle bundle = getIntent().getExtras();
        singleTripSelectedDate = bundle.getString("Date", "");

        dayPickerView = (DayPickerView) findViewById(R.id.pickerView);

        dayPickerView.setController(this, singleTripSelectedDate, tripType, "", "", false, disabledDates, isArabicSelected, isHotelDateSelected);
        Log.v("singleTripSelectedDate", singleTripSelectedDate);
        ActionBar mActionBar = getSupportActionBar();
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);

        mCustomView.findViewById(R.id.calendar_actionbar).setVisibility(View.VISIBLE);
        LinearLayout backLayout = (LinearLayout) mCustomView.findViewById(R.id.back_to_layout);
        TextView backText = (TextView) mCustomView.findViewById(R.id.back_text_header);
        ImageView mImageView = (ImageView) mCustomView.findViewById(R.id.home_to_back);
        Button doneButton = (Button) mCustomView.findViewById(R.id.select_button);
        backText.setText("");


        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (singleDepartureDay == -1) {
                    displayErrorMessage(getString(R.string.label_err_dept_date_msg));
                    return;
                }
                Intent intent = new Intent(CalendarDisplayActivity.this, MainActivity.class);
                intent.putExtra(Constants.SELECTED_DAY, singleDepartureDay);
                intent.putExtra(Constants.SELECTED_MONTH, singleDepartureMonth);
                intent.putExtra(Constants.SELECTED_YEAR, singleDepartureYear);
                setResult(RESULT_OK, intent);
                finish();
                overridePendingTransition(0, R.anim.push_down_in);



            }
        });
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(0, R.anim.push_down_in);

            }
        });
        ActionBar.LayoutParams layout = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        mActionBar.setCustomView(mCustomView, layout);
        mActionBar.setDisplayShowCustomEnabled(true);

        errorView = (RelativeLayout) findViewById(R.id.errorView);
        errorView.setVisibility(View.GONE);
        errorMessageText = (TextView) findViewById(R.id.errorMessageText);
        errorMessageText.setTypeface(titleFace);
    }

    @Override
    public int getMaxYear() {
        Calendar calendar = Calendar.getInstance();
        return (calendar.get(Calendar.YEAR) + 1);
    }

    @Override
    public void onDayOfMonthSelected(int year, int month, int day, boolean isDepartureSelected) {
        //Log.e(TAG, "onDayOfMonthSelected::"+isDepartureSelected);

        String convertedFormat = Utils.convertTocalendardate(year + "-" + (month + 1) + "-" + day, CalendarDisplayActivity.this);

        singleDepartureDay = day;
        singleDepartureMonth = month + 1;
        singleDepartureYear = year;

    }

    @Override
    public void onDateRangeSelected(SimpleMonthAdapter.SelectedDays<SimpleMonthAdapter.CalendarDay> selectedDays) {
        Utils.printMessage(TAG, "Date range selected " + selectedDays.getFirst().toString() + " --> " + selectedDays.getLast().toString());
    }

    private RelativeLayout errorView = null;
    private TextView errorMessageText = null;

    @SuppressWarnings("ResourceType")
    private void displayErrorMessage(String message) {
        errorMessageText.setText(message);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) errorMessageText.getLayoutParams();
        if (errorMessageText.getLineCount() == 3) {
            params.height = getResources().getDimensionPixelSize(R.dimen.search_screen_layout_height);
            errorMessageText.setLayoutParams(params);
        } else {
            params.height = getResources().getDimensionPixelSize(R.dimen.signIn_page_reset_mail_view);
            errorMessageText.setLayoutParams(params);
        }
        if (errorView.getAlpha() == 0) {
            errorView.setTranslationY(-60);
            errorView.setVisibility(View.VISIBLE);
            errorView.setBackgroundResource(R.color.error_message_background);
            errorView.animate().translationY(0)
                    .alpha(1)
                    .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    errorView.animate()
                            .alpha(0)
                            .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
                }
            }, 2000);
        }
    }
}
