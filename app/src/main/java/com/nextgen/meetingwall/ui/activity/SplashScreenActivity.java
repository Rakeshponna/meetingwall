package com.nextgen.meetingwall.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.AnticipateOvershootInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;

import com.nextgen.meetingwall.R;




public class SplashScreenActivity extends AppCompatActivity {

    LinearLayout  icon2, icon3 ;


    boolean runningIAm = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        loadLogoAnimation();
        Handler handler=new Handler();
        Runnable r=new Runnable()
        {
            public void run() {
                Intent i=new Intent(SplashScreenActivity.this,LoginActivity.class);
                startActivity(i);
                finish();
                overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
            }
        };
        handler.postDelayed(r, 3000);

    }
    private void loadLogoAnimation() {
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.logo_animation_in);
        findViewById(R.id.splash).startAnimation(anim);
        icon2 = (LinearLayout) findViewById(R.id.icon2);
        icon3 = (LinearLayout) findViewById(R.id.icon3);

        final ViewTreeObserver observer = icon2.getViewTreeObserver();

        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {
                try {
                    observer.removeGlobalOnLayoutListener(this);
                } catch (Exception e) {
                        e.printStackTrace();
                }
                if (runningIAm) {
                    runningIAm = false;
                    DisplayMetrics displaymetrics = new DisplayMetrics();
                    getWindowManager().getDefaultDisplay().getMetrics(
                            displaymetrics);
                    int height = displaymetrics.heightPixels;
                    int width = displaymetrics.widthPixels;
                    icon2.startAnimation(fromAtoBNew(
                            ((width/150.0f ) - (icon2.getX()+150)),
                            ((height/2.0f ) - (icon2.getY()-100)), 0.0f, 0.0f, 1600));
                    icon3.startAnimation(fromAtoBNew(
                            ((width ) - icon3.getX()),
                            ((height /2.0f)- (icon2.getY()-100)), 0.0f, 0.0f, 1600));

                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub

                        }
                    }, 1600);
                }

            }

        });

    }

    public Animation fromAtoBNew(float fromX, float fromY, float toX,
                                 float toY, int speed) {
        TranslateAnimation fromAtoB = new TranslateAnimation(fromX, toX, fromY,
                toY);
        fromAtoB.setFillAfter(true);
        fromAtoB.setDuration(speed);
        fromAtoB.setInterpolator(new AnticipateOvershootInterpolator(1.0f));

        return fromAtoB;
    }
}