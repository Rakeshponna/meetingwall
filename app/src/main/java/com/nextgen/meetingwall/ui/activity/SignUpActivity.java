package com.nextgen.meetingwall.ui.activity;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;


import com.nextgen.meetingwall.R;
import com.nextgen.meetingwall.ui.dialog.DialogFactory;
import com.nextgen.meetingwall.utils.FormValidator;
import com.nextgen.meetingwall.utils.Helper;
import com.nextgen.meetingwall.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;

@SuppressWarnings("ALL")
public class SignUpActivity extends AppCompatActivity {


    @BindView(R.id.input_layout_firstname)
    protected TextInputLayout mFirstNameLayout;
    @BindView(R.id.input_layout_lastname)
    protected TextInputLayout mLastNameLayout;
    @BindView(R.id.input_layout_phonenumber)
    protected TextInputLayout mPhoneNumberLayout;
    @BindView(R.id.input_layout_email)
    protected TextInputLayout mUserEmailLayout;
    @BindView(R.id.input_layout_passwd)
    protected TextInputLayout mPasswordLayout;
    @BindView(R.id.input_layout_confirm_passwd)
    protected TextInputLayout mConfirmpasswordLayout;

    @BindView(R.id.input_firstname)
    protected EditText mFirstNameEditText;
    @BindView(R.id.input_lastname)
    protected EditText mLastNameEditText;
    @BindView(R.id.input_phonenumber)
    protected EditText mPhoneNumberEditText;
    @BindView(R.id.input_email)
    protected EditText mEmailEditText;
    @BindView(R.id.input_passwd)
    protected EditText mPasswordEditText;
    @BindView(R.id.input_confirm_password)
    protected EditText mConfirmPasswordEditText;

    @OnTouch(R.id.scroll_layout)
    protected boolean touch(){
        Helper.hideKeyboard(SignUpActivity.this);
        return false;
    }


    private static final String TAG = "SignUpActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getSupportActionBar().hide();
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);




    }

    @OnClick(R.id.btn_register)
    protected void onRegisterPressed() {
        if (validateForm()) {

            //Log.e(TAG, "onRegisterPressed");
            if (Utils.isConnectingToInternet(this)) {

                final ProgressDialog progressDialog = new ProgressDialog(SignUpActivity.this);
                progressDialog.setIndeterminate(true);
                progressDialog.setMessage("Authenticating...");
                progressDialog.show();
                new android.os.Handler().postDelayed(
                        new Runnable() {
                            public void run() {
                                // On complete call either onLoginSuccess or onLoginFailed
                                onLoginSuccess();
                                // onLoginFailed();
                                progressDialog.dismiss();
                            }
                        }, 3000);
            } else {
                dialog(getResources().getString(R.string.label_connection));
            }
        }

    }



    private boolean validateForm() {
        if (!FormValidator.notEmptyEditText(mFirstNameEditText, R.string.label_err_msg_firstname, mFirstNameLayout))
            return false;
        if (!FormValidator.notEmptyEditText(mLastNameEditText, R.string.label_err_msg_lastname, mLastNameLayout))
            return false;
        if (!FormValidator.mobileNumberLimit(mPhoneNumberEditText, R.string.label_panlimit, mPhoneNumberLayout)) {
            return false;
        }
        if (!FormValidator.validateEmail(mEmailEditText, R.string.label_err_msg_email, mUserEmailLayout))
            return false;
        if (!FormValidator.validatePasswordLength(mPasswordEditText, R.string.label_err_msg_password_length, mPasswordLayout))
            return false;
        if (!FormValidator.inputValuesMatch(mPasswordEditText, mConfirmPasswordEditText, R.string.label_err_msg_confirm_password, mConfirmpasswordLayout))
            return false;
        return true;
    }

    public void onLoginSuccess() {
        Utils.showToast(getString(R.string.label_success), SignUpActivity.this);
        Intent intent = new Intent(SignUpActivity.this, DashBoardActivity.class);
        startActivity(intent);
        finish();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }






    private void dialog(final String message) {
        DialogFactory.showAConfirmationDialog(this, message, 0,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
    }

    @Override
    public void onBackPressed() {
              super.onBackPressed();
        overridePendingTransition(R.anim.stable, R.anim.slide_out_right);
    }


}
