package com.nextgen.meetingwall.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nextgen.meetingwall.R;
import com.nextgen.meetingwall.network.Network;
import com.nextgen.meetingwall.network.RequestCallback;
import com.nextgen.meetingwall.network.json.AddUpdateTagsResponce;
import com.nextgen.meetingwall.network.json.AddUpdateTagsWrapper;
import com.nextgen.meetingwall.network.json.EssentialsSaveUpdateModuleDataWrapper;
import com.nextgen.meetingwall.network.json.PickListDataResponce;
import com.nextgen.meetingwall.network.json.PickListDataWraper;
import com.nextgen.meetingwall.network.json.SaveUpdateModuleDataResponce;
import com.nextgen.meetingwall.network.json.SaveUpdateModuleDataWraper;
import com.nextgen.meetingwall.stateprogressbar.StateProgressBar;
import com.nextgen.meetingwall.utils.SharedPreference;
import com.nextgen.meetingwall.utils.TagsEditText;
import com.nextgen.meetingwall.utils.Utils;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cc.cloudist.acplibrary.ACProgressFlower;

/*
 * Created by cas on 30-03-2017.
 */

@SuppressWarnings("deprecation")
public class EssentialsActivity extends AppCompatActivity implements TagsEditText.TagsEditListener{

    private static final String TAG = "EssentialsActivity";
    protected String[] descriptionData = {"Setup", "Agenda", "Essentials", "Review"};



    String MeetingPk_Id;
    String Meeting_Name;
    String Meeting_StartTime;
    String Meeting_EndTime;

    private SharedPreference sharedPreference;

    @BindView(R.id.usage_stateprogressbar)
    protected StateProgressBar stateprogressbar;
    @BindView(R.id.material_spinner_Categories)
    protected MaterialBetterSpinner materialSpinnerCategories;
    @BindView(R.id.Essential_Objective_textView)
    protected EditText EssentialObjectiveTextView;
    @BindView(R.id.Essential_OutCome_textView)
    protected EditText EssentialOutComeTextView;
    @BindView(R.id.Essential_Assigned_textView)
    protected EditText EssentialAssignedTextView;
    @BindView(R.id.Essential_Instruction_textView)
    protected EditText EssentialInstructionTextView;
    @BindView(R.id.errorView)
    protected RelativeLayout errorView;
    @BindView(R.id.errorMessageText)
    protected TextView errorMessageText;
    private ACProgressFlower dialog;
    @BindView(R.id.tagsEditText)
    protected TagsEditText mTagsEditText;
    private ArrayList<Object> mSaveUpdateModuleDataArrayList = new ArrayList<>();
    Object[] AddUpdateTags;
    int MeetingPk_id ;
    private ArrayList<PickListDataResponce.PicklistDataRowsBeen> SpinnerpicklistdataArray = new ArrayList<>();
    private Network network =new Network(this);
    @OnClick(R.id.btn_submit_Essential)
    void  btnAgendaSubmit(){
        dialog = new ACProgressFlower.Builder(EssentialsActivity.this)
                .text(getResources().getString(R.string.label_loading))
                .build();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        MeetingAddUpdateTags();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_essentials);
        ButterKnife.bind(this);
        stateprogressbar.setStateDescriptionData(descriptionData);
        stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.THREE);
        if (android.os.Build.VERSION.SDK_INT >= 21){
            getWindow().setStatusBarColor(getResources()
                    .getColor(R.color.colorPrimary));
        }
        LinearLayout backLayout = (LinearLayout) findViewById(R.id.back_to_layout);
        TextView backText = (TextView) findViewById(R.id.back_text_header);
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(0, R.anim.push_down_in);

            }
        });
        Button doneButton = (Button) findViewById(R.id.select_button);
        backText.setText(getResources().getString(R.string.Nav_NewMeeting));


        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                 if((materialSpinnerCategories.getText().toString()).isEmpty()){
                    displayErrorMessage(getString(R.string.label_meeting_Categories));
                }else if(EssentialObjectiveTextView.getText().toString().isEmpty()){
                    displayErrorMessage(getString(R.string.label_meeting_Objective));
                }else if(EssentialAssignedTextView.getText().toString().isEmpty()){
                   displayErrorMessage(getString(R.string.label_meeting_Assigned));
                }else if(EssentialOutComeTextView.getText().toString().isEmpty()){
                    displayErrorMessage(getString(R.string.label_meeting_OutCome));
                }else if(EssentialInstructionTextView.getText().toString().isEmpty()){
                    displayErrorMessage(getString(R.string.label_meeting_Instruction));
                }else{
                    if(Utils.isConnectingToInternet(EssentialsActivity.this)){
                        dialog = new ACProgressFlower.Builder(EssentialsActivity.this)
                                .text(getResources().getString(R.string.label_loading))
                                .build();
                        dialog.setCanceledOnTouchOutside(false);
                        dialog.show();

                        MeetingAddUpdateTags();

                    }else{
                        displayErrorMessage(getString(R.string.label_connection));
                    }

                }
            }
        });


        ListDataMeetingTypes();

        mTagsEditText.setTagsListener(this);
        mTagsEditText.setTagsWithSpacesEnabled(true);
        mTagsEditText.setThreshold(1);

        sharedPreference = new SharedPreference(this);

        MeetingPk_Id =   sharedPreference.getMeetingSaveData("MeetingPk_Id");
        Meeting_Name =   sharedPreference.getMeetingSaveData("Meeting_Name");
        Meeting_StartTime =   sharedPreference.getMeetingSaveData("Meeting_StartTime");
        Meeting_EndTime =   sharedPreference.getMeetingSaveData("Meeting_EndTime");
    }

    @Override
    public void onTagsChanged(Collection<String> tags) {
        Log.d(TAG, "Tags changed: ");
        Log.d(TAG, Arrays.toString(tags.toArray()));
        AddUpdateTags = (tags.toArray());

    }

    @Override
    public void onEditingFinished() {
        Log.d(TAG,"OnEditing finished");
//        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
//        imm.hideSoftInputFromWindow(mTagsEditText.getWindowToken(), 0);
//        //mTagsEditText.clearFocus();
    }



    private void MeetingAddUpdateTags() {


        AddUpdateTagsWrapper wrapper = new AddUpdateTagsWrapper();
        wrapper.setAddUpdateTagsArray(AddUpdateTags);



        network.AddUpdateTags(Integer.parseInt(MeetingPk_Id),wrapper, new RequestCallback<AddUpdateTagsResponce>() {
            @Override
            public void onSuccess(AddUpdateTagsResponce response) {
                Log.e(TAG, "Success response::" + response);
                onSuccesAddUpdateTagsResponce(response);
            }

            @Override
            public void onFailed(String message) {
                Log.e(TAG, "Failed response::" + message);
            }
        });

    }
    private void onSuccesAddUpdateTagsResponce(final AddUpdateTagsResponce response) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (response.getSuccess()) {
                    CreateMeeting();
                }
            }
        });

    }

    private void ListDataMeetingTypes() {
        PickListDataWraper wrapper = new PickListDataWraper();
        wrapper.setFirstInt(0);
        wrapper.setRowsInt(10);
        wrapper.setSortOrderInt(1);
        wrapper.setPickListId(8);

        network.PickListData(wrapper, new RequestCallback<PickListDataResponce>() {
            @Override
            public void onSuccess(PickListDataResponce response) {
                Log.e(TAG, "Success response::" + response);
                onSuccessLocationResponce(response);
            }

            @Override
            public void onFailed(String message) {
               Log.e(TAG, "Failed response::" + message);
            }
        });

    }
    private void onSuccessLocationResponce(final PickListDataResponce response) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (response.getJqGridData().getRowsArray().size() > 0) {
                    for (int i = 0; i < response.getJqGridData().getRowsArray().size(); i++) {
                        SpinnerpicklistdataArray.add(response.getJqGridData().getRowsArray().get(i));
                    }
                    ArrayAdapter<PickListDataResponce.PicklistDataRowsBeen> adapter = new ArrayAdapter<>(EssentialsActivity.this, R.layout.custom_spinner_dropdown_item, SpinnerpicklistdataArray);
                    materialSpinnerCategories.setAdapter(adapter);
                }
            }
        });

    }


    public void CreateMeeting() {
        int Ids = 0;
        if (materialSpinnerCategories.getText().toString() != null) {
            for (int i = 0; i < SpinnerpicklistdataArray.size(); i++) {
                if (SpinnerpicklistdataArray.get(i).getPicklistDataCategories().equalsIgnoreCase(materialSpinnerCategories.getText().toString())) {
                    Ids = SpinnerpicklistdataArray.get(i).getPicklistDataId1();
                }
            }
        }
        EssentialsSaveUpdateModuleDataWrapper.EssentialsDisplayColsArray fieldsdataArray = new EssentialsSaveUpdateModuleDataWrapper.EssentialsDisplayColsArray();
        fieldsdataArray.setMeetingEssentialsName("Name");
        fieldsdataArray.setMeetingEssentialsDb_field_name("field_name");
        fieldsdataArray.setMeetingEssentialsType("Text");
        fieldsdataArray.setMeetingEssentialsBlock_pk_id(43);
        fieldsdataArray.setMeetingEssentialsValue(Meeting_Name);
        mSaveUpdateModuleDataArrayList.add(fieldsdataArray);

        EssentialsSaveUpdateModuleDataWrapper.EssentialsDisplayColsArray  fieldsdataArray1 = new EssentialsSaveUpdateModuleDataWrapper.EssentialsDisplayColsArray ();
        fieldsdataArray1.setMeetingEssentialsName("Assigned To");
        fieldsdataArray1.setMeetingEssentialsDb_field_name("field_assigned_to");
        fieldsdataArray1.setMeetingEssentialsType("Owner");
        fieldsdataArray1.setMeetingEssentialsBlock_pk_id(43);
        fieldsdataArray1.setMeetingEssentialsValue("User :: Superadmin :: admin");
        mSaveUpdateModuleDataArrayList.add(fieldsdataArray1);

        EssentialsSaveUpdateModuleDataWrapper.EssentialsDisplayColsArray  fieldsdataArray2 = new EssentialsSaveUpdateModuleDataWrapper.EssentialsDisplayColsArray ();
        fieldsdataArray2.setMeetingEssentialsName("Module Number");
        fieldsdataArray2.setMeetingEssentialsDb_field_name("field_module_number__@__field_module_number_prefix");
        fieldsdataArray2.setMeetingEssentialsType("Module Number");
        fieldsdataArray2.setMeetingEssentialsBlock_pk_id(43);
        fieldsdataArray2.setMeetingEssentialsValue("MEETING"+MeetingPk_Id);
        mSaveUpdateModuleDataArrayList.add(fieldsdataArray2);

//        ArrayList<EssentialsSaveUpdateModuleDataWrapper.EssentialsType> Array = new ArrayList<>();
//        EssentialsSaveUpdateModuleDataWrapper.EssentialsType essentialsType  = new EssentialsSaveUpdateModuleDataWrapper.EssentialsType();
//        essentialsType.setEssentialsTypeName("Type");
//        essentialsType.setEssentialsTypeDb_field_name("field_module_meetings_r0id");
//        essentialsType.setEssentialsTypePick_list_id(43);
//        essentialsType.setEssentialsTypeOfType("Pick List");
//        essentialsType.getEssentialsTypeBlock_pk_id();
//        EssentialsSaveUpdateModuleDataWrapper.EssentialsType.EssentialsTypeOfValue fieldsdataArray4 = new EssentialsSaveUpdateModuleDataWrapper.EssentialsType.EssentialsTypeOfValue();
//        fieldsdataArray4.setEssentialsTypeOfValueTypeValue(materialSpinnerCategories.getText().toString());
//        fieldsdataArray4.setEssentialsTypeOfValueTypeValuePkId(Ids);
//        essentialsType.setEssentialsTypeOfValue(fieldsdataArray4);
//        Array.add(essentialsType);
//        mSaveUpdateModuleDataArrayList.add(Array.get(0));



        EssentialsSaveUpdateModuleDataWrapper.EssentialsDisplayColsArray  fieldsdataArrayStartTime = new EssentialsSaveUpdateModuleDataWrapper.EssentialsDisplayColsArray ();
        fieldsdataArrayStartTime.setMeetingEssentialsName("Start Time");
        fieldsdataArrayStartTime.setMeetingEssentialsDb_field_name("field_start_time");
        fieldsdataArrayStartTime.setMeetingEssentialsType("Time Stamp");
        fieldsdataArrayStartTime.setMeetingEssentialsBlock_pk_id(43);
        fieldsdataArrayStartTime.setMeetingEssentialsValue(Meeting_StartTime);
        mSaveUpdateModuleDataArrayList.add(fieldsdataArrayStartTime);

        EssentialsSaveUpdateModuleDataWrapper.EssentialsDisplayColsArray  fieldsdataArrayEndTime = new EssentialsSaveUpdateModuleDataWrapper.EssentialsDisplayColsArray ();
        fieldsdataArrayEndTime.setMeetingEssentialsName("End Time");
        fieldsdataArrayEndTime.setMeetingEssentialsDb_field_name("field_end_time");
        fieldsdataArrayEndTime.setMeetingEssentialsType("Time Stamp");
        fieldsdataArrayEndTime.setMeetingEssentialsBlock_pk_id(43);
        fieldsdataArrayEndTime.setMeetingEssentialsValue(Meeting_EndTime);
        mSaveUpdateModuleDataArrayList.add(fieldsdataArrayEndTime);

        EssentialsSaveUpdateModuleDataWrapper.EssentialsCategories essentialsCategories = new EssentialsSaveUpdateModuleDataWrapper.EssentialsCategories();
        essentialsCategories.setEssentialsCategoriesName("Categories");
        essentialsCategories.setEssentialsCategoriesDb_field_name("field_categories");
        essentialsCategories.setEssentialsCategoriesPick_list_id(8);
        EssentialsSaveUpdateModuleDataWrapper.EssentialsCategories.EssentialsCategoriesValue essentialsCategoriesValue =new EssentialsSaveUpdateModuleDataWrapper.EssentialsCategories.EssentialsCategoriesValue();
        essentialsCategoriesValue.setEssentialsCategoriesValue_pk_id(Ids);
        essentialsCategoriesValue.setEssentialsCategoriesValue_value((materialSpinnerCategories.getText().toString()));
        essentialsCategories.setEssentialsCategoriesValue(essentialsCategoriesValue);
        essentialsCategories.setEssentialsCategoriesType("Pick List");
        essentialsCategories.setEssentialsCategoriesBlock_pk_id(51);
        mSaveUpdateModuleDataArrayList.add(essentialsCategories);
        EssentialsSaveUpdateModuleDataWrapper.EssentialsCategories.EssentialsObjective essentialsObjective = new EssentialsSaveUpdateModuleDataWrapper.EssentialsCategories.EssentialsObjective();
        essentialsObjective .setEssentialsObjectiveName("Objective");
        essentialsObjective.setEssentialsObjectiveDb_field_name("field_objective");
        essentialsObjective.setEssentialsObjectiveType("Text");
        essentialsObjective.setEssentialsObjectivevalue(EssentialObjectiveTextView.getText().toString());
        essentialsObjective.setEssentialsObjectiveBlock_pk_id(51);
        mSaveUpdateModuleDataArrayList.add(essentialsObjective);
        EssentialsSaveUpdateModuleDataWrapper.EssentialsCategories.EssentialsObjective essentialsUserGroups = new EssentialsSaveUpdateModuleDataWrapper.EssentialsCategories.EssentialsObjective();
        essentialsUserGroups  .setEssentialsObjectiveName("User Groups");
        essentialsUserGroups .setEssentialsObjectiveDb_field_name("field_user_groups");
        essentialsUserGroups .setEssentialsObjectiveType("Text Area");
        essentialsUserGroups.setEssentialsObjectivevalue(EssentialAssignedTextView.getText().toString());
        essentialsUserGroups .setEssentialsObjectiveBlock_pk_id(51);
        mSaveUpdateModuleDataArrayList.add(essentialsUserGroups);
        EssentialsSaveUpdateModuleDataWrapper.EssentialsCategories.EssentialsObjective essentialsOutcome = new EssentialsSaveUpdateModuleDataWrapper.EssentialsCategories.EssentialsObjective();
        essentialsOutcome .setEssentialsObjectiveName("Outcome");
        essentialsOutcome.setEssentialsObjectiveDb_field_name("field_outcome");
        essentialsOutcome.setEssentialsObjectiveType("Text Area");
        essentialsOutcome.setEssentialsObjectivevalue(EssentialOutComeTextView.getText().toString());
        essentialsOutcome.setEssentialsObjectiveBlock_pk_id(51);
        mSaveUpdateModuleDataArrayList.add(essentialsOutcome);
        EssentialsSaveUpdateModuleDataWrapper.EssentialsCategories.EssentialsObjective essentialsInstructions = new EssentialsSaveUpdateModuleDataWrapper.EssentialsCategories.EssentialsObjective();
        essentialsInstructions .setEssentialsObjectiveName("Instructions");
        essentialsInstructions.setEssentialsObjectiveDb_field_name("field_instructions");
        essentialsInstructions.setEssentialsObjectiveType("Text Area");
        essentialsInstructions.setEssentialsObjectivevalue(EssentialInstructionTextView.getText().toString());
        essentialsInstructions.setEssentialsObjectiveBlock_pk_id(51);
        mSaveUpdateModuleDataArrayList.add(essentialsInstructions);
        EssentialsSaveUpdateModuleDataWrapper essentialsSaveUpdateModuleDataWrapper = new EssentialsSaveUpdateModuleDataWrapper();
        essentialsSaveUpdateModuleDataWrapper.setEssentialsModuleString("Meetings");
        essentialsSaveUpdateModuleDataWrapper.setEssentialsActionString("Update");
        essentialsSaveUpdateModuleDataWrapper.setEssentialsPk_idString(MeetingPk_Id);
        essentialsSaveUpdateModuleDataWrapper.setEssentialsDisplayColsArray(mSaveUpdateModuleDataArrayList);

        network.EssentialsSaveUpdateModuleData(essentialsSaveUpdateModuleDataWrapper, new RequestCallback<SaveUpdateModuleDataResponce>() {
            @Override
            public void onSuccess(SaveUpdateModuleDataResponce response) {
                Log.e(TAG, "Success response::" + response);
                onSuccesSaveUpdateResponce(response);
            }

            @Override
            public void onFailed(String message) {
                Log.e(TAG, "Failed response::" + message);
            }
        });
    }

    private void onSuccesSaveUpdateResponce(final SaveUpdateModuleDataResponce response) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (response.isSuccess()) {
                    dialog.cancel();
                    Intent intent = new Intent(EssentialsActivity.this, MeetingReviewActivity.class);
                    startActivity(intent);
                } else {
                    dialog.cancel();
                    Toast.makeText(EssentialsActivity.this, response.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public void displayErrorMessage(String message) {
        errorMessageText.setText(message);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) errorMessageText.getLayoutParams();
        if (errorMessageText.getLineCount() == 3) {
            params.height = getResources().getDimensionPixelSize(R.dimen.search_screen_layout_height);
            errorMessageText.setLayoutParams(params);
        } else {
            params.height = getResources().getDimensionPixelSize(R.dimen.signIn_page_reset_mail_view);
            errorMessageText.setLayoutParams(params);
        }
        if (errorView.getAlpha() == 0) {
            errorView.setTranslationY(-60);
            stateprogressbar.setVisibility(View.GONE);
            errorView.setVisibility(View.VISIBLE);
            errorView.setBackgroundResource(R.color.error_message_background);
            errorView.animate().translationY(0)
                    .alpha(1)
                    .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    errorView.animate()
                            .alpha(0)
                            .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
                    errorView.setVisibility(View.GONE);
                    stateprogressbar.setVisibility(View.VISIBLE);
                }
            }, 2000);

        }
    }

}
