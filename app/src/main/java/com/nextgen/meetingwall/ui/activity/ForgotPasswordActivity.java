package com.nextgen.meetingwall.ui.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.widget.EditText;


import com.nextgen.meetingwall.R;
import com.nextgen.meetingwall.ui.dialog.DialogFactory;
import com.nextgen.meetingwall.utils.FormValidator;
import com.nextgen.meetingwall.utils.Helper;
import com.nextgen.meetingwall.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;

@SuppressWarnings("ALL")
public class ForgotPasswordActivity extends AppCompatActivity {
    @BindView(R.id.input_layout_email_verify)
    protected TextInputLayout mUserEmailLayout;
    @BindView(R.id.input_email_edittext)
    protected EditText mEmailEditText;

    @OnTouch(R.id.linearlayout_screen)
    protected boolean touch(){
        Helper.hideKeyboard(ForgotPasswordActivity.this);
        return false;
    }

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getSupportActionBar().hide();
        setContentView(R.layout.activity_forgotpassword);
        ButterKnife.bind(this);

    }

    @OnClick(R.id.btn_submit_email)
    protected void onSubmit() {
        if (validate()) {

            if (Utils.isConnectingToInternet(this)) {
                final ProgressDialog progressDialog = new ProgressDialog(ForgotPasswordActivity.this);
                progressDialog.setIndeterminate(true);
                progressDialog.setMessage("Authenticating...");
                progressDialog.show();
                Thread timerThread = new Thread() {
                    public void run() {
                        try {
                            sleep(3 * 1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } finally {
                            progressDialog.dismiss();
                            Intent intent = new Intent(ForgotPasswordActivity.this, LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }
                };
                timerThread.start();

            } else {
                dialog(getResources().getString(R.string.label_connection));
            }
        }
    }





    private boolean validate() {
        boolean validEmail = FormValidator.validateEmail(mEmailEditText, R.string.label_err_msg_email, mUserEmailLayout);
        return validEmail;
    }

    private void dialog(final String message) {
        DialogFactory.showAConfirmationDialog(this, message, 0,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
    }

    private void dialogOnExit() {
        DialogFactory.showAConfirDialog(this, getResources().getString(R.string.label_exit), 0,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.stable, R.anim.slide_out_right);
    }

}
