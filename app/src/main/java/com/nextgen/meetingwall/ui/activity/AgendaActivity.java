package com.nextgen.meetingwall.ui.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nextgen.meetingwall.R;
import com.nextgen.meetingwall.adapter.CustomExpandableListAdapter;
import com.nextgen.meetingwall.model.ChildObject;
import com.nextgen.meetingwall.model.ModuleListMetadataArray;
import com.nextgen.meetingwall.model.ParentObject;
import com.nextgen.meetingwall.network.Network;
import com.nextgen.meetingwall.network.RequestCallback;
import com.nextgen.meetingwall.network.json.AgendasListResponce;
import com.nextgen.meetingwall.network.json.AgendasListWraper;
import com.nextgen.meetingwall.network.json.SaveUpdateModuleDataResponce;
import com.nextgen.meetingwall.stateprogressbar.StateProgressBar;
import com.nextgen.meetingwall.ui.dialog.DialogFactory;
import com.nextgen.meetingwall.utils.SharedPreference;
import com.nextgen.meetingwall.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cc.cloudist.acplibrary.ACProgressFlower;


@SuppressWarnings("deprecation")
public class AgendaActivity extends AppCompatActivity {

    private static final String TAG = "AgendaActivity";
    protected String[] descriptionData = {"Setup", "Agenda", "Essentials", "Review"};

    @BindView(R.id.usage_stateprogressbar)
    protected StateProgressBar stateprogressbar;
    @BindView(R.id.expandableListView)
    protected ExpandableListView expandableListView;
    @BindView(R.id.errorView)
    protected RelativeLayout errorView;
    @BindView(R.id.errorMessageText)
    protected TextView errorMessageText;

    private List<ParentObject> parentObjects = new ArrayList<>();
    private List<ChildObject> childObjects = new ArrayList<>();
    private List<ModuleListMetadataArray> moduleListMetadataArrays = new ArrayList<>();
    private ArrayList<AgendasListResponce.AgendasRowsBeen> mAgendasListResponse = new ArrayList<>();
    private ArrayList<String> AgendaSubTitleslist = new ArrayList<>();

    protected String Meeting_PK_Id;

    private ACProgressFlower dialog;
    private final Network network = new Network(this);
    Intent intent;
    private SharedPreference sharedPreference;
    @OnClick(R.id.btn_submit_Agenda)
    void  btnAgendaSubmit(){
        Intent intent = new Intent(AgendaActivity.this, EssentialsActivity.class);
         startActivity(intent);
    }
    @OnClick(R.id.Add_Agenda_ImageView)
    void  btnAgendaCreation(){
        Intent intent = new Intent(AgendaActivity.this, AgendaCreationActivity.class);
        intent.putExtra("Meeting_PkId",Meeting_PK_Id);
         startActivity(intent);
    }

    String MeetingPk_Id;
    String Meeting_Name;
    String Meeting_StartTime;
    String Meeting_EndTime;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agenda);
        ButterKnife.bind(this);
        stateprogressbar.setStateDescriptionData(descriptionData);
        stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.TWO);
 //       expandableListView.setOnGroupExpandListener(onGroupExpandListenser);
        sharedPreference = new SharedPreference(this);

         MeetingPk_Id =   sharedPreference.getMeetingSaveData("MeetingPk_Id");
         Meeting_Name =   sharedPreference.getMeetingSaveData("Meeting_Name");
         Meeting_StartTime =   sharedPreference.getMeetingSaveData("Meeting_StartTime");
         Meeting_EndTime =   sharedPreference.getMeetingSaveData("Meeting_EndTime");
        intent = getIntent();
      Meeting_PK_Id  = intent.getStringExtra("Meeting_PkId");
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            getWindow().setStatusBarColor(getResources()
                    .getColor(R.color.colorPrimary));
        }

        LinearLayout backLayout = (LinearLayout) findViewById(R.id.back_to_layout);
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(0, R.anim.push_down_in);

            }
        });
        Button doneButton = (Button)findViewById(R.id.select_button);
        doneButton.setText(""+getResources().getString(R.string.Skip_button_string)+"");
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AgendaActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(0, R.anim.push_down_in);
            }
        });
        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                Toast.makeText(getApplicationContext(),
                        parentObjects.get(groupPosition) + " List Expanded.",
                        Toast.LENGTH_SHORT).show();
            }
        });

        expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                Toast.makeText(getApplicationContext(),
                        parentObjects.get(groupPosition) + " List Collapsed.",
                        Toast.LENGTH_SHORT).show();

            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {

                return false;
            }
        });

        if(Utils.isConnectingToInternet(AgendaActivity.this)){
            prepareMeetingsData();
        }else{
            displayErrorMessage(getString(R.string.label_connection));
        }

    }
    public  List<ParentObject> getData()
    {

        for (int i = 0; i<mAgendasListResponse.size(); i++)
        {
            parentObjects.add(new ParentObject(mAgendasListResponse.get(i), getChildren(i)));

        }
        return parentObjects;
    }

    private  List<ChildObject> getChildren(int childCount)
    {

        for (int i =0; i<childCount; i++)
        {
            childObjects.add(new ChildObject(AgendaSubTitleslist.get(i), 10 +i ));
        }
        return childObjects;
    }
    public void prepareMeetingsData() {

        dialog = new ACProgressFlower.Builder(this)
                .text(getResources().getString(R.string.label_loading))
                .build();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();


        AgendaSubTitleslist.add("Welcome Note by John Doe");
        AgendaSubTitleslist.add("Introduction to the New HR Guidelines Process by Williams Strings");
        AgendaSubTitleslist.add("Closing Comments by John Doe");
        AgendaSubTitleslist.add("Introduction to the New HR Guidelines Process by Williams Strings");
        AgendaSubTitleslist.add("Closing Comments by John Doe");


        prepareMeetingListItemEmployees();

    }

    private void prepareMeetingListItemEmployees() {

        network.MeetingAgendas(new RequestCallback<ArrayList<ModuleListMetadataArray>>() {
            @Override
            public void onSuccess(ArrayList<ModuleListMetadataArray> response) {
                Log.e(TAG, "Success response::" + response);
                onSuccesEmployees(response);
            }

            @Override
            public void onFailed(String message) {

                Log.e(TAG, "Failed response::" + message);
            }
        });
    }
    private void onSuccesEmployees(final ArrayList<ModuleListMetadataArray> jobsList) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                moduleListMetadataArrays.clear();
                if (jobsList.size() == 0) {
                    dialog(getResources().getString(R.string.label_server_data_notfound));
                } else {
                    for (int i = 0; i < jobsList.size(); i++) {
                        moduleListMetadataArrays.add(jobsList.get(i));
                    }
                        ListDataMeetingEmployees();

                }
            }
        });
    }


    private void ListDataMeetingEmployees(){
        AgendasListWraper agendasListWraper = new AgendasListWraper();
        agendasListWraper.setFirstInt(0);
        agendasListWraper.setRowsInt(10);
        agendasListWraper.setSortOrderInt(1);
        agendasListWraper.setPageInt(1);
        //    wrapper.setFiltersObject(new FiltersObjectBeen());
        //    wrapper.setGlobalFilter(null);
        AgendasListWraper.RelObject relObject = new AgendasListWraper.RelObject();
        relObject.setFromModuleString("Meetings");
        relObject.setFromDbTableString("module_meetings");
        relObject.setFromRelationInt(1);
        relObject.setFromFieldIdInt(559);
        relObject.setFromBlockIdInt(43);
        relObject.setFromBlockString("Basic Information");
        relObject.setFromFieldString("Agendas");
        relObject.setFromDbFieldString("pk_id");
        relObject.setToModuleString("Agendas");
        relObject.setToTableString("module_agendas");
        relObject.setToRelationInt(0);
        relObject.setToFieldIdInt(558);
        relObject.setToBlockIdInt(47);
        relObject.setToBlockString("Basic Information");
        relObject.setToFieldString("Meeting");
        relObject.setToDbFieldString("field_meeting");
        relObject.setXrefTableNameString("module_agendas");
        relObject.setToRecordIdentifierString("field_name");
        relObject.setPkIdString(Meeting_PK_Id);
        agendasListWraper.setRelobject(relObject);
        agendasListWraper.setDisplayColsArray(moduleListMetadataArrays);
        network.ListMeetingAgendas(agendasListWraper,new RequestCallback<AgendasListResponce>() {
            @Override
            public void onSuccess(AgendasListResponce  response) {
                Log.e(TAG, "Success response::" + response);

                onSucessListMeetingParticipants(response);
            }

            @Override
            public void onFailed(String message) {

                Log.e(TAG, "Failed response::" + message);
            }
        });

    }

    private void  onSucessListMeetingParticipants(final AgendasListResponce response){
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mAgendasListResponse.clear();
                if (response.getJqGridData().getRowsArray().size() > 0) {
                    for (int i = 0; i < response.getJqGridData().getRowsArray().size(); i++) {
                        mAgendasListResponse.add(response.getJqGridData().getRowsArray().get(i));
                    }
                }

                UpdateList();
            }

        });
    }

    public void UpdateList(){
        CustomExpandableListAdapter adapter = new CustomExpandableListAdapter(this, getData());
        expandableListView.setAdapter(adapter);
        dialog.cancel();
    }

    private void dialog(final String message) {
        DialogFactory.showAConfirmationDialog(this, message, 0,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
    }
    public void displayErrorMessage(String message) {
        errorMessageText.setText(message);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) errorMessageText.getLayoutParams();
        if (errorMessageText.getLineCount() == 3) {
            params.height = getResources().getDimensionPixelSize(R.dimen.search_screen_layout_height);
            errorMessageText.setLayoutParams(params);
        } else {
            params.height = getResources().getDimensionPixelSize(R.dimen.signIn_page_reset_mail_view);
            errorMessageText.setLayoutParams(params);
        }
        if (errorView.getAlpha() == 0) {
            errorView.setTranslationY(-60);
            stateprogressbar.setVisibility(View.GONE);
            errorView.setVisibility(View.VISIBLE);
            errorView.setBackgroundResource(R.color.error_message_background);
            errorView.animate().translationY(0)
                    .alpha(1)
                    .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    errorView.animate()
                            .alpha(0)
                            .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
                    errorView.setVisibility(View.GONE);
                    stateprogressbar.setVisibility(View.VISIBLE);
                }
            }, 2000);

        }
    }

}