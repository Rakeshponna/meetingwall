package com.nextgen.meetingwall.ui.fragments;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;


import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.nextgen.meetingwall.R;
import com.nextgen.meetingwall.ui.activity.MainActivity;
import com.nextgen.meetingwall.model.MeetingBeen;
import com.nextgen.meetingwall.model.UserOnlineStatusBean;
import com.nextgen.meetingwall.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by cas on 07-02-2017.
 */

@SuppressWarnings("ALL")
public class MeetingPreviewFragment extends Fragment implements View.OnClickListener {
    private Fragment fragment;
    private List<MeetingBeen> meetingBeenArrayList = new ArrayList<>();
    private Context ctx;
    private LayoutInflater inflater;
    private Boolean more = false;
    private Boolean expandable = true;
    private Boolean expand = false;
    @BindView(R.id.Descreption_textview)
    TextView DescreptionTextView;
    @BindView(R.id.Invitees_details_Layout)
    LinearLayout InviteeslinearLayout;
    @BindView(R.id.Invitees_agenda_Layout)
    LinearLayout AgendalinearLayout;
    @BindView(R.id.Requirements_Layout)
    LinearLayout RequirementslinearLayout;
    @BindView(R.id.Attachments_Layout)
    LinearLayout AttachmentslinearLayout;
    @BindView(R.id.More_text)
    TextView MoreInviteesTextview;
    @BindView(R.id.Less_text)
    TextView LessInviteesTextview;
    @BindView(R.id.SeeMore_button)
    TextView SeeMoreTextview;

    @OnClick(R.id.More_text)
    void More() {
        // TODO call server...
        InviteeslinearLayout.removeAllViews();
        more = true;
        InviteesMeeting();
    }

    @OnClick(R.id.Less_text)
    void Less() {
        // TODO call server...
        InviteeslinearLayout.removeAllViews();
        more = false;
        InviteesMeeting();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_meeting_preview, container, false);
        ButterKnife.bind(this, view);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Design review");
        setHasOptionsMenu(true);
        ActionBar bar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(getActivity().getResources()
                .getColor(R.color.Meeting_preview_ActionBar_color)));

        if (android.os.Build.VERSION.SDK_INT >= 21) {
            getActivity().getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getActivity().getWindow().setStatusBarColor(getActivity().getResources()
                    .getColor(R.color.Meeting_preview_ActionBar_color));
        }

        MainActivity activity = (MainActivity) getActivity();
        activity.hideCalenderView();

        DescreptionTextView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (expandable) {
                    expandable = false;
                    if (DescreptionTextView.getLineCount() > 4) {
                        SeeMoreTextview.setVisibility(View.VISIBLE);
                        ObjectAnimator animation = ObjectAnimator.ofInt(DescreptionTextView, "maxLines", 4);
                        animation.setDuration(0).start();
                    }
                }
            }
        });

        SeeMoreTextview.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if (!expand) {
                    expand = true;
                    SeeMoreTextview.setText("Less...");
                    ObjectAnimator animation = ObjectAnimator.ofInt(DescreptionTextView, "maxLines", 40);
                    animation.setDuration(100).start();
                } else {
                    expand = false;
                    SeeMoreTextview.setText("More...");
                    ObjectAnimator animation = ObjectAnimator.ofInt(DescreptionTextView, "maxLines", 4);
                    animation.setDuration(100).start();
                }

            }
        });
        InviteesMeeting();

        for (int i = 0; i < 3; i++) {
            inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View meetingsView = inflater.inflate(R.layout.view_agenda_meeting, null);
            TextView AgendaTimeTextView = ButterKnife.findById(meetingsView, R.id.agenda_time_text);
            TextView AgendaDescriptionTextView = ButterKnife.findById(meetingsView, R.id.agenda_description_text);
            TextView AgendaEmailidTextView = ButterKnife.findById(meetingsView, R.id.agenda_mailId_text);
            View AgendaBottomView = ButterKnife.findById(meetingsView, R.id.agenda_bottom_view);


            AgendaTimeTextView.setText("5.00pm-5.10pm");
            AgendaDescriptionTextView.setText("Demo of this week release");
            AgendaEmailidTextView.setText("by<nagaraju.c@gmail.com>");
            AgendalinearLayout.addView(meetingsView);
            if (i == 0) {
                AgendaBottomView.setVisibility(View.GONE);
            }

        }
        for (int i = 0; i < 2; i++) {
            inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View meetingsView = inflater.inflate(R.layout.view_requirements_meeting, null);
            TextView RequirementNameTextView = ButterKnife.findById(meetingsView, R.id.Requirement_Name_text);
            TextView RequirementdesignationTextView = ButterKnife.findById(meetingsView, R.id.Requirement_Manager_text);
            TextView RequirementDescriptionTextView = ButterKnife.findById(meetingsView, R.id.Requirement_Description_text);
            ImageView RequirementimageView = ButterKnife.findById(meetingsView, R.id.Requirment_profile_ImageView);
            View RequirementsBottomView = ButterKnife.findById(meetingsView, R.id.requirements_bottom_view);
            Picasso.with(inflater.getContext())
                    .load(Utils.imageURLArray[i])
                    .into(RequirementimageView);
            RequirementNameTextView.setText("Willium String");
            RequirementdesignationTextView.setText("Maganger");
            RequirementDescriptionTextView.setText("Come up with the leave plans for the next week and any other concerns related to the work.\n" +
                    "on or before 17-10-16");
            RequirementslinearLayout.addView(meetingsView);
            if (i == 0) {
                RequirementsBottomView.setVisibility(View.GONE);
            }
        }
        for (int i = 0; i < 2; i++) {
            inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View meetingsView = inflater.inflate(R.layout.view_attachments_meeting, null);
            TextView AttachmentNameTextView = ButterKnife.findById(meetingsView, R.id.Attachment_Name_text);
            TextView AttachmentSizeTextView = ButterKnife.findById(meetingsView, R.id.Attachment_Size_text);
            TextView AttachmentTypeTextView = ButterKnife.findById(meetingsView, R.id.Attachment_Type_text);
            ImageView AttachmentimageView = ButterKnife.findById(meetingsView, R.id.Attachment_Image);
            View AttachmentsBottomView = ButterKnife.findById(meetingsView, R.id.attachments_bottom_view);
            Picasso.with(inflater.getContext())
                    .load(Utils.imageURLArray[i])
                    .into(AttachmentimageView);
            AttachmentNameTextView.setText("Project poster");
            AttachmentSizeTextView.setText("145kb");
            AttachmentTypeTextView.setText("image");
            AttachmentslinearLayout.addView(meetingsView);
            if (i == 0) {
                AttachmentsBottomView.setVisibility(View.GONE);
            }
        }


        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

        }
    }


    public void InviteesMeeting() {
        for (int i = 0; i < 5; i++) {
            inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View meetingsView = inflater.inflate(R.layout.view_invitees_meeting, null);
            TextView NameTextView = ButterKnife.findById(meetingsView, R.id.Invitees_Name_text);
            TextView designationTextView = ButterKnife.findById(meetingsView, R.id.Invitees_Manager_text);
            TextView EmailidTextView = ButterKnife.findById(meetingsView, R.id.Invitees_Email_text);

            ImageView imageView = ButterKnife.findById(meetingsView, R.id.Invitees_profile_ImageView);
            Picasso.with(inflater.getContext())
                    .load(Utils.imageURLArray[i])
                    .into(imageView);
            NameTextView.setText("Willium String");
            designationTextView.setText("Maganger");
            EmailidTextView.setText("nagaraju.c@gmail.com");
            if (more) {
                InviteeslinearLayout.addView(meetingsView);
                MoreInviteesTextview.setVisibility(View.GONE);
                LessInviteesTextview.setVisibility(View.VISIBLE);
            } else {
                if (i < 3) {
                    InviteeslinearLayout.addView(meetingsView);
                    MoreInviteesTextview.setVisibility(View.VISIBLE);
                    LessInviteesTextview.setVisibility(View.GONE);
                }
            }


        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.cross, menu);

        return;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_close) {


            fragment = new AllMeetingsFragment();
            ((MainActivity) getActivity()). popFrag();
//            FragmentManager fragmentManager = getFragmentManager();
//            FragmentTransaction ft = fragmentManager.beginTransaction();
//            ft.setCustomAnimations(R.anim.slideright, R.anim.zoom_out);
//            ft.replace(R.id.frame_container, fragment);
//            ft.commit();
//            Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
//            Animation RightSwipe = AnimationUtils.loadAnimation(getActivity(), R.anim.slideright);
//            toolbar.startAnimation(RightSwipe);
        //        getFragmentManager().popBackStack();


        }
        return super.onOptionsItemSelected(item);
    }



    private void prepareMovieData() {
        meetingBeenArrayList = new ArrayList<>();
        MeetingBeen been = new MeetingBeen();
        ArrayList<UserOnlineStatusBean> list = new ArrayList<>();
        UserOnlineStatusBean userOnlineStatusBean = new UserOnlineStatusBean();
        if (meetingBeenArrayList.size() == 0) {
            been = new MeetingBeen();
            been.setDesignReview("Design Review");
            been.setReviewDescription("Agri App");
            been.setCountry("Kenya");
            been.setTimeDuration("10:15-11:45");
            been.setHeaderTime("Thu,Feb02");
            list = new ArrayList<>();
            userOnlineStatusBean = new UserOnlineStatusBean();
            userOnlineStatusBean.setImage("1");
            userOnlineStatusBean.setOnlineStatus(true);
            list.add(userOnlineStatusBean);
            userOnlineStatusBean = new UserOnlineStatusBean();
            userOnlineStatusBean.setImage("4");
            userOnlineStatusBean.setOnlineStatus(true);
            list.add(userOnlineStatusBean);
            userOnlineStatusBean = new UserOnlineStatusBean();
            userOnlineStatusBean.setImage("12");
            userOnlineStatusBean.setOnlineStatus(false);
            list.add(userOnlineStatusBean);
            userOnlineStatusBean = new UserOnlineStatusBean();
            userOnlineStatusBean.setImage("5");
            userOnlineStatusBean.setOnlineStatus(false);
            list.add(userOnlineStatusBean);
            userOnlineStatusBean = new UserOnlineStatusBean();
            userOnlineStatusBean.setImage("10");
            userOnlineStatusBean.setOnlineStatus(false);
            list.add(userOnlineStatusBean);
            userOnlineStatusBean = new UserOnlineStatusBean();
            userOnlineStatusBean.setImage("6");
            userOnlineStatusBean.setOnlineStatus(false);
            list.add(userOnlineStatusBean);

            been.setImages(list);
            meetingBeenArrayList.add(been);


        }
    }


}



