package com.nextgen.meetingwall.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;

import com.nextgen.meetingwall.R;


public class DialogFactory {
    Dialog dialog;

    public static Dialog createAlertDialog(Context context, @StringRes int title, @StringRes int message,
                                           final DialogInterface.OnCancelListener listener) {
        final AlertDialog.Builder builder = createBasicDialogBuilder(context, title, message);
        builder.setNegativeButton(R.string.OK, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (listener != null) {
                    listener.onCancel(dialog);
                }
            }
        });

        Dialog alertDialog = builder.create();
        alertDialog.setOnCancelListener(listener);
        return alertDialog;
    }

    @NonNull
    public static Dialog createAlertDialog(@NonNull Context context, @Nullable String title, @NonNull String message,
                                           @Nullable final DialogInterface.OnCancelListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.My_Dialog);
        builder.setMessage(message);
        if (title != null) {
            builder.setTitle(title);
        }
        builder.setNegativeButton(R.string.OK, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (listener != null) {
                    listener.onCancel(dialog);
                }
            }
        });

        Dialog dialog = builder.create();
        dialog.setOnCancelListener(listener);
        return dialog;
    }

    public static void showAConfirmationDialog(@NonNull Context context, String message, @StringRes int title,
                                               @NonNull final DialogInterface.OnClickListener confirmationListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.My_Dialog);
        if (title != 0) {
            builder.setTitle(title);
        }
        builder.setMessage(message);
//        builder.setNegativeButton( R.string.Cancel, new DialogInterface.OnClickListener()
//        {
//            @Override
//            public void onClick( DialogInterface dialog, int which )
//            {
//                dialog.dismiss();
//            }
//        } );
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.OK, confirmationListener);
        builder.create().show();
    }
    public static void showAConfirDialog(@NonNull Context context, String message, @StringRes int title,
                                         @NonNull final DialogInterface.OnClickListener confirmationListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.My_Dialog);
        if (title != 0) {
            builder.setTitle(title);
        }
        builder.setMessage(message);
        builder.setNegativeButton( R.string.Cancel, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which )
            {
                dialog.dismiss();
            }
        } );
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.OK, confirmationListener);
        builder.create().show();
    }


    private static AlertDialog.Builder createBasicDialogBuilder(@NonNull Context context, @StringRes int title,
                                                                @StringRes int message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.My_Dialog);

        if (title != 0) {
            builder.setTitle(title);
        }
        builder.setMessage(message);

        return builder;

    }


}