package com.nextgen.meetingwall.ui.activity;

import android.database.Observable;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

interface ApiConfig {
    @Multipart
    @POST("UploadRawFiles")
    Call<ServerResponse> uploadFile(@Part MultipartBody.Part files,
                                    @Part("name") RequestBody pkidname,@Part MultipartBody.Part file,
                                    @Part("file") RequestBody name);



    @Multipart
    @POST("UploadRawFiles")
    Call<ServerResponse> uploadMulFile(@Part MultipartBody.Part file1,
                                       @Part MultipartBody.Part file2);
}
