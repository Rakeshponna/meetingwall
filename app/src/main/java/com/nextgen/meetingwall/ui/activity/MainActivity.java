package com.nextgen.meetingwall.ui.activity;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.view.menu.MenuBuilder;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.nextgen.meetingwall.R;
import com.nextgen.meetingwall.model.ContactAddingBeen;
import com.nextgen.meetingwall.network.json.MeetingEmployeesResponse;
import com.nextgen.meetingwall.network.json.MeetingLocationsResponse;
import com.nextgen.meetingwall.stateprogressbar.StateProgressBar;
import com.nextgen.meetingwall.ui.fragments.AboutMeetingWallFragment;
import com.nextgen.meetingwall.ui.fragments.AllMeetingsFragment;
import com.nextgen.meetingwall.ui.fragments.ContactUsFragment;
import com.nextgen.meetingwall.ui.fragments.MeetingPreviewFragment;
import com.nextgen.meetingwall.ui.fragments.CreateMeetingFragment;
import com.nextgen.meetingwall.ui.fragments.NewMeetingFragment;
import com.nextgen.meetingwall.utils.Constants;
import com.nextgen.meetingwall.utils.Helper;
import com.nextgen.meetingwall.utils.Utils;


import org.joda.time.DateTime;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


@SuppressWarnings("ALL")
public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    public ArrayList<Fragment> stack = new ArrayList<>();
    private static final String TAG = "MainActivity";

    private int SeceltedDay;
    private int SelectedMonth;
    private int mPaddingSeparator;
    private int SelectedYear;
    private Date dates;
    private LayoutInflater mLayoutInflater;
    private MenuBuilder mMenu;
    private Object mFragmentItemID = null;
    private Bundle extras;
    private Boolean isfiresttime=false;
    protected String[] descriptionData = {"Setup", "Agenda", "Essentials", "Review"};

    @BindView(R.id.usage_stateprogressbar)
    protected StateProgressBar stateprogressbar;
    @BindView(R.id.drawer_layout)
    protected DrawerLayout drawer;
    @BindView(R.id.nav_view)
    protected NavigationView navigationView;
    @BindView(R.id.spinner_nav)
    protected TextView spinner_nav;
    @BindView(R.id.spinner_dropdown_image)
    protected ImageView Dropdownimage;
    Toolbar toolbar;
    @BindView(R.id.FarmcontainerLayout)
    protected LinearLayout FramLayout;

    @BindView(R.id.errorView)
    protected RelativeLayout errorView;
    @BindView(R.id.errorMessageText)
    protected TextView errorMessageText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
         toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        errorView.setVisibility(View.GONE);
        stateprogressbar.setStateDescriptionData(descriptionData);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);

        }

        dates = new Date();

        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        toggle.setDrawerIndicatorEnabled(false);

        toolbar.setNavigationIcon(R.drawable.menu_icon);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Helper.hideKeyboard(MainActivity.this);
                drawer.openDrawer(Gravity.LEFT);

            }
        });


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        extras = this.getIntent().getExtras();
        if (extras != null) {
            mFragmentItemID = extras.get(Constants.FRAGMENT_ID);
        }
        if (mFragmentItemID == null) {
            displayView(R.id.nav_Meeting);
        } else {
            displayView(mFragmentItemID);
        }

    }


    @OnClick(R.id.spinner_nav)
    void SpinnerText(){
        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(MainActivity.this,R.style.BottomSheetDialog);
        final View  view1 = getLayoutInflater().inflate(R.layout.content_dialog_bottom_sheet, null);
        final RadioGroup radioGroup = ButterKnife.findById(view1, R.id.radioGroup);
        Button DoneButton = ButterKnife.findById(view1, R.id.Done_button);
        RadioButton rbu1 =ButterKnife.findById(view1,R.id.allmeeting_radio_button);
        RadioButton rbu2 =ButterKnife.findById(view1,R.id.singelday_radio_button);
        RadioButton rbu3 =ButterKnife.findById(view1,R.id.multiday_radio_button);
        RadioButton rbu4 =ButterKnife.findById(view1,R.id.recurring_radio_button);
        bottomSheetDialog.setContentView(view1);
        bottomSheetDialog.show();

        if(spinner_nav.getText().toString().equals(getResources().getString(R.string.bottomsheet_allmeetings_string))){
            rbu1.setChecked(true);
        }
        else if(spinner_nav.getText().toString().equals(getResources().getString(R.string.bottomsheet_singleday_string))){
            rbu2.setChecked(true);
        }
        else if(spinner_nav.getText().toString().equals(getResources().getString(R.string.bottomsheet_multiday_string))){
            rbu3.setChecked(true);

        }else if(spinner_nav.getText().toString().equals(getResources().getString(R.string.bottomsheet_recurring_string))){
            rbu4.setChecked(true);
        }

        DoneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedId=radioGroup.getCheckedRadioButtonId();
                RadioButton   radioSexButton=(RadioButton)view1.findViewById(selectedId);
                bottomSheetDialog.dismiss();
                spinner_nav.setText(radioSexButton.getText());
            }
        });


        if (android.os.Build.VERSION.SDK_INT >= 21){
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setStatusBarColor(getResources()
                    .getColor(R.color.colorPrimary));
        }

    }

    boolean doubleBackToExitPressedOnce = false;
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);

        } else {
            popFrag();
          //  super.onBackPressed();
        }
    }

    private void displayView(Object mItemObject) {
        Fragment fragment = null;
        int mItem = (int) mItemObject;
        if (mItem == R.string.Nav_Meeting || mItem == R.id.nav_Meeting) {
            fragment = new AllMeetingsFragment();
            setTitle(R.string.Nav_Meeting);
         //   ShowCalenderView();
        } else if (mItem == R.string.Nav_NewContact || mItem == R.id.nav_NewContact) {
            fragment = new CreateMeetingFragment();
            setTitle(R.string.Nav_NewContact);
            hideCalenderView();
        }else if (mItem == R.string.Nav_NewMeeting ) {
            fragment = new NewMeetingFragment();
            setTitle(R.string.Nav_NewMeeting);
            spinner_nav.setVisibility(View.GONE);
            Dropdownimage.setVisibility(View.GONE);
        } else if (mItem == R.string.Nav_ContactUs || mItem == R.id.nav_ContactUs) {
            fragment = new ContactUsFragment();
            setTitle(R.string.Nav_ContactUs);
            hideCalenderView();
        } else if (mItem == R.string.Nav_AboutMeetingWall || mItem == R.id.nav_AboutMeetingWall) {
            fragment = new AboutMeetingWallFragment();
            setTitle(R.string.Nav_AboutMeetingWall);
            hideCalenderView();
        } else if (mItem == R.string.Nav_SignOut || mItem == R.id.nav_SignOut) {

            Intent i = new Intent(getApplication(), LoginActivity.class);
            startActivity(i);
            finish();
        } else if (mItem == R.string.Meeting_preview_fragment) {
            fragment = new MeetingPreviewFragment();
            setTitle(R.string.Meeting_preview_fragment);
            hideCalenderView();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        if(!isfiresttime){
            pushFragment(fragment);
            isfiresttime=true;
        }else{
            final Fragment finalFragment = fragment;
            Handler handler=new Handler();
            Runnable r=new Runnable()
            {
                public void run()
                {
                    if (finalFragment != null) {
                        pushFragment(finalFragment);
                    }
                }
            };
            handler.postDelayed(r, 350);
        }




    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        displayView(id);
        return true;

    }


    public void hideCalenderView() {
    //    calendarView.setVisibility(View.GONE);
    //    linearLayout.setVisibility(View.GONE);
        spinner_nav.setVisibility(View.GONE);
        Dropdownimage.setVisibility(View.GONE);
        stateprogressbar.setVisibility(View.GONE);
    }

    public void ShowCalenderView() {
        Animation RightSwipe = AnimationUtils.loadAnimation(MainActivity.this, R.anim.slideright);
     //   linearLayout.startAnimation(RightSwipe);
     //   calendarView.setVisibility(View.VISIBLE);
     //   linearLayout.setVisibility(View.VISIBLE);
        spinner_nav.setVisibility(View.VISIBLE);
        Dropdownimage.setVisibility(View.VISIBLE);
        stateprogressbar.setVisibility(View.GONE);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (data != null) {
                SeceltedDay = data.getIntExtra(Constants.SELECTED_DAY, 0);
                SelectedMonth = data.getIntExtra(Constants.SELECTED_MONTH, 0);
                SelectedYear = data.getIntExtra(Constants.SELECTED_YEAR, 0);
                AllMeetingsFragment fragment = (AllMeetingsFragment) getSupportFragmentManager().findFragmentById(R.id.frame_container);
                fragment.ResetHorizentalCalender(SelectedYear,SelectedMonth,SeceltedDay);
            }

        }else if(requestCode==2){
            if (data != null) {
                String dataee=data.getStringExtra(Constants.SELECTED_DAY);
               System.out.print("Seleced Date :::"+data.getStringExtra(Constants.SELECTED_DAY));
                NewMeetingFragment fragment = (NewMeetingFragment) getSupportFragmentManager().findFragmentById(R.id.frame_container);
                fragment.dateselection(dataee);
            }
        }else if(requestCode==3){
            if (data != null) {
                String SelectedTime=data.getStringExtra(Constants.SELECTED_DAY);
                System.out.print("Seleced Date :::"+data.getStringExtra(Constants.SELECTED_DAY));
                NewMeetingFragment fragment = (NewMeetingFragment) getSupportFragmentManager().findFragmentById(R.id.frame_container);
                fragment.TimeSelected(SelectedTime);
            }
        }else if(requestCode==4){
            if (data != null) {
                ArrayList<MeetingLocationsResponse.RowsBeen> location =(ArrayList<MeetingLocationsResponse.RowsBeen>)data.getSerializableExtra(Constants.SELECTED_DAY);
                System.out.print("Seleced Date :::"+data.getStringExtra(Constants.SELECTED_DAY));
                NewMeetingFragment fragment = (NewMeetingFragment) getSupportFragmentManager().findFragmentById(R.id.frame_container);
                fragment.LocationSelected( location);
            }
        }else if(requestCode==5){
            if (data != null) {
               ArrayList<MeetingEmployeesResponse.RowsBeen> Array = (ArrayList<MeetingEmployeesResponse.RowsBeen>) data.getSerializableExtra(Constants.SELECTED_DAY);
      //          ArrayList<ContactAddingBeen> Array=(ArrayList<ContactAddingBeen>)data.getStringArrayListExtra(Constants.SELECTED_DAY);

                NewMeetingFragment fragment = (NewMeetingFragment) getSupportFragmentManager().findFragmentById(R.id.frame_container);
                fragment.ContactsSelected(Array);
            }
        }

    }

    public void CalenderView(DateTime datetimes){
        Intent intent = new Intent(MainActivity.this, CalendarDisplayActivity.class);
        String currentDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(datetimes.getMillis());
        intent.putExtra("Date", currentDateFormat);
        startActivityForResult(intent, 1);
        overridePendingTransition(R.anim.push_up_in, R.anim.stable);
    }

    public void BasicActivity(){
        Intent intent = new Intent(MainActivity.this, CalendarActivityDecorated.class);
        startActivityForResult(intent, 2);
        overridePendingTransition(R.anim.push_up_in, R.anim.stable);
    }

    public void TimeSloat(String mselectedTime){
        Intent intent = new Intent(MainActivity.this, MeetingTimeSlotActivity.class);
        intent.putExtra("Previes_selected_TIme",mselectedTime);
        startActivityForResult(intent, 3);
        overridePendingTransition(R.anim.push_up_in, R.anim.stable);
    }
    public void LocationOnClick(){
        Intent intent = new Intent(MainActivity.this, MeetingLocationActivity.class);
        startActivityForResult(intent, 4);
        overridePendingTransition(R.anim.push_up_in, R.anim.stable);
    }public void EdditTextLocationOnClick(String s){
        Intent intent = new Intent(MainActivity.this, MeetingLocationActivity.class);
        intent.putExtra("Selected_Data",s);
        startActivityForResult(intent, 4);
        overridePendingTransition(R.anim.push_up_in, R.anim.stable);
    }public void InviteesAddingOnClick(){
        Intent intent = new Intent(MainActivity.this, ContactsListActivity.class);
        startActivityForResult(intent, 5);
        overridePendingTransition(R.anim.push_up_in, R.anim.stable);
    }public void SaveandContinuebtnOnClick(String saveUpdate_pkId){
        Intent intent = new Intent(MainActivity.this, AgendaCreationActivity.class);
        intent.putExtra("Meeting_PkId",saveUpdate_pkId);
        startActivityForResult(intent, 6);
        overridePendingTransition(R.anim.push_up_in, R.anim.stable);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    public void pushFragment(final Fragment frag) {
        if (frag instanceof MeetingPreviewFragment) {
            if(stack.size()!=0) {
                Fragment searchFrag = stack.get(0);
                stack.clear();
                stack.add(searchFrag);
            }
        } else if (frag instanceof AllMeetingsFragment) {
            if(stack.size()!=0) {
                Fragment searchFrag = stack.get(0);
                stack.clear();
                stack.add(searchFrag);
            }
            Iterator itr = stack.iterator();
            while (itr.hasNext()) {
                Fragment fragment = (Fragment) itr.next();
                if (fragment instanceof AllMeetingsFragment )
                    continue;
                itr.remove();
            }
        }else if (frag instanceof CreateMeetingFragment) {
            if(stack.size()!=0){
                Fragment searchFrag = stack.get(0);
                stack.clear();
                stack.add(searchFrag);
            }

            Iterator itr = stack.iterator();
            while (itr.hasNext()) {
                Fragment fragment = (Fragment) itr.next();
                if ( fragment instanceof CreateMeetingFragment)
                    continue;
                itr.remove();
            }
        }else if (frag instanceof ContactUsFragment) {
            Fragment searchFrag = stack.get(0);
            stack.clear();
            stack.add(searchFrag);
            Iterator itr = stack.iterator();
            while (itr.hasNext()) {
                Fragment fragment = (Fragment) itr.next();
                if (fragment instanceof ContactUsFragment )
                    continue;
                itr.remove();
            }
        }else if (frag instanceof AboutMeetingWallFragment) {
            Fragment searchFrag = stack.get(0);
            stack.clear();
            stack.add(searchFrag);
            Iterator itr = stack.iterator();
            while (itr.hasNext()) {
                Fragment fragment = (Fragment) itr.next();
                if (fragment instanceof AboutMeetingWallFragment)
                    continue;
                itr.remove();
            }
        }
//        }else if (frag instanceof CalendarActivityDecorated) {
//            spinner_nav.setVisibility(View.GONE);
//            Dropdownimage.setVisibility(View.GONE);
//            Fragment searchFrag = stack.get(0);
//            stack.clear();
//            stack.add(searchFrag);
//
//        }

        Utils.printMessage(TAG, "fragment::" + frag);
        if (stack != null) {
            stack.add(frag);
            if (stack.size() > 1) {
                Fragment removingFrag = stack.get(stack.size() - 2);
                Utils.printMessage(TAG, "Removing::" + removingFrag);
            //    stack.remove(stack.size() - 1);
                Fragment fragment = stack.get(stack.size() - 1);
                Utils.printMessage(TAG, "fragment::" + fragment);
                if (fragment instanceof MeetingPreviewFragment) {
                    Utils.printMessage(TAG, ":: SearchFragment ::");
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction ft = fragmentManager.beginTransaction();
                    ft.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.popenter, R.anim.popexit);
                    ft.add(R.id.frame_container, fragment);
                    ft.remove(removingFrag);
                    ft.commitAllowingStateLoss();
                    Animation RightSwipe = AnimationUtils.loadAnimation(this, R.anim.enter);
                    //     toolbar.startAnimation(RightSwipe);

                }
//                }else if (fragment instanceof CalendarActivityDecorated) {
//                    Utils.printMessage(TAG, ":: SearchFragment ::");
//                    FragmentManager fragmentManager = getSupportFragmentManager();
//                    FragmentTransaction ft = fragmentManager.beginTransaction();
//                    ft.setCustomAnimations(R.anim.enter, R.anim.exit,R.anim.popenter, R.anim.popexit);
//                    ft.add(R.id.frame_container, fragment);
//                    ft.remove(removingFrag);
//                    ft.commitAllowingStateLoss();
//                    Animation RightSwipe = AnimationUtils.loadAnimation(this, R.anim.enter);
//                    //     toolbar.startAnimation(RightSwipe);
//                }
            }else{
                if(frag instanceof AllMeetingsFragment){
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction ft = fragmentManager.beginTransaction();
                    ft.setCustomAnimations(R.anim.meetnigenter, R.anim.meetingexit,R.anim.meetingpopenter, R.anim.meetingpopexit);
                    ft.replace(R.id.frame_container, frag);
                    ft.commitAllowingStateLoss();
                }else{
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction ft = fragmentManager.beginTransaction();
                    ft.setCustomAnimations(R.anim.enter, R.anim.exit,R.anim.popenter, R.anim.popexit);
                    ft.replace(R.id.frame_container, frag);
                    ft.commitAllowingStateLoss();
                }

            }



        }
        Utils.printMessage(TAG, "after push stack size: " + stack.size());
    }


    public void popFrag() {
        if (stack == null) {
            super.onBackPressed();
            return;
        }
        Utils.printMessage(TAG, "stack size" + stack.size());
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (stack.size() > 1) {
            Fragment removingFrag = stack.get(stack.size() - 1);
            Utils.printMessage(TAG, "Removing::" + removingFrag.isRemoving());
            stack.remove(stack.size() - 1);
            Fragment fragment = stack.get(stack.size()-1 );
            Utils.printMessage(TAG, "fragment::" + fragment);
            if (fragment instanceof AllMeetingsFragment) {
                Utils.printMessage(TAG, ":: SearchFragment ::");
                FragmentTransaction ft = fragmentManager.beginTransaction();
                //    ft.setCustomAnimations(R.anim.popexit, R.anim.popenter,R.anim.exit, R.anim.enter);
                ft.setCustomAnimations(R.anim.stable, R.anim.slide_out_right);
                ft.add(R.id.frame_container, fragment);
                ft.remove( removingFrag);
                ft.commitAllowingStateLoss();
                Animation RightSwipe = AnimationUtils.loadAnimation(this, R.anim.slide_out_right);
             //   toolbar.startAnimation(RightSwipe);
            }else if (fragment instanceof NewMeetingFragment) {
                Utils.printMessage(TAG, ":: SearchFragment ::");
                FragmentTransaction ft = fragmentManager.beginTransaction();
                //    ft.setCustomAnimations(R.anim.popexit, R.anim.popenter,R.anim.exit, R.anim.enter);
                ft.setCustomAnimations(R.anim.stable, R.anim.slide_out_right);
                ft.add(R.id.frame_container, fragment);
                ft.remove( removingFrag);
                ft.commitAllowingStateLoss();
                Animation RightSwipe = AnimationUtils.loadAnimation(this, R.anim.slide_out_right);
                //   toolbar.startAnimation(RightSwipe);
            }else{
                super.onBackPressed();
            }


//         //   toolbar.startAnimation(RightSwipe);
      //      FramLayout.startAnimation(RightSwipe);
//            getSupportFragmentManager().executePendingTransactions();

        } else if (stack.size() == 1) {
            super.onBackPressed();
            return;
        }
    }

    public void displayErrorMessage(String message) {
        errorMessageText.setText(message);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) errorMessageText.getLayoutParams();
        if (errorMessageText.getLineCount() == 3) {
            params.height = getResources().getDimensionPixelSize(R.dimen.search_screen_layout_height);
            errorMessageText.setLayoutParams(params);
        } else {
            params.height = getResources().getDimensionPixelSize(R.dimen.signIn_page_reset_mail_view);
            errorMessageText.setLayoutParams(params);
        }
        if (errorView.getAlpha() == 0) {
            errorView.setTranslationY(-60);
            stateprogressbar.setVisibility(View.GONE);
            errorView.setVisibility(View.VISIBLE);
            errorView.setBackgroundResource(R.color.error_message_background);
            errorView.animate().translationY(0)
                    .alpha(1)
                    .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    errorView.animate()
                            .alpha(0)
                            .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
                    errorView.setVisibility(View.GONE);
                    stateprogressbar.setVisibility(View.VISIBLE);
                }
            }, 2000);

        }
    }

    public void displaySuccesMessage(String message) {
        errorMessageText.setText(message);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) errorMessageText.getLayoutParams();
        if (errorMessageText.getLineCount() == 3) {
            params.height = getResources().getDimensionPixelSize(R.dimen.search_screen_layout_height);
            errorMessageText.setLayoutParams(params);
        } else {
            params.height = getResources().getDimensionPixelSize(R.dimen.signIn_page_reset_mail_view);
            errorMessageText.setLayoutParams(params);
        }
        if (errorView.getAlpha() == 0) {
            errorView.setTranslationY(-60);
            stateprogressbar.setVisibility(View.GONE);
            errorView.setVisibility(View.VISIBLE);
            errorView.setBackgroundResource(R.color.success_message_background);
            errorView.animate().translationY(0)
                    .alpha(1)
                    .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    errorView.animate()
                            .alpha(0)
                            .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
                    errorView.setVisibility(View.GONE);
                    stateprogressbar.setVisibility(View.VISIBLE);
                }
            }, 2000);

        }
    }

}
