package com.nextgen.meetingwall.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.nextgen.meetingwall.R;
import com.nextgen.meetingwall.adapter.MeeetingTimeSlotAdapter;
import com.nextgen.meetingwall.adapter.NationalitylocationAdapter;
import com.nextgen.meetingwall.adapter.RoomlocationAdapter;
import com.nextgen.meetingwall.model.OthermeetingdetailsBeen;
import com.nextgen.meetingwall.model.RoomLocationBeen;
import com.nextgen.meetingwall.model.SearchLocationBeen;
import com.nextgen.meetingwall.model.TimeslotBeen;
import com.nextgen.meetingwall.stateprogressbar.StateProgressBar;
import com.nextgen.meetingwall.utils.Constants;
import com.nextgen.meetingwall.utils.SharedPreference;
import com.nextgen.meetingwall.utils.Utils;
import com.noman.weekcalendar.WeekCalendar;
import com.noman.weekcalendar.listener.OnDateClickListener;
import com.noman.weekcalendar.listener.OnWeekChangeListener;


import org.joda.time.DateTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;

@SuppressWarnings("ALL")
public class MeetingTimeSlotActivity extends AppCompatActivity  {
    public SharedPreference sharedPreference;
    private  Boolean isFirstTimeSelect=false;
    private  Boolean SearchLocationClick=false;
    private  Boolean firsttimeClick =false;

    private int FirstTimePostion;
    private int SecoundTimeSelectionPosition = 0;
    private int NationalityLostposition = 0;

    private String SelectedTime;
    private String myTime = "08:00";
    private String newTime;

    private static final String TAG = "DashBoardActivity";
    private float mActionBarHeight;
    private ActionBar mActionBar;
    private View currentSelectedView;
    private LayoutInflater inflater;
    private ListView NationalityListview,RoomListview;

    @BindView(R.id.meetings_timeslot_recycler_view)
    protected  ListView MeetingsRecyclerView=null;
    @BindView(R.id.infalte_search_list_layout)
    protected LinearLayout InflateSrachLocation;
    @BindView(R.id.Calender_linearlayout)
    protected LinearLayout HorizentalCalendarLayout;
    @BindView(R.id.Meeting_Location_Name_Textview)
    protected TextView MeetingLocationTextview;
    @BindView(R.id.Selected_month)
    protected TextView mSelectedMonth;
    @BindView(R.id.previews_selected_time_location)
    protected TextView mPreviewsSelectedTimeLocation;
    @BindView(R.id.previews_time_layout)
    protected LinearLayout mPreviewsTimeLinearlayout;
    @BindView(R.id.weekCalendar)
    protected WeekCalendar weekCalendar;

    protected String[] descriptionData = {"Setup", "Agenda", "Essentials", "Review"};
    @BindView(R.id.usage_stateprogressbar)
    protected StateProgressBar stateprogressbar;



    private MeeetingTimeSlotAdapter mAdapter;
    private DateTime datetimes;


    private ArrayList<SearchLocationBeen> SearchlocationArrayList = new ArrayList<>();
    private ArrayList<RoomLocationBeen> AlaskalocationsArray = new ArrayList<>();
    private ArrayList<RoomLocationBeen> HydlocationsArray = new ArrayList<>();
    private ArrayList<RoomLocationBeen> LondlocationsArray = new ArrayList<>();
    private ArrayList<RoomLocationBeen> SydlocationsArray = new ArrayList<>();
    private ArrayList<TimeslotBeen> meetingBeenArrayList = new ArrayList<>();
    private ArrayList<OthermeetingdetailsBeen> list = new ArrayList<>();
    private ArrayList<ArrayList<RoomLocationBeen>> LocationlistRoomArray = new ArrayList<>();
    private ArrayList<String> Datelist = new ArrayList<>();
    private ArrayList<String> meetingStatuslist = new ArrayList<>();
    private ArrayList<String> Daylist = new ArrayList<>();
    private ArrayList<String> DesignReviewlist = new ArrayList<>();
    private ArrayList<String> Timelist = new ArrayList<>();
    private ArrayList<String> SearchlocationsArray = new ArrayList<>();
    private ArrayList<String> MeetingNamesArray = new ArrayList<>();
    private ArrayList<String> MeetingTimeArray = new ArrayList<>();
    private ArrayList<String> MeetingLocationArray = new ArrayList<>();
    private ArrayList<String> meetingLocationlistTime = new ArrayList<>();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_time_slot);
        ButterKnife.bind(this);
        sharedPreference=new SharedPreference(this);
        stateprogressbar.setStateDescriptionData(descriptionData);
        weekCalendar.setBackground(getResources().getColor(R.color.black_color), 1);
        String monthname = (String) android.text.format.DateFormat.format("MMMM", new Date());
        String yearname = (String) android.text.format.DateFormat.format("yyyy", new Date());
        mSelectedMonth.setText("" + monthname + " " + yearname);
        datetimes=new DateTime();

        weekCalendar.setOnDateClickListener(new OnDateClickListener() {
            @Override
            public void onDateClick(DateTime dateTime) {
                try {
                    datetimes=dateTime;
                    SimpleDateFormat outFormat = new SimpleDateFormat("yyyy-MMMM-dd");
                    String dtOut = outFormat.format(dateTime.getMillis());
                    String monthsplit[] = dtOut.split("-");
                    mSelectedMonth.setText("" + monthsplit[1] + " " + monthsplit[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        });
        weekCalendar.setOnWeekChangeListener(new OnWeekChangeListener() {
            @Override
            public void onWeekChange(DateTime firstDayOfTheWeek, boolean forward) {
                SimpleDateFormat outFormat = new SimpleDateFormat("yyyy-MMMM-dd");
                String dtOut = outFormat.format(firstDayOfTheWeek.getMillis());
                String monthsplit[] = dtOut.split("-");
                mSelectedMonth.setText("" + monthsplit[1] + " " + monthsplit[0]);

            }
        });


        LinearLayout backLayout = (LinearLayout) findViewById(R.id.back_to_layout);
        TextView backText = (TextView) findViewById(R.id.back_text_header);

        Button doneButton = (Button) findViewById(R.id.select_button);
        backText.setText("" + getResources().getString(R.string.Nav_NewMeeting));

        if (android.os.Build.VERSION.SDK_INT >= 21) {
            getWindow().setStatusBarColor(getResources()
                    .getColor(R.color.colorPrimary));
        }
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(SelectedTime!=null){
                    Intent intent = new Intent(MeetingTimeSlotActivity.this, MainActivity.class);
                    intent.putExtra(Constants.SELECTED_DAY, SelectedTime);
                    setResult(RESULT_OK, intent);
                    finish();
                    overridePendingTransition(0, R.anim.push_down_in);
                }else{
                    displayErrorMessage(getString(R.string.label_err_dept_date_msg));
                }



            }
        });
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(0, R.anim.push_down_in);

            }
        });

        errorView = (RelativeLayout) findViewById(R.id.errorView);
        errorView.setVisibility(View.GONE);
        errorMessageText = (TextView) findViewById(R.id.errorMessageText);
        Intent intent = getIntent();
        String SelectedTimes =  sharedPreference.getTimeSlot();
        intent.getStringExtra("Previes_selected_TIme");
        if(!SelectedTimes.equalsIgnoreCase("")){
             mPreviewsSelectedTimeLocation.setText(SelectedTimes);
        }else{
            mPreviewsTimeLinearlayout.setVisibility(View.GONE);
        }

        prepareMeetingsData();
        dataSetListview();

    }

    @OnClick(R.id.Search_location_Layout)
    void Locationserch(){
        if(!firsttimeClick){
            firsttimeClick=true;
            inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View SearchLocationView = inflater.inflate(R.layout.multipule_listview, null);
            NationalityListview = ButterKnife.findById(SearchLocationView, R.id.Nationality_ListView);
            RoomListview = ButterKnife.findById(SearchLocationView, R.id.Room_listView);
            InflateSrachLocation.addView(SearchLocationView);
            InflateSrachLocation.setVisibility(View.VISIBLE);
            HorizentalCalendarLayout.setVisibility(View.GONE);
            SearchLocations(NationalityListview);
            UpdateRoomList(NationalityLostposition, RoomListview);
        }else{
            if(!SearchLocationClick){
                SearchLocationClick=true;
                InflateSrachLocation.setVisibility(View.VISIBLE);
                HorizentalCalendarLayout.setVisibility(View.GONE);
            }else{

                InflateSrachLocation.setVisibility(View.GONE);
                HorizentalCalendarLayout.setVisibility(View.VISIBLE);
                SearchLocationClick=false;
            }
        }


    }

    @OnTouch(R.id.meetings_timeslot_recycler_view)
    protected boolean OnTouch(){
        InflateSrachLocation.setVisibility(View.GONE);
        HorizentalCalendarLayout.setVisibility(View.VISIBLE);
        SearchLocationClick=false;
        return false;
    }
    public void     prepareMeetingsData() {
        meetingStatuslist.add("08:15 Am TO 08:30 Am");meetingStatuslist.add("08:15 Am TO 08:30 Am");meetingStatuslist.add("08:30 Am TO 08:45 Am");meetingStatuslist.add("like");meetingStatuslist.add("dislike");meetingStatuslist.add("like");

        meetingStatuslist.add("Feb,15");meetingStatuslist.add("Feb,14");meetingStatuslist.add("Feb,14");meetingStatuslist.add("Feb,13");meetingStatuslist.add("Feb,12");meetingStatuslist.add("Feb,11");

        meetingStatuslist.add("11:15 Am TO 11:45 Am");meetingStatuslist.add("11:15 Am TO 11:45 Am");

        DesignReviewlist.add("ABC STANDUP");DesignReviewlist.add("ABC ProjectReview");DesignReviewlist.add("ABC STANDUP");DesignReviewlist.add("Friday");

        DesignReviewlist.add("Design Review");DesignReviewlist.add("Design");DesignReviewlist.add("Review");DesignReviewlist.add("Android Review");DesignReviewlist.add("Meeting Review");DesignReviewlist.add("Design Review");

        DesignReviewlist.add("10:12-15:22");DesignReviewlist.add("11:12-15:30");DesignReviewlist.add("10:15-15:20");DesignReviewlist.add("ABC BRAINSTROMING");DesignReviewlist.add("11:12-15:22");DesignReviewlist.add("10:12-10:30");


        Timelist.add("02");Timelist.add("02");Timelist.add("02");Timelist.add("02");

        Timelist.add("02");Timelist.add("02");Timelist.add("02");Timelist.add("02 Review");Timelist.add("02");Timelist.add("02");

        Timelist.add("02");Timelist.add("02");Timelist.add("05");Timelist.add("05");Timelist.add("05");Timelist.add("10:12-10:30");

        Daylist.add("2");Daylist.add("2");Daylist.add("3");Daylist.add("2");

        Daylist.add("2");Daylist.add("2");Daylist.add("2");Daylist.add("2");Daylist.add("2");Daylist.add("2");

        Daylist.add("2");Daylist.add("2");Daylist.add("5");Daylist.add("5");Daylist.add("5");Daylist.add("1");

        MeetingNamesArray.add("XYZ Design Review");MeetingNamesArray.add("Design Review");MeetingNamesArray.add("Android Design");MeetingNamesArray.add("UI Meentig");

        MeetingNamesArray.add("Mobile App");MeetingNamesArray.add("UI Design Review");

        MeetingTimeArray.add("11:15 Am TO 11:45 Am");MeetingTimeArray.add("11:15 Am TO 11:45 Am");MeetingTimeArray.add("11:15 Am TO 11:45 Am");MeetingTimeArray.add("11:15 Am TO 11:45 Am");

        MeetingTimeArray.add("11:15 Am TO 11:45 Am");MeetingTimeArray.add("11:15 Am TO 11:45 Am");

        MeetingLocationArray.add("Royal Arena, Hyderabad");MeetingLocationArray.add("Mystic Revier, Hyderabad");MeetingLocationArray.add("Taj krishna, Hyderabad");MeetingLocationArray.add("Asok, Hyderabad");

        MeetingLocationArray.add("Swageth, Hyderabad");MeetingLocationArray.add("Royal Arena, Hyderabad");

        meetingLocationlistTime.add("08:30 TO 08:45");meetingLocationlistTime.add("08:30 TO 08:45");meetingLocationlistTime.add("08:30 TO 08:45");meetingLocationlistTime.add("08:30 TO 08:45");meetingLocationlistTime.add("08:30 TO 08:45");meetingLocationlistTime.add("08:30 TO 08:45");

        meetingLocationlistTime.add("08:30 TO 08:45");meetingLocationlistTime.add("08:30 TO 08:45");


        SearchlocationsArray.add("Alaska,Us");SearchlocationsArray.add("Hyderabad,India");SearchlocationsArray.add("London,Uk");SearchlocationsArray.add("Sydney,Australia");SearchlocationsArray.add("08:30 TO 08:45");SearchlocationsArray.add("08:30 TO 08:45");

        SearchlocationsArray.add("08:30 TO 08:45");SearchlocationsArray.add("08:30 TO 08:45");



        RoomLocationBeen roomLocationBeen = new RoomLocationBeen();
        roomLocationBeen.setRoomLocation("All Locations");
        roomLocationBeen.setRoomSelected(false);
        AlaskalocationsArray.add(roomLocationBeen);
         roomLocationBeen = new RoomLocationBeen();
        roomLocationBeen.setRoomLocation("Mystic Revier");
        roomLocationBeen.setRoomSelected(false);
        AlaskalocationsArray.add(roomLocationBeen);
         roomLocationBeen = new RoomLocationBeen();
        roomLocationBeen.setRoomLocation("Pearl Meadows");
        roomLocationBeen.setRoomSelected(false);
        AlaskalocationsArray.add(roomLocationBeen);
         roomLocationBeen = new RoomLocationBeen();
        roomLocationBeen.setRoomLocation("Royal Arena");
        roomLocationBeen.setRoomSelected(false);
        AlaskalocationsArray.add(roomLocationBeen);
        roomLocationBeen = new RoomLocationBeen();
        roomLocationBeen.setRoomLocation("Taj Krishna");
        roomLocationBeen.setRoomSelected(false);
        HydlocationsArray.add(roomLocationBeen);
        roomLocationBeen = new RoomLocationBeen();
        roomLocationBeen.setRoomLocation("Mystic");
        roomLocationBeen.setRoomSelected(false);
        HydlocationsArray.add(roomLocationBeen);
        roomLocationBeen = new RoomLocationBeen();
        roomLocationBeen.setRoomLocation("Revier Hotel");
        roomLocationBeen.setRoomSelected(false);
        HydlocationsArray.add(roomLocationBeen);
        roomLocationBeen = new RoomLocationBeen();
        roomLocationBeen.setRoomLocation("Royal");
        roomLocationBeen.setRoomSelected(false);
        HydlocationsArray.add(roomLocationBeen);
        roomLocationBeen = new RoomLocationBeen();
        roomLocationBeen.setRoomLocation("Krishna");
        roomLocationBeen.setRoomSelected(false);
        LondlocationsArray.add(roomLocationBeen);
        roomLocationBeen = new RoomLocationBeen();
        roomLocationBeen.setRoomLocation("Revier");
        roomLocationBeen.setRoomSelected(false);
        LondlocationsArray.add(roomLocationBeen);
        roomLocationBeen = new RoomLocationBeen();
        roomLocationBeen.setRoomLocation("Pearl ");
        roomLocationBeen.setRoomSelected(false);
        LondlocationsArray.add(roomLocationBeen);
        roomLocationBeen = new RoomLocationBeen();
        roomLocationBeen.setRoomLocation("Meadows");
        roomLocationBeen.setRoomSelected(false);
        LondlocationsArray.add(roomLocationBeen);
        roomLocationBeen = new RoomLocationBeen();
        roomLocationBeen.setRoomLocation(" Krishna");
        roomLocationBeen.setRoomSelected(false);
        SydlocationsArray.add(roomLocationBeen);
        roomLocationBeen = new RoomLocationBeen();
        roomLocationBeen.setRoomLocation("Mystic Meadows");
        roomLocationBeen.setRoomSelected(false);
        SydlocationsArray.add(roomLocationBeen);
        roomLocationBeen = new RoomLocationBeen();
        roomLocationBeen.setRoomLocation("Meadows Revier Hotel");
        roomLocationBeen.setRoomSelected(false);
        SydlocationsArray.add(roomLocationBeen);
        roomLocationBeen = new RoomLocationBeen();
        roomLocationBeen.setRoomLocation("Royal Meadows");
        roomLocationBeen.setRoomSelected(false);
        SydlocationsArray.add(roomLocationBeen);

        LocationlistRoomArray.add(AlaskalocationsArray);
        LocationlistRoomArray.add(HydlocationsArray);
        LocationlistRoomArray.add(LondlocationsArray);
        LocationlistRoomArray.add(SydlocationsArray);
    }

    @Override
    public void onBackPressed() {

             super.onBackPressed();

    }

    public void dataSetListview(){
        for (int i = 0; i < 41; i++) {
            TimeslotBeen Been = new TimeslotBeen();

            SimpleDateFormat df = new SimpleDateFormat("hh:mm");
            SimpleDateFormat dff = new SimpleDateFormat("a");
            Date d = null;
            try {
                Calendar cal = Calendar.getInstance();
                if (i == 0) {
                    d = df.parse(myTime);
                    cal.setTime(d);
                    newTime = myTime;
                    Been.setAmString(dff.format(cal.getTime()));
                    Been.setAmorPm(true);
                    Been.setBackground(true);
                    Been.setSelectTime(false);
                } else {
                    d = df.parse(newTime);
                    cal.setTime(d);
                    cal.add(Calendar.MINUTE, 15);
                    newTime = df.format(cal.getTime());
                    Been.setAmString(dff.format(cal.getTime()));
                    Been.setSelectTime(false);
                    if (i == 16) {
                        Been.setAmorPm(true);
                    } else {
                        if (i < 10) {
                            if (i == 1 || i == 2) {
                                Been.setShowMeetingDetails(true);
                                Been.setMeetingTime(meetingStatuslist.get(i));
                                Been.setMeetingType(DesignReviewlist.get(i));
                                Been.setMeetingLocation(Timelist.get(i));
                                Been.setOtherMeetingNumber(Daylist.get(i));
                                list = new ArrayList<>();
                                for (int j = 0; j < 3; j++) {
                                    OthermeetingdetailsBeen othermeetingdetailsBeen = new OthermeetingdetailsBeen();
                                    othermeetingdetailsBeen.setMeetingName(MeetingNamesArray.get(j));
                                    othermeetingdetailsBeen.setMeetingTime(MeetingTimeArray.get(j));
                                    othermeetingdetailsBeen.setMeetingLocation(MeetingLocationArray.get(j));
                                    othermeetingdetailsBeen.setMeetingLocationTime(meetingLocationlistTime.get(j));
                                    list.add(othermeetingdetailsBeen);
                                    Been.setMeetings(list);
                                }


                            }
                            Been.setBackground(true);
                        } else {
                            if (i == 13) {
                                Been.setShowMeetingDetails(true);
                                Been.setMeetingTime(meetingStatuslist.get(i));
                                Been.setMeetingType(DesignReviewlist.get(i));
                                Been.setMeetingLocation(Timelist.get(i));
                                Been.setOtherMeetingNumber(Daylist.get(i));
                                list = new ArrayList<>();
                                for (int j = 0; j < 5; j++) {
                                    OthermeetingdetailsBeen othermeetingdetailsBeen = new OthermeetingdetailsBeen();
                                    othermeetingdetailsBeen.setMeetingName(MeetingNamesArray.get(j));
                                    othermeetingdetailsBeen.setMeetingTime(MeetingTimeArray.get(j));
                                    othermeetingdetailsBeen.setMeetingLocation(MeetingLocationArray.get(j));
                                    othermeetingdetailsBeen.setMeetingLocationTime(meetingLocationlistTime.get(j));
                                    list.add(othermeetingdetailsBeen);
                                    Been.setMeetings(list);
                                }

                            }
                            Been.setBackground(false);
                        }
                        Been.setAmorPm(false);
                    }

                }
            } catch (ParseException e) {
                e.printStackTrace();
            }


            Been.setTimeSting(newTime);
            meetingBeenArrayList.add(Been);
        }
        MeetingsRecyclerView.setNestedScrollingEnabled(true);
        mAdapter = new MeeetingTimeSlotAdapter(this, meetingBeenArrayList);
        MeetingsRecyclerView.setAdapter(mAdapter);
        MeetingsRecyclerView.setFocusable(false);
        mAdapter.notifyDataSetChanged();


        Utils.setListViewHeightBasedOnChildren(MeetingsRecyclerView);

        MeetingsRecyclerView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (!isFirstTimeSelect) {
                    isFirstTimeSelect = true;
                    FirstTimePostion = position;
                    for (int i = 0; i < 41; i++) {
                        meetingBeenArrayList.get(i).setSelectTime(false);
                    }
                    meetingBeenArrayList.get(position).setSelectTime(true);
                    SelectedTime = meetingBeenArrayList.get(position).getTimeSting() + " " + meetingBeenArrayList.get(position).getAmString() + " - " + meetingBeenArrayList.get(position + 1).getTimeSting() + " " + meetingBeenArrayList.get(position + 1).getAmString();
                } else {
                    SecoundTimeSelectionPosition = position;
                    isFirstTimeSelect = false;
                    for (int i = 0; i < position + 1; i++) {
                        if (FirstTimePostion <= i) {
                            if (FirstTimePostion <= position) {
                                meetingBeenArrayList.get(i).setSelectTime(true);
                            } else {
                                meetingBeenArrayList.get(position).setSelectTime(false);
                            }
                        }

                    }
                    SelectedTime = meetingBeenArrayList.get(FirstTimePostion).getTimeSting() + " " + meetingBeenArrayList.get(FirstTimePostion).getAmString() + " - " + meetingBeenArrayList.get(position).getTimeSting() + " " + meetingBeenArrayList.get(position).getAmString();
                }

                mAdapter.notifyDataSetChanged();

            }
        });
    }

public void SearchLocations(final ListView nationalityListview){

    for (int j = 0; j < 4; j++) {
        SearchLocationBeen searchLocationBeen = new SearchLocationBeen();
        searchLocationBeen.setCountryLocation(SearchlocationsArray.get(j));
        searchLocationBeen. setHotelListArray(LocationlistRoomArray.get(j));

        SearchlocationArrayList.add(searchLocationBeen);
    }

    final NationalitylocationAdapter mAdapter = new NationalitylocationAdapter(this,  SearchlocationArrayList);
    nationalityListview.setAdapter(mAdapter);
    nationalityListview.setFocusable(false);
    mAdapter.notifyDataSetChanged();

 //   Utils.setListViewHeightBasedOnChildren(NationalityListview);

    nationalityListview.setOnItemClickListener(new AdapterView.OnItemClickListener()
    {
        @Override
        public void onItemClick(AdapterView<?> adapter, View v, int position,
                                long arg3)
        {
            UpdateRoomList(position, RoomListview);
            if(position!=0){
                nationalityListview.getChildAt(0).setBackgroundResource(R.color.black_light_color);
            }else{
                nationalityListview.getChildAt(0).setBackgroundResource(R.drawable.listviewselecterbackgroud);
            }


        }
    });



}

    public void UpdateRoomList(final int nationalityLostposition, ListView roomListview){

        final RoomlocationAdapter RoomAdapter = new RoomlocationAdapter(this,  SearchlocationArrayList, nationalityLostposition);
        roomListview.setAdapter(RoomAdapter);
        roomListview.setFocusable(false);
  //      Utils.setListViewHeightBasedOnChildren(RoomListview);
        roomListview.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position,
                                    long arg3)
            {

                for (int i=0; i<LocationlistRoomArray.size(); i++) {
                    for(int j=0; j<LocationlistRoomArray.get(i).size(); j++){
                        LocationlistRoomArray.get(i).get(j).setRoomSelected(false);
                    }
                }
                LocationlistRoomArray.get(nationalityLostposition).get(position).setRoomSelected(true);
                SearchlocationArrayList.get(nationalityLostposition).setHotelListArray(LocationlistRoomArray.get(nationalityLostposition));
                RoomAdapter.notifyDataSetChanged();
                String locationName   =   LocationlistRoomArray.get(nationalityLostposition).get(position).getRoomLocation();
                String NationalLocation =  SearchlocationArrayList.get(nationalityLostposition).getCountryLocation();

                MeetingLocationTextview.setText(locationName+" in "+NationalLocation);
            }
        });
    }

    private RelativeLayout errorView = null;
    private TextView errorMessageText = null;
    @SuppressWarnings("ResourceType")
    private void displayErrorMessage(String message) {
        errorMessageText.setText(message);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) errorMessageText.getLayoutParams();
        if (errorMessageText.getLineCount() == 3) {
            params.height = getResources().getDimensionPixelSize(R.dimen.search_screen_layout_height);
            errorMessageText.setLayoutParams(params);
        } else {
            params.height = getResources().getDimensionPixelSize(R.dimen.signIn_page_reset_mail_view);
            errorMessageText.setLayoutParams(params);
        }
        if (errorView.getAlpha() == 0) {
            errorView.setTranslationY(-60);
            errorView.setVisibility(View.VISIBLE);
            errorView.setBackgroundResource(R.color.error_message_background);
            errorView.animate().translationY(0)
                    .alpha(1)
                    .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    errorView.animate()
                            .alpha(0)
                            .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
                    errorView.setVisibility(View.GONE);
                }
            }, 2000);
        }
    }
}

