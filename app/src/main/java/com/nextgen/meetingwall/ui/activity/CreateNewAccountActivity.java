package com.nextgen.meetingwall.ui.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.nextgen.meetingwall.R;
import com.nextgen.meetingwall.utils.FormValidator;
import com.nextgen.meetingwall.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by cas on 27-02-2017.
 */
@SuppressWarnings("ALL")
public class CreateNewAccountActivity extends AppCompatActivity {

    @BindView(R.id.email_id_layout)
    TextInputLayout inputLayoutEmail;
    @BindView(R.id.password_layout)
    TextInputLayout inputLayoutPassword;
    @BindView(R.id.confirm_password_layout)
    TextInputLayout inputLayoutConfirmPassword;
    @BindView(R.id.edit_text_first_name)
    protected EditText mFirstname;
    @BindView(R.id.edit_text_last_name)
    protected EditText mLastname;
    @BindView(R.id.edit_text_mobile_number)
    protected EditText mMobilenumber;
    @BindView(R.id.edit_text_email_id)
    protected EditText mEmail;
    @BindView(R.id.edit_text_password)
    protected EditText mpassword;
    @BindView(R.id.edit_text_confirm_password)
    protected EditText mconfirmpassword;
    @BindView(R.id.btn_signup)
    protected Button mSignUpbutton;

    private static final String TAG = "SignUpActivity";
    @OnClick(R.id.btn_signup)
    void SignUp(){
        Signup();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setContentView(R.layout.activity_create_new_account);
        getSupportActionBar().hide();
        ButterKnife.bind(this);
    }

    public void Signup() {
        Log.d(TAG, "Login");

        if (!validateLogin()) {
            //   onLoginFailed();
            return;
        }

        mSignUpbutton.setEnabled(false);
        final ProgressDialog progressDialog = new ProgressDialog(CreateNewAccountActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Authenticating...");
        progressDialog.show();

        String email = mEmail.getText().toString();
        String password = mpassword.getText().toString();

        // TODO: Implement your own authentication logic here.

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        // On complete call either onLoginSuccess or onLoginFailed
                        onLoginSuccess();
                        // onLoginFailed();
                        progressDialog.dismiss();
                    }
                }, 3000);
    }
    private boolean validateLogin() {
        boolean email = FormValidator.validateEmail(mEmail, R.string.label_err_msg_email, inputLayoutEmail);
        boolean password = FormValidator.passwordValidate(mpassword, R.string.label_err_msg_password, inputLayoutPassword);
        return email && password;
    }
    public void onLoginSuccess() {
        mSignUpbutton.setEnabled(true);
        Utils.showToast(getString(R.string.label_success), CreateNewAccountActivity.this);
        Intent intent = new Intent(CreateNewAccountActivity.this, DashBoardActivity.class);
        startActivity(intent);
        finish();
    }
}
