package com.nextgen.meetingwall.ui.fragments;

import android.app.DatePickerDialog;

import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.nextgen.meetingwall.R;
import com.nextgen.meetingwall.adapter.AddingContactListAdapter;
import com.nextgen.meetingwall.adapter.NewContactAddingViewAdapter;
import com.nextgen.meetingwall.listeners.OnCustomItemSelectListener;
import com.nextgen.meetingwall.model.ContactAddingBeen;
import com.nextgen.meetingwall.model.UserOnlineStatusBean;
import com.nextgen.meetingwall.ui.activity.MainActivity;
import com.nextgen.meetingwall.utils.Helper;
import com.nextgen.meetingwall.utils.Utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;


/**
 * Created by admin on 01/02/17.
 */
@SuppressWarnings("ALL")
public class CreateMeetingFragment extends Fragment implements OnCustomItemSelectListener {


    @BindView(R.id.invitees_adding_edit_text)
    protected EditText InviteesAddingEditTex = null;
    @BindView(R.id.invitees_adding_button)
    protected ImageView InviteesAddingButton = null;
    @BindView(R.id.invitees_recycler_view)
    protected ListView mMyRefListView = null;
    @BindView(R.id.ScrollView)
    protected ScrollView MeetingScreenScrollview=null;
    @BindView(R.id.Spinner)
    protected Spinner MeetingTypeSpinner=null;
    @BindView(R.id.Startdate_textview)
    protected TextView mStartDatetext = null;
    @BindView(R.id.enddate_textview)
    protected TextView mEndDatetext = null;
    @BindView(R.id.Starttime_textview)
    protected TextView mStartTimetext = null;
    @BindView(R.id.endtime_textview)
    protected TextView mEndTimetext = null;
    @BindView(R.id.time_duration_textview)
    protected TextView mTimeDurationtext = null;
    @BindView(R.id.meeting_endDate_layout)
    protected LinearLayout mMeetingEndDateLayout = null;
    @BindView(R.id.daylongevent_checkBox)
    protected CheckBox mDaylongEventCBox = null;
    @BindView(R.id.Meeting_outcome_edittext)
    protected EditText mMeetingOutcomeEditText = null;
    @BindView(R.id.meeting_purpose_edittext)
    protected EditText mMeetingPurposeEditText = null;
    @BindView(R.id.purpose_option_text)
    protected TextView purposeOptionText;
    @BindView(R.id.outcome_option_text)
    protected TextView OutComeOptionText;
    @BindView(R.id.add_another_instruction_text)
     protected TextView AddAnotherInstructionText;
    @BindView(R.id.instruction_three_edittext)
    protected TextInputLayout InstructionEdittextThree;
    @BindView(R.id.instruction_two_edittext)
    protected TextInputLayout instructionEdittextTwo;

    @OnTouch(R.id.ScrollView)
    protected boolean OnTouch(){
        Helper.hideKeyboard(getActivity());
        return false;
    }

    SimpleDateFormat dateFormatter;
    private DialogFragment dateFragment;
    private int year;
    private int month;
    private int day;
    private Boolean StartDate=false;
    private Boolean EndDate=false;
    private Boolean StartTime=false;
    private Boolean EndTime=false;
    private String  Spinneritem;
    private Boolean AddInstruction=false;

    @OnClick(R.id.Startdate_textview)
    protected void StartDate() {
        StartDate=true;
        EndDate =false;
        datePick();
    } @OnClick(R.id.enddate_textview)
    protected void EndDate() {
        StartDate=false;
        EndDate=true;
        datePick();
    } @OnClick(R.id.Starttime_textview)
    protected void StartTime() {
        StartTime=true;
        EndTime=false;
        TimePicker();
    } @OnClick(R.id.endtime_textview)
    protected void EndTime() {
        StartTime=false;
        EndTime=true;
        TimePicker();

    }
    @OnClick(R.id.invitees_adding_button)
    void addInvitees() {
        // TODO call server...

        prepareMeetingsData();
        InviteesAddingEditTex.getText().clear();
    }
    int i = 0;
    @OnClick (R.id.add_another_instruction_text)
    protected void Addinstruction(){
            if(!AddInstruction){
                i++;
                if(i==1){
                    instructionEdittextTwo.setVisibility(View.VISIBLE);
                }else {
                    InstructionEdittextThree.setVisibility(View.VISIBLE);
                    AddAnotherInstructionText.setVisibility(View.GONE);
                }
            }
    }

    private Context ctx;
    private NewContactAddingViewAdapter mAdapter;
    ArrayList<ContactAddingBeen> meetingBeenArrayList = new ArrayList<>();
    private AddingContactListAdapter forumAnswerListAdapter = null;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_meeting, container, false);
        ButterKnife.bind(this, view);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Create meeting");
        ActionBar bar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(getActivity().getResources()
                .getColor(R.color.colorPrimary)));

        if (android.os.Build.VERSION.SDK_INT >= 21){
            getActivity().getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getActivity().getWindow().setStatusBarColor(getActivity().getResources()
                    .getColor(R.color.colorPrimary) );
        }
        dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        ((MainActivity)getActivity()).hideCalenderView();


        AddAnotherInstructionText.setPaintFlags(AddAnotherInstructionText.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        //set on text change listener for edittext
        mMeetingPurposeEditText.addTextChangedListener(textWatcher());
        mMeetingOutcomeEditText.addTextChangedListener(textWatcher());

        // Spinner click listener
        MeetingTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Spinneritem = parent.getItemAtPosition(position).toString();

                if(Spinneritem.equalsIgnoreCase("Multi Day Meeting")){
                    mMeetingEndDateLayout.setVisibility(View.VISIBLE);
                    mDaylongEventCBox.setChecked(true);
                }else if(Spinneritem.equalsIgnoreCase("Recurring Meeting")){
                    mMeetingEndDateLayout.setVisibility(View.VISIBLE);
                    mDaylongEventCBox.setChecked(true);
                }else{
                    mMeetingEndDateLayout.setVisibility(View.GONE);
                    mDaylongEventCBox.setChecked(false);
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();

        categories.add("Single Day Meeting");
        categories.add("Multi Day Meeting");
        categories.add("Recurring Meeting");


        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        MeetingTypeSpinner.setAdapter(dataAdapter);

        return view;
    }


    private TextWatcher textWatcher() {
        return new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!mMeetingPurposeEditText.getText().toString().equals("")) { //if edittext include text
                    purposeOptionText.setVisibility(View.GONE);
                } else { //not include text
                    purposeOptionText.setVisibility(View.VISIBLE);
                } if (!mMeetingOutcomeEditText.getText().toString().equals("")) {
                    OutComeOptionText.setVisibility(View.GONE);
                } else { //not include text
                    OutComeOptionText.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
    }


    public void prepareMeetingsData() {

        ContactAddingBeen been = new ContactAddingBeen();
        ArrayList<UserOnlineStatusBean> list = new ArrayList<>();
        UserOnlineStatusBean userOnlineStatusBean = new UserOnlineStatusBean();

        been = new ContactAddingBeen();
        been.setDesignReview("Willium String");
        been.setReviewDescription("Manager");
        been.setCountry("Kenya");
        been.setTimeDuration("10:15-11:45");
        been.setHeaderTime("Thu,Feb02");
        list = new ArrayList<>();
        userOnlineStatusBean = new UserOnlineStatusBean();
        userOnlineStatusBean.setImage("1");
        userOnlineStatusBean.setOnlineStatus(true);
        list.add(userOnlineStatusBean);
        userOnlineStatusBean = new UserOnlineStatusBean();
        userOnlineStatusBean.setImage("4");
        userOnlineStatusBean.setOnlineStatus(true);
        list.add(userOnlineStatusBean);
        userOnlineStatusBean = new UserOnlineStatusBean();
        userOnlineStatusBean.setImage("12");
        userOnlineStatusBean.setOnlineStatus(false);
        list.add(userOnlineStatusBean);
        userOnlineStatusBean = new UserOnlineStatusBean();
        userOnlineStatusBean.setImage("5");
        userOnlineStatusBean.setOnlineStatus(false);
        list.add(userOnlineStatusBean);
        userOnlineStatusBean = new UserOnlineStatusBean();
        userOnlineStatusBean.setImage("10");
        userOnlineStatusBean.setOnlineStatus(false);
        list.add(userOnlineStatusBean);
        userOnlineStatusBean = new UserOnlineStatusBean();
        userOnlineStatusBean.setImage("6");
        userOnlineStatusBean.setOnlineStatus(false);
        list.add(userOnlineStatusBean);

        been.setImages(list);
        if (InviteesAddingEditTex.getText().toString().equalsIgnoreCase("Willium String")) {
            meetingBeenArrayList.add(been);
        }

        been = new ContactAddingBeen();
        been.setDesignReview("Design");
        been.setReviewDescription("Manager");
        been.setCountry("India");
        been.setTimeDuration("10:45-11:45");
        been.setHeaderTime("Thu,Feb02");
        list = new ArrayList<>();
        userOnlineStatusBean.setImage("6");
        userOnlineStatusBean.setOnlineStatus(true);
        list.add(userOnlineStatusBean);
        userOnlineStatusBean = new UserOnlineStatusBean();
        userOnlineStatusBean.setImage("5");
        userOnlineStatusBean.setOnlineStatus(true);
        list.add(userOnlineStatusBean);

        been.setImages(list);
        if (InviteesAddingEditTex.getText().toString().equalsIgnoreCase("Design")) {
            meetingBeenArrayList.add(been);
        }

        been = new ContactAddingBeen();
        been.setDesignReview("Review");
        been.setReviewDescription("Android");
        been.setCountry("India");
        been.setTimeDuration("12:15-13:45");
        been.setHeaderTime("Thu,Feb02");
        list = new ArrayList<>();
        userOnlineStatusBean.setImage("10");
        userOnlineStatusBean.setOnlineStatus(true);
        list.add(userOnlineStatusBean);
        userOnlineStatusBean = new UserOnlineStatusBean();
        userOnlineStatusBean.setImage("14");
        userOnlineStatusBean.setOnlineStatus(true);
        list.add(userOnlineStatusBean);
        userOnlineStatusBean = new UserOnlineStatusBean();
        userOnlineStatusBean.setImage("12");
        userOnlineStatusBean.setOnlineStatus(false);
        list.add(userOnlineStatusBean);
        userOnlineStatusBean = new UserOnlineStatusBean();
        userOnlineStatusBean.setImage("15");
        userOnlineStatusBean.setOnlineStatus(false);
        list.add(userOnlineStatusBean);
        userOnlineStatusBean = new UserOnlineStatusBean();
        userOnlineStatusBean.setImage("13");
        userOnlineStatusBean.setOnlineStatus(false);
        list.add(userOnlineStatusBean);
        userOnlineStatusBean = new UserOnlineStatusBean();
        userOnlineStatusBean.setImage("10");
        userOnlineStatusBean.setOnlineStatus(false);
        list.add(userOnlineStatusBean);

        been.setImages(list);
        if (InviteesAddingEditTex.getText().toString().equalsIgnoreCase("Review")) {
            meetingBeenArrayList.add(been);
        }
        //  meetingBeenArrayList.add(been);
        forumAnswerListAdapter = new AddingContactListAdapter(getActivity(), meetingBeenArrayList, this);
        mMyRefListView.setAdapter(forumAnswerListAdapter);
        mMyRefListView.setFocusable(false);
        Utils.setListViewHeightBasedOnChildren(mMyRefListView);
        forumAnswerListAdapter.notifyDataSetChanged();


    }




    @Override
    public void onItemClick(int position) {

        meetingBeenArrayList.remove(position);
        forumAnswerListAdapter.notifyDataSetChanged();
        Utils.setListViewHeightBasedOnChildren(mMyRefListView);

    }

    @Override
    public void onSelectedItemClick(int position, int type) {

    }

    void datePick() {
        if(StartDate) {
            if (mStartDatetext.getText().toString().equalsIgnoreCase("")) {
                Calendar calendar = Calendar.getInstance();
                year = calendar.get(Calendar.YEAR);
                month = (calendar.get(Calendar.MONTH));
                day = calendar.get(Calendar.DATE);
            } else {
                String dateFormat;
                if (mStartDatetext.getText().toString().contains("/")) {
                    String[] datetext = mStartDatetext.getText().toString().split("/");
                    year = Integer.parseInt(datetext[0]);
                    month = Integer.parseInt(datetext[1]) - 1;
                    day = Integer.parseInt(datetext[2]);
                } else {
                    dateFormat = Utils.dateFormatToDate(mStartDatetext.getText().toString());
                    String[] datetext = dateFormat.split("/");
                    year = Integer.parseInt(datetext[0]);
                    month = Integer.parseInt(datetext[1]);
                    day = Integer.parseInt(datetext[2]);
                }
            }
            DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), R.style.My_Dialog,
                    new DatePickerDialog.OnDateSetListener() {


                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            Calendar newDate = Calendar.getInstance();

                            newDate.set(year, monthOfYear, dayOfMonth);


                                mStartDatetext.setText(dateFormatter.format(newDate.getTime()));



                        }
                    }, day, month, year);
            Calendar newDate = Calendar.getInstance();
            datePickerDialog.getDatePicker().setMinDate(newDate.getTimeInMillis());
            datePickerDialog.setTitle("");
            datePickerDialog.show();

        } else if(EndDate) {
            if (mEndDatetext.getText().toString().equalsIgnoreCase("")) {
                if(mStartDatetext.getText().toString().contains("/")){
                    String[] datetext = mStartDatetext.getText().toString().split("/");
                    year = Integer.parseInt(datetext[0]);
                    month = Integer.parseInt(datetext[1]) - 1;
                    day = Integer.parseInt(datetext[2]);
                }else{
                    Calendar calendar = Calendar.getInstance();
                    year = calendar.get(Calendar.YEAR);
                    month = (calendar.get(Calendar.MONTH));
                    day = calendar.get(Calendar.DATE);
                }

            } else if (mEndDatetext.getText().toString().contains("/")) {
                String[] datetext = mEndDatetext.getText().toString().split("/");
                year = Integer.parseInt(datetext[0]);
                month = Integer.parseInt(datetext[1]) - 1;
                day = Integer.parseInt(datetext[2]);
            } else {
                String dateFormat;
                dateFormat = Utils.dateFormatToDate(mEndDatetext.getText().toString());
                String[] datetext = dateFormat.split("/");
                year = Integer.parseInt(datetext[0]);
                month = Integer.parseInt(datetext[1]);
                day = Integer.parseInt(datetext[2]);
            }
            DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), R.style.My_Dialog,
                    new DatePickerDialog.OnDateSetListener() {


                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            Calendar newDate = Calendar.getInstance();

                            newDate.set(year, monthOfYear, dayOfMonth);


                                mEndDatetext.setText(dateFormatter.format(newDate.getTime()));


                        }
                    },  day, month, year);
            Calendar newDate = Calendar.getInstance();
            if (mStartDatetext.getText().toString().contains("/")) {
                String[] datetext = mStartDatetext.getText().toString().split("/");
                int year = Integer.parseInt(datetext[0]);
                int month = Integer.parseInt(datetext[1]) - 1;
                int day = Integer.parseInt(datetext[2]);
                newDate.set(year, month, day);
            }
            datePickerDialog.getDatePicker().setMinDate(newDate.getTimeInMillis());
            datePickerDialog.setTitle("");
            datePickerDialog.show();


        }


    }
    void TimePicker(){
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                if(StartTime){
                    mStartTimetext.setText( selectedHour + ":" + selectedMinute);
                    if(!mEndTimetext.getText().toString().equals("")){
                        TImeDiffference();
                    }
                }else{
                    mEndTimetext.setText( selectedHour + ":" + selectedMinute);
                    if(!mStartTimetext.getText().toString().equals("")){
                        TImeDiffference();
                    }
                }

            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle("");
        mTimePicker.show();
    }


    void TImeDiffference(){
        SimpleDateFormat format = null;
        String dateStart,dateStop;
        if(Spinneritem.equalsIgnoreCase("Single Day Meeting")){
            //HH converts hour in 24 hours format (0-23), day calculation
             format = new SimpleDateFormat("HH:mm");
             dateStart = mStartTimetext.getText().toString();
             dateStop = mEndTimetext.getText().toString();
        }else{
            format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
             dateStart = mStartDatetext.getText().toString()+" "+mStartTimetext.getText().toString();
             dateStop = mEndDatetext.getText().toString()+" "+mEndTimetext.getText().toString();
        }


        Date d1 = null;
        Date d2 = null;

        try {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateStop);

            //in milliseconds
            long diff = d2.getTime() - d1.getTime();

            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);

            System.out.print(diffDays + " days, ");
            System.out.print(diffHours + " hours, ");
            System.out.print(diffMinutes + " minutes, ");
            System.out.print(diffSeconds + " seconds.");
            if(Spinneritem.equalsIgnoreCase("Single Day Meeting")){
                mTimeDurationtext.setText(getResources().getString(R.string.duration_textview_string)+" "+diffHours + " hours "+diffMinutes + " minutes ");
            }else{
                mTimeDurationtext.setText(getResources().getString(R.string.duration_textview_string)+" "+diffDays +" days "+diffHours + " hours "+diffMinutes + " minutes ");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
