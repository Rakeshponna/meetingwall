package com.nextgen.meetingwall.ui.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nextgen.meetingwall.R;
import com.nextgen.meetingwall.adapter.AgendaPresenterListAdapter;
import com.nextgen.meetingwall.listeners.OnCustomItemSelectListener;
import com.nextgen.meetingwall.model.ModuleListMetadataArray;
import com.nextgen.meetingwall.network.Network;
import com.nextgen.meetingwall.network.RequestCallback;
import com.nextgen.meetingwall.network.json.AgendaMeetingEmployeeWraper;
import com.nextgen.meetingwall.network.json.CreateAgendaSaveWraper;
import com.nextgen.meetingwall.network.json.CreateParticipantsResponce;
import com.nextgen.meetingwall.network.json.MeetingEmployeesResponse;
import com.nextgen.meetingwall.network.json.SaveUpdateModuleAgendasDataWraper;
import com.nextgen.meetingwall.network.json.SaveUpdateModuleDataResponce;
import com.nextgen.meetingwall.stateprogressbar.StateProgressBar;
import com.nextgen.meetingwall.ui.dialog.DialogFactory;
import com.nextgen.meetingwall.utils.SharedPreference;
import com.nextgen.meetingwall.utils.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cc.cloudist.acplibrary.ACProgressFlower;


@SuppressWarnings("deprecation")
public class AgendaCreationActivity extends AppCompatActivity implements OnCustomItemSelectListener{

    protected String[] descriptionData = {"Setup", "Agenda", "Essentials", "Review"};

    @BindView(R.id.Title_of_Agenda_item)
    protected EditText TitleOfAgendaItemEditText;
    @BindView(R.id.Agenda_Duration_in_mints)
    protected EditText AgendaDurationInMintsEditText;
    @BindView(R.id.Agenda_Presenter_Name)
    protected EditText AgendaPresenterNameEditText;
    @BindView(R.id.usage_stateprogressbar)
    protected StateProgressBar stateprogressbar;
    @BindView(R.id.Agenda_Contacts_listView)
    protected ListView AgendaContactsListView;
    @BindView(R.id.errorView)
    protected RelativeLayout errorView;
    @BindView(R.id.errorMessageText)
    protected TextView errorMessageText;

    @OnClick(R.id.Agenda_Presenter_Name)
    void PresenterOnClick(){
        AgendaContactsListView.setVisibility(View.VISIBLE);
    }

    private ACProgressFlower dialog;
    private final Network network = new Network(this);
    private static final String TAG = "Agenda Creation Page";

    private String StartTime;
    private String EndTime;
    private AgendaPresenterListAdapter agendaPresenterListAdapter;

    private ArrayList<ModuleListMetadataArray> mMeetingListMetaDataArray = new ArrayList<>();
    private ArrayList<MeetingEmployeesResponse.RowsBeen> AgendaPresenterArrayList = new ArrayList<>();
    private ArrayList<MeetingEmployeesResponse.RowsBeen> SelectedAgendaPresenterArrayList = new ArrayList<>();
    private ArrayList<Object> mSaveUpdateModuleDataArrayList = new ArrayList<>();

    String MeetingPk_Id;
    String Meeting_Name;
    String Meeting_StartTime;
    String Meeting_EndTime;
    private SharedPreference sharedPreference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agenda_creation);
        ButterKnife.bind(this);
        stateprogressbar.setStateDescriptionData(descriptionData);
        stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.TWO);
        errorView.setVisibility(View.GONE);

        sharedPreference = new SharedPreference(this);

        MeetingPk_Id =   sharedPreference.getMeetingSaveData("MeetingPk_Id");
        Meeting_Name =   sharedPreference.getMeetingSaveData("Meeting_Name");
        Meeting_StartTime =   sharedPreference.getMeetingSaveData("Meeting_StartTime");
        Meeting_EndTime =   sharedPreference.getMeetingSaveData("Meeting_EndTime");
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }

        LinearLayout backLayout = (LinearLayout) findViewById(R.id.back_to_layout);
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(0, R.anim.push_down_in);

            }
        });

        Button doneButton = (Button) findViewById(R.id.Agenda_Save_button);
        doneButton.setText("" + getResources().getString(R.string.Agenda_save_string) + "");
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(TitleOfAgendaItemEditText.getText().toString().isEmpty()){
                    displayErrorMessage(getString(R.string.label_agenda_name));
                }else if(AgendaDurationInMintsEditText.getText().toString().isEmpty()){
                    displayErrorMessage(getString(R.string.label_agenda_time));
                }else if(AgendaPresenterNameEditText.getText().toString().isEmpty()){
                    displayErrorMessage(getString(R.string.label_agenda_presenter));
                }else{
                    Calendar cal = Calendar.getInstance();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss", Locale.ENGLISH);

                    StartTime = Meeting_StartTime;
              //      StartTime =StartTime+".000Z";
                    try {
                        cal.setTime( sdf.parse(Meeting_EndTime));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    cal.add(Calendar.MINUTE, Integer.parseInt(AgendaDurationInMintsEditText.getText().toString()));
                    EndTime = sdf.format(cal.getTimeInMillis());
                    EndTime = EndTime+".000Z";
                    if(Utils.isConnectingToInternet(AgendaCreationActivity.this)){
                        CreateMeeting();
                    }else{
                        displayErrorMessage(getString(R.string.label_connection));
                    }

                }

            }
        });




        prepareMeetingsEmployees();

        agendaPresenterListAdapter = new AgendaPresenterListAdapter(this, AgendaPresenterArrayList,  this);
        AgendaContactsListView.setAdapter(agendaPresenterListAdapter);
        AgendaContactsListView.setFocusable(false);
           Utils.setListViewHeightBasedOnChildren(AgendaContactsListView);
        agendaPresenterListAdapter.notifyDataSetChanged();

    }

    private void prepareMeetingsEmployees() {

        network.MeetingEmployees(new RequestCallback<ArrayList<ModuleListMetadataArray>>() {
            @Override
            public void onSuccess(ArrayList<ModuleListMetadataArray> response) {

                Log.e(TAG, "Success response::" + response);
                onSuccesEmployees(response);

            }

            @Override
            public void onFailed(String message) {

                Log.e(TAG, "Failed response::" + message);
            }
        });


    }
    private void onSuccesEmployees(final ArrayList<ModuleListMetadataArray> jobsList) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                mMeetingListMetaDataArray.clear();
                if (jobsList.size() == 0) {
                    dialog(getResources().getString(R.string.label_server_data_notfound));
                } else {
                    for (int i = 0; i < jobsList.size(); i++) {

                        mMeetingListMetaDataArray.add(jobsList.get(i));
                    }

                    ListDataMeetingEmployees();
                }
            }
        });
    }

    private void ListDataMeetingEmployees(){
        AgendaMeetingEmployeeWraper agendaMeetingEmployeeWraper = new AgendaMeetingEmployeeWraper();
        agendaMeetingEmployeeWraper.setFirstInt(0);
        agendaMeetingEmployeeWraper.setRowsInt(10);
        agendaMeetingEmployeeWraper.setSortOrderInt(1);
        AgendaMeetingEmployeeWraper.OneRelBean oneRelBean =new AgendaMeetingEmployeeWraper.OneRelBean();
        oneRelBean.setModule("Meetings");
        oneRelBean.setModule_id(85);
        oneRelBean.setId(Integer.parseInt(MeetingPk_Id));
        agendaMeetingEmployeeWraper.setOneRel(oneRelBean);
        //    wrapper.setFiltersObject(new FiltersObjectBeen());
        //    wrapper.setGlobalFilter(null);
        agendaMeetingEmployeeWraper.setDisplayColsArray(mMeetingListMetaDataArray);

        network.AgendaListDataMeetingEmployees(agendaMeetingEmployeeWraper,new RequestCallback<MeetingEmployeesResponse>() {
            @Override
            public void onSuccess(MeetingEmployeesResponse response) {

                Log.e(TAG, "Success response::" + response);
                onSuccesEmployeesResponce(response);
            }

            @Override
            public void onFailed(String message) {

                Log.e(TAG, "Failed response::" + message);
            }
        });


    }

    private void onSuccesEmployeesResponce(final MeetingEmployeesResponse response){
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                AgendaPresenterArrayList.clear();

                if (response.getJqGridData().getRowsArray().size() > 0) {
                    for (int i = 0; i < response.getJqGridData().getRowsArray().size(); i++) {
                        response.getJqGridData().getRowsArray().get(i).setSelected(false);
                        AgendaPresenterArrayList.add(response.getJqGridData().getRowsArray().get(i));
                    }


                    Collections.sort(AgendaPresenterArrayList, new Comparator<MeetingEmployeesResponse.RowsBeen>() {
                        public int compare(MeetingEmployeesResponse.RowsBeen v1, MeetingEmployeesResponse.RowsBeen v2) {
                            return v1.getEmployeeFirstName().compareTo(v2.getEmployeeFirstName());
                        }
                    });

                          Utils.setListViewHeightBasedOnChildren(AgendaContactsListView);
                    agendaPresenterListAdapter.notifyDataSetChanged();
                }
            }
        });

    }


    private void dialog(final String message) {
        DialogFactory.showAConfirmationDialog(this, message, 0,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
    }
    public void CreateMeeting() {
        dialog = new ACProgressFlower.Builder(this)
                .text(getResources().getString(R.string.label_loading))
                .build();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        SaveUpdateModuleAgendasDataWraper.FieldsdataArray fieldsdataArray = new SaveUpdateModuleAgendasDataWraper.FieldsdataArray();
        fieldsdataArray.setName("Name");
        fieldsdataArray.setDb_field_name("field_name");
        fieldsdataArray.setType("Text");
        fieldsdataArray.setBlock_pk_id(47);
        fieldsdataArray.setValue(TitleOfAgendaItemEditText.getText().toString());
        mSaveUpdateModuleDataArrayList.add(fieldsdataArray);

        SaveUpdateModuleAgendasDataWraper.FieldsdataArray fieldsdataArray1 = new SaveUpdateModuleAgendasDataWraper.FieldsdataArray();
        fieldsdataArray1.setName("Start Time");
        fieldsdataArray1.setDb_field_name("field_start_time");
        fieldsdataArray1.setType("Time Stamp");
        fieldsdataArray1.setBlock_pk_id(47);
        fieldsdataArray1.setValue(StartTime);
        mSaveUpdateModuleDataArrayList.add(fieldsdataArray1);

        SaveUpdateModuleAgendasDataWraper.FieldsdataArray fieldsdataArray2 = new SaveUpdateModuleAgendasDataWraper.FieldsdataArray();
        fieldsdataArray2.setName("End Time");
        fieldsdataArray2.setDb_field_name("field_end_time");
        fieldsdataArray2.setType("Time Stamp");
        fieldsdataArray2.setBlock_pk_id(47);
        fieldsdataArray2.setValue(EndTime);
        mSaveUpdateModuleDataArrayList.add(fieldsdataArray2);

        SaveUpdateModuleAgendasDataWraper.Location Locationtype = new SaveUpdateModuleAgendasDataWraper.Location();
        Locationtype.setName("Employee");
        Locationtype.setDb_field_name("field_employee");
        Locationtype.setType("Relation");
        Locationtype.setBlock_pk_id(47);
        Locationtype.setRel(1);
        SaveUpdateModuleAgendasDataWraper.Location.Employees employees =new SaveUpdateModuleAgendasDataWraper.Location.Employees ();
        employees.setEmployeePk_idInt(SelectedAgendaPresenterArrayList.get(0).getEmployeePkId());
        employees.setEmployeeFirstNameString(SelectedAgendaPresenterArrayList.get(0).getEmployeeFirstName());
        employees.setEmployeeEmailString(SelectedAgendaPresenterArrayList.get(0).getEmployeeEmail());
        employees.setEmployeeModuleNumberString(SelectedAgendaPresenterArrayList.get(0).getEmployeeNumber());
        employees.setEmployeeRoleString(SelectedAgendaPresenterArrayList.get(0).getEmployeeRole());
        employees.setEmployeeRolePk_idInt(SelectedAgendaPresenterArrayList.get(0).getEmployeeRolepkid());
        employees.setEmployeeReportsToString(SelectedAgendaPresenterArrayList.get(0).getEmployeeReportsTo());
        employees.setEmployeeReportsToPk_idInt(SelectedAgendaPresenterArrayList.get(0).getEmployeeReportsTopk_id());
        Locationtype.setEmployeesbean(employees);
        mSaveUpdateModuleDataArrayList.add(Locationtype);

        SaveUpdateModuleAgendasDataWraper saveUpdateModuleDataWraper = new SaveUpdateModuleAgendasDataWraper();
        saveUpdateModuleDataWraper.setModule("Agendas");
        saveUpdateModuleDataWraper.setAction("Create");
        saveUpdateModuleDataWraper.setDisplayColsArray(mSaveUpdateModuleDataArrayList);

        network.SaveUpdateAgendaModuleData(saveUpdateModuleDataWraper, new RequestCallback<SaveUpdateModuleDataResponce>() {
            @Override
            public void onSuccess(SaveUpdateModuleDataResponce response) {
                Log.e(TAG, "Success response::" + response);
                CreateParticipants(response.getDataObject().getSaveUpdate_pkId());
            }

            @Override
            public void onFailed(String message) {
                dialog.cancel();
                Log.e(TAG, "Failed response::" + message);
            }
        });

    }

    public void CreateParticipants(String saveUpdate_pkId) {

        CreateAgendaSaveWraper createAgendaSaveWraper = new CreateAgendaSaveWraper();
        createAgendaSaveWraper.setFromModuleString("Meetings");
        createAgendaSaveWraper.setFromDbTableString("module_meetings");
        createAgendaSaveWraper.setFromRelationInt(1);
        createAgendaSaveWraper.setFromFieldIdInt(559);
        createAgendaSaveWraper.setFromBlockIdInt(43);
        createAgendaSaveWraper.setFromBlockString("Basic Information");
        createAgendaSaveWraper.setFromFieldString("Agendas");
        createAgendaSaveWraper.setFromDbFieldString("pk_id");
        createAgendaSaveWraper.setToModuleString("Agendas");
        createAgendaSaveWraper.setToTableString("module_agendas");
        createAgendaSaveWraper.setToRelationInt(0);
        createAgendaSaveWraper.setToFieldIdInt(558);
        createAgendaSaveWraper.setToBlockIdInt(47);
        createAgendaSaveWraper.setToBlockString("Basic Information");
        createAgendaSaveWraper.setToFieldString("Meeting");
        createAgendaSaveWraper.setToDbFieldString("field_meeting");
        createAgendaSaveWraper.setXrefTableNameString("module_agendas");
        createAgendaSaveWraper.setToRecordIdentifier("field_name");
        createAgendaSaveWraper.setToIsUserInt(0);
        createAgendaSaveWraper.setPkIdString(MeetingPk_Id);
        ArrayList<CreateAgendaSaveWraper.valueRowsBeen> value =new ArrayList<>();
        CreateAgendaSaveWraper.valueRowsBeen valueRowsBeen =new CreateAgendaSaveWraper.valueRowsBeen();
        valueRowsBeen.setValuePkIdInt(Integer.parseInt(saveUpdate_pkId));
        value.add(valueRowsBeen);
        createAgendaSaveWraper.setDataArray(value);
        mSaveUpdateModuleDataArrayList.add(createAgendaSaveWraper);


        network.SaveCreateAgendas(createAgendaSaveWraper, new RequestCallback<CreateParticipantsResponce>() {
            @Override
            public void onSuccess(CreateParticipantsResponce response) {
                Log.e(TAG, "Success response::" + response);
                onSuccesSaveUpdateResponce(response);
            }

            @Override
            public void onFailed(String message) {
                Log.e(TAG, "Failed response::" + message);
            }
        });
    }

    private void onSuccesSaveUpdateResponce(final CreateParticipantsResponce response) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialog.cancel();
                Intent intent = new Intent(AgendaCreationActivity.this, AgendaActivity.class);
                intent.putExtra("Meeting_PkId",MeetingPk_Id);
                startActivity(intent);
                finish();
                overridePendingTransition(0, R.anim.push_down_in);
            }
        });

    }

    @Override
    public void onItemClick(int position) {

        SelectedAgendaPresenterArrayList.add(AgendaPresenterArrayList.get(position));
        AgendaPresenterNameEditText.setText(AgendaPresenterArrayList.get(position).getEmployeeFirstName());
        AgendaContactsListView.setVisibility(View.GONE);

    }

    @Override
    public void onSelectedItemClick(int position, int type) {

    }

    public void displayErrorMessage(String message) {
        errorMessageText.setText(message);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) errorMessageText.getLayoutParams();
        if (errorMessageText.getLineCount() == 3) {
            params.height = getResources().getDimensionPixelSize(R.dimen.search_screen_layout_height);
            errorMessageText.setLayoutParams(params);
        } else {
            params.height = getResources().getDimensionPixelSize(R.dimen.signIn_page_reset_mail_view);
            errorMessageText.setLayoutParams(params);
        }
        if (errorView.getAlpha() == 0) {
            errorView.setTranslationY(-60);
            stateprogressbar.setVisibility(View.GONE);
            errorView.setVisibility(View.VISIBLE);
            errorView.setBackgroundResource(R.color.error_message_background);
            errorView.animate().translationY(0)
                    .alpha(1)
                    .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    errorView.animate()
                            .alpha(0)
                            .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime));
                    errorView.setVisibility(View.GONE);
                    stateprogressbar.setVisibility(View.VISIBLE);
                }
            }, 2000);

        }
    }

}
