package com.nextgen.meetingwall.ui.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nextgen.meetingwall.R;
import com.nextgen.meetingwall.model.ModuleListMetadataArray;
import com.nextgen.meetingwall.network.Network;
import com.nextgen.meetingwall.network.RequestCallback;
import com.nextgen.meetingwall.network.json.AgendasListResponce;
import com.nextgen.meetingwall.network.json.AgendasListWraper;
import com.nextgen.meetingwall.network.json.MeetingEmployeesResponse;
import com.nextgen.meetingwall.network.json.MeetingLocationsResponse;
import com.nextgen.meetingwall.network.json.MeetingLocationsWraper;
import com.nextgen.meetingwall.network.json.MeetingParticipantsWraper;
import com.nextgen.meetingwall.network.json.MeetingReviewResponse;
import com.nextgen.meetingwall.stateprogressbar.StateProgressBar;
import com.nextgen.meetingwall.ui.dialog.DialogFactory;
import com.nextgen.meetingwall.utils.SharedPreference;
import com.nextgen.meetingwall.utils.Utils;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cc.cloudist.acplibrary.ACProgressFlower;

/*
 * Created by cas on 30-03-2017.
 */

public class MeetingReviewActivity extends AppCompatActivity {
    private LayoutInflater inflater;
    private Boolean more = false;
    @BindView(R.id.More_text)
    TextView MoreInviteesTextview;
    @BindView(R.id.Less_text)
    TextView LessInviteesTextview;
    @OnClick(R.id.More_text)
    void More() {
        // TODO call server...
        InviteeslinearLayout.removeAllViews();
        more = true;
       // InviteesMeeting(response);
    }

    @OnClick(R.id.Less_text)
    void Less() {
        // TODO call server...
        InviteeslinearLayout.removeAllViews();
        more = false;
    //    InviteesMeeting(response);
    }

    @BindView(R.id.Invitees_details_Layout)
    LinearLayout InviteeslinearLayout;
    @BindView(R.id.Invitees_agenda_Layout)
    LinearLayout AgendalinearLayout;
    protected String[] descriptionData = {"Setup", "Agenda", "Essentials", "Review"};
    @BindView(R.id.usage_stateprogressbar)
    protected StateProgressBar stateprogressbar;
    @BindView(R.id.Meeting_Start_Time_textview)
    protected TextView MeetingStartTimeTextView;
    @BindView(R.id.Meeting_Duration_textview)
    protected TextView MeetingDurationTextView;
    @BindView(R.id.Meeting_Agenda_Objective_textview)
    protected TextView MeetingAgendaObjectiveTextView;
    @BindView(R.id.Meeting_Agenda_Expected_Outcome_textview)
    protected TextView MeetingAgendaExpectedOutcomeTextView;
    @BindView(R.id.Meeting_Location_Name_Textview)
    protected TextView MeetingLocationNameTextView;
    @BindView(R.id.Meeting_Location_Time_textview)
    protected TextView MeetingLocationTimeTextView;
    private ACProgressFlower dialog;
    private SharedPreference sharedPreference;
    String MeetingPk_Id;

    private static final String TAG = "Meeting Review";
    private final Network network = new Network(this);
    private List<ModuleListMetadataArray> moduleListMetadataArrays = new ArrayList<>();
    private ArrayList<AgendasListResponce.AgendasRowsBeen> mAgendasListResponse = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meeting_review);
        ButterKnife.bind(this);
        inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        stateprogressbar.setStateDescriptionData(descriptionData);
        stateprogressbar.setCurrentStateNumber(StateProgressBar.StateNumber.FOUR);
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            getWindow().setStatusBarColor(getResources()
                    .getColor(R.color.colorPrimary));
        }

        LinearLayout backLayout = (LinearLayout) findViewById(R.id.back_to_layout);
        backLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(0, R.anim.push_down_in);

            }
        });
        sharedPreference = new SharedPreference(this);

        MeetingPk_Id =  sharedPreference.getMeetingSaveData("MeetingPk_Id");

        IntiService();
      //  prepareMeetingListItemAgendas();

    }


    public void IntiService(){
        dialog = new ACProgressFlower.Builder(MeetingReviewActivity.this)
                .text(getResources().getString(R.string.label_loading))
                .build();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        prepareMeetingData();


    }
    private void prepareMeetingData() {

        network.ReviewMeeting(Integer.parseInt(MeetingPk_Id),new RequestCallback<MeetingReviewResponse>() {
            @Override
            public void onSuccess(MeetingReviewResponse response) {
                Log.e(TAG, "Success response::" + response);
                onSuccesMeeting(response);
            }

            @Override
            public void onFailed(String message) {

                Log.e(TAG, "Failed response::" + message);
            }
        });
    }
    private void onSuccesMeeting(final MeetingReviewResponse meetingReviewResponse) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Date timestamp = null;
                String StartTimeoutput = null;
                String EndTimeOutput =null;
                if(meetingReviewResponse.getMeetingReviewResponseSuccess()){
                    for (int i = 0; i <meetingReviewResponse.getMeetingReviewResponseSuccessData().getMeetingReviewResponseSuccessblocks().size(); i++) {

                        for(int j= 0; j< meetingReviewResponse.getMeetingReviewResponseSuccessData().getMeetingReviewResponseSuccessblocks().get(i).getFields().size();j++){
                            if(meetingReviewResponse.getMeetingReviewResponseSuccessData().getMeetingReviewResponseSuccessblocks().get(i).getFields().get(j).getName().equalsIgnoreCase("Start Time")){
                                DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss",Locale.getDefault());
                                utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                                DateFormat indianFormat = new SimpleDateFormat("MMM dd,yyyy hh:mm a",Locale.getDefault());
                                utcFormat.setTimeZone(TimeZone.getTimeZone("IST"));

                                try {
                                    timestamp = utcFormat.parse(meetingReviewResponse.getMeetingReviewResponseSuccessData().getMeetingReviewResponseSuccessblocks().get(i).getFields().get(j).getValue().toString());
                                    StartTimeoutput = indianFormat.format(timestamp);
                                    MeetingStartTimeTextView.setText(StartTimeoutput);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }


                            }
                            if(meetingReviewResponse.getMeetingReviewResponseSuccessData().getMeetingReviewResponseSuccessblocks().get(i).getFields().get(j).getName().equalsIgnoreCase("End Time")){
                                DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss",Locale.getDefault());
                                utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                                DateFormat indianFormat = new SimpleDateFormat("hh:mm a",Locale.getDefault());
                                utcFormat.setTimeZone(TimeZone.getTimeZone("IST"));
                                Date Endtimestamp = null;
                                try {

                                    Endtimestamp = utcFormat.parse(meetingReviewResponse.getMeetingReviewResponseSuccessData().getMeetingReviewResponseSuccessblocks().get(i).getFields().get(j).getValue().toString());
                                    EndTimeOutput = indianFormat.format(Endtimestamp);
                                    long difference = Endtimestamp.getTime() - timestamp.getTime();
                                    int    days = (int) (difference / (1000*60*60*24));
                                    int    hours = (int) ((difference - (1000*60*60*24*days)) / (1000*60*60));
                                    int    min = (int) (difference - (1000*60*60*24*days) - (1000*60*60*hours)) / (1000*60);
                                    hours = (hours < 0 ? -hours : hours);
                                    Log.i("======= Hours"," :: "+hours);
                                    if(days!=0){
                                        MeetingDurationTextView.setText(days+"Day"+hours+"hrs"+min+"Mins");
                                    }else if(hours!=0){
                                        MeetingDurationTextView.setText(hours+"hrs"+min+"Mins");
                                    }else{
                                        MeetingDurationTextView.setText(""+min+" Mins");
                                    }
                                    MeetingLocationTimeTextView.setText(StartTimeoutput+" - "+EndTimeOutput);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }
                            if(meetingReviewResponse.getMeetingReviewResponseSuccessData().getMeetingReviewResponseSuccessblocks().get(i).getFields().get(j).getName().equalsIgnoreCase("Objective")){
                                MeetingAgendaObjectiveTextView.setText(meetingReviewResponse.getMeetingReviewResponseSuccessData().getMeetingReviewResponseSuccessblocks().get(i).getFields().get(j).getValue().toString());
                            }
                            if(meetingReviewResponse.getMeetingReviewResponseSuccessData().getMeetingReviewResponseSuccessblocks().get(i).getFields().get(j).getName().equalsIgnoreCase("Outcome")){
                                MeetingAgendaExpectedOutcomeTextView.setText(meetingReviewResponse.getMeetingReviewResponseSuccessData().getMeetingReviewResponseSuccessblocks().get(i).getFields().get(j).getValue().toString());
                            }

                        }
                    }
                    prepareMeetingsPlaces();
                }


            }
        });
    }
    private void prepareMeetingsPlaces() {

        network.MeetingPlaces(new RequestCallback<ArrayList<ModuleListMetadataArray>>() {
            @Override
            public void onSuccess(ArrayList<ModuleListMetadataArray> response) {
                Log.e(TAG, "Success response::" + response);
                onSuccesPlaces(response);
            }

            @Override
            public void onFailed(String message) {

                Log.e(TAG, "Failed response::" + message);
            }
        });


    }
    private void onSuccesPlaces(final ArrayList<ModuleListMetadataArray> jobsList) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                moduleListMetadataArrays.clear();
                if (jobsList.size() == 0) {
                    dialog(getResources().getString(R.string.label_server_data_notfound));
                } else {
                    for (int i = 0; i < jobsList.size(); i++) {
                        moduleListMetadataArrays.add(jobsList.get(i));
                    }
                    ListDataMeetingPlaces( );
                }
            }
        });
    }
    private void ListDataMeetingPlaces(){
        MeetingLocationsWraper wrapper = new MeetingLocationsWraper();
        wrapper.setFirstInt(0);
        wrapper.setRowsInt(10);
        wrapper.setSortOrderInt(1);
        wrapper.setPageInt(1);
        //    wrapper.setFiltersObject(new FiltersObjectBeen());
        //    wrapper.setGlobalFilter(null);
        MeetingLocationsWraper.RelObject relObject = new MeetingLocationsWraper.RelObject();
        relObject.setFromModuleString("Meetings");
        relObject.setFromDbTableString("module_meetings");
        relObject.setFromRelationInt(1);
        relObject.setFromFieldIdInt(556);
        relObject.setFromBlockIdInt(43);
        relObject.setFromBlockString("Basic Information");
        relObject.setFromFieldString("Place");
        relObject.setFromDbFieldString("pk_id");
        relObject.setToModuleString("Meeting Places");
        relObject.setToTableString("module_meeting_places");
        relObject.setToRelationInt(0);
        relObject.setToFieldIdInt(557);
        relObject.setToBlockIdInt(46);
        relObject.setToBlockString("Basic Information");
        relObject.setToFieldString("Meetings");
        relObject.setToDbFieldString("field_meeting");
        relObject.setXrefTableNameString("module_meeting_places");
        relObject.setToRecordIdentifierString("field_name");
        relObject.setPkIdString(MeetingPk_Id);
        wrapper.setRelobject(relObject);
        wrapper.setDisplayColsArray(moduleListMetadataArrays);
        network.ListMeetingPlaces(wrapper,new RequestCallback<MeetingLocationsResponse>() {
            @Override
            public void onSuccess(MeetingLocationsResponse response) {
                Log.e(TAG, "Success response::" + response);
                onSuccesListOfPlaces(response);

            }

            @Override
            public void onFailed(String message) {

                Log.e(TAG, "Failed response::" + message);
            }
        });

    }

    private void onSuccesListOfPlaces(final MeetingLocationsResponse response) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for (int i=0; i < response.getJqGridData().getRowsArray().size(); i++){
                    MeetingLocationNameTextView.setText(response.getJqGridData().getRowsArray().get(i).getLocation());
                }
                prepareMeetingListItemEmployees();
            }
        });
    }


    private void prepareMeetingListItemEmployees() {

        network.MeetingEmployees(new RequestCallback<ArrayList<ModuleListMetadataArray>>() {
            @Override
            public void onSuccess(ArrayList<ModuleListMetadataArray> response) {
                Log.e(TAG, "Success response::" + response);
                onSuccesEmployees(response);
            }

            @Override
            public void onFailed(String message) {

                Log.e(TAG, "Failed response::" + message);
            }
        });
    }
    private void onSuccesEmployees(final ArrayList<ModuleListMetadataArray> jobsList) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                moduleListMetadataArrays.clear();
                if (jobsList.size() == 0) {
                    dialog(getResources().getString(R.string.label_server_data_notfound));
                } else {
                    for (int i = 0; i < jobsList.size(); i++) {
                        moduleListMetadataArrays.add(jobsList.get(i));
                    }
                    ListDataMeetingEmployees();
                }
            }
        });
    }

    private void ListDataMeetingEmployees(){
        MeetingParticipantsWraper wrapper = new MeetingParticipantsWraper();
        wrapper.setFirstInt(0);
        wrapper.setRowsInt(10);
        wrapper.setSortOrderInt(1);
        wrapper.setPageInt(1);
        //    wrapper.setFiltersObject(new FiltersObjectBeen());
        //    wrapper.setGlobalFilter(null);
        MeetingParticipantsWraper.RelObject relObject = new MeetingParticipantsWraper.RelObject();
        relObject.setFromModuleString("Meetings");
        relObject.setFromDbTableString("module_meetings");
        relObject.setFromRelationInt(0);
        relObject.setFromFieldIdInt(506);
        relObject.setFromBlockIdInt(43);
        relObject.setFromBlockString("Basic Information");
        relObject.setFromFieldString("Participants");
        relObject.setFromDbFieldString("field_participants_id");
        relObject.setToModuleIconString("fa-users");
        relObject.setToModuleString("Employees");
        relObject.setToTableString("app_employee");
        relObject.setToRelationInt(0);
        relObject.setToFieldIdInt(507);
        relObject.setToBlockIdInt(8);
        relObject.setToBlockString("Basic Information");
        relObject.setToFieldString("Meetings");
        relObject.setToDbFieldString("field_meetings_id");
        relObject.setXrefTableNameString("module_meetings_xref_employee");
        relObject.setPkIdString(MeetingPk_Id);
        wrapper.setRelobject(relObject);
        wrapper.setDisplayColsArray(moduleListMetadataArrays);

        network.ListMeetingParticipants(wrapper,new RequestCallback<MeetingEmployeesResponse>() {
            @Override
            public void onSuccess(MeetingEmployeesResponse response) {
                Log.e(TAG, "Success response::" + response);
                onSucessListMeetingInvitees(response);


            }

            @Override
            public void onFailed(String message) {
                Log.e(TAG, "Failed response::" + message);
            }
        });


    }

    private void  onSucessListMeetingInvitees(final MeetingEmployeesResponse response){
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mAgendasListResponse.clear();
             View meetingsView = null;
               if(response.getSuccess()) {

                   if (response.getJqGridData().getRowsArray().size() > 0) {
                       for (int i = 0; i < response.getJqGridData().getRowsArray().size(); i++) {
                           meetingsView = inflater.inflate(R.layout.view_invitees_meeting, null);
                           TextView NameTextView = ButterKnife.findById(meetingsView, R.id.Invitees_Name_text);
                           TextView designationTextView = ButterKnife.findById(meetingsView, R.id.Invitees_Manager_text);
                           TextView EmailidTextView = ButterKnife.findById(meetingsView, R.id.Invitees_Email_text);

                           ImageView imageView = ButterKnife.findById(meetingsView, R.id.Invitees_profile_ImageView);
                           Picasso.with(inflater.getContext())
                                   .load(Utils.imageURLArray[i])
                                   .into(imageView);
                           NameTextView.setText(response.getJqGridData().getRowsArray().get(i).getEmployeeFirstName());
                           designationTextView.setText(response.getJqGridData().getRowsArray().get(i).getEmployeeRole());
                           EmailidTextView.setText(response.getJqGridData().getRowsArray().get(i).getEmployeeEmail());
                           if (response.getJqGridData().getRowsArray().size() < 3) {
                               InviteeslinearLayout.addView(meetingsView);
                               MoreInviteesTextview.setVisibility(View.GONE);
                               LessInviteesTextview.setVisibility(View.GONE);
                           }else{
                               InviteeslinearLayout.addView(meetingsView);
                           }
                       }

                   }
                   dialog.cancel();
                   prepareMeetingListItemAgendas();
               }else{
                   dialog.cancel();
               }



            }

        });
    }




    private void prepareMeetingListItemAgendas() {

        network.MeetingAgendas(new RequestCallback<ArrayList<ModuleListMetadataArray>>() {
            @Override
            public void onSuccess(ArrayList<ModuleListMetadataArray> response) {
                Log.e(TAG, "Success response::" + response);
                onSuccesAgendas(response);
            }

            @Override
            public void onFailed(String message) {

                Log.e(TAG, "Failed response::" + message);
            }
        });
    }
    private void onSuccesAgendas(final ArrayList<ModuleListMetadataArray> jobsList) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                moduleListMetadataArrays.clear();
                if (jobsList.size() == 0) {
                    dialog(getResources().getString(R.string.label_server_data_notfound));
                } else {
                    for (int i = 0; i < jobsList.size(); i++) {
                        moduleListMetadataArrays.add(jobsList.get(i));
                    }

                    ListDataMeetingAgendas();

                }
            }
        });
    }
    private void ListDataMeetingAgendas(){
        AgendasListWraper agendasListWraper = new AgendasListWraper();
        agendasListWraper.setFirstInt(0);
        agendasListWraper.setRowsInt(10);
        agendasListWraper.setSortOrderInt(1);
        agendasListWraper.setPageInt(1);
        //    wrapper.setFiltersObject(new FiltersObjectBeen());
        //    wrapper.setGlobalFilter(null);
        AgendasListWraper.RelObject relObject = new AgendasListWraper.RelObject();
        relObject.setFromModuleString("Meetings");
        relObject.setFromDbTableString("module_meetings");
        relObject.setFromRelationInt(1);
        relObject.setFromFieldIdInt(559);
        relObject.setFromBlockIdInt(43);
        relObject.setFromBlockString("Basic Information");
        relObject.setFromFieldString("Agendas");
        relObject.setFromDbFieldString("pk_id");
        relObject.setToModuleString("Agendas");
        relObject.setToTableString("module_agendas");
        relObject.setToRelationInt(0);
        relObject.setToFieldIdInt(558);
        relObject.setToBlockIdInt(47);
        relObject.setToBlockString("Basic Information");
        relObject.setToFieldString("Meeting");
        relObject.setToDbFieldString("field_meeting");
        relObject.setXrefTableNameString("module_agendas");
        relObject.setToRecordIdentifierString("field_name");
        relObject.setPkIdString(MeetingPk_Id);//Meeting Pk Id
        agendasListWraper.setRelobject(relObject);
        agendasListWraper.setDisplayColsArray(moduleListMetadataArrays);
        network.ListMeetingAgendas(agendasListWraper,new RequestCallback<AgendasListResponce>() {
            @Override
            public void onSuccess(AgendasListResponce  response) {
                Log.e(TAG, "Success response::" + response);

                onSucessListMeetingAgendas(response);
            }

            @Override
            public void onFailed(String message) {

                Log.e(TAG, "Failed response::" + message);
            }
        });

    }
    private void  onSucessListMeetingAgendas(final AgendasListResponce response){
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mAgendasListResponse.clear();
                if (response.getJqGridData().getRowsArray().size() > 0) {
                    for (int i = 0; i < response.getJqGridData().getRowsArray().size(); i++) {
                        mAgendasListResponse.add(response.getJqGridData().getRowsArray().get(i));

                        final View meetingsView = inflater.inflate(R.layout.view_agenda_meeting_review, null);
                        TextView AgendaTimeTextView = ButterKnife.findById(meetingsView, R.id.agenda_time_text);
                        TextView AgendaDescriptionTextView = ButterKnife.findById(meetingsView, R.id.agenda_description_text);

                        AgendaTimeTextView.setText(response.getJqGridData().getRowsArray().get(i).getAgendasRowsStartTimeString());
                        AgendaDescriptionTextView.setText(response.getJqGridData().getRowsArray().get(i).getAgendasRowsNameString()+" by "+response.getJqGridData().getRowsArray().get(i).getAgendasRowsEmployeeString());
                        DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss",Locale.getDefault());
                        utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                        DateFormat indianFormat = new SimpleDateFormat("hh:mm a",Locale.getDefault());
                        utcFormat.setTimeZone(TimeZone.getTimeZone("IST"));
                        Date timestamp = null;
                        Date timestampEndTime =null;
                        try {
                            timestamp = utcFormat.parse(response.getJqGridData().getRowsArray().get(i).getAgendasRowsStartTimeString());
                            timestampEndTime =utcFormat.parse(response.getJqGridData().getRowsArray().get(i).getAgendasRowsEndTimeString());
                            String StartTimeoutput = indianFormat.format(timestamp);
                            String EndTimeoutput = indianFormat.format(timestampEndTime);
                            AgendaTimeTextView.setText(StartTimeoutput+" - "+EndTimeoutput);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        dialog.cancel();
                        AgendalinearLayout.addView(meetingsView);
                    }


                }


            }

        });
    }

    private void dialog(final String message) {
        DialogFactory.showAConfirmationDialog(this, message, 0,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
    }
}
