package com.nextgen.meetingwall.ui.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nextgen.meetingwall.R;
import com.nextgen.meetingwall.model.MeetingDatesAscendingBean;
import com.nextgen.meetingwall.model.ModuleListMetadataArray;
import com.nextgen.meetingwall.network.Network;
import com.nextgen.meetingwall.network.RequestCallback;
import com.nextgen.meetingwall.network.json.MeetingEmployeesResponse;
import com.nextgen.meetingwall.network.json.MeetingListDataResponce;
import com.nextgen.meetingwall.network.json.MeetingLocationsResponse;
import com.nextgen.meetingwall.network.json.MeetingLocationsWraper;
import com.nextgen.meetingwall.network.json.MeetingParticipantsWraper;
import com.nextgen.meetingwall.network.json.MeetingWraper;
import com.nextgen.meetingwall.ui.activity.MainActivity;
import com.nextgen.meetingwall.adapter.MeetingsViewAdapter;
import com.nextgen.meetingwall.listeners.OnCustomItemSelectListener;
import com.nextgen.meetingwall.ui.dialog.DialogFactory;
import com.noman.weekcalendar.WeekCalendar;
import com.noman.weekcalendar.listener.OnDateClickListener;
import com.noman.weekcalendar.listener.OnWeekChangeListener;

import org.joda.time.DateTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import java.util.Calendar;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cc.cloudist.acplibrary.ACProgressFlower;


/**
 * Created by cas on 06-02-2017.
 */

@SuppressWarnings("ALL")
public class AllMeetingsFragment extends Fragment  implements OnCustomItemSelectListener,SearchView.OnQueryTextListener {

 //   private HorizontalCalendar horizontalCalendar;
    private Context ctx;
    private View view;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.Calender_linearlayout)
    protected LinearLayout linearLayout;
    @BindView(R.id.Selected_month)
    protected TextView calendarView;
    @BindView(R.id.Calender_imageView)
    protected ImageView CalenderImageView;


    private static final String TAG = "Network";
    private Network network ;
    private ACProgressFlower dialog;
    private MeetingsViewAdapter mAdapter;
    private WeekCalendar weekCalendar;
    private DateTime datetimes;
    private int pagenumber=1;
    private Boolean isloadMore=false;

    private List<ModuleListMetadataArray> moduleListMetadataArrays = new ArrayList<>();
    private ArrayList<MeetingListDataResponce> meetingListDataResponces = new ArrayList<>();
    private ArrayList<MeetingListDataResponce.MeetinglistDataRowsBeen> meetinglistDataRowsBeenArrayList = new ArrayList<>();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         view = inflater.inflate(R.layout.fragment_allmeetings, container, false);
        ButterKnife.bind(this, view);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        setHasOptionsMenu(true);
        ActionBar bar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(getActivity().getResources()
                .getColor(R.color.colorPrimary)));
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            getActivity().getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getActivity().getWindow().setStatusBarColor(getActivity().getResources()
                    .getColor(R.color.colorPrimary));
        }
        network = new Network(getActivity());
        moduleListMetadataArrays = new ArrayList<>();
        init();

        return view;
    }

    @OnClick(R.id.Calender_imageView)
    void More() {
        // TODO call server...
        ((MainActivity)getActivity()).CalenderView(datetimes);
    }
    private void init() {
        weekCalendar = (WeekCalendar) view. findViewById(R.id.weekCalendar);
        weekCalendar.setBackground(getResources().getColor(R.color.colorYellow),2);
        weekCalendar.reset();
        String monthname = (String) android.text.format.DateFormat.format("MMMM", new Date());
        String yearname = (String) android.text.format.DateFormat.format("yyyy", new Date());
        calendarView.setText("" + monthname + " " + yearname);
        datetimes=new DateTime();

        weekCalendar.setOnDateClickListener(new OnDateClickListener() {
            @Override
            public void onDateClick(DateTime dateTime) {
                try {
                    datetimes=dateTime;
                    SimpleDateFormat outFormat = new SimpleDateFormat("yyyy-MMMM-dd");
                    String dtOut = outFormat.format(dateTime.getMillis());
                    String monthsplit[] = dtOut.split("-");
                    calendarView.setText("" + monthsplit[1] + " " + monthsplit[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        });
        weekCalendar.setOnWeekChangeListener(new OnWeekChangeListener() {
            @Override
            public void onWeekChange(DateTime firstDayOfTheWeek, boolean forward) {
                SimpleDateFormat outFormat = new SimpleDateFormat("yyyy-MMMM-dd");
                String dtOut = outFormat.format(firstDayOfTheWeek.getMillis());
                String monthsplit[] = dtOut.split("-");
                calendarView.setText("" + monthsplit[1] + " " + monthsplit[0]);

            }
        });


        recyclerView.setNestedScrollingEnabled(true);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ctx);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.HORIZONTAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new MeetingsViewAdapter(meetinglistDataRowsBeenArrayList, getActivity(),this);

        mAdapter.setLoadMoreListener(new MeetingsViewAdapter.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                try{
                    meetinglistDataRowsBeenArrayList.add(null);
                    mAdapter.notifyItemInserted(meetinglistDataRowsBeenArrayList.size() - 1);
                }catch (Exception e){
                    e.printStackTrace();
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.e("haint", "Load More 2");
                        pagenumber = pagenumber+ 1;
                        //Remove loading item
                        meetinglistDataRowsBeenArrayList.remove(meetinglistDataRowsBeenArrayList.size() - 1);
                        mAdapter.notifyItemRemoved(meetinglistDataRowsBeenArrayList.size());
                        loadMore();
                    }
                }, 5000);
                //Calling loadMore function in Runnable to fix the
                // java.lang.IllegalStateException: Cannot call this method while RecyclerView is computing a layout or scrolling error
            }
        });

        recyclerView.setAdapter(mAdapter);
        load();
        ((MainActivity)getActivity()).ShowCalenderView();
    }

    public void ResetHorizentalCalender(int selectedYear, int selectedMonth, int seceltedDay) {

        Calendar startDate = Calendar.getInstance();
        startDate.set(selectedYear, selectedMonth-1, seceltedDay);
        String monthname = (String) android.text.format.DateFormat.format("MMMM", startDate);
        String yearname = (String) android.text.format.DateFormat.format("yyyy", startDate);
        calendarView.setText("" + monthname + " " + yearname);
        datetimes=new DateTime().withDate(selectedYear, selectedMonth, seceltedDay);
        //** start before 1 month from now *//
        weekCalendar.setSelectedDate(new DateTime().withDate(selectedYear, selectedMonth, seceltedDay));
    }
    private void load(){
        dialog = new ACProgressFlower.Builder(getActivity())
                .text(getResources().getString(R.string.label_loading))
                .build();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        prepareMeetingsListData();
    }
    private void loadMore(){
        isloadMore=true;
        prepareMeetingsListData();
    }
    private void prepareMeetingsListData() {
        network.ModuleListMetadata(new RequestCallback<ArrayList<ModuleListMetadataArray>>() {
            @Override
            public void onSuccess(ArrayList<ModuleListMetadataArray> response) {
                Log.e(TAG, "Success response::" + response);
                onSucces(response);
            }
            @Override
            public void onFailed(String message) {
                if(!isloadMore){
                    dialog.cancel();
                }
                Log.e(TAG, "Failed response::" + message);
            }
        });
    }

    private void onSucces(final ArrayList<ModuleListMetadataArray> jobsList) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                moduleListMetadataArrays.clear();
                if (jobsList.size() == 0) {
                    dialog(getResources().getString(R.string.label_server_data_notfound));
                } else {
                    for (int i = 0; i < jobsList.size(); i++) {
                        moduleListMetadataArrays.add(jobsList.get(i));
                    }
                    ModuleListData();
                }
            }
        });
    }

    private void ModuleListData(){
        MeetingWraper wrapper = new MeetingWraper();
        wrapper.setFirstInt(0);
        wrapper.setRowsInt(10);
        wrapper.setSortOrderInt(1);
        wrapper.setPageInt(pagenumber);
        //    wrapper.setFiltersObject(new FiltersObjectBeen());
        //    wrapper.setGlobalFilter(null);
        wrapper.setDisplayColsArray(moduleListMetadataArrays);

        network.ModuleListData(wrapper,new RequestCallback<MeetingListDataResponce>() {
            @Override
            public void onSuccess(MeetingListDataResponce response) {
                Log.e(TAG, "Success response::" + response);
                onSuccesSpares(response);
            }

            @Override
            public void onFailed(String message) {
                if(!isloadMore){
                    dialog.cancel();
                }
                Log.e(TAG, "Failed response::" + message);
            }
        });
    }
    private void onSuccesSpares(final MeetingListDataResponce response){
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                meetingListDataResponces.add(response);
                SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
                Date date = null;
                MeetingDatesAscendingBean datesAscendingBean = new MeetingDatesAscendingBean();

                if (response.getJqGridData().getRowsArray().size() > 0) {
                    for (int i = 0; i < response.getJqGridData().getRowsArray().size(); i++) {
                        try {
                            datesAscendingBean = new MeetingDatesAscendingBean();
                            date = fmt.parse(response.getJqGridData().getRowsArray().get(i).getMeetingStartTime());
                            datesAscendingBean.setDateTime(date);
                            datesAscendingBean.setisdate(false);
                            meetinglistDataRowsBeenArrayList.add(response.getJqGridData().getRowsArray().get(i));
                            meetinglistDataRowsBeenArrayList.get(i).setMeetingDatesAscendingBean(datesAscendingBean);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }

                    prepareMeetingListItemEmployees();
                }else{
                    mAdapter.setMoreDataAvailable(false);
                    //telling adapter to stop calling load more as no more server data available
                    Toast.makeText(getActivity(),"No More Data Available",Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    private void prepareMeetingListItemEmployees() {

        network.MeetingEmployees(new RequestCallback<ArrayList<ModuleListMetadataArray>>() {
            @Override
            public void onSuccess(ArrayList<ModuleListMetadataArray> response) {
                Log.e(TAG, "Success response::" + response);
                onSuccesEmployees(response);
            }

            @Override
            public void onFailed(String message) {
                if(!isloadMore){
                    dialog.cancel();
                }
                Log.e(TAG, "Failed response::" + message);
            }
        });
    }

    private void onSuccesEmployees(final ArrayList<ModuleListMetadataArray> jobsList) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                moduleListMetadataArrays.clear();
                if (jobsList.size() == 0) {
                    dialog(getResources().getString(R.string.label_server_data_notfound));
                } else {
                    for (int i = 0; i < jobsList.size(); i++) {
                        moduleListMetadataArrays.add(jobsList.get(i));
                    }
                    for (int i = 0; i < meetinglistDataRowsBeenArrayList.size(); i++) {
                        meetinglistDataRowsBeenArrayList.get(i).getMeetinglistDataId1();
                        ListDataMeetingEmployees( meetinglistDataRowsBeenArrayList.get(i).getMeetinglistDataId1(),i);
                    }
                }
            }
        });
    }

    private void ListDataMeetingEmployees(int meetinglistDataId1, final int i){
        MeetingParticipantsWraper wrapper = new MeetingParticipantsWraper();
        wrapper.setFirstInt(0);
        wrapper.setRowsInt(10);
        wrapper.setSortOrderInt(1);
        wrapper.setPageInt(pagenumber);
        //    wrapper.setFiltersObject(new FiltersObjectBeen());
        //    wrapper.setGlobalFilter(null);
        MeetingParticipantsWraper.RelObject relObject = new MeetingParticipantsWraper.RelObject();
        relObject.setFromModuleString("Meetings");
        relObject.setFromDbTableString("module_meetings");
        relObject.setFromRelationInt(0);
        relObject.setFromFieldIdInt(506);
        relObject.setFromBlockIdInt(43);
        relObject.setFromBlockString("Basic Information");
        relObject.setFromFieldString("Participants");
        relObject.setFromDbFieldString("field_participants_id");
        relObject.setToModuleIconString("fa-users");
        relObject.setToModuleString("Employees");
        relObject.setToTableString("app_employee");
        relObject.setToRelationInt(0);
        relObject.setToFieldIdInt(507);
        relObject.setToBlockIdInt(8);
        relObject.setToBlockString("Basic Information");
        relObject.setToFieldString("Meetings");
        relObject.setToDbFieldString("field_meetings_id");
        relObject.setXrefTableNameString("module_meetings_xref_employee");
        relObject.setPkIdString(String.valueOf(meetinglistDataId1));
        wrapper.setRelobject(relObject);
        wrapper.setDisplayColsArray(moduleListMetadataArrays);

        network.ListMeetingParticipants(wrapper,new RequestCallback<MeetingEmployeesResponse>() {
            @Override
            public void onSuccess(MeetingEmployeesResponse response) {
                Log.e(TAG, "Success response::" + response);
                meetinglistDataRowsBeenArrayList.get(i).setMeetingEmployeesResponse(response);
                if(meetinglistDataRowsBeenArrayList.size()==(i+1)){
                    prepareMeetingsPlaces();
                }
            }

            @Override
            public void onFailed(String message) {
                if(!isloadMore){
                    dialog.cancel();
                }
                Log.e(TAG, "Failed response::" + message);
            }
        });


    }



    private void prepareMeetingsPlaces() {

        network.MeetingPlaces(new RequestCallback<ArrayList<ModuleListMetadataArray>>() {
            @Override
            public void onSuccess(ArrayList<ModuleListMetadataArray> response) {
                Log.e(TAG, "Success response::" + response);
                onSuccesPlaces(response);
            }

            @Override
            public void onFailed(String message) {
                if(!isloadMore){
                    dialog.cancel();
                }
                Log.e(TAG, "Failed response::" + message);
            }
        });


    }

    private void onSuccesPlaces(final ArrayList<ModuleListMetadataArray> jobsList) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                moduleListMetadataArrays.clear();
                if (jobsList.size() == 0) {
                    dialog(getResources().getString(R.string.label_server_data_notfound));
                } else {
                    for (int i = 0; i < jobsList.size(); i++) {
                        moduleListMetadataArrays.add(jobsList.get(i));
                    }
                    for (int i = 0; i < meetinglistDataRowsBeenArrayList.size(); i++) {
                        meetinglistDataRowsBeenArrayList.get(i).getMeetinglistDataId1();
                        ListDataMeetingPlaces( meetinglistDataRowsBeenArrayList.get(i).getMeetinglistDataId1(),i);
                    }
                }
            }
        });
    }


    private void ListDataMeetingPlaces(int meetinglistDataId1, final int i){
        MeetingLocationsWraper wrapper = new MeetingLocationsWraper();
        wrapper.setFirstInt(0);
        wrapper.setRowsInt(10);
        wrapper.setSortOrderInt(1);
        wrapper.setPageInt(pagenumber);
        //    wrapper.setFiltersObject(new FiltersObjectBeen());
        //    wrapper.setGlobalFilter(null);
        MeetingLocationsWraper.RelObject relObject = new MeetingLocationsWraper.RelObject();
        relObject.setFromModuleString("Meetings");
        relObject.setFromDbTableString("module_meetings");
        relObject.setFromRelationInt(1);
        relObject.setFromFieldIdInt(556);
        relObject.setFromBlockIdInt(43);
        relObject.setFromBlockString("Basic Information");
        relObject.setFromFieldString("Place");
        relObject.setFromDbFieldString("pk_id");
        relObject.setToModuleString("Meeting Places");
        relObject.setToTableString("module_meeting_places");
        relObject.setToRelationInt(0);
        relObject.setToFieldIdInt(557);
        relObject.setToBlockIdInt(46);
        relObject.setToBlockString("Basic Information");
        relObject.setToFieldString("Meetings");
        relObject.setToDbFieldString("field_meeting");
        relObject.setXrefTableNameString("module_meeting_places");
        relObject.setToRecordIdentifierString("field_name");
        relObject.setPkIdString(String.valueOf(meetinglistDataId1));
        wrapper.setRelobject(relObject);
        wrapper.setDisplayColsArray(moduleListMetadataArrays);
        network.ListMeetingPlaces(wrapper,new RequestCallback<MeetingLocationsResponse>() {
            @Override
            public void onSuccess(MeetingLocationsResponse response) {
                Log.e(TAG, "Success response::" + response);
                meetinglistDataRowsBeenArrayList.get(i).setMeetingLocationsResponse(response);
                if(meetinglistDataRowsBeenArrayList.size()==(i+1)){
                    onSucessListMeetingParticipants();
                }
            }

            @Override
            public void onFailed(String message) {
                if(!isloadMore){
                    dialog.cancel();
                }
                Log.e(TAG, "Failed response::" + message);
            }
        });

    }

    private void  onSucessListMeetingParticipants(){
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Sorting();
            }

        });
    }

    public void  Sorting(){
        Collections.sort( meetinglistDataRowsBeenArrayList, new Comparator<MeetingListDataResponce.MeetinglistDataRowsBeen>() {
            public int compare(MeetingListDataResponce.MeetinglistDataRowsBeen o1, MeetingListDataResponce.MeetinglistDataRowsBeen o2) {
                if (o1.getMeetingStartTime() == null || o2.getMeetingStartTime() == null)
                    return 0;
                try {
                    return new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss", Locale.ENGLISH).parse(o1.getMeetingStartTime()).compareTo(new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss", Locale.ENGLISH).parse(o2.getMeetingStartTime()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                return 0;
            }
        });

        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        MeetingDatesAscendingBean datesAscendingBean = new MeetingDatesAscendingBean();
        ArrayList<MeetingDatesAscendingBean> dateList = new ArrayList<>();
        Boolean isDate =false;
        for(int j =0; j<meetinglistDataRowsBeenArrayList.size(); j++){
            datesAscendingBean = new MeetingDatesAscendingBean();
            try {
                if(meetinglistDataRowsBeenArrayList.get(j).getMeetingStartTime()!=null){
                    date = fmt.parse(meetinglistDataRowsBeenArrayList.get(j).getMeetingStartTime());
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
            for(int i =0; i<dateList.size(); i++ ){
                if (dateList.get(i).getDateTime().equals(date)) {
                    isDate = true;
                }
            }
            if(isDate){
                isDate =false;
                isDate =false;
                datesAscendingBean.setDateTime(date);
                datesAscendingBean.setisdate(false);
            }else{
                datesAscendingBean.setDateTime(date);
                datesAscendingBean.setisdate(true);
            }
            dateList.add(datesAscendingBean);
            meetinglistDataRowsBeenArrayList.get(j).setMeetingDatesAscendingBean(datesAscendingBean);
            meetinglistDataRowsBeenArrayList.get(j).setType("Meeting");
        }
        if(!isloadMore){
            dialog.cancel();
        }
        mAdapter.notifyDataChanged();
    }

    private void dialog(final String message) {
        DialogFactory.showAConfirmationDialog(getActivity(), message, 0,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
    }
    @Override
    public void onItemClick(int position) {


        for(int i=0; i<moduleListMetadataArrays.size(); i++){
    //        meetingBeenArrayList.get(position).setOnclick(true);
        }
        mAdapter.notifyDataChanged();
        Fragment fragment = new MeetingPreviewFragment();
        ((MainActivity) getActivity()).pushFragment(fragment);
    }

    @Override
    public void onSelectedItemClick(int position, int type) {

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.search, menu);

        return;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        MenuItem searchViewMenuItem = menu.findItem(R.id.action_search);
        SearchView  mSearchView = (SearchView) searchViewMenuItem.getActionView();
        int searchImgId = android.support.v7.appcompat.R.id.search_button;
        ImageView v = (ImageView) mSearchView.findViewById(searchImgId);
        v.setImageResource(R.drawable.search_icon);
        mSearchView.setOnQueryTextListener(this);
        return ;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_search) {
            return true;
        }else if(id==R.id.action_plus){

            Fragment fragment = new CreateMeetingFragment();
            ((MainActivity) getActivity()).pushFragment(fragment);

        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public boolean onQueryTextSubmit(String query) {
        serachQuery(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        serachQuery(newText);
        return false;
    }


    private void serachQuery(String searchString) {
        int textlength = searchString.length();
        ArrayList<MeetingListDataResponce.MeetinglistDataRowsBeen> tempArrayList = new ArrayList<>();
        for (MeetingListDataResponce.MeetinglistDataRowsBeen bean : meetinglistDataRowsBeenArrayList) {
            if (textlength <= bean.getMeetingName().length()) {
                if (bean.getMeetingName().toLowerCase().contains(searchString.toString().toLowerCase())) {
                    tempArrayList.add(bean);
                }
            }
        }

        mAdapter = new MeetingsViewAdapter(tempArrayList, getActivity(),this);
        //      recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }



}
