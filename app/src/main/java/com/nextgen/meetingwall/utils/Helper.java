package com.nextgen.meetingwall.utils;

import android.app.Activity;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by cas on 17-02-2017.
 */

public class Helper {

    public static void hideKeyboard(Activity activity) {
        if (activity == null || activity.getCurrentFocus() == null) {
            return;
        }

        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
    }
}
