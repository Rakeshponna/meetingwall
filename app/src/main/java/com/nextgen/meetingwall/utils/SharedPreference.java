package com.nextgen.meetingwall.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.nextgen.meetingwall.network.json.SaveUpdateModuleDataResponce;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class SharedPreference {
	Context ctx;
    public SharedPreference(Context context){
	this.ctx=context;
}
public boolean sessionOut(){
	
	Boolean bool = false;
	File data=new File(ctx.getFilesDir().getPath());
	if(data.exists()){
		android.content.SharedPreferences localSharedPreferences = ctx.getSharedPreferences("user_login_data", 0);
	       
		android.content.SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctx);
		bool=prefs.edit().clear().commit()&&localSharedPreferences.edit().clear().commit()&&data.delete();
		
		
	}
	return bool;
}




public boolean addTimeSlot(String opp){
	android.content.SharedPreferences.Editor localEditor = ctx.getSharedPreferences("New_Meeting", 0).edit();
    localEditor.putString("Time_slot",opp);
    return localEditor.commit();
}
public String getTimeSlot(){
	android.content.SharedPreferences localSharedPreferences = ctx.getSharedPreferences("New_Meeting", 0);
    return localSharedPreferences.getString("Time_slot", "");
	
}

public boolean addLocation(ArrayList<String> LocationList){
	Set<String> set = new HashSet<String>();
	set.addAll(LocationList);
	android.content.SharedPreferences.Editor localEditor = ctx.getSharedPreferences("New_Meeting", 0).edit();
    localEditor.putStringSet("Location", set);
    return localEditor.commit();
}
public Set<String> getLocation(){
	android.content.SharedPreferences localSharedPreferences = ctx.getSharedPreferences("New_Meeting", 0);
    return localSharedPreferences.getStringSet("Location", null);
	
}
public boolean addAmount(String amount){
	android.content.SharedPreferences.Editor localEditor = ctx.getSharedPreferences("user_login_data", 0).edit();
    localEditor.putString("payment_amount",amount);
    return localEditor.commit();
}
public String getAmount(){
	android.content.SharedPreferences localSharedPreferences = ctx.getSharedPreferences("user_login_data", 0);
    return localSharedPreferences.getString("payment_amount", "");
	
}
public boolean addMeetingSaveData(String MeetingPk_Id,String MeetingName,String MeetingStartTime,String MeetingEndTime){
	android.content.SharedPreferences.Editor localEditor = ctx.getSharedPreferences("Created_Meeting_data", 0).edit();
    localEditor.putString("MeetingPk_Id",MeetingPk_Id );
    localEditor.putString("Meeting_Name",MeetingName);
    localEditor.putString("Meeting_StartTime",MeetingStartTime);
    localEditor.putString("Meeting_EndTime",MeetingEndTime);
    return localEditor.commit();
}

    public String getMeetingSaveData(String id){
        String h=null;
        android.content.SharedPreferences localSharedPreferences = ctx.getSharedPreferences("Created_Meeting_data", 0);
        h= localSharedPreferences.getString(id, "");
        return h;
    }

public boolean addserviceType(String type){
	android.content.SharedPreferences.Editor localEditor = ctx.getSharedPreferences("user_login_data", 0).edit();
    localEditor.putString("recharge_type",type);
    return localEditor.commit();
}
public String getserviceType(){
	android.content.SharedPreferences localSharedPreferences = ctx.getSharedPreferences("user_login_data", 0);
    return localSharedPreferences.getString("recharge_type", "");
	
}
public boolean addNumber(String number){
	android.content.SharedPreferences.Editor localEditor = ctx.getSharedPreferences("user_login_data", 0).edit();
    localEditor.putString("recharge_number",number);
    return localEditor.commit();
}
public String getNumber(){
	android.content.SharedPreferences localSharedPreferences = ctx.getSharedPreferences("user_login_data", 0);
    return localSharedPreferences.getString("recharge_number", "");
	
}
public boolean addOperator(String opp){
	
	android.content.SharedPreferences.Editor localEditor = ctx.getSharedPreferences("user_login_data", 0).edit();
    localEditor.putString("recharge_operator",opp);
    return localEditor.commit();
}
public String getOperator(){
	android.content.SharedPreferences localSharedPreferences = ctx.getSharedPreferences("user_login_data", 0);
    return localSharedPreferences.getString("recharge_operator", "");
	
}

public boolean validateEmail(String email){
	  if(email.matches("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")){
		  return true;
	  }else{
		 return false; 
	  }
	  
}
public void showMessage(String title, String message)
{
	  AlertDialog.Builder adb=new AlertDialog.Builder(ctx);
		adb.setTitle(title);
		adb.setMessage(message);
		
		adb.setNegativeButton("OK", null);
		adb.create();
		adb.show();
}
public void showASPXResponse(String title, String data){
	  
        AlertDialog.Builder adb=new AlertDialog.Builder(ctx);
      		adb.setTitle(title);
      		final WebView input = new WebView(ctx);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                                    LinearLayout.LayoutParams.MATCH_PARENT,
                                    LinearLayout.LayoutParams.MATCH_PARENT);
              input.setLayoutParams(lp);
              input.loadData(data,  "text/html; charset=UTF-8", null);
              adb.setView(input);
      		
      		
      		adb.setNegativeButton("OK", null);
      		adb.create();
      		adb.show();
}

	public boolean sessionStart(String email, String name, String uid, String clientid,String UserORAgent){
	
	android.content.SharedPreferences.Editor localEditor = ctx.getSharedPreferences("user_login_data", 0).edit();
    localEditor.putString("email",email);
    localEditor.putString("name",name);
    localEditor.putString("userid",uid);
    localEditor.putString("Clientid",clientid);
    localEditor.putString("UserOrAgent",UserORAgent);


  return localEditor.commit();
	
}
public String getData(String id){
	String h=null;
	android.content.SharedPreferences localSharedPreferences = ctx.getSharedPreferences("user_login_data", 0);
    h= localSharedPreferences.getString(id, "");
	return h;
}
public boolean isActive(){
	File data=new File(ctx.getFilesDir().getPath());
	
	if(data.exists()){
	android.content.SharedPreferences localSharedPreferences = ctx.getSharedPreferences("user_login_data", 0);
    String h= localSharedPreferences.getString("uid", "");
    
    if(h.matches("")){
    	return false;
    }else{
    	return true;
    }
    
    }else{
    	return data.exists();
    }
	
}
public boolean updateData(String id, String data){
	Boolean ret;
	if(isActive()){
		android.content.SharedPreferences.Editor localEditor = ctx.getSharedPreferences("user_login_data", 0).edit();
	      localEditor.putString(id,data);
	      ret=localEditor.commit();
	    		  
	}else{
		ret=false;
	}
	return ret;
}

}
