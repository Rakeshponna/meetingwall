package com.nextgen.meetingwall.utils;

import android.Manifest;
import android.Manifest.permission;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Build.VERSION_CODES;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.nextgen.meetingwall.R;
import com.nextgen.meetingwall.network.json.MeetingEmployeesResponse;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


@SuppressWarnings("deprecation")
public class Utils {

    public static int getJobs;
    public static boolean GUEST_LOGIN = false;
    public static String USER_ID = null;
    public static String SERACHTEXT = "";
    public static String JOBID = "";
    public static boolean RECORDVIDEO_CANCELLED = false;

    public static String getYearName(String yearString, Context context) {
        String convertedString = "";
        switch (yearString.toLowerCase()) {
            case "jan":
                convertedString = context.getString(R.string.jan_date_lbl);
                break;
            case "feb":
                convertedString = context.getString(R.string.feb_date_lbl);
                break;
            case "mar":
                convertedString = context.getString(R.string.mar_date_lbl);
                break;
            case "apr":
                convertedString = context.getString(R.string.apr_date_lbl);
                break;
            case "may":
                convertedString = context.getString(R.string.may_date_lbl);
                break;
            case "jun":
                convertedString = context.getString(R.string.jun_date_lbl);
                break;
            case "jul":
                convertedString = context.getString(R.string.jul_date_lbl);
                break;
            case "aug":
                convertedString = context.getString(R.string.aug_date_lbl);
                break;
            case "sep":
                convertedString = context.getString(R.string.sep_date_lbl);
                break;
            case "oct":
                convertedString = context.getString(R.string.oct_date_lbl);
                break;
            case "nov":
                convertedString = context.getString(R.string.nov_date_lbl);
                break;
            case "dec":
                convertedString = context.getString(R.string.dec_date_lbl);
                break;
        }
        return convertedString;
    }

    public static String convertTocalendardate(String strCurrentDate, Context context) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date newDate = null;
        try {
            newDate = format.parse(strCurrentDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        format = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
        String date = format.format(newDate);
        String str[] = date.split(" ");
        date = str[0] + " " + getYearName(str[1], context) + " " + str[2];
        return date;
    }


    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }
    public static int getColor(Context context, int id) {
        final int version = Build.VERSION.SDK_INT;
        if (version >= 23) {
            return ContextCompat.getColor(context, id);
        } else {
            return context.getResources().getColor(id);
        }
    }

    public static void printMessage(String tag, String message) {
        if (Constants.IS_LOG_ENABLED) {
            Log.e(tag, message);
        }
    }

    public static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static Spannable addCommentPerson(Activity activity, String inputText) {
        Spannable textField = new SpannableString(" " + inputText);
        textField.setSpan(new ForegroundColorSpan(activity.getResources().getColor(R.color.comment_person_color)), 0, textField.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return textField;
    }

    public static Spannable addAnswerCount(Activity activity, String inputText) {
        Spannable textField = new SpannableString(" " + inputText);
        textField.setSpan(new ForegroundColorSpan(activity.getResources().getColor(R.color.last_comment_text_color)), 0, textField.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return textField;
    }

    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }

        }
        return false;
    }


    public static void devicePermissions(final Activity context) {
        if ((ContextCompat.checkSelfPermission(context,
                permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                || (ContextCompat.checkSelfPermission(context,
                permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                || (ContextCompat.checkSelfPermission(context,
                permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
                || (ContextCompat.checkSelfPermission(context,
                permission.CAPTURE_VIDEO_OUTPUT) != PackageManager.PERMISSION_GRANTED)
                ) {
            ActivityCompat.requestPermissions
                    (context, new String[]{
                            permission.READ_EXTERNAL_STORAGE,
                            permission.WRITE_EXTERNAL_STORAGE,
                            permission.RECORD_AUDIO,
                            permission.CAMERA, permission.READ_CONTACTS
                    }, 0);
        }
    }

    public static void devicePermissionsContacts(final Activity context) {
        if ((ContextCompat.checkSelfPermission(context,
                permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                || (ContextCompat.checkSelfPermission(context,
                permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                || (ContextCompat.checkSelfPermission(context,
                permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED)

                ) {
            ActivityCompat.requestPermissions
                    (context, new String[]{
                            permission.READ_EXTERNAL_STORAGE,
                            permission.WRITE_EXTERNAL_STORAGE,
                            permission.CAMERA, permission.READ_CONTACTS
                    }, 0);
        }
    }

    public static String formattimeToString(String datestring) {
        String resultStr = "";
        if (datestring != null) {
            String[] separated = datestring.split("T");
            if (separated.length == 2) {
                String[] timeseparated = separated[1].split(":");
                if (timeseparated.length == 3) {
                    resultStr = timeseparated[0] + ":" + timeseparated[1];
                }
            }
        }
        return resultStr;
    }


    /**
     * Convert timestamp to time zone format
     **/
    public static String dateFormatToDateTime(String dateTime) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM d yyyy hh:mmaaa", Locale.ENGLISH);
        SimpleDateFormat mdformat = new SimpleDateFormat("yyyy/MM/dd");
        Date date = null;
        try {
            date = dateFormat.parse(dateTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String timeInMin = mdformat.format(date.getTime());
        return timeInMin;
    }

    public static String dateFormatToDate(String dateTime) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM d yyyy hh:mmaaa", Locale.ENGLISH);
        SimpleDateFormat mdformat = new SimpleDateFormat("yyyy/MM/dd");
        Date date = null;
        try {
            date = mdformat.parse(dateTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String timeInMin = dateFormat.format(date.getTime());
        return timeInMin;
    }

    /**
     * convert timestamp to dd month yyyy format
     **/

    public static String formatTimeStampToDate(String dateTime) {

        DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
        SimpleDateFormat mdformat = new SimpleDateFormat("d MMM yyyy");
        Date date = null;
        try {
            date = originalFormat.parse(dateTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String formattedDate = mdformat.format(date.getTime());
        return formattedDate;
    }

    static final String DISPLAY_MESSAGE_ACTION = " com.happymindsms.happyminds.DISPLAY_MESSAGE";
    static final String EXTRA_MESSAGE = "message";

    public static void displayMessage(Context context, String message) {
        Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
        intent.putExtra(EXTRA_MESSAGE, message);
        context.sendBroadcast(intent);
    }


    public static byte[] convertImageToByte(Uri uri, Activity context) {
        byte[] data = null;
        try {
            ContentResolver cr = context.getContentResolver();
            InputStream inputStream = cr.openInputStream(uri);
            Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            data = baos.toByteArray();

//            String converted_txt = "";
//            for (int i = 0; i < data.length; i++) {
//                converted_txt = converted_txt + data[i];
//            }
//            Log.w("Image Conversion", converted_txt);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return data;
    }

    public static Bitmap convertByteImage(byte[] image, Context context) {
        // byte[] data = image.getBytes();
        Bitmap bmp = BitmapFactory.decodeByteArray(image, 0, image.length);
        // imageview.setImageBitmap(bmp);
        return bmp;
    }


    public static String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }


    public static String numberAsAmount(String amount) {
        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
        String numberAsString = numberFormat.format(Integer.parseInt(amount));
        return numberAsString;
    }

    public static File createDirIfNotExists(String path) {
        boolean ret = true;

        File file = new File("/sdcard", "HappyMinds");
        if (!file.exists()) {
            if (!file.mkdirs()) {
                //Log.e("TravellerLog :: ", "Problem creating Image folder");
                ret = false;
            }
        }
        Calendar cal = Calendar.getInstance();
        String fileName = "video_" + cal.getTimeInMillis() + ".mp4";
        File videoFile = new File(file, fileName);
        return videoFile;
    }

    public static File recordedVideoPath() {
        File file = new File("/sdcard", "HappyMinds");
        if (!file.exists()) {
            if (!file.mkdirs()) {
                //Log.e("TravellerLog :: ", "Problem creating Image folder");

            }
        }
        return file;
    }


    public static boolean videoMimeType(String types) {
        String[] type = new String[]{"video/msvideo", "video/avi", "video/x-msvideo", "video/mp4"};
        for (String mimne : type) {
            if (mimne.matches(types)) {
                return true;
            }
        }
        return false;
    }

    public static boolean imageMimeType(String types) {
        String[] type = new String[]{"image/jpeg", "image/png"};
        for (String mimne : type) {
            if (mimne.matches(types)) {
                return true;
            }
        }
        return false;
    }

    public static boolean fileMimeType(String types) {
        String[] type = new String[]{"application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/pdf", "application/rtf", "text/plain"};
        for (String mimne : type) {
            if (mimne.matches(types)) {
                return true;
            }
        }
        return false;
    }


    public static String getMimeType(String fileUrl) {
        String extension = MimeTypeMap.getFileExtensionFromUrl(fileUrl);
        return MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
    }

    /**
     * Get a file path from a Uri. This will get the the path for Storage Access
     * Framework Documents, as well as the _data field for the MediaStore and
     * other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri     The Uri to query.
     * @author paulburke
     */
    @TargetApi(VERSION_CODES.KITKAT)
    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            } else if ("content".equalsIgnoreCase(uri.getScheme())) {

                // Return the remote address
//                               if (isGoogleDriveUri(uri))
//                    return path;

                return getGDriveDataColumn(context, uri, null, null);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    private static String path(Uri _uri, Context context) {
        String filePath = null;
        Log.d("", "URI = " + _uri);
        if (_uri != null && "content".equals(_uri.getScheme())) {
            Cursor cursor = context.getContentResolver().query(_uri, new String[]{MediaStore.Files.FileColumns.DATA}, null, null, null);
            cursor.moveToFirst();
            filePath = cursor.getString(0);
            cursor.close();
        } else {
            filePath = _uri.getPath();
        }
        Log.d("", "Chosen path = " + filePath);
        return filePath;
    }


    public static String getGDriveDataColumn(Context context, Uri uri, String selection,
                                             String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "document_id";
        final String[] projection = {
                column
        };
        try {
            InputStream cur = context.getContentResolver().openInputStream(uri);

        } catch (Exception e) {
        }


        try {
            cursor = context.getContentResolver().query(uri, null, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;

    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    public static boolean isGoogleDriveUri(Uri uri) {
        return "com.google.android.apps.docs.storage".equals(uri.getAuthority());
    }

    public static void showToast(final String text, Activity getActivity) {
        final Activity activity = getActivity;
        if (activity != null) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(activity, text, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public static String[] imageURLArray = new String[]{
            "http://farm8.staticflickr.com/7315/9046944633_881f24c4fa_s.jpg",
            "http://farm4.staticflickr.com/3777/9049174610_bf51be8a07_s.jpg",
            "http://farm8.staticflickr.com/7324/9046946887_d96a28376c_s.jpg",
            "http://farm3.staticflickr.com/2828/9046946983_923887b17d_s.jpg",
            "http://farm4.staticflickr.com/3810/9046947167_3a51fffa0b_s.jpg",
            "http://farm4.staticflickr.com/3773/9049175264_b0ea30fa75_s.jpg",
            "http://farm4.staticflickr.com/3781/9046945893_f27db35c7e_s.jpg",
            "http://farm6.staticflickr.com/5344/9049177018_4621cb63db_s.jpg",
            "http://farm8.staticflickr.com/7307/9046947621_67e0394f7b_s.jpg",
            "http://farm6.staticflickr.com/5457/9046948185_3be564ac10_s.jpg",
            "http://farm4.staticflickr.com/3752/9046946459_a41fbfe614_s.jpg",
            "http://farm8.staticflickr.com/7403/9046946715_85f13b91e5_s.jpg",
            "http://farm8.staticflickr.com/7315/9046944633_881f24c4fa_s.jpg",
            "http://farm4.staticflickr.com/3777/9049174610_bf51be8a07_s.jpg",
            "http://farm8.staticflickr.com/7324/9046946887_d96a28376c_s.jpg",
            "http://farm3.staticflickr.com/2828/9046946983_923887b17d_s.jpg",
            "http://farm4.staticflickr.com/3810/9046947167_3a51fffa0b_s.jpg",
            "http://farm4.staticflickr.com/3773/9049175264_b0ea30fa75_s.jpg",
            "http://farm4.staticflickr.com/3781/9046945893_f27db35c7e_s.jpg",
            "http://farm6.staticflickr.com/5344/9049177018_4621cb63db_s.jpg",
            "http://farm8.staticflickr.com/7307/9046947621_67e0394f7b_s.jpg",
            "http://farm6.staticflickr.com/5457/9046948185_3be564ac10_s.jpg",
            "http://farm4.staticflickr.com/3752/9046946459_a41fbfe614_s.jpg",
            "http://farm8.staticflickr.com/7403/9046946715_85f13b91e5_s.jpg"};

    public static Object checkStringValue(String value) {
        if (value.equalsIgnoreCase("null")) {
            return JSONObject.NULL;
        } else {
            return value;
        }
    }

    public static String checkStringVal(String value) {
        if (value.equalsIgnoreCase("null")) {
            return "";
        } else {
            return value;
        }
    }


    public static GradientDrawable EmpoliesProfileResponce(Context context, MeetingEmployeesResponse meetingEmployeesResponse, GradientDrawable drawable, int i){

        if (meetingEmployeesResponse.getJqGridData().getRowsArray().get(i).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("A")) {
            drawable.setColor(context.getResources().getColor(R.color.Letter_A));
        } else if(meetingEmployeesResponse.getJqGridData().getRowsArray().get(i).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("B")) {
            drawable.setColor(context.getResources().getColor(R.color.Letter_B));
        }else if(meetingEmployeesResponse.getJqGridData().getRowsArray().get(i).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("C")) {
            drawable.setColor(context.getResources().getColor(R.color.Letter_C));
        }else if(meetingEmployeesResponse.getJqGridData().getRowsArray().get(i).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("D")) {
            drawable.setColor(context.getResources().getColor(R.color.Letter_D));
        }else if(meetingEmployeesResponse.getJqGridData().getRowsArray().get(i).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("E")) {
            drawable.setColor(context.getResources().getColor(R.color.Letter_E));
        }else if(meetingEmployeesResponse.getJqGridData().getRowsArray().get(i).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("F")) {
            drawable.setColor(context.getResources().getColor(R.color.Letter_F));
        }else if(meetingEmployeesResponse.getJqGridData().getRowsArray().get(i).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("G")) {
            drawable.setColor(context.getResources().getColor(R.color.Letter_G));
        }else if(meetingEmployeesResponse.getJqGridData().getRowsArray().get(i).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("H")) {
            drawable.setColor(context.getResources().getColor(R.color.Letter_H));
        }else if(meetingEmployeesResponse.getJqGridData().getRowsArray().get(i).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("I")) {
            drawable.setColor(context.getResources().getColor(R.color.Letter_I));
        }else if(meetingEmployeesResponse.getJqGridData().getRowsArray().get(i).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("J")) {
            drawable.setColor(context.getResources().getColor(R.color.Letter_J));
        }else if(meetingEmployeesResponse.getJqGridData().getRowsArray().get(i).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("K")) {
            drawable.setColor(context.getResources().getColor(R.color.Letter_K));
        }else if(meetingEmployeesResponse.getJqGridData().getRowsArray().get(i).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("L")) {
            drawable.setColor(context.getResources().getColor(R.color.Letter_L));
        }else if(meetingEmployeesResponse.getJqGridData().getRowsArray().get(i).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("M")) {
            drawable.setColor(context.getResources().getColor(R.color.Letter_M));
        }else if(meetingEmployeesResponse.getJqGridData().getRowsArray().get(i).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("N")) {
            drawable.setColor(context.getResources().getColor(R.color.Letter_N));
        }else if(meetingEmployeesResponse.getJqGridData().getRowsArray().get(i).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("O")) {
            drawable.setColor(context.getResources().getColor(R.color.Letter_O));
        }else if(meetingEmployeesResponse.getJqGridData().getRowsArray().get(i).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("P")) {
            drawable.setColor(context.getResources().getColor(R.color.Letter_P));
        }else if(meetingEmployeesResponse.getJqGridData().getRowsArray().get(i).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("Q")) {
            drawable.setColor(context.getResources().getColor(R.color.Letter_Q));
        }else if(meetingEmployeesResponse.getJqGridData().getRowsArray().get(i).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("R")) {
            drawable.setColor(context.getResources().getColor(R.color.Letter_R));
        }else if(meetingEmployeesResponse.getJqGridData().getRowsArray().get(i).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("S")) {
            drawable.setColor(context.getResources().getColor(R.color.Letter_S));
        }else if(meetingEmployeesResponse.getJqGridData().getRowsArray().get(i).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("T")) {
            drawable.setColor(context.getResources().getColor(R.color.Letter_T));
        }else if(meetingEmployeesResponse.getJqGridData().getRowsArray().get(i).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("U")) {
            drawable.setColor(context.getResources().getColor(R.color.Letter_U));
        }else if(meetingEmployeesResponse.getJqGridData().getRowsArray().get(i).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("V")) {
            drawable.setColor(context.getResources().getColor(R.color.Letter_V));
        }else if(meetingEmployeesResponse.getJqGridData().getRowsArray().get(i).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("W")) {
            drawable.setColor(context.getResources().getColor(R.color.Letter_W));
        }else if(meetingEmployeesResponse.getJqGridData().getRowsArray().get(i).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("X")) {
            drawable.setColor(context.getResources().getColor(R.color.Letter_X));
        }else if(meetingEmployeesResponse.getJqGridData().getRowsArray().get(i).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("Y")) {
            drawable.setColor(context.getResources().getColor(R.color.Letter_Y));
        }else if(meetingEmployeesResponse.getJqGridData().getRowsArray().get(i).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("Z")) {
            drawable.setColor(context.getResources().getColor(R.color.Letter_Z));
        }
        return drawable;
    }
}
