package com.nextgen.meetingwall.utils;

import com.nextgen.meetingwall.model.SearchLocationBeen;
import com.nextgen.meetingwall.model.SelectedLocationBeen;


import java.util.ArrayList;

public class Singleton {
    private static Singleton singleton = new Singleton();
    public ArrayList<SearchLocationBeen> selectedLocationBeenArrayList = new ArrayList<>();

    private Singleton() {
    }



    public static Singleton getInstance() {
        return singleton;
    }


}
