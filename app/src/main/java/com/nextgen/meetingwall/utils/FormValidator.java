package com.nextgen.meetingwall.utils;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.design.widget.TextInputLayout;
import android.util.Patterns;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.nextgen.meetingwall.R;

import java.util.regex.Matcher;


public class FormValidator {
    public static boolean validateEmail(@NonNull EditText emailEditText, @StringRes int errorResId, TextInputLayout textInputLayout) {
        String email = emailEditText.getText().toString();
        Matcher matcher = Patterns.EMAIL_ADDRESS.matcher(email);
        if (matcher.find()) {
            textInputLayout.setError(null);
            return true;
        } else {
            Context context = textInputLayout.getContext();
            textInputLayout.setError(context.getString(errorResId));
            return false;
        }
    }

    public static boolean notEmpty(@NonNull TextView inputEditText, @StringRes int errorResId, TextInputLayout textInputLayout) {
        String inputText = inputEditText.getText().toString();
        if (inputText.length() > 0) {
            textInputLayout.setError(null);
            return true;
        } else {
            Context context = textInputLayout.getContext();
            textInputLayout.setError(context.getString(errorResId));
            return false;
        }
    }

    public static boolean notEmptyEditText(@NonNull EditText inputEditText, @StringRes int errorResId, TextInputLayout textInputLayout) {
        String inputText = inputEditText.getText().toString();
        if (inputText.length() > 0) {
            textInputLayout.setError(null);
            return true;
        } else {
            Context context = textInputLayout.getContext();
            textInputLayout.setError(context.getString(errorResId));
            return false;
        }
    }

    public static boolean passwordValidate(@NonNull EditText inputEditText, @StringRes int errorResId, TextInputLayout textInputLayout) {
        String inputText = inputEditText.getText().toString();
        if (inputText.isEmpty()) {
            Context context = textInputLayout.getContext();
            textInputLayout.setError(context.getString(R.string.label_err_msg_field_notempty));
            return false;
        } else {
            if (inputText.length() < 4 || inputText.length() > 10) {
                Context context = textInputLayout.getContext();
                textInputLayout.setError(context.getString(errorResId));
                return false;
            } else {
                textInputLayout.setError(null);
                return true;
            }
        }



//        if (inputText.length() > 0) {
//            textInputLayout.setError(null);
//            return true;
//        } else {
//            /*
//              if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
//            passwordText.setError("between 4 and 10 alphanumeric characters");
//            valid = false;
//        } else {
//            passwordText.setError(null);
//        }
//            */
//
//
//
//        }
    }


    public static boolean mobileNumberLimit(@NonNull EditText inputEditText, @StringRes int errorResId, TextInputLayout textInputLayout) {
        if (inputEditText.getText().toString().length() < 10) {
            Context context = textInputLayout.getContext();
            textInputLayout.setError(context.getString(R.string.label_panlimit));
            return false;
        } else {
            textInputLayout.setError(null);
            return true;
        }
    }

    public static boolean validatePasswordLength(@NonNull EditText password, @StringRes int errorResId, TextInputLayout textInputLayout) {
        String passwordLength = password.getText().toString();
        Context context = textInputLayout.getContext();
        if (passwordLength.length() >= 6) {
            textInputLayout.setError(null);
            return true;
        } else {
            textInputLayout.setError(context.getString(errorResId));
            return false;
        }
    }
    public static boolean notSelectedLayout(String matches, @NonNull TextView textview, @StringRes int errorResId) {
        if (matches.equalsIgnoreCase(textview.getText().toString())) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean notEmpty(@NonNull Spinner inputEditText, @StringRes int errorResId) {
        String inputText = inputEditText.getSelectedItem().toString();

        if ("".equals(inputText)) {
            return false;
        } else {
            return true;
        }

    }


    public static boolean notEmptyTextView(String textString, @NonNull TextView inputText, @StringRes int errorResId) {
        if (textString.equalsIgnoreCase(inputText.getText().toString())) {
            return false;
        } else {
            return true;
        }

    }


    public static boolean inputValuesMatch(@NonNull EditText editText1, @NonNull EditText editText2,
                                           @StringRes int errorResId, TextInputLayout textInputLayout) {
        String text1 = editText1.getText().toString();
        String text2 = editText2.getText().toString();
        Context context = editText1.getContext();
        if (text1.equals(text2)) {
            textInputLayout.setError(null);
            return true;
        } else {
            textInputLayout.setError(context.getString(errorResId));
            return false;
        }
    }

    private void requestFocus(View view, Activity context) {
        if (view.requestFocus()) {
            context.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

}

