package com.nextgen.meetingwall.utils;

public class Constants {

    public static final String UPLOADRESUME = "http://139.59.61.185/MeetingServices/UploadRawFiles";
    public static final String FILE_ERR = "File error";
    public static final String ERROR_CODE_STATUS = " Error occurred! Http Status Code: ";

    public static final boolean IS_LOG_ENABLED = true;
    public static final String FRAGMENT_ID = "fragment_id";
    public static final String SELECTED_DAY = "day";
    public static final int SELECTED_POSTION = 0;
    public static final String SELECTED_MONTH = "month";
    public static final String SELECTED_YEAR = "year";
    public static final String FONT_ROBOTO_LIGHT = "fonts/Roboto-Light.ttf";
    public static final String FONT_ROBOTO_REGULAR = "fonts/Roboto-Regular.ttf";
    public static final String FONT_ROBOTO_MEDIUM = "fonts/Roboto-Medium.ttf";
    public static final String FONT_ROBOTO_BOLD = "fonts/Roboto-Bold.ttf";

    public static final String FAILED_RESPONSE = "Details not found";
    public static final String connectionProblem = "Something went wrong, Please try again later!";
    public static final String GCM_TOKEN = "gcm_token_id";

    public static String mConnectionUrl ="http://139.59.61.185/MeetingServices/";// "http://139.59.61.185/ServonovoServices/";//"http://192.168.0.67:8081/DynamicServices/";//

    public static final String IS_USER_ID = "user_id";
    public static final String IS_USERNAME = "username";
    public static final String JOBROLL = "job_Roll";
}
