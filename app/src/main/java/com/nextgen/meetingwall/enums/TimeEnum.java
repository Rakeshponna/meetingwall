package com.nextgen.meetingwall.enums;

/**
 * Created by Administrator on 20-04-2016.
 */
public enum TimeEnum {


        JANUARY("JAN",0), FEBRUARY("FEB",1), MARCH("MAR",2), APRIL("APR",3), MAY("MAY",4), JUNE("JUNE",5),
        JULY("JULY",6), AUGUST("AUG",7), SEPTEMBER("SEP",8), OCTOBER("OCT",9), NOVEMBER("NOV",10), DECEMBER("DEC",11);
    private String stringValue;
    private int intValue;
    TimeEnum(String month, int numaricmonth) {
        stringValue =month ;
        intValue = numaricmonth;

    }
    public String month() {
        return stringValue;
    }

    public int numaricmonth() {
        return intValue;
    }
}
