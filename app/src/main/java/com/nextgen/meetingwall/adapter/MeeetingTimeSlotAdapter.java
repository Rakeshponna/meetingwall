package com.nextgen.meetingwall.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.nextgen.meetingwall.R;
import com.nextgen.meetingwall.model.OthermeetingdetailsBeen;
import com.nextgen.meetingwall.model.TimeslotBeen;
import com.nextgen.meetingwall.ui.activity.MeetingTimeSlotActivity;
import com.nextgen.meetingwall.ui.dialog.EasyDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by cas on 21-03-2017.
 */

public class MeeetingTimeSlotAdapter extends BaseAdapter {


    private static final int SELECTED_COLOR = R.color.timeslot_color;
    private static final String OthersText = String.valueOf(R.string.Others_string_here);

    private Context mContext;
    private ArrayList<TimeslotBeen> mReferalBeanArrayList = null;


    public MeeetingTimeSlotAdapter(MeetingTimeSlotActivity context, ArrayList<TimeslotBeen> mArray) {
        this.mContext = context;
        this.mReferalBeanArrayList = mArray;
    }

    @Override
    public int getCount() {
        return mReferalBeanArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return mReferalBeanArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Holder mHolder;
        LayoutInflater layoutInflater;
        if (convertView == null) {
            layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_item, null);
            mHolder = new Holder();
            ButterKnife.bind(this, convertView);
            mHolder.TimeTextView = (TextView) convertView.findViewById(R.id.hourTV);
            mHolder.AmTextView = (TextView) convertView.findViewById(R.id.amTV);
            mHolder.MeetingslottimingTextView = (TextView) convertView.findViewById(R.id.Meeting_slot_timing);
            mHolder.MeetingslotMeetingTypeTextView = (TextView) convertView.findViewById(R.id.Meeting_slot_MeetingType);
            mHolder.MeetingslotOtherMeetingsTextView = (TextView) convertView.findViewById(R.id.Meeting_slot_OtherMeetings);
            mHolder.MeetingslotmeetingLocationTextView = (TextView) convertView.findViewById(R.id.Meeting_slot_meetingLocation);
            mHolder.MeetingslotmeetingLocationDetailsTextView = (TextView) convertView.findViewById(R.id.Meeting_slot_LocationsDetails);
            mHolder.BackgroudLayout=(LinearLayout)convertView.findViewById(R.id.time_slot_BackgroundLayout);
            mHolder.MeetingtimeSlotdetails=(LinearLayout)convertView.findViewById(R.id.Meeting_time_Slot_details);
            mHolder.MeetingSlotdetailslayout=(LinearLayout)convertView.findViewById(R.id.other_listmeeting_layout);
            mHolder.MeetingslotLocationLayout=(LinearLayout)convertView.findViewById(R.id.Meeting_location_layout);
            mHolder.MeetingslotTimeSlotLayout=(LinearLayout)convertView.findViewById(R.id.Meeting_time_Slot);


            convertView.setTag(mHolder);
        } else {
            mHolder = (Holder) convertView.getTag();
        }
        mHolder.TimeTextView.setText(mReferalBeanArrayList.get(position).getTimeSting());

        if(mReferalBeanArrayList.get(position).isSelectTime()){
            mHolder.MeetingslotTimeSlotLayout.setVisibility(View.VISIBLE);
        }else{
            mHolder.MeetingslotTimeSlotLayout.setVisibility(View.GONE);
        }
        if(mReferalBeanArrayList.get(position).isAmorPm()){
            mHolder. AmTextView.setVisibility( View.VISIBLE);
            mHolder.AmTextView.setText(mReferalBeanArrayList.get(position).getAmString());
        }else{
            mHolder. AmTextView.setVisibility( View.VISIBLE);
            mHolder.AmTextView.setText("");
        }

        if(mReferalBeanArrayList.get(position).isBackground()){
            mHolder.BackgroudLayout.setBackgroundResource(SELECTED_COLOR);
        }else{
            mHolder.BackgroudLayout.setBackgroundColor(Color.WHITE);
        }
        if(mReferalBeanArrayList.get(position).isShowMeetingDetails()){
            mHolder.MeetingtimeSlotdetails.setVisibility(View.VISIBLE);
            mHolder.MeetingslottimingTextView.setText(mReferalBeanArrayList.get(position).getMeetingTime());
            mHolder.MeetingslotMeetingTypeTextView.setText(mReferalBeanArrayList.get(position).getMeetingType());
            mHolder.MeetingslotOtherMeetingsTextView.setText(" & "+mReferalBeanArrayList.get(position).getOtherMeetingNumber()+" "+mContext.getResources().getString(R.string.Others_string_here));
            mHolder.MeetingslotOtherMeetingsTextView.setPaintFlags(mHolder.MeetingslotOtherMeetingsTextView.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
            mHolder.MeetingslotmeetingLocationTextView.setText(mReferalBeanArrayList.get(position).getMeetingLocation());
        }else{
            mHolder.MeetingtimeSlotdetails.setVisibility(View.GONE);
        }
        mHolder.MeetingSlotdetailslayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<OthermeetingdetailsBeen> Meetings = mReferalBeanArrayList.get(position).getMeetings();
                View easyView = ((MeetingTimeSlotActivity)mContext).getLayoutInflater().inflate(R.layout.layout_tip_content_horizontal, null);

                new EasyDialog(mContext)
                        .setLayout(easyView)
                        .setGravity(EasyDialog.GRAVITY_BOTTOM)
                        .setBackgroundColor(mContext.getResources().getColor(R.color.colorYellow))
                        .setLocationByAttachedView(mHolder.MeetingslotOtherMeetingsTextView)
                        .setAnimationTranslationShow(EasyDialog.DIRECTION_Y, 1000, 800, -100, -50, 50, 0)
                        .setAnimationTranslationDismiss(EasyDialog.DIRECTION_Y, 500, 0, 800)
                        .setAnimationAlphaShow(1000, 0.3f, 1.0f)
                        .setTouchOutsideDismiss(true)
                        .setMatchParent(false)
                        .setMarginLeftAndRight(24, 24)
                        .show();

                ListView listView = (ListView) easyView.findViewById(R.id.Other_meeting_listview);

                OtherMeeetingsListAdapter    mAdapter = new OtherMeeetingsListAdapter(((MeetingTimeSlotActivity)mContext),  Meetings);
                listView.setAdapter(mAdapter);
            }
        });
        mHolder.MeetingslotLocationLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<OthermeetingdetailsBeen> Meetings = mReferalBeanArrayList.get(position).getMeetings();
                View easyView = ((MeetingTimeSlotActivity)mContext).getLayoutInflater().inflate(R.layout.layout_tip_content_horizontal, null);

                new EasyDialog(mContext)
                        .setLayout(easyView)
                        .setGravity(EasyDialog.GRAVITY_BOTTOM)
                        .setBackgroundColor(mContext.getResources().getColor(R.color.colorYellow))
                        .setLocationByAttachedView(mHolder.MeetingslotmeetingLocationDetailsTextView)
                        .setAnimationTranslationShow(EasyDialog.DIRECTION_X, 350, 400, 0)
                        .setAnimationTranslationDismiss(EasyDialog.DIRECTION_X, 350, 0, 400)
                        .setAnimationAlphaShow(1000, 0.3f, 1.0f)
                        .setTouchOutsideDismiss(true)
                        .setMatchParent(false)
                        .setMarginLeftAndRight(24, 100)
                        .show();

                ListView listView = (ListView) easyView.findViewById(R.id.Other_meeting_listview);

                OtherMeeetingsLocationListAdapter    mAdapter = new OtherMeeetingsLocationListAdapter(((MeetingTimeSlotActivity)mContext),  Meetings);
                listView.setAdapter(mAdapter);
            }
        });

        return convertView;
    }

    public class Holder {

        TextView MeetingslotmeetingLocationDetailsTextView,TimeTextView,AmTextView,MeetingslotmeetingLocationTextView,MeetingslotMeetingTypeTextView,MeetingslottimingTextView,MeetingslotOtherMeetingsTextView;
        LinearLayout BackgroudLayout,MeetingtimeSlotdetails,MeetingslotLocationLayout,MeetingSlotdetailslayout,MeetingslotTimeSlotLayout;

    }

}
