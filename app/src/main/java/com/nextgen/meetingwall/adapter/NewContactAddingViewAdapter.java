package com.nextgen.meetingwall.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nextgen.meetingwall.R;
import com.nextgen.meetingwall.model.ContactAddingBeen;
import com.nextgen.meetingwall.model.UserOnlineStatusBean;
import com.nextgen.meetingwall.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NewContactAddingViewAdapter extends RecyclerView.Adapter<NewContactAddingViewAdapter.MyViewHolder> {

    private List<ContactAddingBeen> moviesList;
    private Context context;

    class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.Invitees_adding_Name_text) TextView ProfileNameTextview;
        @BindView(R.id.Invitees_adding_Manager_text) TextView ProfileDesignationTextview;
        @BindView(R.id.Invitees_adding_profile_ImageView) ImageView ProfileImageView;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }


    public NewContactAddingViewAdapter(List<ContactAddingBeen> moviesList, Context context) {
        this.moviesList = moviesList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.invites_adding_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        ArrayList<UserOnlineStatusBean> Image = moviesList.get(position).getImages();

        try {
            Picasso.with(context)
                    .load(Utils.imageURLArray[Integer.parseInt(Image.get(position).getImage())])
                    .into(holder.ProfileImageView);
            holder. ProfileImageView.setVisibility(View.VISIBLE);
            holder. ProfileNameTextview.setText(moviesList.get(position).getDesignReview());
            holder. ProfileDesignationTextview.setText(moviesList.get(position).getReviewDescription());

        } catch (final Exception e) {
            Log.e("TAG", "Json parsing error: " + e.getMessage());

        }





    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
