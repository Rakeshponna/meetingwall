package com.nextgen.meetingwall.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nextgen.meetingwall.R;
import com.nextgen.meetingwall.network.json.MeetingLocationsResponse;
import com.nextgen.meetingwall.ui.activity.MeetingLocationActivity;

import java.util.ArrayList;

import butterknife.ButterKnife;

/*
 * Created by cas on 21-03-2017.
 */

@SuppressWarnings("deprecation")
public class MeetingLocationAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<MeetingLocationsResponse.RowsBeen> mMeetingLocationsArrayList = null;



    public MeetingLocationAdapter(MeetingLocationActivity context, ArrayList<MeetingLocationsResponse.RowsBeen> mArray) {
        this.mContext = context;
        this.mMeetingLocationsArrayList = mArray;

    }

    @Override
    public int getCount() {
        return mMeetingLocationsArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return mMeetingLocationsArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Holder mHolder;
        LayoutInflater layoutInflater;
        if (convertView == null) {
            layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.lication_list_itemview, parent,false);
            mHolder = new Holder();
            ButterKnife.bind(this, convertView);
            mHolder.NationalityLocationTextView = (TextView) convertView.findViewById(R.id.Location_Textview);
            mHolder.LocationTickImageView = (ImageView) convertView.findViewById(R.id.location_tick_imageview);

            convertView.setTag(mHolder);
        } else {
            mHolder = (Holder) convertView.getTag();
        }
        mHolder.NationalityLocationTextView.setText(mMeetingLocationsArrayList.get(position).getName());
        mHolder.NationalityLocationTextView.setTextColor(mContext.getResources().getColor(R.color.black_color));
//        ArrayList<RoomLocationBeen> RoomsList= mReferalBeanArrayList.get(NationalityListPosition).getHotelListArray();
//
//        mHolder.NationalityLocationTextView.setText(RoomsList.get(position).getRoomLocation());
//        mHolder.NationalityLocationTextView.setTextColor(mContext.getResources().getColor(R.color.black_color));
//        if(position==0){
//            convertView.setBackgroundResource(R.drawable.countrylistviewselecterbackgroud);
//        }
//        if(RoomsList.get(position).getRoomSelected()){
//            mHolder.LocationTickImageView.setVisibility(View.GONE);
//        }else{
//            mHolder.LocationTickImageView.setVisibility(View.GONE);
//        }

        return convertView;
    }

    public class Holder {
        TextView NationalityLocationTextView;
        ImageView LocationTickImageView;
    }

}