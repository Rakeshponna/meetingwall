package com.nextgen.meetingwall.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nextgen.meetingwall.R;
import com.nextgen.meetingwall.listeners.OnCustomItemSelectListener;
import com.nextgen.meetingwall.network.json.MeetingListDataResponce;
import com.nextgen.meetingwall.network.json.MeetingLocationsResponse;

import java.util.ArrayList;

import butterknife.ButterKnife;

public class ProductsListAdapter extends BaseAdapter {
    private Activity activity = null;
    private ArrayList<MeetingListDataResponce.MeetinglistDataRowsBeen> forumQuestionsArrayList = null;
    private LayoutInflater inflater = null;
    private Context context;
    private OnCustomItemSelectListener onCustomItemSelectListener;

    public ProductsListAdapter(Context context, ArrayList<MeetingListDataResponce.MeetinglistDataRowsBeen> forumArrayList, OnCustomItemSelectListener onCustomItemSelectListener) {
        this.context = context;
        this.forumQuestionsArrayList = forumArrayList;
        this.onCustomItemSelectListener=onCustomItemSelectListener;

    }



    @Override
    public int getCount() {
        return forumQuestionsArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return forumQuestionsArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup Parent) {
        final Holder mHolder;
        LayoutInflater layoutInflater;
        if (convertView == null) {
            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.productslist_item, Parent, false);
            mHolder = new Holder();
               ButterKnife.bind(this, convertView);
            mHolder.SpareNameTextView = (TextView) convertView.findViewById(R.id.SpareName_TextView);
            mHolder.OrderNumberTextView = (TextView) convertView.findViewById(R.id.OrderNumber_TextView);
            convertView.setTag(mHolder);
        } else {
            mHolder = (Holder) convertView.getTag();
        }

        String FormData = forumQuestionsArrayList.get(position).getMeetingName();
        mHolder.SpareNameTextView.setText(forumQuestionsArrayList.get(position).getMeetingType());
        mHolder.OrderNumberTextView.setText(forumQuestionsArrayList.get(position).getMeetingEndTime());

       convertView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // TODO Auto-generated method stub

                onCustomItemSelectListener.onItemClick(position);


            }
        });

        return convertView;
    }

    public class Holder {
   //     @BindView(R.id.SpareName_TextView)
        TextView SpareNameTextView;
   //     @BindView(R.id.OrderNumber_TextView)
        TextView OrderNumberTextView;
    }


}
