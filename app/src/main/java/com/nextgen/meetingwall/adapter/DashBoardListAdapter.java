package com.nextgen.meetingwall.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nextgen.meetingwall.R;
import com.nextgen.meetingwall.ui.activity.DashBoardActivity;
import com.nextgen.meetingwall.model.DesignReviewMeetingBeen;

import java.util.ArrayList;

import butterknife.ButterKnife;


public class DashBoardListAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<DesignReviewMeetingBeen> mReferalBeanArrayList = null;
    private LayoutInflater inflater = null;
    RecyclerView.ViewHolder holder;

    public DashBoardListAdapter(DashBoardActivity context, ArrayList<DesignReviewMeetingBeen> mArray) {
        this.mContext = context;
        this.mReferalBeanArrayList = mArray;
    }

    @Override
    public int getCount() {
        return mReferalBeanArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return mReferalBeanArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder mHolder;
        LayoutInflater layoutInflater;
        if (convertView == null) {
            layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.view_recent_meetings, null);
            mHolder = new Holder();
            ButterKnife.bind(this, convertView);
            mHolder.headerTextView = (TextView) convertView.findViewById(R.id.header_text);
            mHolder.DateTextViewDescreption = (TextView) convertView.findViewById(R.id.date_text);
            mHolder.DayTextView = (TextView) convertView.findViewById(R.id.day_text);
            mHolder.TimeTextView = (TextView) convertView.findViewById(R.id.time_text);

            mHolder.imageView1 = (ImageView) convertView.findViewById(R.id.imageview);


            convertView.setTag(mHolder);
        } else {
            mHolder = (Holder) convertView.getTag();
        }
        if((mReferalBeanArrayList.get(position).getProfileImage().equals("time"))){
            mHolder.imageView1.setImageResource(R.drawable.timer);
        }else if((mReferalBeanArrayList.get(position).getProfileImage().equals("like"))){
            mHolder.imageView1.setImageResource(R.drawable.like);
        }else if((mReferalBeanArrayList.get(position).getProfileImage().equals("dislike"))){
            mHolder.imageView1.setImageResource(R.drawable.dislike);
        }
//        Picasso.with(mContext)
//                .load(Utils.imageURLArray[Integer.parseInt(mReferalBeanArrayList.get(position).getProfileImage())])
//                .into(mHolder.imageView1);
        mHolder. headerTextView.setText(mReferalBeanArrayList.get(position).getDesignReview());
        mHolder. DateTextViewDescreption.setText(mReferalBeanArrayList.get(position).getDate());
        mHolder. DayTextView.setText(mReferalBeanArrayList.get(position).getDay());
        mHolder.TimeTextView.setText(mReferalBeanArrayList.get(position).getTime());

        return convertView;
    }

    public class Holder {

        TextView headerTextView;
        TextView DateTextViewDescreption;
        TextView DayTextView;
        TextView TimeTextView;
        ImageView imageView1;
    }

}
