package com.nextgen.meetingwall.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nextgen.meetingwall.R;
import com.nextgen.meetingwall.model.DesignReviewMeetingBeen;
import com.nextgen.meetingwall.model.MeetingBeen;
import com.nextgen.meetingwall.model.MeetingDatesAscendingBean;
import com.nextgen.meetingwall.model.UserOnlineStatusBean;
import com.nextgen.meetingwall.utils.Utils;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DashBoardRecentMeetingsAdapter extends RecyclerView.Adapter<DashBoardRecentMeetingsAdapter.MyViewHolder> {

    private List<DesignReviewMeetingBeen> moviesList;
    private Context context;

    class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.header_text)
        TextView headerTextView;
        @BindView(R.id.date_text)
        TextView DateTextViewDescreption;
        @BindView(R.id.day_text)
        TextView DayTextView;
        @BindView(R.id.time_text)
        TextView TimeTextView;
        @BindView(R.id.imageview)
        ImageView imageView1;


        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);



        }
    }


    public DashBoardRecentMeetingsAdapter(List<DesignReviewMeetingBeen> moviesList, Context context) {
        this.moviesList = moviesList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_recent_meetings, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {


                   Picasso.with(context)
                    .load(Utils.imageURLArray[Integer.parseInt(moviesList.get(position).getProfileImage())])
                    .into(holder.imageView1);


        holder. headerTextView.setText(moviesList.get(position).getDesignReview());
        holder. DateTextViewDescreption.setText(moviesList.get(position).getDate());
        holder. DayTextView.setText(moviesList.get(position).getDay());
        holder.TimeTextView.setText(moviesList.get(position).getTime());

    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
