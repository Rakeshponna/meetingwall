package com.nextgen.meetingwall.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nextgen.meetingwall.R;
import com.nextgen.meetingwall.listeners.OnCustomItemSelectListener;
import com.nextgen.meetingwall.model.CalendarMeetingsBeen;
import com.nextgen.meetingwall.model.ContactAddingBeen;
import com.nextgen.meetingwall.model.UserOnlineStatusBean;
import com.nextgen.meetingwall.ui.activity.MainActivity;
import com.nextgen.meetingwall.ui.fragments.AllMeetingsFragment;
import com.nextgen.meetingwall.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by cas on 16-03-2017.
 */

public class CalendarMeetingDetailsAdapter extends BaseAdapter {
    private Activity activity = null;
    private ArrayList<CalendarMeetingsBeen> forumQuestionsArrayList = null;
    private LayoutInflater inflater = null;
    private Context context;
    private OnCustomItemSelectListener onCustomItemSelectListener;

    public CalendarMeetingDetailsAdapter(Context context, ArrayList<CalendarMeetingsBeen> forumArrayList,OnCustomItemSelectListener onCustomItemSelectListener) {
        this.context = context;
        this.forumQuestionsArrayList = forumArrayList;
        this.onCustomItemSelectListener=onCustomItemSelectListener;

    }



    @Override
    public int getCount() {
        return forumQuestionsArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return forumQuestionsArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup Parent) {
        final Holder mHolder;
        LayoutInflater layoutInflater;
        if (convertView == null) {
            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.calendar_meetingdetails_list_item, Parent, false);
            mHolder = new Holder();
            //   ButterKnife.bind(this, convertView);
            mHolder.MeetingTimeTextview = (TextView) convertView.findViewById(R.id.Meeting_time_text);
            mHolder.MeetingNameTextview = (TextView) convertView.findViewById(R.id.Meeting_Name_text);


            convertView.setTag(mHolder);
        } else {
            mHolder = (Holder) convertView.getTag();
        }

        mHolder.MeetingTimeTextview.setText(forumQuestionsArrayList.get(position).getMeetingTime());
        mHolder.MeetingNameTextview.setText(forumQuestionsArrayList.get(position).getMeetingName());

        convertView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // TODO Auto-generated method stub

                onCustomItemSelectListener.onItemClick(position);

//                forumQuestionsArrayList.remove(position);
//                notifyDataSetChanged();

            }
        });
        return convertView;
    }

    public class Holder {
        TextView MeetingTimeTextview;
        TextView MeetingNameTextview;

    }




}
