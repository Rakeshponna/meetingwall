package com.nextgen.meetingwall.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nextgen.meetingwall.R;
import com.nextgen.meetingwall.model.SearchLocationBeen;
import com.nextgen.meetingwall.ui.activity.MeetingTimeSlotActivity;

import java.util.ArrayList;

import butterknife.ButterKnife;

/**
 * Created by cas on 21-03-2017.
 */

public class NationalitylocationAdapter extends BaseAdapter {


    private static final int SELECTED_COLOR = R.color.timeslot_color;
    private static final String OthersText = String.valueOf(R.string.Others_string_here);

    private Context mContext;
    private ArrayList<SearchLocationBeen> mReferalBeanArrayList = null;


    public NationalitylocationAdapter(MeetingTimeSlotActivity context, ArrayList<SearchLocationBeen> mArray) {
        this.mContext = context;
        this.mReferalBeanArrayList = mArray;
    }


    @Override
    public int getCount() {
        return mReferalBeanArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return mReferalBeanArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Holder mHolder;
        LayoutInflater layoutInflater;
        if (convertView == null) {

            layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.lication_list_itemview, null);
            mHolder = new Holder();
            ButterKnife.bind(this, convertView);
            mHolder.NationalityLocationTextView = (TextView) convertView.findViewById(R.id.Location_Textview);



            convertView.setTag(mHolder);
        } else {
            mHolder = (Holder) convertView.getTag();
        }
        mHolder.NationalityLocationTextView.setText(mReferalBeanArrayList.get(position).getCountryLocation());

        if(position==0){
            convertView.setBackgroundResource(R.drawable.listviewselecterbackgroud);
        }

        return convertView;
    }

    public class Holder {

        TextView NationalityLocationTextView;


    }

}
