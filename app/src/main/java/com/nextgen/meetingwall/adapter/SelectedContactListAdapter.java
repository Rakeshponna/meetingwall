package com.nextgen.meetingwall.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nextgen.meetingwall.R;
import com.nextgen.meetingwall.listeners.OnCustomItemSelectListener;
import com.nextgen.meetingwall.model.ContactAddingBeen;
import com.nextgen.meetingwall.model.UserOnlineStatusBean;
import com.nextgen.meetingwall.network.json.MeetingEmployeesResponse;
import com.nextgen.meetingwall.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

@SuppressWarnings("deprecation")
public class SelectedContactListAdapter extends BaseAdapter {
    private Activity activity = null;
    private ArrayList<MeetingEmployeesResponse.RowsBeen> forumQuestionsArrayList = null;
    private LayoutInflater inflater = null;
    private Context context;
    private OnCustomItemSelectListener onCustomItemSelectListener;

    public SelectedContactListAdapter(Context context, ArrayList<MeetingEmployeesResponse.RowsBeen> forumArrayList, OnCustomItemSelectListener onCustomItemSelectListener) {
        this.context = context;
        this.forumQuestionsArrayList = forumArrayList;
        this.onCustomItemSelectListener=onCustomItemSelectListener;

    }



    @Override
    public int getCount() {
        return forumQuestionsArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return forumQuestionsArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup Parent) {
        final Holder mHolder;
        LayoutInflater layoutInflater;
        if (convertView == null) {
            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.contacts_listview_item, Parent, false);
            mHolder = new Holder();
            //   ButterKnife.bind(this, convertView);
            mHolder.ProfileNameTextview = (TextView) convertView.findViewById(R.id.Invitees_adding_Name_text);
            mHolder.ProfileDesignationTextview = (TextView) convertView.findViewById(R.id.Invitees_adding_Manager_text);
            mHolder.ProfileImageView = (TextView) convertView.findViewById(R.id.Invitees_adding_profile_ImageView);
            mHolder.InviteesAvailabulityTextviewView = (TextView) convertView.findViewById(R.id.Invitees_availabulity_TextView);
            mHolder.InviteesEPEITextviewView = (TextView) convertView.findViewById(R.id.Invitees_EPEI_TextView);
            mHolder.SelectedItemBackground = (LinearLayout) convertView.findViewById(R.id.Selected_layout_backgroud);
            mHolder.InviteesdeleteImageView = (ImageView) convertView.findViewById(R.id.Invitees_delete_ImageView);

            convertView.setTag(mHolder);
        } else {
            mHolder = (Holder) convertView.getTag();
        }
   //     ArrayList<UserOnlineStatusBean> Image = forumQuestionsArrayList.get(position).getImages();

        try {
//            Picasso.with(context)
//                    .load(Utils.imageURLArray[Integer.parseInt(Image.get(position).getImage())])
//                    .into(mHolder.ProfileImageView);
            mHolder.ProfileImageView.setBackgroundResource(R.drawable.tags_rounded_corners);
            GradientDrawable drawable = (GradientDrawable) mHolder.ProfileImageView.getBackground();
            if (forumQuestionsArrayList.get(position).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("A")) {
                drawable.setColor(context.getResources().getColor(R.color.Letter_A));
            } else if(forumQuestionsArrayList.get(position).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("B")) {
                drawable.setColor(context.getResources().getColor(R.color.Letter_B));
            }else if(forumQuestionsArrayList.get(position).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("C")) {
                drawable.setColor(context.getResources().getColor(R.color.Letter_C));
            }else if(forumQuestionsArrayList.get(position).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("D")) {
                drawable.setColor(context.getResources().getColor(R.color.Letter_D));
            }else if(forumQuestionsArrayList.get(position).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("E")) {
                drawable.setColor(context.getResources().getColor(R.color.Letter_E));
            }else if(forumQuestionsArrayList.get(position).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("F")) {
                drawable.setColor(context.getResources().getColor(R.color.Letter_F));
            }else if(forumQuestionsArrayList.get(position).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("G")) {
                drawable.setColor(context.getResources().getColor(R.color.Letter_G));
            }else if(forumQuestionsArrayList.get(position).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("H")) {
                drawable.setColor(context.getResources().getColor(R.color.Letter_H));
            }else if(forumQuestionsArrayList.get(position).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("I")) {
                drawable.setColor(context.getResources().getColor(R.color.Letter_I));
            }else if(forumQuestionsArrayList.get(position).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("J")) {
                drawable.setColor(context.getResources().getColor(R.color.Letter_J));
            }else if(forumQuestionsArrayList.get(position).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("K")) {
                drawable.setColor(context.getResources().getColor(R.color.Letter_K));
            }else if(forumQuestionsArrayList.get(position).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("L")) {
                drawable.setColor(context.getResources().getColor(R.color.Letter_L));
            }else if(forumQuestionsArrayList.get(position).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("M")) {
                drawable.setColor(context.getResources().getColor(R.color.Letter_M));
            }else if(forumQuestionsArrayList.get(position).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("N")) {
                drawable.setColor(context.getResources().getColor(R.color.Letter_N));
            }else if(forumQuestionsArrayList.get(position).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("O")) {
                drawable.setColor(context.getResources().getColor(R.color.Letter_O));
            }else if(forumQuestionsArrayList.get(position).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("P")) {
                drawable.setColor(context.getResources().getColor(R.color.Letter_P));
            }else if(forumQuestionsArrayList.get(position).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("Q")) {
                drawable.setColor(context.getResources().getColor(R.color.Letter_Q));
            }else if(forumQuestionsArrayList.get(position).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("R")) {
                drawable.setColor(context.getResources().getColor(R.color.Letter_R));
            }else if(forumQuestionsArrayList.get(position).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("S")) {
                drawable.setColor(context.getResources().getColor(R.color.Letter_S));
            }else if(forumQuestionsArrayList.get(position).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("T")) {
                drawable.setColor(context.getResources().getColor(R.color.Letter_T));
            }else if(forumQuestionsArrayList.get(position).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("U")) {
                drawable.setColor(context.getResources().getColor(R.color.Letter_U));
            }else if(forumQuestionsArrayList.get(position).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("V")) {
                drawable.setColor(context.getResources().getColor(R.color.Letter_V));
            }else if(forumQuestionsArrayList.get(position).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("W")) {
                drawable.setColor(context.getResources().getColor(R.color.Letter_W));
            }else if(forumQuestionsArrayList.get(position).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("X")) {
                drawable.setColor(context.getResources().getColor(R.color.Letter_X));
            }else if(forumQuestionsArrayList.get(position).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("Y")) {
                drawable.setColor(context.getResources().getColor(R.color.Letter_Y));
            }else if(forumQuestionsArrayList.get(position).getEmployeeFirstName().substring(0,1).toUpperCase().equalsIgnoreCase("Z")) {
                drawable.setColor(context.getResources().getColor(R.color.Letter_Z));
            }

            mHolder.ProfileImageView.setText(forumQuestionsArrayList.get(position).getEmployeeFirstName().substring(0,1));
            mHolder.ProfileImageView.setVisibility(View.VISIBLE);
            mHolder.ProfileNameTextview.setText(forumQuestionsArrayList.get(position).getEmployeeFirstName());
            mHolder.ProfileDesignationTextview.setText(forumQuestionsArrayList.get(position).getEmployeeRole());

        } catch (final Exception e) {
            Log.e("TAG", "Json parsing error: " + e.getMessage());

        }
        mHolder.InviteesdeleteImageView.setVisibility(View.VISIBLE);
//        if(Image.get(position).isAvailable()){
//            mHolder.InviteesAvailabulityTextviewView.setText(context.getResources().getString(R.string.Available_Text));
//            mHolder.InviteesAvailabulityTextviewView.setTextColor(context.getResources().getColor(R.color.Available_TextView_color));
//        }else{
//            mHolder.InviteesAvailabulityTextviewView.setText(context.getResources().getString(R.string.NotAvailable_Text));
//            mHolder.InviteesAvailabulityTextviewView.setTextColor(context.getResources().getColor(R.color.NotAvailable_TextView_Color));
//        }

        if(position==0 || position==3){
            mHolder.InviteesEPEITextviewView.setText(forumQuestionsArrayList.get(position).getEmployeeNumber());
            mHolder.InviteesEPEITextviewView.setTextColor(context.getResources().getColor(R.color.NotAvailable_TextView_Color));
        }else if(position==1 ||position==4){
            mHolder.InviteesEPEITextviewView.setText(forumQuestionsArrayList.get(position).getEmployeeNumber());
            mHolder.InviteesEPEITextviewView.setTextColor(context.getResources().getColor(R.color.EPEI_TextView_color));
        }else{
            mHolder.InviteesEPEITextviewView.setText(forumQuestionsArrayList.get(position).getEmployeeNumber());
            mHolder.InviteesEPEITextviewView.setTextColor(context.getResources().getColor(R.color.Available_TextView_color));
        }

        mHolder.InviteesdeleteImageView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // TODO Auto-generated method stub

                onCustomItemSelectListener.onItemClick(position);

//                forumQuestionsArrayList.remove(position);
//                notifyDataSetChanged();

            }
        });

        return convertView;
    }

    public class Holder {
        TextView ProfileNameTextview,ProfileImageView;
        TextView ProfileDesignationTextview;
        ImageView InviteesdeleteImageView;
        TextView InviteesAvailabulityTextviewView;
        TextView InviteesEPEITextviewView;
        LinearLayout SelectedItemBackground;
    }


}
