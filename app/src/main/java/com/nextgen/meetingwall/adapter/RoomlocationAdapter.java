package com.nextgen.meetingwall.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nextgen.meetingwall.R;
import com.nextgen.meetingwall.model.RoomLocationBeen;
import com.nextgen.meetingwall.model.SearchLocationBeen;
import com.nextgen.meetingwall.model.UserOnlineStatusBean;
import com.nextgen.meetingwall.ui.activity.MeetingTimeSlotActivity;

import java.util.ArrayList;

import butterknife.ButterKnife;

/**
 * Created by cas on 21-03-2017.
 */

public class RoomlocationAdapter extends BaseAdapter {


    private static final int SELECTED_COLOR = R.color.timeslot_color;
    private static final String OthersText = String.valueOf(R.string.Others_string_here);

    private Context mContext;
    private ArrayList<SearchLocationBeen> mReferalBeanArrayList = null;
    private int NationalityListPosition;


    public RoomlocationAdapter(MeetingTimeSlotActivity context, ArrayList<SearchLocationBeen> mArray, int position) {
        this.mContext = context;
        this.mReferalBeanArrayList = mArray;
        this.NationalityListPosition=position;
    }

    @Override
    public int getCount() {
        return mReferalBeanArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return mReferalBeanArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Holder mHolder;
        LayoutInflater layoutInflater;
        if (convertView == null) {
            layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.lication_list_itemview, null);
            mHolder = new Holder();
            ButterKnife.bind(this, convertView);
            mHolder.NationalityLocationTextView = (TextView) convertView.findViewById(R.id.Location_Textview);
            mHolder.LocationTickImageView = (ImageView) convertView.findViewById(R.id.location_tick_imageview);



            convertView.setTag(mHolder);
        } else {
            mHolder = (Holder) convertView.getTag();
        }
        ArrayList<RoomLocationBeen> RoomsList= mReferalBeanArrayList.get(NationalityListPosition).getHotelListArray();

        mHolder.NationalityLocationTextView.setText(RoomsList.get(position).getRoomLocation());

        if(RoomsList.get(position).getRoomSelected()){
            mHolder.LocationTickImageView.setVisibility(View.VISIBLE);
        }else{
            mHolder.LocationTickImageView.setVisibility(View.GONE);
        }

        return convertView;
    }

    public class Holder {

        TextView NationalityLocationTextView;
        ImageView LocationTickImageView;


    }

}
