package com.nextgen.meetingwall.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.nextgen.meetingwall.R;
import com.nextgen.meetingwall.utils.Utils;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DashBoardGridViewAdapter extends BaseAdapter {

    private Context mContext;
    private final String[] gridViewString;
    private final int[] gridViewImageId;
    private LayoutInflater inflater;

    public DashBoardGridViewAdapter(Context context, String[] gridViewString, int[] gridViewImageId) {
        mContext = context;
        this.gridViewImageId = gridViewImageId;
        this.gridViewString = gridViewString;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return gridViewString.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        @SuppressLint("ViewHolder") View view = inflater.inflate(R.layout.dashboard_grid_view, parent, false);
        holder = new ViewHolder(view);


     /*   Picasso.with(inflater.getContext())
                .load(Utils.imageURLArray[position])
                .into(holder.image);*/

        holder.text.setText(gridViewString[position]);
        holder.image.setImageResource(gridViewImageId[position]);

        return view;
    }

    static class ViewHolder {
        @BindView(R.id.gridview_image)
        ImageView image;
        @BindView(R.id.gridview_text)
        TextView text;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}