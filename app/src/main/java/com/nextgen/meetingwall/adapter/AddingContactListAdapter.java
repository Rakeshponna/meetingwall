package com.nextgen.meetingwall.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.nextgen.meetingwall.R;
import com.nextgen.meetingwall.listeners.OnCustomItemSelectListener;
import com.nextgen.meetingwall.model.ContactAddingBeen;
import com.nextgen.meetingwall.model.UserOnlineStatusBean;
import com.nextgen.meetingwall.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AddingContactListAdapter extends BaseAdapter {
    private Activity activity = null;
    private ArrayList<ContactAddingBeen> forumQuestionsArrayList = null;
    private LayoutInflater inflater = null;
    private Context context;
    private OnCustomItemSelectListener onCustomItemSelectListener;

    public AddingContactListAdapter(Context context, ArrayList<ContactAddingBeen> forumArrayList,OnCustomItemSelectListener onCustomItemSelectListener) {
        this.context = context;
        this.forumQuestionsArrayList = forumArrayList;
        this.onCustomItemSelectListener=onCustomItemSelectListener;

    }



    @Override
    public int getCount() {
        return forumQuestionsArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return forumQuestionsArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup Parent) {
        final Holder mHolder;
        LayoutInflater layoutInflater;
        if (convertView == null) {
            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.invites_adding_item, Parent, false);
            mHolder = new Holder();
            //   ButterKnife.bind(this, convertView);
            mHolder.ProfileNameTextview = (TextView) convertView.findViewById(R.id.Invitees_adding_Name_text);
            mHolder.ProfileDesignationTextview = (TextView) convertView.findViewById(R.id.Invitees_adding_Manager_text);
            mHolder.ProfileImageView = (ImageView) convertView.findViewById(R.id.Invitees_adding_profile_ImageView);
            mHolder.InviteesDeleteImageView = (ImageView) convertView.findViewById(R.id.Invitees_delete_ImageView);

            convertView.setTag(mHolder);
        } else {
            mHolder = (Holder) convertView.getTag();
        }
        ArrayList<UserOnlineStatusBean> Image = forumQuestionsArrayList.get(position).getImages();

        try {
            Picasso.with(context)
                    .load(Utils.imageURLArray[Integer.parseInt(Image.get(position).getImage())])
                    .into(mHolder.ProfileImageView);
            mHolder.ProfileImageView.setVisibility(View.VISIBLE);
            mHolder.ProfileNameTextview.setText(forumQuestionsArrayList.get(position).getDesignReview());
            mHolder.ProfileDesignationTextview.setText(forumQuestionsArrayList.get(position).getReviewDescription());

        } catch (final Exception e) {
            Log.e("TAG", "Json parsing error: " + e.getMessage());

        }

        mHolder.InviteesDeleteImageView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // TODO Auto-generated method stub

                onCustomItemSelectListener.onItemClick(position);

//                forumQuestionsArrayList.remove(position);
//                notifyDataSetChanged();

            }
        });

        return convertView;
    }

    public class Holder {
        TextView ProfileNameTextview;
        TextView ProfileDesignationTextview;
        ImageView ProfileImageView, InviteesDeleteImageView;
    }


}
