package com.nextgen.meetingwall.adapter;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nextgen.meetingwall.R;
import com.nextgen.meetingwall.listeners.OnCustomItemSelectListener;
import com.nextgen.meetingwall.model.MeetingDatesAscendingBean;
import com.nextgen.meetingwall.network.json.MeetingEmployeesResponse;
import com.nextgen.meetingwall.network.json.MeetingListDataResponce;
import com.nextgen.meetingwall.network.json.MeetingLocationsResponse;
import com.nextgen.meetingwall.utils.Utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MeetingsViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int TYPE_MOVIE = 0;
    private final int TYPE_LOAD = 1;

    private OnLoadMoreListener loadMoreListener;
    private boolean isLoading = false, isMoreDataAvailable = true;

    private List<MeetingListDataResponce.MeetinglistDataRowsBeen> moviesList;
    private Context context;
    private OnCustomItemSelectListener listener;

    public MeetingsViewAdapter(ArrayList<MeetingListDataResponce.MeetinglistDataRowsBeen> moviesList, Context context, OnCustomItemSelectListener listener) {
        this.moviesList = moviesList;
        this.listener = listener;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        if(viewType==TYPE_MOVIE){
            return new MovieHolder(inflater.inflate(R.layout.view_design_review,parent,false));
        }else{
            return new LoadHolder(inflater.inflate(R.layout.progress_layout,parent,false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if(position>=getItemCount()-1 && isMoreDataAvailable && !isLoading && loadMoreListener!=null){
            isLoading = true;
            loadMoreListener.onLoadMore();
        }

        if(getItemViewType(position)==TYPE_MOVIE){
            ((MovieHolder)holder).bindData(moviesList.get(position),listener,context,position);
        }
        //No else part needed as load holder doesn't bind any data

    }
    @Override
    public int getItemViewType(int position) {
        if(moviesList.get(position)!=null){
            return TYPE_MOVIE;
        }else {
            return TYPE_LOAD;
        }

    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }


     /* VIEW HOLDERS */

    static class MovieHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.header_text)
        TextView headerTextView;
        @BindView(R.id.App_text_descreption)
        TextView AppTextDescreption;
        @BindView(R.id.Country_text)
        TextView CountryTextView;
        @BindView(R.id.date_text)
        TextView HeaderDateTextView;
        @BindView(R.id.person_count)
        TextView person_countTextView;
        @BindView(R.id.time_duration_text)
        TextView timeTextView;
        //        @BindView(R.id.online_status_profile1)
//        TextView onlineStatusProfile1;
//        @BindView(R.id.online_status_profile2)
//        TextView onlineStatusProfile2;
//        @BindView(R.id.online_status_profile3)
//        TextView onlineStatusProfile3;
//        @BindView(R.id.online_status_profile4)
//        TextView onlineStatusProfile4;
//        @BindView(R.id.Circle_profile_image1)
//        ImageView imageView1;
//        @BindView(R.id.Circle_profile_image2)
//        ImageView imageView2;
//        @BindView(R.id.Circle_profile_image3)
//        ImageView imageView3;
//        @BindView(R.id.Circle_profile_image4)
//        ImageView imageView4;
        @BindView(R.id.meeting_view_vertical_View)
        View VerticalView;
        @BindView(R.id.globe_gray_Imageview)
        ImageView GlobeGrayImageView;
        @BindView(R.id.RightArrow_Imageview)
        ImageView RightArrowImageView;
        @BindView(R.id.Meeting_Participant_profile_ImageView)
         TextView MeetingParticipantProfileImage;
        @BindView(R.id.Meeting_Participant_profile_ImageView2)
         TextView MeetingParticipantProfileImage2;
        @BindView(R.id.Meeting_Participant_profile_ImageView3)
         TextView MeetingParticipantProfileImage3;
        @BindView(R.id.Meeting_Participant_profile_ImageView4)
         TextView MeetingParticipantProfileImage4;

        private MovieHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bindData(MeetingListDataResponce.MeetinglistDataRowsBeen meetinglistDataRowsBeen, final OnCustomItemSelectListener listener, Context context, final int position){
            MeetingEmployeesResponse meetingEmployeesResponse = meetinglistDataRowsBeen.getMeetingEmployeesResponse();

            if(meetingEmployeesResponse.getJqGridData().getRowsArray().size()!=0){
                for(int i=0; i<meetingEmployeesResponse.getJqGridData().getRowsArray().size(); i++){
                    if(i==0){
                        MeetingParticipantProfileImage.setBackgroundResource(R.drawable.tags_rounded_corners);
                        GradientDrawable drawable = (GradientDrawable) MeetingParticipantProfileImage.getBackground();
                        Utils.EmpoliesProfileResponce(context,meetingEmployeesResponse,drawable,i);
                        MeetingParticipantProfileImage.setText(meetingEmployeesResponse.getJqGridData().getRowsArray().get(i).getEmployeeFirstName().substring(0,1));
                        MeetingParticipantProfileImage.setVisibility(View.VISIBLE);
                    }else if(i==1){
                        MeetingParticipantProfileImage2.setBackgroundResource(R.drawable.tags_rounded_corners);
                        GradientDrawable drawable = (GradientDrawable) MeetingParticipantProfileImage2.getBackground();
                        Utils.EmpoliesProfileResponce(context,meetingEmployeesResponse,drawable, i);
                        MeetingParticipantProfileImage2.setText(meetingEmployeesResponse.getJqGridData().getRowsArray().get(i).getEmployeeFirstName().substring(0,1));
                        MeetingParticipantProfileImage2.setVisibility(View.VISIBLE);
                    }else if(i==2){
                        MeetingParticipantProfileImage3.setBackgroundResource(R.drawable.tags_rounded_corners);
                        GradientDrawable drawable = (GradientDrawable) MeetingParticipantProfileImage3.getBackground();
                        Utils.EmpoliesProfileResponce(context,meetingEmployeesResponse,drawable, i);
                        MeetingParticipantProfileImage3.setText(meetingEmployeesResponse.getJqGridData().getRowsArray().get(i).getEmployeeFirstName().substring(0,1));
                        MeetingParticipantProfileImage3.setVisibility(View.VISIBLE);
                    }else if(i==3){
                        MeetingParticipantProfileImage4.setBackgroundResource(R.drawable.tags_rounded_corners);
                        GradientDrawable drawable = (GradientDrawable) MeetingParticipantProfileImage4.getBackground();
                        Utils.EmpoliesProfileResponce(context,meetingEmployeesResponse,drawable, i);
                        MeetingParticipantProfileImage4.setText(meetingEmployeesResponse.getJqGridData().getRowsArray().get(i).getEmployeeFirstName().substring(0,1));
                        MeetingParticipantProfileImage4.setVisibility(View.VISIBLE);
                    }else{
                        person_countTextView.setText("+" + meetingEmployeesResponse.getJqGridData().getRowsArray().size() + "");
                        person_countTextView.setVisibility(View.VISIBLE);
                    }
                }

            }else{
                MeetingParticipantProfileImage.setVisibility(View.GONE);
            }


//        try {
//            Picasso.with(context)
//                    .load(Utils.imageURLArray[Integer.parseInt(Image.get(0).getImage())])
//                    .into(holder.imageView1);
//            holder.imageView1.setVisibility(View.VISIBLE);
//            if (Image.get(0).isOnlineStatus()) {
//                holder.onlineStatusProfile1.setBackgroundResource(R.drawable.useronlinecircle);
//            } else {
//                holder.onlineStatusProfile1.setBackgroundResource(R.drawable.circlelayout);
//            }
//            Picasso.with(context)
//                    .load(Utils.imageURLArray[Integer.parseInt(Image.get(1).getImage())])
//                    .into(holder.imageView2);
//            holder.imageView2.setVisibility(View.VISIBLE);
//            if (Image.get(1).isOnlineStatus()) {
//                holder.onlineStatusProfile2.setBackgroundResource(R.drawable.useronlinecircle);
//            } else {
//                holder.onlineStatusProfile2.setBackgroundResource(R.drawable.circlelayout);
//            }
//            Picasso.with(context)
//                    .load(Utils.imageURLArray[Integer.parseInt(Image.get(2).getImage())])
//                    .into(holder.imageView3);
//            holder.imageView3.setVisibility(View.VISIBLE);
//            if (Image.get(2).isOnlineStatus()) {
//                holder.onlineStatusProfile3.setBackgroundResource(R.drawable.useronlinecircle);
//            } else {
//                holder.onlineStatusProfile3.setBackgroundResource(R.drawable.circlelayout);
//            }
//            Picasso.with(context)
//                    .load(Utils.imageURLArray[Integer.parseInt(Image.get(3).getImage())])
//                    .into(holder.imageView4);
//            if (Image.get(3).isOnlineStatus()) {
//                holder.onlineStatusProfile4.setBackgroundResource(R.drawable.useronlinecircle);
//            } else {
//                holder.onlineStatusProfile4.setBackgroundResource(R.drawable.circlelayout);
//            }
//            holder.imageView4.setVisibility(View.VISIBLE);
//            int imagecount = Image.size() - 4;
//            if (imagecount != 0) {
//                holder.person_countTextView.setText("+" + imagecount + "");
//                holder.person_countTextView.setVisibility(View.VISIBLE);
//            }
//
//
////            holder.itemView.setOnClickListener(new View.OnClickListener() {
////                @Override
////                public void onClick(View v) {
////
////
////                    holder.VerticalView.setBackgroundColor(Color.GREEN);
////                    holder.GlobeGrayImageView.setImageResource(R.drawable.globe_green);
////                    holder.RightArrowImageView.setImageResource(R.drawable.arrow_green);
////
////
////                }
////            });
//
//        } catch (final Exception e) {
//            Log.e("TAG", "Json parsing error: " + e.getMessage());
//
//        }
//        if (moviesList.get(position).isOnclick()) {
//            holder.VerticalView.setBackgroundColor(Color.GREEN);
//            holder.GlobeGrayImageView.setImageResource(R.drawable.globe_green);
//            holder.RightArrowImageView.setImageResource(R.drawable.arrow_green);
//        }else{
//            holder.GlobeGrayImageView.setImageResource(R.drawable.globe_gray);
//            holder.RightArrowImageView.setImageResource(R.drawable.arrow_gray);
//        }

            headerTextView.setText(meetinglistDataRowsBeen.getMeetingName());
            AppTextDescreption.setText(meetinglistDataRowsBeen.getMeetingType());
            MeetingLocationsResponse meetingLocationsResponse = meetinglistDataRowsBeen.getMeetingLocationsResponse();
            try{
                if(meetingLocationsResponse.getJqGridData().getRowsArray().size()!=0){
                    CountryTextView.setText(meetingLocationsResponse.getJqGridData().getRowsArray().get(0).getName());
                }else{
                    CountryTextView.setVisibility(View.GONE);
                }

            }catch (Exception e){
                e.printStackTrace();
            }

            if(meetinglistDataRowsBeen.getMeetingStartTime()!=null){
                String StartTime = meetinglistDataRowsBeen.getMeetingStartTime();
                String EndTime = meetinglistDataRowsBeen.getMeetingEndTime();


                DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss",Locale.getDefault());
                utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                DateFormat indianFormat = new SimpleDateFormat("hh:mm",Locale.getDefault());
                utcFormat.setTimeZone(TimeZone.getTimeZone("IST"));
                Date timestamp = null;
                Date timestampEndTime =null;
                try {
                    timestamp = utcFormat.parse(StartTime);
                    timestampEndTime =utcFormat.parse(EndTime);
                    String StartTimeoutput = indianFormat.format(timestamp);
                    String EndTimeoutput = indianFormat.format(timestampEndTime);
                    timeTextView.setText(StartTimeoutput+" - "+EndTimeoutput);
                } catch (ParseException e) {
                    e.printStackTrace();
                }


                MeetingDatesAscendingBean DateTime = meetinglistDataRowsBeen.getMeetingDatesAscendingBean();
                String date = new SimpleDateFormat("EEE MMM,dd", Locale.getDefault()).format(DateTime.getDateTime());

                if (DateTime.isdate()) {
                    HeaderDateTextView.setText(date);
                    HeaderDateTextView.setVisibility(View.VISIBLE);
                }else{
                    HeaderDateTextView.setText(date);
                    HeaderDateTextView.setVisibility(View.GONE);
                }
            }
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {

                        listener.onItemClick(position);
                    }
                }
            });
        }
    }

    private static class LoadHolder extends RecyclerView.ViewHolder{
        private LoadHolder(View itemView) {
            super(itemView);
        }
    }

    public void setMoreDataAvailable(boolean moreDataAvailable) {
        isMoreDataAvailable = moreDataAvailable;
    }

    /* notifyDataSetChanged is final method so we can't override it
         call adapter.notifyDataChanged(); after update the list
         */
    public void notifyDataChanged(){
        notifyDataSetChanged();
        isLoading = false;
    }


    public interface OnLoadMoreListener{
        void onLoadMore();
    }

    public void setLoadMoreListener(OnLoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }
}
