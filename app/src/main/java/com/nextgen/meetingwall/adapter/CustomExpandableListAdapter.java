package com.nextgen.meetingwall.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.nextgen.meetingwall.R;
import com.nextgen.meetingwall.model.ChildObject;
import com.nextgen.meetingwall.model.ParentObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by cas on 28-03-2017.
 */

public class CustomExpandableListAdapter extends BaseExpandableListAdapter {
    Context context;
    List<ParentObject> parentObjects;

    public CustomExpandableListAdapter(Context context, List<ParentObject> parentObjects)
    {
        this.context = context;
        this.parentObjects = parentObjects;
    }
    @Override
    public int getGroupCount() {
        return parentObjects.size();
    }

    //Cộng thêm 2 vào childrenCount.Row đầu tiên và row cuối cùng dẽ được sử làm header và footer
    //Add 2 to childcount. The first row and the last row are used as header and footer to childview
    @Override
    public int getChildrenCount(int i) {
        return parentObjects.get(i).childObjects.size() +1;
    }

    @Override
    public ParentObject getGroup(int i) {
        return parentObjects.get(i);
    }

    @Override
    public ChildObject getChild(int i, int i2) {
        return parentObjects.get(i).childObjects.get(i2);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i2) {
        return i2;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup)
    {
        ParentObject currentParent = parentObjects.get(i);
        if(view ==null)
        {
            LayoutInflater inflater =(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.expandable_list_group,null);
        }
        TextView AgendaNameTextView = (TextView)view.findViewById(R.id.Agenda_Name);
        TextView AgendaTimeTextView = (TextView)view.findViewById(R.id.Agenda_Time_text);

        AgendaNameTextView.setText(currentParent.mother.getAgendasRowsNameString()+" by "+currentParent.mother.getAgendasRowsEmployeeString());
        try {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss", Locale.getDefault());
            Date date;
            Date dt;
            date = df.parse(currentParent.mother.getAgendasRowsStartTimeString());
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
            SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm a", Locale.getDefault());
                String shortTimeStr = sdf.format(date);
                dt = sdf.parse(shortTimeStr);
                AgendaTimeTextView.setText(sdfs.format(dt));
        } catch (ParseException e) {
            e.printStackTrace();
        }


        return view;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean b, View view, ViewGroup viewGroup) {
        ParentObject currentParent = getGroup(groupPosition);
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //the first row is used as header
//        if(childPosition ==0)
//        {
//            view = inflater.inflate(R.layout.child_header, null);
//            TextView txtHeader = (TextView)view.findViewById(R.id.txtHeader);
//            txtHeader.setText(currentParent.textToHeader);
//        }

        //Here is the ListView of the ChildView
        if(childPosition ==0||childPosition>0 && childPosition<getChildrenCount(groupPosition)-1)
        {
            ChildObject currentChild = getChild(groupPosition,childPosition);
            view = inflater.inflate(R.layout.expandable_list_item,null);
            TextView txtChildName = (TextView)view.findViewById(R.id.expandedListItem);
            txtChildName.setText(currentChild.childName);

        }
        //the last row is used as footer
        if(childPosition == getChildrenCount(groupPosition)-1)
        {
            view = inflater.inflate(R.layout.expandable_list_child_footer,null);
            TextView txtFooter = (TextView)view.findViewById(R.id.txtFooter);

        }
        return view;
    }

    @Override
    public boolean isChildSelectable(int i, int i2) {
        return false;
    }
}
