package com.nextgen.meetingwall.listeners;

import android.view.Menu;

/**
 * Created by RAGHAVENDRA.D on 18-12-2015.
 */
public interface OnCustomItemSelectListener {



    public void onItemClick(int position);

    public void onSelectedItemClick(int position, int type);
}
