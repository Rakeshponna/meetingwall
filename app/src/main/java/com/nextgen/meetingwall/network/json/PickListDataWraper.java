package com.nextgen.meetingwall.network.json;

import com.google.gson.annotations.SerializedName;
import com.nextgen.meetingwall.model.FiltersObjectBeen;
import com.nextgen.meetingwall.model.ModuleListMetadataArray;

import java.util.List;

/**
 * Created by cas on 07-04-2017.
 */

public class PickListDataWraper {

    @SerializedName("first")
    private int FirstInt;
    @SerializedName("rows")
    private int RowsInt;
    @SerializedName("sortOrder")
    private int SortOrderInt;
    @SerializedName("PickListId")
    private int PickListId ;

    public int getFirstInt() {
        return FirstInt;
    }

    public void setFirstInt(int firstInt) {
        FirstInt = firstInt;
    }

    public int getRowsInt() {
        return RowsInt;
    }

    public void setRowsInt(int rowsInt) {
        RowsInt = rowsInt;
    }

    public int getSortOrderInt() {
        return SortOrderInt;
    }

    public void setSortOrderInt(int sortOrderInt) {
        SortOrderInt = sortOrderInt;
    }

    public int getPickListId() {
        return PickListId;
    }

    public void setPickListId(int pickListId) {
        PickListId = pickListId;
    }






}
