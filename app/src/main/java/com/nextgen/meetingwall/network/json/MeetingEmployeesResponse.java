package com.nextgen.meetingwall.network.json;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by cas on 07-04-2017.
 */

public class MeetingEmployeesResponse {


    public jqGridDataBeen getJqGridData() {
        return jqGridData;
    }

    public void setJqGridData(jqGridDataBeen jqGridData) {
        this.jqGridData = jqGridData;
    }



    public Boolean getSuccess() {
        return Success;
    }

    public void setSuccess(Boolean success) {
        Success = success;
    }

    @SerializedName("success")
    private Boolean Success;
    @SerializedName("jqGridData")
    private jqGridDataBeen jqGridData ;

    public class jqGridDataBeen {
        public int getRecords() {
            return Records;
        }

        public void setRecords(int records) {
            Records = records;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

        public ArrayList<RowsBeen> getRowsArray() {
            return RowsArray;
        }

        public void setRowsArray(ArrayList<RowsBeen> rowsArray) {
            RowsArray = rowsArray;
        }

        @SerializedName("records")
        private int Records;

        @SerializedName("total")
        private int total;

        @SerializedName("page")
        private int page;

        @SerializedName("size")
        private int size;

        @SerializedName("rows")
        private ArrayList<RowsBeen> RowsArray ;
    }

    public static class RowsBeen implements Parcelable {

        public RowsBeen(Parcel in) {
            EmployeePkId = in.readInt();
            EmployeeFirstName = in.readString();
            EmployeeEmail = in.readString();
            EmployeeLastLogIn = in.readString();
            EmployeeNumber = in.readString();
            EmployeeRole = in.readString();
            EmployeeRolepkid = in.readInt();
            EmployeeReportsTo = in.readString();
            EmployeeReportsTopk_id = in.readInt();
        }

        public static final Creator<RowsBeen> CREATOR = new Creator<RowsBeen>() {
            @Override
            public RowsBeen createFromParcel(Parcel in) {
                return new RowsBeen(in);
            }

            @Override
            public RowsBeen[] newArray(int size) {
                return new RowsBeen[size];
            }
        };

        public int getEmployeePkId() {
            return EmployeePkId;
        }

        public void setEmployeePkId(int employeePkId) {
            EmployeePkId = employeePkId;
        }

        public String getEmployeeFirstName() {
            return EmployeeFirstName;
        }

        public void setEmployeeFirstName(String employeeFirstName) {
            EmployeeFirstName = employeeFirstName;
        }

        public String getEmployeeEmail() {
            return EmployeeEmail;
        }

        public void setEmployeeEmail(String employeeEmail) {
            EmployeeEmail = employeeEmail;
        }

        public String getEmployeeLastLogIn() {
            return EmployeeLastLogIn;
        }

        public void setEmployeeLastLogIn(String employeeLastLogIn) {
            EmployeeLastLogIn = employeeLastLogIn;
        }

        public String getEmployeeNumber() {
            return EmployeeNumber;
        }

        public void setEmployeeNumber(String employeeNumber) {
            EmployeeNumber = employeeNumber;
        }

        public String getEmployeeRole() {
            return EmployeeRole;
        }

        public void setEmployeeRole(String employeeRole) {
            EmployeeRole = employeeRole;
        }

        public int getEmployeeRolepkid() {
            return EmployeeRolepkid;
        }

        public void setEmployeeRolepkid(int employeeRolepkid) {
            EmployeeRolepkid = employeeRolepkid;
        }

        public String getEmployeeReportsTo() {
            return EmployeeReportsTo;
        }

        public void setEmployeeReportsTo(String employeeReportsTo) {
            EmployeeReportsTo = employeeReportsTo;
        }

        public int getEmployeeReportsTopk_id() {
            return EmployeeReportsTopk_id;
        }

        public void setEmployeeReportsTopk_id(int employeeReportsTopk_id) {
            EmployeeReportsTopk_id = employeeReportsTopk_id;
        }

        @SerializedName("pk_id")
        private int EmployeePkId;
        @SerializedName("First Name")
        private String EmployeeFirstName;
        @SerializedName("Email")
        private String EmployeeEmail;
        @SerializedName("Last Log In")
        private String EmployeeLastLogIn;
        @SerializedName("Module Number")
        private String EmployeeNumber;
        @SerializedName("Role")
        private String EmployeeRole;
        @SerializedName("Role pk_id")
        private int EmployeeRolepkid;
        @SerializedName("Reports To")
        private String EmployeeReportsTo;
        @SerializedName("Reports To pk_id")
        private int EmployeeReportsTopk_id;

        public Boolean getSelected() {
            return isSelected;
        }

        public void setSelected(Boolean selected) {
            isSelected = selected;
        }

        private Boolean isSelected =false;


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(EmployeePkId);
            dest.writeString(EmployeeFirstName);
            dest.writeString(EmployeeEmail);
            dest.writeString(EmployeeLastLogIn);
            dest.writeString(EmployeeNumber);
            dest.writeString(EmployeeRole);
            dest.writeInt(EmployeeRolepkid);
            dest.writeString(EmployeeReportsTo);
            dest.writeInt(EmployeeReportsTopk_id);
        }
    }
}
