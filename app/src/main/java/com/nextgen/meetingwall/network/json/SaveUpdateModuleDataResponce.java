package com.nextgen.meetingwall.network.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by cas on 10-04-2017.
 */

public class SaveUpdateModuleDataResponce {

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public SaveUpdateModuleDataResponce.DataObject getDataObject() {
        return DataObject;
    }

    public void setDataObject(SaveUpdateModuleDataResponce.DataObject dataObject) {
        DataObject = dataObject;
    }

    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    private DataObject DataObject;

    public class DataObject implements Serializable {

        public String getSaveUpdate_pkId() {
            return SaveUpdate_pkId;
        }

        public void setSaveUpdate_pkId(String saveUpdate_pkId) {
            SaveUpdate_pkId = saveUpdate_pkId;
        }

        public Boolean getSaveUpdate_status() {
            return SaveUpdate_status;
        }

        public void setSaveUpdate_status(Boolean saveUpdate_status) {
            SaveUpdate_status = saveUpdate_status;
        }

        public errors getSaveUpdate_errors() {
            return SaveUpdate_errors;
        }

        public void setSaveUpdate_errors(errors saveUpdate_errors) {
            SaveUpdate_errors = saveUpdate_errors;
        }

        @SerializedName("pk_id")
        public String SaveUpdate_pkId;
        @SerializedName("status")
        public Boolean SaveUpdate_status;
        @SerializedName("errors")
        public errors SaveUpdate_errors;


        public class errors{

        }

    }


}
