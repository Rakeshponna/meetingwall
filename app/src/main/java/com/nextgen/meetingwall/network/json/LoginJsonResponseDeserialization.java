package com.nextgen.meetingwall.network.json;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by ASHUTOSH.N on 17-11-16.
 */
public class LoginJsonResponseDeserialization implements JsonDeserializer<LoginResponse> {

    @Override
    public LoginResponse deserialize(JsonElement je, Type type, JsonDeserializationContext jdc)
            throws JsonParseException {
        JsonElement content = null;
        LoginResponse wrapper = new LoginResponse();
        try {
            // Get the "content" element from the parsed JSON
            if (je.getAsJsonObject().has("success")) {
                content = je.getAsJsonObject().get("success");
                wrapper = new Gson().fromJson(content, type);
                wrapper.setSuccess(je.getAsJsonObject().get("success").getAsBoolean());
                wrapper.setMessage(je.getAsJsonObject().get("message").getAsString());




            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        // Deserialize it. You use a new instance of Gson to avoid infinite recursion
        // to this deserializer
        return wrapper;
    }
}
