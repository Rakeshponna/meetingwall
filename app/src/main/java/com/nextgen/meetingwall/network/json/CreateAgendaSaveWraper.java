package com.nextgen.meetingwall.network.json;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by cas on 12-04-2017.
 */

public class CreateAgendaSaveWraper {



    @SerializedName("from_module")
    private String FromModuleString;
    @SerializedName("from_db_table")
    private String FromDbTableString;
    @SerializedName("from_relation")
    private int FromRelationInt;
    @SerializedName("from_field_id")
    private int FromFieldIdInt;
    @SerializedName("from_block_id")
    private int FromBlockIdInt;
    @SerializedName("from_block")
    private String FromBlockString;
    @SerializedName("from_field")
    private String FromFieldString;
    @SerializedName("from_db_field")
    private String FromDbFieldString;
    @SerializedName("to_module")
    private String ToModuleString;
    @SerializedName("to_table")
    private String ToTableString;
    @SerializedName("to_relation")
    private int ToRelationInt;
    @SerializedName("to_field_id")
    private int ToFieldIdInt;
    @SerializedName("to_block_id")
    private int ToBlockIdInt;
    @SerializedName("to_block")
    private String ToBlockString;
    @SerializedName("to_field")
    private String ToFieldString;
    @SerializedName("to_db_field")
    private String ToDbFieldString;
    @SerializedName("xref_table_name")
    private String xrefTableNameString;
    @SerializedName("to_record_identifier")
    private String ToRecordIdentifier;
    @SerializedName("to_is_user")
    private int ToIsUserInt;
    @SerializedName("pk_id")
    private String pkIdString;
    @SerializedName("value")
    private ArrayList<valueRowsBeen> DataArray ;

    public ArrayList<valueRowsBeen> getDataArray() {
        return DataArray;
    }
    public void setDataArray(ArrayList<valueRowsBeen> rowsBeens) {
        DataArray = rowsBeens;
    }




    public String getFromModuleString() {
        return FromModuleString;
    }

    public void setFromModuleString(String fromModuleString) {
        FromModuleString = fromModuleString;
    }

    public String getFromDbTableString() {
        return FromDbTableString;
    }

    public void setFromDbTableString(String fromDbTableString) {
        FromDbTableString = fromDbTableString;
    }

    public int getFromRelationInt() {
        return FromRelationInt;
    }

    public void setFromRelationInt(int fromRelationInt) {
        FromRelationInt = fromRelationInt;
    }

    public int getFromFieldIdInt() {
        return FromFieldIdInt;
    }

    public void setFromFieldIdInt(int fromFieldIdInt) {
        FromFieldIdInt = fromFieldIdInt;
    }

    public int getFromBlockIdInt() {
        return FromBlockIdInt;
    }

    public void setFromBlockIdInt(int fromBlockIdInt) {
        FromBlockIdInt = fromBlockIdInt;
    }

    public String getFromBlockString() {
        return FromBlockString;
    }

    public void setFromBlockString(String fromBlockString) {
        FromBlockString = fromBlockString;
    }

    public String getFromFieldString() {
        return FromFieldString;
    }

    public void setFromFieldString(String fromFieldString) {
        FromFieldString = fromFieldString;
    }

    public String getFromDbFieldString() {
        return FromDbFieldString;
    }

    public void setFromDbFieldString(String fromDbFieldString) {
        FromDbFieldString = fromDbFieldString;
    }

    public String getToModuleString() {
        return ToModuleString;
    }

    public void setToModuleString(String toModuleString) {
        ToModuleString = toModuleString;
    }

    public String getToTableString() {
        return ToTableString;
    }

    public void setToTableString(String toTableString) {
        ToTableString = toTableString;
    }

    public int getToRelationInt() {
        return ToRelationInt;
    }

    public void setToRelationInt(int toRelationInt) {
        ToRelationInt = toRelationInt;
    }

    public int getToFieldIdInt() {
        return ToFieldIdInt;
    }

    public void setToFieldIdInt(int toFieldIdInt) {
        ToFieldIdInt = toFieldIdInt;
    }

    public int getToBlockIdInt() {
        return ToBlockIdInt;
    }

    public void setToBlockIdInt(int toBlockIdInt) {
        ToBlockIdInt = toBlockIdInt;
    }

    public String getToBlockString() {
        return ToBlockString;
    }

    public void setToBlockString(String toBlockString) {
        ToBlockString = toBlockString;
    }

    public String getToFieldString() {
        return ToFieldString;
    }

    public void setToFieldString(String toFieldString) {
        ToFieldString = toFieldString;
    }

    public String getToDbFieldString() {
        return ToDbFieldString;
    }

    public void setToDbFieldString(String toDbFieldString) {
        ToDbFieldString = toDbFieldString;
    }

    public String getXrefTableNameString() {
        return xrefTableNameString;
    }

    public void setXrefTableNameString(String xrefTableNameString) {
        this.xrefTableNameString = xrefTableNameString;
    }

    public String getPkIdString() {
        return pkIdString;
    }

    public void setPkIdString(String pkIdString) {
        this.pkIdString = pkIdString;
    }



    public String getToRecordIdentifier() {
        return ToRecordIdentifier;
    }

    public void setToRecordIdentifier(String toRecordIdentifier) {
        ToRecordIdentifier = toRecordIdentifier;
    }

    public int getToIsUserInt() {
        return ToIsUserInt;
    }

    public void setToIsUserInt(int toIsUserInt) {
        ToIsUserInt = toIsUserInt;
    }


    public static class valueRowsBeen{
        public int getValuePkIdInt() {
            return ValuePkIdInt;
        }

        public void setValuePkIdInt(int valuePkIdInt) {
            ValuePkIdInt = valuePkIdInt;
        }

        @SerializedName("pk_id")
        private int ValuePkIdInt;
    }

}
