package com.nextgen.meetingwall.network.json;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by cas on 21-04-2017.
 */

public class AddUpdateTagsWrapper {

    public Object[] getAddUpdateTagsArray() {
        return addUpdateTagsArray;
    }

    public void setAddUpdateTagsArray(Object[] addUpdateTagsArray) {
        this.addUpdateTagsArray = addUpdateTagsArray;
    }

    @SerializedName("tags")
    private Object[] addUpdateTagsArray ;

}
