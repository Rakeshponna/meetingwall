package com.nextgen.meetingwall.network.json;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/*
 * Created by cas on 07-04-2017.
 */

public class MeetingLocationsResponse {


    public jqGridDataBeen getJqGridData() {
        return jqGridData;
    }

    public void setJqGridData(jqGridDataBeen jqGridData) {
        this.jqGridData = jqGridData;
    }



    public Boolean getSuccess() {
        return Success;
    }

    public void setSuccess(Boolean success) {
        Success = success;
    }

    @SerializedName("success")
    private Boolean Success;
    @SerializedName("jqGridData")
    private jqGridDataBeen jqGridData ;

    public class jqGridDataBeen {
        public int getRecords() {
            return Records;
        }

        public void setRecords(int records) {
            Records = records;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

        public ArrayList<RowsBeen> getRowsArray() {
            return RowsArray;
        }

        public void setRowsArray(ArrayList<RowsBeen> rowsArray) {
            RowsArray = rowsArray;
        }

        @SerializedName("records")
        private int Records;

        @SerializedName("total")
        private int total;

        @SerializedName("page")
        private int page;

        @SerializedName("size")
        private int size;

        @SerializedName("rows")
        private ArrayList<RowsBeen> RowsArray ;
    }

    public static class RowsBeen implements Parcelable {

        @SerializedName("pk_id")
        private int PkId;
        @SerializedName("Name")
        private String Name;
        @SerializedName("Modified Time")
        private String ModifiedTime;
        @SerializedName("Modified By")
        private String ModifiedBy;
        @SerializedName("Modified By pk_id")
        private int ModifiedByPkId;
        @SerializedName("Location")
        private String Location;
        @SerializedName("Location pk_id")
        private int LocationpkId;

        protected RowsBeen(Parcel in) {
            PkId = in.readInt();
            Name = in.readString();
            ModifiedTime = in.readString();
            ModifiedBy = in.readString();
            ModifiedByPkId = in.readInt();
            Location = in.readString();
            LocationpkId = in.readInt();
        }

        public static final Creator<RowsBeen> CREATOR = new Creator<RowsBeen>() {
            @Override
            public RowsBeen createFromParcel(Parcel in) {
                return new RowsBeen(in);
            }

            @Override
            public RowsBeen[] newArray(int size) {
                return new RowsBeen[size];
            }
        };

        public String getLocation() {
            return Location;
        }

        public void setLocation(String location) {
            Location = location;
        }

        public int getLocationpkId() {
            return LocationpkId;
        }

        public void setLocationpkId(int locationpkId) {
            LocationpkId = locationpkId;
        }


        public int getPkId() {
            return PkId;
        }

        public void setPkId(int pkId) {
            PkId = pkId;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }


        public String getModifiedTime() {
            return ModifiedTime;
        }

        public void setModifiedTime(String modifiedTime) {
            ModifiedTime = modifiedTime;
        }



        public String getModifiedBy() {
            return ModifiedBy;
        }

        public void setModifiedBy(String modifiedBy) {
            ModifiedBy = modifiedBy;
        }

        public int getModifiedByPkId() {
            return ModifiedByPkId;
        }

        public void setModifiedByPkId(int modifiedByPkId) {
            ModifiedByPkId = modifiedByPkId;
        }


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(PkId);
            dest.writeString(Name);
            dest.writeString(ModifiedTime);
            dest.writeString(ModifiedBy);
            dest.writeInt(ModifiedByPkId);
            dest.writeString(Location);
            dest.writeInt(LocationpkId);
        }
    }
}
