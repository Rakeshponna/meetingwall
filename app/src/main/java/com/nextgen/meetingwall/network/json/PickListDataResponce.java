package com.nextgen.meetingwall.network.json;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by cas on 12-04-2017.
 */

public class PickListDataResponce {

    public jqGridDataBeen getJqGridData() {
        return jqGridData;
    }

    public void setJqGridData(jqGridDataBeen jqGridData) {
        this.jqGridData = jqGridData;
    }



    public Boolean getSuccess() {
        return Success;
    }

    public void setSuccess(Boolean success) {
        Success = success;
    }

    @SerializedName("success")
    private Boolean Success;
    @SerializedName("jqGridData")
    private jqGridDataBeen jqGridData ;

    public class jqGridDataBeen {
        public int getRecords() {
            return Records;
        }

        public void setRecords(int records) {
            Records = records;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

        public ArrayList<PicklistDataRowsBeen> getRowsArray() {
            return RowsArray;
        }

        public void setRowsArray(ArrayList<PicklistDataRowsBeen> rowsArray) {
            RowsArray = rowsArray;
        }

        @SerializedName("records")
        private int Records;

        @SerializedName("total")
        private int total;

        @SerializedName("page")
        private int page;

        @SerializedName("size")
        private int size;

        @SerializedName("rows")
        private ArrayList<PicklistDataRowsBeen> RowsArray ;
    }

    public class PicklistDataRowsBeen{

        @SerializedName("Id1")
        private int PicklistDataId1;
        @SerializedName("Meeting_Type")
        private String PicklistDataType;
        @SerializedName("Categories")
        private String PicklistDataCategories;

        public String getPicklistDataCategories() {
            return PicklistDataCategories;
        }

        public void setPicklistDataCategories(String picklistDataCategories) {
            PicklistDataCategories = picklistDataCategories;
        }




        public int getPicklistDataId1() {
            return PicklistDataId1;
        }

        public void setPicklistDataId1(int picklistDataId1) {
            PicklistDataId1 = picklistDataId1;
        }

        public String getPicklistDataType() {
            return PicklistDataType;
        }

        public void setPicklistDataType(String picklistDataType) {
            PicklistDataType = picklistDataType;
        }

        @Override
        public String toString() {
            if(PicklistDataType!=null){
                return PicklistDataType;
            }else{
                return PicklistDataCategories;
            }

        }

    }
}
