package com.nextgen.meetingwall.network.json;

import com.google.gson.annotations.SerializedName;

/**
 * Created by cas on 12-04-2017.
 */

public class CreateParticipantsResponce {

    public Boolean getSuccess() {
        return Success;
    }

    public void setSuccess(Boolean success) {
        Success = success;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    @SerializedName("success")
    private Boolean Success;
    @SerializedName("message")
    private String Message;
}
