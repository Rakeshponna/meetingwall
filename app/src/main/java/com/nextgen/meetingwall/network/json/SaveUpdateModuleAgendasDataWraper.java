package com.nextgen.meetingwall.network.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by cas on 07-04-2017.
 */

public class SaveUpdateModuleAgendasDataWraper {


    @SerializedName("Fields")
    private List<Object> displayColsArray ;
    @SerializedName("Module")
    private String Module;
    @SerializedName("Action")
    private String Action;



    public List<Object> getDisplayColsArray() {
        return displayColsArray;
    }

    public void setDisplayColsArray(List<Object> displayColsArray) {
        this.displayColsArray = displayColsArray;
    }

    public String getModule() {
        return Module;
    }

    public void setModule(String module) {
        Module = module;
    }

    public String getAction() {
        return Action;
    }

    public void setAction(String action) {
        Action = action;
    }







public static class FieldsdataArray {


    @SerializedName("name")
    private String Name = null;
    @SerializedName("db_field_name")
    private String db_field_name = null;
    @Expose
    @SerializedName("value")
    private String value ;
    @SerializedName("type")
    private String type = null;
    @SerializedName("block_pk_id")
    private int block_pk_id = 0;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDb_field_name() {
        return db_field_name;
    }

    public void setDb_field_name(String db_field_name) {
        this.db_field_name = db_field_name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getBlock_pk_id() {
        return block_pk_id;
    }

    public void setBlock_pk_id(int block_pk_id) {
        this.block_pk_id = block_pk_id;
    }


}




    public static class Location{

        @SerializedName("name")
        private String Name = null;
        @SerializedName("db_field_name")
        private String db_field_name = null;
        @Expose
        @SerializedName("value")
        private Employees employeesbean ;
        @SerializedName("type")
        private String type = null;
        @SerializedName("block_pk_id")
        private int block_pk_id = 0;
        @SerializedName("rel")
        private int rel = 0;


        public Employees getEmployeesbean() {
            return employeesbean;
        }

        public void setEmployeesbean(Employees employeesbean) {
            this.employeesbean = employeesbean;
        }



        public int getRel() {
            return rel;
        }

        public void setRel(int rel) {
            this.rel = rel;
        }



        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public String getDb_field_name() {
            return db_field_name;
        }

        public void setDb_field_name(String db_field_name) {
            this.db_field_name = db_field_name;
        }



        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public int getBlock_pk_id() {
            return block_pk_id;
        }

        public void setBlock_pk_id(int block_pk_id) {
            this.block_pk_id = block_pk_id;
        }

        public static class Employees{

            public int getEmployeePk_idInt() {
                return EmployeePk_idInt;
            }

            public void setEmployeePk_idInt(int employeePk_idInt) {
                EmployeePk_idInt = employeePk_idInt;
            }

            public String getEmployeeFirstNameString() {
                return EmployeeFirstNameString;
            }

            public void setEmployeeFirstNameString(String employeeFirstNameString) {
                EmployeeFirstNameString = employeeFirstNameString;
            }

            public String getEmployeeEmailString() {
                return EmployeeEmailString;
            }

            public void setEmployeeEmailString(String employeeEmailString) {
                EmployeeEmailString = employeeEmailString;
            }

            public String getEmployeeModuleNumberString() {
                return EmployeeModuleNumberString;
            }

            public void setEmployeeModuleNumberString(String employeeModuleNumberString) {
                EmployeeModuleNumberString = employeeModuleNumberString;
            }

            public String getEmployeeRoleString() {
                return EmployeeRoleString;
            }

            public void setEmployeeRoleString(String employeeRoleString) {
                EmployeeRoleString = employeeRoleString;
            }

            public int getEmployeeRolePk_idInt() {
                return EmployeeRolePk_idInt;
            }

            public void setEmployeeRolePk_idInt(int employeeRolePk_idInt) {
                EmployeeRolePk_idInt = employeeRolePk_idInt;
            }

            public String getEmployeeReportsToString() {
                return EmployeeReportsToString;
            }

            public void setEmployeeReportsToString(String employeeReportsToString) {
                EmployeeReportsToString = employeeReportsToString;
            }

            public int getEmployeeReportsToPk_idInt() {
                return EmployeeReportsToPk_idInt;
            }

            public void setEmployeeReportsToPk_idInt(int employeeReportsToPk_idInt) {
                EmployeeReportsToPk_idInt = employeeReportsToPk_idInt;
            }

            @SerializedName("pk_id")
            private int EmployeePk_idInt = 0;
            @SerializedName("First Name")
            private String EmployeeFirstNameString = null;
            @SerializedName("Email")
            private String EmployeeEmailString ;
            @SerializedName("Module Number")
            private String EmployeeModuleNumberString = null;
            @SerializedName("Role")
            private String EmployeeRoleString = null;
            @SerializedName("Role pk_id")
            private int EmployeeRolePk_idInt = 0;
            @SerializedName("Reports To")
            private String EmployeeReportsToString = null;
            @SerializedName("Reports To pk_id")
            private int EmployeeReportsToPk_idInt = 0;
        }
    }
}
