package com.nextgen.meetingwall.network;



import android.content.Context;

import com.nextgen.meetingwall.ui.activity.ImageUploadActivity;
import com.nextgen.meetingwall.utils.Constants;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.File;

@SuppressWarnings("deprecation")
public class UploadFileOrVideo {

    private int serverResponseCode;
    private JSONObject jsonResume;
    private String responceResume;
    private HttpParams httpParameters;
    public Context context;


    public String upload(final File file, String jobIid, String userId, ImageUploadActivity imageUploadActivity) {
        String responseString = null;

        httpParameters = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParameters, 180000);
        HttpConnectionParams.setSoTimeout(httpParameters, 180000);
        HttpClient httpclient = new DefaultHttpClient();

        HttpPost httppost = new HttpPost(Constants.UPLOADRESUME);
        httppost.setHeader("Cookie", String.valueOf(new AddCookiesInterceptor(imageUploadActivity)));
         try {
            MultipartEntity entity = new MultipartEntity();
            FileBody fileBody = new FileBody(file);
            entity.addPart("filename", fileBody);
            entity.addPart("name", new StringBody("uploadedFiles"));
            httppost.setEntity(entity);
            System.out.print("File name" + fileBody.getFilename());
            HttpResponse response = null;
            try {
                response = httpclient.execute(httppost);
            } catch (Exception e1) {
                e1.printStackTrace();
                responceResume = Constants.FILE_ERR;
            }
            HttpEntity r_entity = response.getEntity();

            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                // Server response
                responseString = EntityUtils.toString(r_entity);
                jsonResume = new JSONObject(responseString);
                responceResume = jsonResume.getString("Message");
            } else {
                responceResume = Constants.ERROR_CODE_STATUS + statusCode;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return responceResume;
    }

}

