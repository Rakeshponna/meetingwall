package com.nextgen.meetingwall.network.json;

import com.google.gson.annotations.SerializedName;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okio.BufferedSink;

/**
 * Created by cas on 26-04-2017.
 */

public class PkIdObject extends RequestBody {
    public int getPk_idInt() {
        return pk_idInt;
    }

    public void setPk_idInt(int pk_idInt) {
        this.pk_idInt = pk_idInt;
    }

    @SerializedName("pk_id")
    private int pk_idInt;

    @Override
    public MediaType contentType() {
        return null;
    }

    @Override
    public void writeTo(BufferedSink sink) throws IOException {

    }
}
