package com.nextgen.meetingwall.network.json;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class LoginResponse implements Serializable {


    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("Data")
    private DataObject DataObject;




    public class DataObject implements Serializable {

        @SerializedName("UserName")
        public String UserName;
        @SerializedName("roleId")
        public String roleId;
        @SerializedName("userId")
        public String userId;
        @SerializedName("UserObject")
        public UserObjectBean userObjectBean;

        public List<MenuObjectBean> getMenuObjectBean() {
            return menuObjectBean;
        }

        public void setMenuObjectBean(List<MenuObjectBean> menuObjectBean) {
            this.menuObjectBean = menuObjectBean;
        }

        @SerializedName("MenuObject")
        private List<MenuObjectBean> menuObjectBean ;
        //  public MenuObjectBean menuObjectBean;


    }
    public class UserObjectBean implements Serializable {
        @SerializedName("login_email")
        public String loginEmail;
        @SerializedName("user_type")
        public String userType;
        @SerializedName("phone")
        public String phone;
    }



    public class MenuObjectBean implements Serializable {

        @SerializedName("data")
        public DataObjectBean data;
        @SerializedName("menu")
        private List<MenuBean> menuBean ;



        public class DataObjectBean implements Serializable {

            @SerializedName("group_name")
            public String groupName;
            @SerializedName("description")
            public String description;
            @SerializedName("short_name")
            public String short_name;
            @SerializedName("icon_class")
            public String icon_class;

        }
        public class MenuBean implements Serializable {

            @SerializedName("data")
            public DataBeenObject data;
            @SerializedName("list")
            private List<ListObjectBean> listBean ;

            public class DataBeenObject implements Serializable {

                @SerializedName("name")
                public String name;
                @SerializedName("icon_class")
                public String iconclass;


            }
            public class ListObjectBean implements Serializable {

                @SerializedName("access")
                public String access;
                @SerializedName("module")
                public String module;
                @SerializedName("display_name")
                public String display_name;
                @SerializedName("type")
                public String type;
                @SerializedName("module_icon")
                public String module_icon;


            }
        }
    }

}
