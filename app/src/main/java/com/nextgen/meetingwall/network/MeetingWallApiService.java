package com.nextgen.meetingwall.network;



import com.nextgen.meetingwall.network.json.AddUpdateTagsResponce;
import com.nextgen.meetingwall.network.json.AddUpdateTagsWrapper;
import com.nextgen.meetingwall.network.json.AgendaMeetingEmployeeWraper;
import com.nextgen.meetingwall.network.json.AgendasListResponce;
import com.nextgen.meetingwall.network.json.AgendasListWraper;
import com.nextgen.meetingwall.network.json.CreateAgendaSaveWraper;
import com.nextgen.meetingwall.network.json.CreateLocatationsWraper;
import com.nextgen.meetingwall.network.json.CreateParticipantsResponce;
import com.nextgen.meetingwall.network.json.CreateParticipantsWraper;
import com.nextgen.meetingwall.network.json.EssentialsSaveUpdateModuleDataWrapper;
import com.nextgen.meetingwall.network.json.MeetingEmployeesResponse;
import com.nextgen.meetingwall.network.json.MeetingListDataResponce;
import com.nextgen.meetingwall.network.json.MeetingLocationsResponse;
import com.nextgen.meetingwall.network.json.MeetingLocationsWraper;
import com.nextgen.meetingwall.network.json.MeetingParticipantsWraper;
import com.nextgen.meetingwall.network.json.MeetingReviewResponse;
import com.nextgen.meetingwall.network.json.MeetingWraper;
import com.nextgen.meetingwall.model.UserLoginBeen;
import com.nextgen.meetingwall.network.json.LoginResponse;
import com.nextgen.meetingwall.network.json.MeetingsResponce;
import com.nextgen.meetingwall.network.json.PickListDataResponce;
import com.nextgen.meetingwall.network.json.PickListDataWraper;
import com.nextgen.meetingwall.network.json.SaveUpdateModuleAgendasDataWraper;
import com.nextgen.meetingwall.network.json.SaveUpdateModuleDataResponce;
import com.nextgen.meetingwall.network.json.SaveUpdateModuleDataWraper;


import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;


interface MeetingWallApiService {

    @Headers("access-control-allow-credentials: true")
    @POST("processLogin")
    Call<LoginResponse> login(@Body UserLoginBeen userLoginBeen);

    /*
     *  MeetingsTypes Services
     */
    @Headers("access-control-allow-credentials: true")
    @POST("GetAllPickListValuesBySearch")
    Call<PickListDataResponce> PickListData(@Body PickListDataWraper pickListDataWraper);

    /*
     *  Meetings  Services
     */
    @Headers("access-control-allow-credentials: true")
    @GET("getModuleListMetadata/Meetings")
    Call<MeetingsResponce> ModuleListMetadata();


    @Headers("access-control-allow-credentials: true")
    @POST("getModuleListData/Meetings")
    Call<MeetingListDataResponce> ModuleListData(@Body MeetingWraper sparesWraper);

    /*
    *  Meeting%20Places  Services
    */
    @Headers("access-control-allow-credentials: true")
    @GET("getModuleListMetadata/Meeting%20Places")
    Call<MeetingsResponce> MeetingPlaces();

    @Headers("access-control-allow-credentials: true")
    @POST("getModuleListData/Meeting%20Places")
    Call<MeetingLocationsResponse> getModuleListDataMeetingPlaces(@Body MeetingWraper sparesWraper);

    @Headers("access-control-allow-credentials: true")
    @POST("getModuleListData/Meeting%20Places")
    Call<MeetingLocationsResponse> getModuleListMeetingPlaces(@Body MeetingLocationsWraper sparesWraper);


    /*
     *  MeetingEmployees  Services
     */
    @Headers("access-control-allow-credentials: true")
    @GET("getModuleListMetadata/Employees")
    Call<MeetingsResponce> Employees();

    @Headers("access-control-allow-credentials: true")
    @POST("getModuleListData/Employees")
    Call<MeetingEmployeesResponse> getModuleListDataMeetingEmployees(@Body MeetingWraper meetingWraper);

    @Headers("access-control-allow-credentials: true")
    @POST("getModuleListData/Employees")
    Call<MeetingEmployeesResponse> getModuleListDataMeetingEmployeesAgenda(@Body AgendaMeetingEmployeeWraper meetingWraper);

    @Headers("access-control-allow-credentials: true")
    @POST("getModuleListData/Employees")
    Call<MeetingEmployeesResponse> MeetingParticipants(@Body MeetingParticipantsWraper meetingParticipantsWraper);


    /*
     *  MeetingAgendas  Services
     */

    @Headers("access-control-allow-credentials: true")
    @GET("getModuleListMetadata/Agendas")
    Call<MeetingsResponce> getModuleListMetadataAgendas();

    @Headers("access-control-allow-credentials: true")
    @POST("getModuleListData/Agendas")
    Call<AgendasListResponce> Agendas (@Body AgendasListWraper createLocatationsWraper);

    /*
     *  MeetingSaveUpdate & Agendas Services
     */

    @Headers("access-control-allow-credentials: true")
    @POST("SaveUpdateModuleData")
    Call<SaveUpdateModuleDataResponce> SaveUpdateModuleData(@Body SaveUpdateModuleDataWraper saveUpdateModuleDataWraper);

    @Headers("access-control-allow-credentials: true")
    @POST("SaveUpdateModuleData")
    Call<SaveUpdateModuleDataResponce> SaveUpdateAgendaModuleData(@Body SaveUpdateModuleAgendasDataWraper saveUpdateModuleDataWraper);


    @Headers("access-control-allow-credentials: true")
    @POST("SaveModuleRelationManyData")
    Call<CreateParticipantsResponce> CreateParticipants(@Body CreateParticipantsWraper createParticipantsWraper);

    @Headers("access-control-allow-credentials: true")
    @POST("SaveModuleRelationManyData")
    Call<CreateParticipantsResponce> CreateAgendas(@Body CreateAgendaSaveWraper createAgendaSaveWraper);

     @Headers("access-control-allow-credentials: true")
    @POST("SaveModuleRelationManyData")
    Call<CreateParticipantsResponce> CreateLocations (@Body CreateLocatationsWraper createLocatationsWraper);

    @Headers("access-control-allow-credentials: true")
    @POST("addUpdateTags/85/{id}")
    Call<AddUpdateTagsResponce> AddUpdateTags (@Path("id") int groupId, @Body AddUpdateTagsWrapper addUpdateTagsWrapper);


    @Headers("access-control-allow-credentials: true")
    @POST("SaveUpdateModuleData")
    Call<SaveUpdateModuleDataResponce> EssentialsSaveUpdateModuleData(@Body EssentialsSaveUpdateModuleDataWrapper essentialsSaveUpdateModuleDataWrapper);



    /*
   *  MeetingReview  Services
   */
    @Headers("access-control-allow-credentials: true")
    @GET("getModuleViewEditData/Meetings/{id}/full/view")
    Call<MeetingReviewResponse> MeetingReview(@Path("id") int groupId);
}