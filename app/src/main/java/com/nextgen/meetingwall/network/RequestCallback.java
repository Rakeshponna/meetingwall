package com.nextgen.meetingwall.network;

public interface RequestCallback<T>
{
    void onSuccess(T response);
     void onFailed(String message);

}

