package com.nextgen.meetingwall.network.json;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by cas on 25-04-2017.
 */

public class MeetingReviewResponse {



    public MeetingReviewResponseSuccessData getMeetingReviewResponseSuccessData() {
        return meetingReviewResponseSuccessData;
    }

    public void setMeetingReviewResponseSuccessData(MeetingReviewResponseSuccessData meetingReviewResponseSuccessData) {
        this.meetingReviewResponseSuccessData = meetingReviewResponseSuccessData;
    }

    public Boolean getMeetingReviewResponseSuccess() {
        return MeetingReviewResponseSuccess;
    }

    public void setMeetingReviewResponseSuccess(Boolean meetingReviewResponseSuccess) {
        MeetingReviewResponseSuccess = meetingReviewResponseSuccess;
    }

    @SerializedName("success")
    private Boolean MeetingReviewResponseSuccess;
    @SerializedName("data")
    private MeetingReviewResponseSuccessData meetingReviewResponseSuccessData;

    public static class MeetingReviewResponseSuccessData{
        public String getMeetingReviewResponseSuccessPk_id() {
            return MeetingReviewResponseSuccessPk_id;
        }

        public void setMeetingReviewResponseSuccessPk_id(String meetingReviewResponseSuccessPk_id) {
            MeetingReviewResponseSuccessPk_id = meetingReviewResponseSuccessPk_id;
        }

        public ArrayList<blocks> getMeetingReviewResponseSuccessblocks() {
            return MeetingReviewResponseSuccessblocks;
        }

        public void setMeetingReviewResponseSuccessblocks(ArrayList<blocks> meetingReviewResponseSuccessblocks) {
            MeetingReviewResponseSuccessblocks = meetingReviewResponseSuccessblocks;
        }

        @SerializedName("pk_id")
        private String MeetingReviewResponseSuccessPk_id;
        @SerializedName("blocks")
        private ArrayList<blocks> MeetingReviewResponseSuccessblocks ;

    }


    public static class blocks{
        public FieldObject getBlock() {
            return block;
        }

        public void setBlock(FieldObject block) {
            this.block = block;
        }

        @SerializedName("block")
        private FieldObject block ;

        public ArrayList<FieldObject> getFields() {
            return fields;
        }

        public void setFields(ArrayList<FieldObject> fields) {
            this.fields = fields;
        }

        @SerializedName("fields")
        private ArrayList<FieldObject> fields ;

    }

    public class FieldObject {
        public int getPk_id() {
            return pk_id;
        }

        public void setPk_id(int pk_id) {
            this.pk_id = pk_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }



        @SerializedName("pk_id")
        private int pk_id ;
        @SerializedName("name")
        private String name ;
        @SerializedName("type")
        private String type ;
        @SerializedName("value")
        private Object value ;

        public Object getValue() {
            return value;
        }

        public void setValue(Object value) {
            this.value = value;
        }


    }
    public class RelationData {
        @SerializedName("value")
        public String from_module;
        @SerializedName("value")
        public String from_db_table;
        @SerializedName("value")
        public int from_relation;
        @SerializedName("value")
        public int from_field_id;
        @SerializedName("value")
        public int from_block_id;
        @SerializedName("value")
        public String from_block;
        @SerializedName("value")
        public String from_field;
        @SerializedName("value")
        public String from_db_field;
        @SerializedName("value")
        public String to_module_icon;
        @SerializedName("value")
        public String to_module;
        @SerializedName("value")
        public String to_table;
        @SerializedName("value")
        public int to_relation;
        @SerializedName("value")
        public int to_field_id;
        @SerializedName("value")
        public int to_block_id;
        @SerializedName("value")
        public String to_block;
        @SerializedName("value")
        public String to_field;
        @SerializedName("value")
        public String to_db_field;
        @SerializedName("value")
        public String xref_table_name;
        @SerializedName("value")
        public int to_is_user;
    }


}
