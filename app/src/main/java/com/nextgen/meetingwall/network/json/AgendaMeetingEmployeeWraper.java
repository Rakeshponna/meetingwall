package com.nextgen.meetingwall.network.json;

import com.google.gson.annotations.SerializedName;
import com.nextgen.meetingwall.model.FiltersObjectBeen;
import com.nextgen.meetingwall.model.ModuleListMetadataArray;

import java.util.List;

/**
 * Created by cas on 07-04-2017.
 */

public class AgendaMeetingEmployeeWraper {

    public int getFirstInt() {
        return FirstInt;
    }

    public void setFirstInt(int firstInt) {
        FirstInt = firstInt;
    }

    public int getRowsInt() {
        return RowsInt;
    }

    public void setRowsInt(int rowsInt) {
        RowsInt = rowsInt;
    }

    public int getSortOrderInt() {
        return SortOrderInt;
    }

    public void setSortOrderInt(int sortOrderInt) {
        SortOrderInt = sortOrderInt;
    }

    public FiltersObjectBeen getFiltersObject() {
        return FiltersObject;
    }

    public void setFiltersObject(FiltersObjectBeen filtersObject) {
        FiltersObject = filtersObject;
    }

    public String getGlobalFilter() {
        return globalFilter;
    }

    public void setGlobalFilter(String globalFilter) {
        this.globalFilter = globalFilter;
    }

    public List<ModuleListMetadataArray> getDisplayColsArray() {
        return displayColsArray;
    }

    public void setDisplayColsArray(List<ModuleListMetadataArray> displayColsArray) {
        this.displayColsArray = displayColsArray;
    }

    @SerializedName("first")
    private int FirstInt;
    @SerializedName("rows")
    private int RowsInt;
    @SerializedName("sortOrder")
    private int SortOrderInt;
    @SerializedName("filters")
    private FiltersObjectBeen FiltersObject;
    @SerializedName("globalFilter")
    private String globalFilter = null;

    public OneRelBean getOneRel() {
        return OneRel;
    }

    public void setOneRel(OneRelBean oneRel) {
        OneRel = oneRel;
    }

    @SerializedName("OneRel")
    private OneRelBean OneRel;
    @SerializedName("displayCols")
    private List<ModuleListMetadataArray> displayColsArray ;

    public int getPageInt() {
        return PageInt;
    }

    public void setPageInt(int pageInt) {
        PageInt = pageInt;
    }

    @SerializedName("page")
    private int PageInt;

    public static class OneRelBean{
        public String getModule() {
            return module;
        }

        public void setModule(String module) {
            this.module = module;
        }

        public int getModule_id() {
            return module_id;
        }

        public void setModule_id(int module_id) {
            this.module_id = module_id;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        @SerializedName("module")
        private String module = null;
        @SerializedName("module_id")
        private int module_id = 0;
        @SerializedName("id")
        private int id = 0;
    }
}
