package com.nextgen.meetingwall.network.json;

import com.google.gson.annotations.SerializedName;
import com.nextgen.meetingwall.model.FiltersObjectBeen;
import com.nextgen.meetingwall.model.ModuleListMetadataArray;

import java.util.List;

/**
 * Created by cas on 07-04-2017.
 */

public class MeetingWraper {

    public int getFirstInt() {
        return FirstInt;
    }

    public void setFirstInt(int firstInt) {
        FirstInt = firstInt;
    }

    public int getRowsInt() {
        return RowsInt;
    }

    public void setRowsInt(int rowsInt) {
        RowsInt = rowsInt;
    }

    public int getSortOrderInt() {
        return SortOrderInt;
    }

    public void setSortOrderInt(int sortOrderInt) {
        SortOrderInt = sortOrderInt;
    }

    public FiltersObjectBeen getFiltersObject() {
        return FiltersObject;
    }

    public void setFiltersObject(FiltersObjectBeen filtersObject) {
        FiltersObject = filtersObject;
    }

    public String getGlobalFilter() {
        return globalFilter;
    }

    public void setGlobalFilter(String globalFilter) {
        this.globalFilter = globalFilter;
    }

    public List<ModuleListMetadataArray> getDisplayColsArray() {
        return displayColsArray;
    }

    public void setDisplayColsArray(List<ModuleListMetadataArray> displayColsArray) {
        this.displayColsArray = displayColsArray;
    }

    @SerializedName("first")
    private int FirstInt;
    @SerializedName("rows")
    private int RowsInt;
    @SerializedName("sortOrder")
    private int SortOrderInt;
    @SerializedName("filters")
    private FiltersObjectBeen FiltersObject;
    @SerializedName("globalFilter")
    private String globalFilter = null;
    @SerializedName("displayCols")
    private List<ModuleListMetadataArray> displayColsArray ;

    public int getPageInt() {
        return PageInt;
    }

    public void setPageInt(int pageInt) {
        PageInt = pageInt;
    }

    @SerializedName("page")
    private int PageInt;


}
