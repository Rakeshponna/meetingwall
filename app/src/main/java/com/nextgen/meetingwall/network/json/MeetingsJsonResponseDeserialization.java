package com.nextgen.meetingwall.network.json;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Created by ASHUTOSH.N on 17-11-16.
 */
public class MeetingsJsonResponseDeserialization implements JsonDeserializer<MeetingLocationsResponse> {

    @Override
    public MeetingLocationsResponse deserialize(JsonElement je, Type type, JsonDeserializationContext jdc)
            throws JsonParseException {
        JsonElement content = null;
        MeetingLocationsResponse wrapper = new MeetingLocationsResponse();
        try {
            // Get the "content" element from the parsed JSON
            if (je.getAsJsonObject().has("success")) {
                content = je.getAsJsonObject().get("success");
                wrapper = new Gson().fromJson(content, type);
                wrapper.setSuccess(je.getAsJsonObject().get("success").getAsBoolean());
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        // Deserialize it. You use a new instance of Gson to avoid infinite recursion
        // to this deserializer
        return wrapper;
    }
}
