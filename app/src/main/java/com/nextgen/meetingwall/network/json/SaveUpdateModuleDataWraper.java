package com.nextgen.meetingwall.network.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by cas on 07-04-2017.
 */

public class SaveUpdateModuleDataWraper {



    @SerializedName("Module")
    private String Module;
    @SerializedName("Action")
    private String Action;
    @SerializedName("Fields")
    private List<Object> displayColsArray ;


    public List<Object> getDisplayColsArray() {
        return displayColsArray;
    }

    public void setDisplayColsArray(List<Object> displayColsArray) {
        this.displayColsArray = displayColsArray;
    }

    public String getModule() {
        return Module;
    }

    public void setModule(String module) {
        Module = module;
    }

    public String getAction() {
        return Action;
    }

    public void setAction(String action) {
        Action = action;
    }







public static class FieldsdataArray {


    @SerializedName("name")
    private String Name = null;
    @SerializedName("db_field_name")
    private String db_field_name = null;
    @Expose
    @SerializedName("value")
    private String value ;
    @SerializedName("type")
    private String type = null;
    @SerializedName("block_pk_id")
    private int block_pk_id = 0;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDb_field_name() {
        return db_field_name;
    }

    public void setDb_field_name(String db_field_name) {
        this.db_field_name = db_field_name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getBlock_pk_id() {
        return block_pk_id;
    }

    public void setBlock_pk_id(int block_pk_id) {
        this.block_pk_id = block_pk_id;
    }


}

    public static class Type{

        @SerializedName("name")
        private String Name = null;
        @SerializedName("db_field_name")
        private String db_field_name = null;
        @Expose
        @SerializedName("value")
        private TypeValueOfValue typeValueOfValue ;
        @SerializedName("type")
        private String type = null;
        @SerializedName("block_pk_id")
        private int block_pk_id = 0;

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public String getDb_field_name() {
            return db_field_name;
        }

        public void setDb_field_name(String db_field_name) {
            this.db_field_name = db_field_name;
        }

        public TypeValueOfValue getTypeValueOfValue() {
            return typeValueOfValue;
        }

        public void setTypeValueOfValue(TypeValueOfValue typeValueOfValue) {
            this.typeValueOfValue = typeValueOfValue;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public int getBlock_pk_id() {
            return block_pk_id;
        }

        public void setBlock_pk_id(int block_pk_id) {
            this.block_pk_id = block_pk_id;
        }



        public static class TypeValueOfValue{

            @SerializedName("pk_id")
            private int TypeValuePkId = 0;
            @SerializedName("value")
            private String TypeValue = null;

            public int getTypeValuePkId() {
                return TypeValuePkId;
            }

            public void setTypeValuePkId(int typeValuePkId) {
                TypeValuePkId = typeValuePkId;
            }

            public String getTypeValue() {
                return TypeValue;
            }

            public void setTypeValue(String typeValue) {
                TypeValue = typeValue;
            }

        }

    }



    public static class Location{

        @SerializedName("name")
        private String Name = null;
        @SerializedName("db_field_name")
        private String db_field_name = null;
        @Expose
        @SerializedName("value")
        private Serializable locationValueOfValue ;
        @SerializedName("type")
        private String type = null;
        @SerializedName("block_pk_id")
        private int block_pk_id = 0;

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public String getDb_field_name() {
            return db_field_name;
        }

        public void setDb_field_name(String db_field_name) {
            this.db_field_name = db_field_name;
        }

        public Serializable getLocationValueOfValue() {
            return locationValueOfValue;
        }

        public void setLocationValueOfValue(Serializable locationValueOfValue) {
            this.locationValueOfValue = locationValueOfValue;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public int getBlock_pk_id() {
            return block_pk_id;
        }

        public void setBlock_pk_id(int block_pk_id) {
            this.block_pk_id = block_pk_id;
        }




        public static class LocationValueOfValue {

            public int getLocationValuePkId() {
                return LocationValuePkId;
            }

            public void setLocationValuePkId(int locationValuePkId) {
                LocationValuePkId = locationValuePkId;
            }

            public String getLocationValueName() {
                return LocationValueName;
            }

            public void setLocationValueName(String locationValueName) {
                LocationValueName = locationValueName;
            }

            public String getLocationValueModifiedTime() {
                return LocationValueModifiedTime;
            }

            public void setLocationValueModifiedTime(String locationValueModifiedTime) {
                LocationValueModifiedTime = locationValueModifiedTime;
            }

            public String getLocationValueModifiedBy() {
                return LocationValueModifiedBy;
            }

            public void setLocationValueModifiedBy(String locationValueModifiedBy) {
                LocationValueModifiedBy = locationValueModifiedBy;
            }

            public int getLocationValueModifiedByPkId() {
                return LocationValueModifiedByPkId;
            }

            public void setLocationValueModifiedByPkId(int locationValueModifiedByPkId) {
                LocationValueModifiedByPkId = locationValueModifiedByPkId;
            }

            @SerializedName("pk_id")
            private int LocationValuePkId = 0;
            @SerializedName("Name")
            private String LocationValueName = null;
            @SerializedName("Modified Time")
            private String LocationValueModifiedTime = null;
            @SerializedName("Modified By")
            private String LocationValueModifiedBy = null;
            @SerializedName("Modified By pk_id")
            private int LocationValueModifiedByPkId = 0;
        }

    }
}
