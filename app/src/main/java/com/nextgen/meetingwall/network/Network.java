package com.nextgen.meetingwall.network;

import android.content.Context;
import android.util.Log;

import com.google.gson.GsonBuilder;
import com.nextgen.meetingwall.model.ModuleListMetadataArray;
import com.nextgen.meetingwall.network.json.AddUpdateTagsResponce;
import com.nextgen.meetingwall.network.json.AddUpdateTagsWrapper;
import com.nextgen.meetingwall.network.json.AgendaMeetingEmployeeWraper;
import com.nextgen.meetingwall.network.json.AgendasListResponce;
import com.nextgen.meetingwall.network.json.AgendasListWraper;
import com.nextgen.meetingwall.network.json.CreateAgendaSaveWraper;
import com.nextgen.meetingwall.network.json.CreateLocatationsWraper;
import com.nextgen.meetingwall.network.json.CreateParticipantsResponce;
import com.nextgen.meetingwall.network.json.CreateParticipantsWraper;
import com.nextgen.meetingwall.network.json.EssentialsSaveUpdateModuleDataWrapper;
import com.nextgen.meetingwall.network.json.MeetingEmployeesResponse;
import com.nextgen.meetingwall.network.json.MeetingListDataResponce;
import com.nextgen.meetingwall.network.json.MeetingLocationsResponse;
import com.nextgen.meetingwall.network.json.MeetingLocationsWraper;
import com.nextgen.meetingwall.network.json.MeetingParticipantsWraper;
import com.nextgen.meetingwall.network.json.MeetingReviewResponse;
import com.nextgen.meetingwall.network.json.MeetingWraper;
import com.nextgen.meetingwall.model.UserLoginBeen;
import com.nextgen.meetingwall.network.json.LoginResponse;
import com.nextgen.meetingwall.network.json.MeetingsResponce;
import com.nextgen.meetingwall.network.json.PickListDataResponce;
import com.nextgen.meetingwall.network.json.PickListDataWraper;
import com.nextgen.meetingwall.network.json.SaveUpdateModuleAgendasDataWraper;
import com.nextgen.meetingwall.network.json.SaveUpdateModuleDataResponce;
import com.nextgen.meetingwall.network.json.SaveUpdateModuleDataWraper;
import com.nextgen.meetingwall.utils.Constants;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Url;


public class Network {

    private final ExecutorService executorService = Executors.newFixedThreadPool(1);
    private final MeetingWallApiService meetingWallApiService;

    private static final String TAG = "Network";

    public Context context;

    public Network(Context ctx) {
        this.context = ctx;
        OkHttpClient client;


        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        // add logging as last interceptor

        builder.addInterceptor(new AddCookiesInterceptor(context)); // VERY VERY IMPORTANT
        builder.addInterceptor(new RecievedCookiesInterceptor(context)); // VERY VERY IMPORTANT
        builder.addInterceptor(new HttpLoggingInterceptor()); // VERY VERY IMPORTANT
        client = builder.build();

        Retrofit retrofit =
                new Retrofit.Builder()
                        .baseUrl(Constants.mConnectionUrl)
                        .client(client) // VERY VERY IMPORTANT
                        .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().serializeNulls().create()))
                        .build();
        meetingWallApiService = retrofit.create(MeetingWallApiService.class);
    }

    /**
     * Login Service request for Post method.
     */
    public void loginDetails(final UserLoginBeen userLoginBeen, final RequestCallback<LoginResponse> callback) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {

                    Call<LoginResponse> call = meetingWallApiService.login(userLoginBeen);

                    Response<LoginResponse> response = call.execute();
                    LoginResponse mResponse = response.body();

                    if (response.errorBody() != null) {
                        callback.onFailed(Constants.FAILED_RESPONSE);
                    }
                    if (mResponse != null && response.code() == 200) {
                        callback.onSuccess(mResponse);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFailed(Constants.connectionProblem);
                }
            }
        });
    }


    // Meetings Page Services

    /**
     *      ModuleListMetadata Service request for Get method.
     */

    public void ModuleListMetadata(final RequestCallback<ArrayList<ModuleListMetadataArray>> callback) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    ArrayList<ModuleListMetadataArray> list;
                    Call<MeetingsResponce> call = meetingWallApiService.ModuleListMetadata();
                    Response<MeetingsResponce> response = call.execute();
                    MeetingsResponce mResponse = response.body();

                    if (response.errorBody() != null) {
                        callback.onFailed(Constants.FAILED_RESPONSE);
                    }
                    if (mResponse != null && response.code() == 200) {
                        list = response.body().getDataArray();
                        callback.onSuccess(list);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFailed(Constants.connectionProblem);
                }
            }
        });
    }

    /**
     *      ModuleListData Service request for Post method.
     */
    public void ModuleListData(final MeetingWraper meetingWraper, final RequestCallback<MeetingListDataResponce> callback) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Call<MeetingListDataResponce> call = meetingWallApiService.ModuleListData(meetingWraper);
                    Response<MeetingListDataResponce> response = call.execute();
                    MeetingListDataResponce mResponse = response.body();


                    if (response.errorBody() != null) {
                        callback.onFailed(Constants.FAILED_RESPONSE);
                    }
                    if (mResponse != null && response.code() == 200) {
                        callback.onSuccess(mResponse);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFailed(Constants.connectionProblem);
                }
            }
        });
    }




    /**
     *      Places MataData  Request for AllMeetingsPage , MeetingLocation Page
     *      MeetingPlaces ModuleListMetadata Service request for Get method.
     */
    public void MeetingPlaces(final RequestCallback<ArrayList<ModuleListMetadataArray>> callback) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    ArrayList<ModuleListMetadataArray> list;
                    Call<MeetingsResponce> call = meetingWallApiService.MeetingPlaces();
                    Response<MeetingsResponce> response = call.execute();
                    MeetingsResponce mResponse = response.body();

                    if (response.errorBody() != null) {
                        callback.onFailed(Constants.FAILED_RESPONSE);
                    }
                    if (mResponse != null && response.code() == 200) {
                        list = response.body().getDataArray();
                        callback.onSuccess(list);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFailed(Constants.connectionProblem);
                }
            }
        });
    }

    /**
     *      MeetingPlaces Data  Request for  AllMeetingsPage
     *      List Of MeetingPlaces ModuleListData Service request for Post method.
     */

    public void ListMeetingPlaces(final MeetingLocationsWraper meetingLocationsWraper, final RequestCallback<MeetingLocationsResponse> callback) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Call<MeetingLocationsResponse> call = meetingWallApiService.getModuleListMeetingPlaces(meetingLocationsWraper);
                    Log.e(TAG, "request ::" + call.request().body());
                    Response<MeetingLocationsResponse> response = call.execute();
                    MeetingLocationsResponse mResponse = response.body();

                    if (response.errorBody() != null) {
                        callback.onFailed(Constants.FAILED_RESPONSE);
                    }
                    if (mResponse != null && response.code() == 200) {
                        callback.onSuccess(mResponse);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFailed(Constants.connectionProblem);
                }
            }
        });
    }


    /**
     *      MeetingPlaces Data  Request for MeetingLocation Page
     *      List Of MeetingPlaces ModuleListData Service request for Post method.
     */
    public void ListDataMeetingPlaces(final MeetingWraper meetingWraper, final RequestCallback<MeetingLocationsResponse> callback) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Call<MeetingLocationsResponse> call = meetingWallApiService.getModuleListDataMeetingPlaces(meetingWraper);
                    Response<MeetingLocationsResponse> response = call.execute();
                    MeetingLocationsResponse mResponse = response.body();

                    if (response.errorBody() != null) {
                        callback.onFailed(Constants.FAILED_RESPONSE);
                    }
                    if (mResponse != null && response.code() == 200) {
                        callback.onSuccess(mResponse);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFailed(Constants.connectionProblem);
                }
            }
        });
    }


    /**
     *      Employee MataData  Request for AllMeetingsPage , contactsListPage , AgendasCreationPage
     *      MeetingEmployees ModuleListMetadata Service request for Get method.
     */
    public void MeetingEmployees(final RequestCallback<ArrayList<ModuleListMetadataArray>> callback) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    ArrayList<ModuleListMetadataArray> list;
                    Call<MeetingsResponce> call = meetingWallApiService.Employees();
                    Response<MeetingsResponce> response = call.execute();
                    MeetingsResponce mResponse = response.body();

                    if (response.errorBody() != null) {
                        callback.onFailed(Constants.FAILED_RESPONSE);
                    }
                    if (mResponse != null && response.code() == 200) {
                        list = response.body().getDataArray();
                        callback.onSuccess(list);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFailed(Constants.connectionProblem);
                }
            }
        });
    }

    /**
     *      Employee Data  Request for AllMeetingsPage
     *      List Of MeetingParticipants ModuleListData Service request for Post method.
     */

    public void ListMeetingParticipants(final MeetingParticipantsWraper meetingParticipantsWraper, final RequestCallback<MeetingEmployeesResponse> callback) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Call<MeetingEmployeesResponse> call = meetingWallApiService.MeetingParticipants(meetingParticipantsWraper);
                    Response<MeetingEmployeesResponse> response = call.execute();
                    MeetingEmployeesResponse mResponse = response.body();

                    if (response.errorBody() != null) {
                        callback.onFailed(Constants.FAILED_RESPONSE);
                    }
                    if (mResponse != null && response.code() == 200) {
                        callback.onSuccess(mResponse);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFailed(Constants.connectionProblem);
                }
            }
        });
    }


    /**
     *      Employee Data  Request for  contactsListPage
     *      List Of MeetingEmployees ModuleListData Service request for Post method.
     */
    public void ListDataMeetingEmployees(final MeetingWraper meetingWraper, final RequestCallback<MeetingEmployeesResponse> callback) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Call<MeetingEmployeesResponse> call = meetingWallApiService.getModuleListDataMeetingEmployees(meetingWraper);
                    Response<MeetingEmployeesResponse> response = call.execute();
                    MeetingEmployeesResponse mResponse = response.body();

                    if (response.errorBody() != null) {
                        callback.onFailed(Constants.FAILED_RESPONSE);
                    }
                    if (mResponse != null && response.code() == 200) {
                        callback.onSuccess(mResponse);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFailed(Constants.connectionProblem);
                }
            }
        });
    }

    /**
     *      Employee Data  Request for  AgendasCreationPage
     *      List Of MeetingEmployees ModuleListData Service request for Post method.
     */

    public void AgendaListDataMeetingEmployees(final AgendaMeetingEmployeeWraper meetingWraper, final RequestCallback<MeetingEmployeesResponse> callback) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Call<MeetingEmployeesResponse> call = meetingWallApiService.getModuleListDataMeetingEmployeesAgenda(meetingWraper);
                    Response<MeetingEmployeesResponse> response = call.execute();
                    MeetingEmployeesResponse mResponse = response.body();

                    if (response.errorBody() != null) {
                        callback.onFailed(Constants.FAILED_RESPONSE);
                    }
                    if (mResponse != null && response.code() == 200) {
                        callback.onSuccess(mResponse);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFailed(Constants.connectionProblem);
                }
            }
        });
    }

    /**
     *     Create Meeting Data Method for  NewMeetingPage
     *      Meeting data Save to SaveUpdateModuleData  Service request for Post method.
     */

    public void SaveUpdateModuleData(final SaveUpdateModuleDataWraper saveUpdateModuleDataWraper, final RequestCallback<SaveUpdateModuleDataResponce> callback) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Call<SaveUpdateModuleDataResponce> call = meetingWallApiService.SaveUpdateModuleData(saveUpdateModuleDataWraper);
                    Response<SaveUpdateModuleDataResponce> response = call.execute();
                    SaveUpdateModuleDataResponce mResponse = response.body();

                    if (response.errorBody() != null) {
                        callback.onFailed(Constants.FAILED_RESPONSE);
                    }
                    if (mResponse != null && response.code() == 200) {
                        callback.onSuccess(mResponse);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFailed(Constants.connectionProblem);
                }
            }
        });
    }

    /**
     *      Meeting PickListData Method for  NewMeetingPage and Essentials Page for Categories
     *      category and Meeting types data Service request GetAllPickListValuesBySearch for Post method.
     */

    public void PickListData(final PickListDataWraper pickListDataWraper, final RequestCallback<PickListDataResponce> callback) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Call<PickListDataResponce> call = meetingWallApiService.PickListData(pickListDataWraper);
                    Response<PickListDataResponce> response = call.execute();
                    PickListDataResponce mResponse = response.body();

                    if (response.errorBody() != null) {
                        callback.onFailed(Constants.FAILED_RESPONSE);
                    }
                    if (mResponse != null && response.code() == 200) {
                        callback.onSuccess(mResponse);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFailed(Constants.connectionProblem);
                }
            }
        });
    }

    /**
     *     Create Participants Data Method for  NewMeetingPage
     *      Meeting data Save to SaveModuleRelationManyData  Service request for Post method.
     */

    public void createParticipants(final CreateParticipantsWraper createParticipantsWraper, final RequestCallback<CreateParticipantsResponce> callback) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Call<CreateParticipantsResponce> call = meetingWallApiService.CreateParticipants(createParticipantsWraper);
                    Response<CreateParticipantsResponce> response = call.execute();
                    CreateParticipantsResponce mResponse = response.body();

                    if (response.errorBody() != null) {
                        callback.onFailed(Constants.FAILED_RESPONSE);
                    }
                    if (mResponse != null && response.code() == 200) {
                        callback.onSuccess(mResponse);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFailed(Constants.connectionProblem);
                }
            }
        });
    }

    /**
     *     Create Locations Data Method for  NewMeetingPage
     *      Meeting data Save to SaveModuleRelationManyData  Service request for Post method.
     */

    public void createLocations(final CreateLocatationsWraper createLocatationsWraper, final RequestCallback<CreateParticipantsResponce> callback) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Call<CreateParticipantsResponce> call = meetingWallApiService.CreateLocations(createLocatationsWraper);
                    Response<CreateParticipantsResponce> response = call.execute();
                    CreateParticipantsResponce mResponse = response.body();

                    if (response.errorBody() != null) {
                        callback.onFailed(Constants.FAILED_RESPONSE);
                    }
                    if (mResponse != null && response.code() == 200) {
                        callback.onSuccess(mResponse);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFailed(Constants.connectionProblem);
                }
            }
        });
    }

    /**
     *     Create Agendas Data Method for  AgendasPage
     *      Agendas data Save to SaveUpdateModuleData  Service request for Post method.
     */
    public void SaveUpdateAgendaModuleData(final SaveUpdateModuleAgendasDataWraper saveUpdateModuleDataWraper, final RequestCallback<SaveUpdateModuleDataResponce> callback) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Call<SaveUpdateModuleDataResponce> call = meetingWallApiService.SaveUpdateAgendaModuleData(saveUpdateModuleDataWraper);
                    Response<SaveUpdateModuleDataResponce> response = call.execute();
                    SaveUpdateModuleDataResponce mResponse = response.body();

                    if (response.errorBody() != null) {
                        callback.onFailed(Constants.FAILED_RESPONSE);
                    }
                    if (mResponse != null && response.code() == 200) {
                        callback.onSuccess(mResponse);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFailed(Constants.connectionProblem);
                }
            }
        });
    }


    /**
     *      Agendas Data Method for  AgendasPage
     *      Agendas data Save to getModuleListMetadata/Agendas  Service request for GET method.
     */

    public void MeetingAgendas(final RequestCallback<ArrayList<ModuleListMetadataArray>> callback) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    ArrayList<ModuleListMetadataArray> list;
                    Call<MeetingsResponce> call = meetingWallApiService.getModuleListMetadataAgendas();
                    Response<MeetingsResponce> response = call.execute();
                    MeetingsResponce mResponse = response.body();

                    if (response.errorBody() != null) {
                        callback.onFailed(Constants.FAILED_RESPONSE);
                    }
                    if (mResponse != null && response.code() == 200) {
                        list = response.body().getDataArray();
                        callback.onSuccess(list);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFailed(Constants.connectionProblem);
                }
            }
        });
    }

    /**
     *      Agendas Data Method for  AgendasPage
     *     List Of Agendas data Save to getModuleListMetadata/Agendas  Service request for Post method.
     */

    public void ListMeetingAgendas(final AgendasListWraper meetingWraper, final RequestCallback<AgendasListResponce> callback) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Call<AgendasListResponce> call = meetingWallApiService.Agendas(meetingWraper);
                    Response<AgendasListResponce> response = call.execute();
                    AgendasListResponce mResponse = response.body();

                    if (response.errorBody() != null) {
                        callback.onFailed(Constants.FAILED_RESPONSE);
                    }
                    if (mResponse != null && response.code() == 200) {
                        callback.onSuccess(mResponse);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFailed(Constants.connectionProblem);
                }
            }
        });
    }


    /**
     *    Create  Agendas Data Method for  AgendasPage
     *     Create Agendas data Save to SaveModuleRelationManyData  Service request for Post method.
     */
    public void SaveCreateAgendas(final CreateAgendaSaveWraper createAgendaSaveWraper, final RequestCallback<CreateParticipantsResponce> callback) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Call<CreateParticipantsResponce> call = meetingWallApiService.CreateAgendas(createAgendaSaveWraper);
                    Response<CreateParticipantsResponce> response = call.execute();
                    CreateParticipantsResponce mResponse = response.body();

                    if (response.errorBody() != null) {
                        callback.onFailed(Constants.FAILED_RESPONSE);
                    }
                    if (mResponse != null && response.code() == 200) {
                        callback.onSuccess(mResponse);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFailed(Constants.connectionProblem);
                }
            }
        });
    }

    /**
     *    AddUpdateTags in Essentials
     *     Create Agendas data Save to SaveModuleRelationManyData  Service request for Post method.
     */
    public void AddUpdateTags(final int url, final AddUpdateTagsWrapper addUpdateTagsWrapper, final RequestCallback<AddUpdateTagsResponce> callback) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Call<AddUpdateTagsResponce> call = meetingWallApiService.AddUpdateTags(url,addUpdateTagsWrapper);
                    Response<AddUpdateTagsResponce> response = call.execute();
                    AddUpdateTagsResponce mResponse = response.body();

                    if (response.errorBody() != null) {
                        callback.onFailed(Constants.FAILED_RESPONSE);
                    }
                    if (mResponse != null && response.code() == 200) {
                        callback.onSuccess(mResponse);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFailed(Constants.connectionProblem);
                }
            }
        });
    }

    /**
     *     Essentials Meeting Data Method for  EssentialsPage
     *      Meeting data Save to EssentialsSaveUpdateModuleData  Service request for Post method.
     */

    public void EssentialsSaveUpdateModuleData(final EssentialsSaveUpdateModuleDataWrapper essentialsSaveUpdateModuleDataWrapper, final RequestCallback<SaveUpdateModuleDataResponce> callback) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Call<SaveUpdateModuleDataResponce> call = meetingWallApiService.EssentialsSaveUpdateModuleData(essentialsSaveUpdateModuleDataWrapper);
                    Response<SaveUpdateModuleDataResponce> response = call.execute();
                    SaveUpdateModuleDataResponce mResponse = response.body();

                    if (response.errorBody() != null) {
                        callback.onFailed(Constants.FAILED_RESPONSE);
                    }
                    if (mResponse != null && response.code() == 200) {
                        callback.onSuccess(mResponse);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFailed(Constants.connectionProblem);
                }
            }
        });
    }


    /**
     *      Meeting Data for Meeting Review Page
     *      MeetingData ModuleListMetadata Service request for Get method.
     */
    public void ReviewMeeting(final int id ,final RequestCallback<MeetingReviewResponse> callback) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {

                    Call<MeetingReviewResponse> call = meetingWallApiService.MeetingReview(id);
                    Response<MeetingReviewResponse> response = call.execute();
                    MeetingReviewResponse mResponse = response.body();

                    if (response.errorBody() != null) {
                        callback.onFailed(Constants.FAILED_RESPONSE);
                    }
                    if (mResponse != null && response.code() == 200) {

                        callback.onSuccess(mResponse);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    callback.onFailed(Constants.connectionProblem);
                }
            }
        });
    }

}
