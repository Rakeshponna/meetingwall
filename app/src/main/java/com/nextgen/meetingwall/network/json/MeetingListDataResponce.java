package com.nextgen.meetingwall.network.json;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.nextgen.meetingwall.model.MeetingDatesAscendingBean;

import java.util.ArrayList;

/**
 * Created by cas on 12-04-2017.
 */

public class MeetingListDataResponce {

    public jqGridDataBeen getJqGridData() {
        return jqGridData;
    }

    public void setJqGridData(jqGridDataBeen jqGridData) {
        this.jqGridData = jqGridData;
    }



    public Boolean getSuccess() {
        return Success;
    }

    public void setSuccess(Boolean success) {
        Success = success;
    }

    @SerializedName("success")
    private Boolean Success;
    @SerializedName("jqGridData")
    private jqGridDataBeen jqGridData ;

    public class jqGridDataBeen {
        public int getRecords() {
            return Records;
        }

        public void setRecords(int records) {
            Records = records;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

        public ArrayList<MeetinglistDataRowsBeen> getRowsArray() {
            return RowsArray;
        }

        public void setRowsArray(ArrayList<MeetinglistDataRowsBeen> rowsArray) {
            RowsArray = rowsArray;
        }

        @SerializedName("records")
        private int Records;

        @SerializedName("total")
        private int total;

        @SerializedName("page")
        private int page;

        @SerializedName("size")
        private int size;

        @SerializedName("rows")
        private ArrayList<MeetinglistDataRowsBeen> RowsArray ;
    }

    public static class MeetinglistDataRowsBeen implements Comparable<MeetinglistDataRowsBeen>{


        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String type;

        public int getMeetinglistDataId1() {
            return MeetinglistDataId1;
        }

        public void setMeetinglistDataId1(int meetinglistDataId1) {
            MeetinglistDataId1 = meetinglistDataId1;
        }

        public String getMeetingName() {
            return MeetingName;
        }

        public void setMeetingName(String meetingName) {
            MeetingName = meetingName;
        }

        public String getMeetingStartTime() {
            return MeetingStartTime;
        }

        public void setMeetingStartTime(String meetingStartTime) {
            MeetingStartTime = meetingStartTime;
        }

        public String getMeetingEndTime() {
            return MeetingEndTime;
        }

        public void setMeetingEndTime(String meetingEndTime) {
            MeetingEndTime = meetingEndTime;
        }

        public String getMeetingModifiedTime() {
            return MeetingModifiedTime;
        }

        public void setMeetingModifiedTime(String meetingModifiedTime) {
            MeetingModifiedTime = meetingModifiedTime;
        }

        public String getMeetingType() {
            return MeetingType;
        }

        public void setMeetingType(String meetingType) {
            MeetingType = meetingType;
        }

        public int getMeetingTypepk_id() {
            return MeetingTypepk_id;
        }

        public void setMeetingTypepk_id(int meetingTypepk_id) {
            MeetingTypepk_id = meetingTypepk_id;
        }

        public String getMeetingModifiedBy() {
            return MeetingModifiedBy;
        }

        public void setMeetingModifiedBy(String meetingModifiedBy) {
            MeetingModifiedBy = meetingModifiedBy;
        }

        public int getMeetingModifiedBypk_id() {
            return MeetingModifiedBypk_id;
        }

        public void setMeetingModifiedBypk_id(int meetingModifiedBypk_id) {
            MeetingModifiedBypk_id = meetingModifiedBypk_id;
        }

        @SerializedName("pk_id")
        private int MeetinglistDataId1;
        @SerializedName("Name")
        private String MeetingName;
        @SerializedName("Start Time")
        private String MeetingStartTime;
        @SerializedName("End Time")
        private String MeetingEndTime;
        @SerializedName("Modified Time")
        private String MeetingModifiedTime;
        @SerializedName("Type")
        private String MeetingType;
        @SerializedName("Type pk_id")
        private int MeetingTypepk_id;
        @SerializedName("Modified By")
        private String MeetingModifiedBy;
        @SerializedName("Modified By pk_id")
        private int MeetingModifiedBypk_id;
//        public MeetinglistDataRowsBeen(String load){
//            this.type = load;
//        }

//        public MeetinglistDataRowsBeen(String load ,int MeetinglistDataId1 ,String MeetingName,String MeetingStartTime ,String MeetingEndTime,String MeetingModifiedTime
//        ,String MeetingType,int MeetingTypepk_id,String MeetingModifiedBy,int MeetingModifiedBypk_id,MeetingEmployeesResponse meetingEmployeesResponse,
//                                       MeetingDatesAscendingBean meetingDatesAscendingBean,MeetingLocationsResponse meetingLocationsResponse,ArrayList<MeetingDatesAscendingBean> MeetingStartEndTimeArray) {
//            this.type = load;
//            this.MeetinglistDataId1 =MeetinglistDataId1;
//            this.MeetingName = MeetingName;
//            this.MeetingStartTime= MeetingStartTime;
//            this.MeetingEndTime=MeetingEndTime;
//            this.MeetingModifiedTime =MeetingModifiedTime;
//            this.MeetingType=MeetingType;
//            this.MeetingTypepk_id=MeetingTypepk_id;
//            this.MeetingModifiedBy=MeetingModifiedBy;
//            this.MeetingModifiedBypk_id =MeetingModifiedBypk_id;
//            this.meetingEmployeesResponse=meetingEmployeesResponse;
//            this.meetingDatesAscendingBean =meetingDatesAscendingBean;
//            this.meetingLocationsResponse =meetingLocationsResponse;
//            this.MeetingStartEndTimeArray =MeetingStartEndTimeArray;
//        }

        private MeetingEmployeesResponse meetingEmployeesResponse;

        public MeetingDatesAscendingBean getMeetingDatesAscendingBean() {
            return meetingDatesAscendingBean;
        }

        public void setMeetingDatesAscendingBean(MeetingDatesAscendingBean meetingDatesAscendingBean) {
            this.meetingDatesAscendingBean = meetingDatesAscendingBean;
        }

        private MeetingDatesAscendingBean meetingDatesAscendingBean;
        public MeetingEmployeesResponse getMeetingEmployeesResponse() {
            return meetingEmployeesResponse;
        }

        public void setMeetingEmployeesResponse(MeetingEmployeesResponse meetingEmployeesResponse) {
            this.meetingEmployeesResponse = meetingEmployeesResponse;
        }



        public MeetingLocationsResponse getMeetingLocationsResponse() {
            return meetingLocationsResponse;
        }

        public void setMeetingLocationsResponse(MeetingLocationsResponse meetingLocationsResponse) {
            this.meetingLocationsResponse = meetingLocationsResponse;
        }

        private MeetingLocationsResponse meetingLocationsResponse;

        public ArrayList<MeetingDatesAscendingBean> getMeetingStartEndTimeArray() {
            return MeetingStartEndTimeArray;
        }

        public void setMeetingStartEndTimeArray(ArrayList<MeetingDatesAscendingBean> meetingStartEndTimeArray) {
            MeetingStartEndTimeArray = meetingStartEndTimeArray;
        }

        private ArrayList<MeetingDatesAscendingBean> MeetingStartEndTimeArray;


        @Override
        public int compareTo(@NonNull MeetinglistDataRowsBeen o) {
            return getMeetingStartTime().compareTo(o.getMeetingStartTime());
        }
    }
}
