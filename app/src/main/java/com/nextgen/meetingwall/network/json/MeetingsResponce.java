package com.nextgen.meetingwall.network.json;

import com.google.gson.annotations.SerializedName;
import com.nextgen.meetingwall.model.ModuleListMetadataArray;

import java.util.ArrayList;

/**
 * Created by cas on 03-04-2017.
 */

public class MeetingsResponce {


    public ArrayList<ModuleListMetadataArray> getDataArray() {
        return DataArray;
    }

    public void setDataArray(ArrayList<ModuleListMetadataArray> dataArray) {
        DataArray = dataArray;
    }

    @SerializedName("data")
    private ArrayList<ModuleListMetadataArray> DataArray ;

    public Boolean getSuccess() {
        return Success;
    }

    public void setSuccess(Boolean success) {
        Success = success;
    }

    @SerializedName("success")
    private Boolean Success;


}
