package com.nextgen.meetingwall.network.json;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/*
 * Created by cas on 07-04-2017.
 */

public class EssentialsSaveUpdateModuleDataWrapper {


    public String getEssentialsModuleString() {
        return EssentialsModuleString;
    }

    public void setEssentialsModuleString(String essentialsModuleString) {
        EssentialsModuleString = essentialsModuleString;
    }

    public String getEssentialsActionString() {
        return EssentialsActionString;
    }

    public void setEssentialsActionString(String essentialsActionString) {
        EssentialsActionString = essentialsActionString;
    }

    public String getEssentialsPk_idString() {
        return EssentialsPk_idString;
    }

    public void setEssentialsPk_idString(String essentialsPk_idString) {
        EssentialsPk_idString = essentialsPk_idString;
    }

    public List<Object> getEssentialsDisplayColsArray() {
        return EssentialsDisplayColsArray;
    }

    public void setEssentialsDisplayColsArray(List<Object> essentialsDisplayColsArray) {
        EssentialsDisplayColsArray = essentialsDisplayColsArray;
    }

    @SerializedName("Module")
    private String EssentialsModuleString;
    @SerializedName("Action")
    private String EssentialsActionString;
    @SerializedName("pk_id")
    private String EssentialsPk_idString;
    @SerializedName("Fields")
    private List<Object> EssentialsDisplayColsArray ;







public static class EssentialsDisplayColsArray {


    public String getMeetingEssentialsName() {
        return MeetingEssentialsName;
    }

    public void setMeetingEssentialsName(String meetingEssentialsName) {
        MeetingEssentialsName = meetingEssentialsName;
    }

    public String getMeetingEssentialsDb_field_name() {
        return MeetingEssentialsDb_field_name;
    }

    public void setMeetingEssentialsDb_field_name(String meetingEssentialsDb_field_name) {
        MeetingEssentialsDb_field_name = meetingEssentialsDb_field_name;
    }

    public String getMeetingEssentialsValue() {
        return MeetingEssentialsValue;
    }

    public void setMeetingEssentialsValue(String meetingEssentialsValue) {
        MeetingEssentialsValue = meetingEssentialsValue;
    }

    public String getMeetingEssentialsType() {
        return MeetingEssentialsType;
    }

    public void setMeetingEssentialsType(String meetingEssentialsType) {
        MeetingEssentialsType = meetingEssentialsType;
    }

    public int getMeetingEssentialsBlock_pk_id() {
        return MeetingEssentialsBlock_pk_id;
    }

    public void setMeetingEssentialsBlock_pk_id(int meetingEssentialsBlock_pk_id) {
        MeetingEssentialsBlock_pk_id = meetingEssentialsBlock_pk_id;
    }

    @SerializedName("name")
    private String MeetingEssentialsName = null;
    @SerializedName("db_field_name")
    private String MeetingEssentialsDb_field_name = null;
    @SerializedName("value")
    private String MeetingEssentialsValue ;
    @SerializedName("type")
    private String MeetingEssentialsType = null;
    @SerializedName("block_pk_id")
    private int MeetingEssentialsBlock_pk_id = 0;


}

    public static class EssentialsType{

        public String getEssentialsTypeName() {
            return EssentialsTypeName;
        }

        public void setEssentialsTypeName(String essentialsTypeName) {
            EssentialsTypeName = essentialsTypeName;
        }

        public String getEssentialsTypeDb_field_name() {
            return EssentialsTypeDb_field_name;
        }

        public void setEssentialsTypeDb_field_name(String essentialsTypeDb_field_name) {
            EssentialsTypeDb_field_name = essentialsTypeDb_field_name;
        }

        public int getEssentialsTypePick_list_id() {
            return EssentialsTypePick_list_id;
        }

        public void setEssentialsTypePick_list_id(int essentialsTypePick_list_id) {
            EssentialsTypePick_list_id = essentialsTypePick_list_id;
        }

        public EssentialsType.EssentialsTypeOfValue getEssentialsTypeOfValue() {
            return EssentialsTypeOfValue;
        }

        public void setEssentialsTypeOfValue(EssentialsType.EssentialsTypeOfValue essentialsTypeOfValue) {
            EssentialsTypeOfValue = essentialsTypeOfValue;
        }

        public String getEssentialsTypeOfType() {
            return EssentialsTypeOfType;
        }

        public void setEssentialsTypeOfType(String essentialsTypeOfType) {
            EssentialsTypeOfType = essentialsTypeOfType;
        }

        public int getEssentialsTypeBlock_pk_id() {
            return EssentialsTypeBlock_pk_id;
        }

        public void setEssentialsTypeBlock_pk_id(int essentialsTypeBlock_pk_id) {
            EssentialsTypeBlock_pk_id = essentialsTypeBlock_pk_id;
        }

        @SerializedName("name")
        private String EssentialsTypeName = null;
        @SerializedName("db_field_name")
        private String EssentialsTypeDb_field_name = null;
        @SerializedName("pick_list_id")
        private int EssentialsTypePick_list_id = 0;
        @SerializedName("value")
        private EssentialsTypeOfValue EssentialsTypeOfValue ;
        @SerializedName("type")
        private String EssentialsTypeOfType = null;
        @SerializedName("block_pk_id")
        private int  EssentialsTypeBlock_pk_id = 0;



        public static class EssentialsTypeOfValue{

            public int getEssentialsTypeOfValueTypeValuePkId() {
                return EssentialsTypeOfValueTypeValuePkId;
            }

            public void setEssentialsTypeOfValueTypeValuePkId(int essentialsTypeOfValueTypeValuePkId) {
                EssentialsTypeOfValueTypeValuePkId = essentialsTypeOfValueTypeValuePkId;
            }

            public String getEssentialsTypeOfValueTypeValue() {
                return EssentialsTypeOfValueTypeValue;
            }

            public void setEssentialsTypeOfValueTypeValue(String essentialsTypeOfValueTypeValue) {
                EssentialsTypeOfValueTypeValue = essentialsTypeOfValueTypeValue;
            }

            @SerializedName("pk_id")
            private int EssentialsTypeOfValueTypeValuePkId = 0;
            @SerializedName("value")
            private String EssentialsTypeOfValueTypeValue = null;


        }

    }



    public static class EssentialsCategories{

        public String getEssentialsCategoriesName() {
            return EssentialsCategoriesName;
        }

        public void setEssentialsCategoriesName(String essentialsCategoriesName) {
            EssentialsCategoriesName = essentialsCategoriesName;
        }

        public String getEssentialsCategoriesDb_field_name() {
            return EssentialsCategoriesDb_field_name;
        }

        public void setEssentialsCategoriesDb_field_name(String essentialsCategoriesDb_field_name) {
            EssentialsCategoriesDb_field_name = essentialsCategoriesDb_field_name;
        }

        public int getEssentialsCategoriesPick_list_id() {
            return EssentialsCategoriesPick_list_id;
        }

        public void setEssentialsCategoriesPick_list_id(int essentialsCategoriesPick_list_id) {
            EssentialsCategoriesPick_list_id = essentialsCategoriesPick_list_id;
        }

        public EssentialsCategories.EssentialsCategoriesValue getEssentialsCategoriesValue() {
            return EssentialsCategoriesValue;
        }

        public void setEssentialsCategoriesValue(EssentialsCategories.EssentialsCategoriesValue essentialsCategoriesValue) {
            EssentialsCategoriesValue = essentialsCategoriesValue;
        }

        public String getEssentialsCategoriesType() {
            return EssentialsCategoriesType;
        }

        public void setEssentialsCategoriesType(String essentialsCategoriesType) {
            EssentialsCategoriesType = essentialsCategoriesType;
        }

        public int getEssentialsCategoriesBlock_pk_id() {
            return EssentialsCategoriesBlock_pk_id;
        }

        public void setEssentialsCategoriesBlock_pk_id(int essentialsCategoriesBlock_pk_id) {
            EssentialsCategoriesBlock_pk_id = essentialsCategoriesBlock_pk_id;
        }

        @SerializedName("name")
        private String EssentialsCategoriesName = null;
        @SerializedName("db_field_name")
        private String EssentialsCategoriesDb_field_name = null;
        @SerializedName("pick_list_id")
        private int EssentialsCategoriesPick_list_id = 0;
        @SerializedName("value")
        private EssentialsCategoriesValue EssentialsCategoriesValue ;
        @SerializedName("type")
        private String EssentialsCategoriesType = null;
        @SerializedName("block_pk_id")
        private int EssentialsCategoriesBlock_pk_id = 0;


    public static class EssentialsCategoriesValue{

        public int getEssentialsCategoriesValue_pk_id() {
            return EssentialsCategoriesValue_pk_id;
        }

        public void setEssentialsCategoriesValue_pk_id(int essentialsCategoriesValue_pk_id) {
            EssentialsCategoriesValue_pk_id = essentialsCategoriesValue_pk_id;
        }



        public String getEssentialsCategoriesValue_value() {
            return EssentialsCategoriesValue_value;
        }

        public void setEssentialsCategoriesValue_value(String essentialsCategoriesValue_value) {
            EssentialsCategoriesValue_value = essentialsCategoriesValue_value;
        }

        @SerializedName("pk_id")
        private int EssentialsCategoriesValue_pk_id ;
        @SerializedName("value")
        private String EssentialsCategoriesValue_value ;
    }


    public static class EssentialsObjective{

        public String getEssentialsObjectiveName() {
            return EssentialsObjectiveName;
        }

        public void setEssentialsObjectiveName(String essentialsObjectiveName) {
            EssentialsObjectiveName = essentialsObjectiveName;
        }

        public String getEssentialsObjectiveDb_field_name() {
            return EssentialsObjectiveDb_field_name;
        }

        public void setEssentialsObjectiveDb_field_name(String essentialsObjectiveDb_field_name) {
            EssentialsObjectiveDb_field_name = essentialsObjectiveDb_field_name;
        }

        public String getEssentialsObjectiveType() {
            return EssentialsObjectiveType;
        }

        public void setEssentialsObjectiveType(String essentialsObjectiveType) {
            EssentialsObjectiveType = essentialsObjectiveType;
        }

        public int getEssentialsObjectiveBlock_pk_id() {
            return EssentialsObjectiveBlock_pk_id;
        }

        public void setEssentialsObjectiveBlock_pk_id(int essentialsObjectiveBlock_pk_id) {
            EssentialsObjectiveBlock_pk_id = essentialsObjectiveBlock_pk_id;
        }

        @SerializedName("name")
        private String EssentialsObjectiveName = null;
        @SerializedName("db_field_name")
        private String EssentialsObjectiveDb_field_name = null;

        public String getEssentialsObjectivevalue() {
            return EssentialsObjectivevalue;
        }

        public void setEssentialsObjectivevalue(String essentialsObjectivevalue) {
            EssentialsObjectivevalue = essentialsObjectivevalue;
        }

        @SerializedName("value")
        private String EssentialsObjectivevalue = null;
        @SerializedName("type")
        private String EssentialsObjectiveType = null;
        @SerializedName("block_pk_id")
        private int EssentialsObjectiveBlock_pk_id = 0;
    }

    }
}
