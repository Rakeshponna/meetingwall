package com.nextgen.meetingwall.network.json;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by cas on 21-04-2017.
 */

public class AddUpdateTagsResponce {


    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @SerializedName("success")
    private Boolean success ;
    @SerializedName("message")
    private String message ;

}
