package com.nextgen.meetingwall.network.json;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by cas on 12-04-2017.
 */

public class AgendasListResponce {

    public jqGridDataBeen getJqGridData() {
        return jqGridData;
    }

    public void setJqGridData(jqGridDataBeen jqGridData) {
        this.jqGridData = jqGridData;
    }



    public Boolean getSuccess() {
        return Success;
    }

    public void setSuccess(Boolean success) {
        Success = success;
    }

    @SerializedName("success")
    private Boolean Success;
    @SerializedName("jqGridData")
    private jqGridDataBeen jqGridData ;

    public class jqGridDataBeen {
        public int getRecords() {
            return Records;
        }

        public void setRecords(int records) {
            Records = records;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

        public ArrayList<AgendasRowsBeen> getRowsArray() {
            return RowsArray;
        }

        public void setRowsArray(ArrayList<AgendasRowsBeen> rowsArray) {
            RowsArray = rowsArray;
        }

        @SerializedName("records")
        private int Records;

        @SerializedName("total")
        private int total;

        @SerializedName("page")
        private int page;

        @SerializedName("size")
        private int size;

        @SerializedName("rows")
        private ArrayList<AgendasRowsBeen> RowsArray ;
    }

    public class AgendasRowsBeen {

        public int getAgendasRowsPkIdInt() {
            return AgendasRowsPkIdInt;
        }

        public void setAgendasRowsPkIdInt(int agendasRowsPkIdInt) {
            AgendasRowsPkIdInt = agendasRowsPkIdInt;
        }

        public String getAgendasRowsNameString() {
            return AgendasRowsNameString;
        }

        public void setAgendasRowsNameString(String agendasRowsNameString) {
            AgendasRowsNameString = agendasRowsNameString;
        }

        public String getAgendasRowsStartTimeString() {
            return AgendasRowsStartTimeString;
        }

        public void setAgendasRowsStartTimeString(String agendasRowsStartTimeString) {
            AgendasRowsStartTimeString = agendasRowsStartTimeString;
        }

        public String getAgendasRowsEndTimeString() {
            return AgendasRowsEndTimeString;
        }

        public void setAgendasRowsEndTimeString(String agendasRowsEndTimeString) {
            AgendasRowsEndTimeString = agendasRowsEndTimeString;
        }

        public String getAgendasRowsEmployeeString() {
            return AgendasRowsEmployeeString;
        }

        public void setAgendasRowsEmployeeString(String agendasRowsEmployeeString) {
            AgendasRowsEmployeeString = agendasRowsEmployeeString;
        }

        public int getAgendasRowsEmployeePkIdInt() {
            return AgendasRowsEmployeePkIdInt;
        }

        public void setAgendasRowsEmployeePkIdInt(int agendasRowsEmployeePkIdInt) {
            AgendasRowsEmployeePkIdInt = agendasRowsEmployeePkIdInt;
        }

        @SerializedName("pk_id")
        private int AgendasRowsPkIdInt;
        @SerializedName("Name")
        private String AgendasRowsNameString;
        @SerializedName("Start Time")
        private String AgendasRowsStartTimeString;
        @SerializedName("End Time")
        private String AgendasRowsEndTimeString;
        @SerializedName("Employee")
        private String AgendasRowsEmployeeString;
        @SerializedName("Employee pk_id")
        private int AgendasRowsEmployeePkIdInt;
    }


}
