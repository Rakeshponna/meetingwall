package com.nextgen.meetingwall.model;

/**
 * Created by cas on 07-02-2017.
 */

public class DesignReviewMeetingBeen {

    private  String Time;
    private String DesignReview;
    private String Date;
    private  String Day;
    private  String ProfileImage;

    public String getProfileImage() {
        return ProfileImage;
    }

    public void setProfileImage(String profileImage) {
        ProfileImage = profileImage;
    }


    public String getDesignReview() {
        return DesignReview;
    }

    public void setDesignReview(String designReview) {
        DesignReview = designReview;
    }



    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }



    public String getDay() {
        return Day;
    }

    public void setDay(String day) {
        Day = day;
    }



    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }


}
