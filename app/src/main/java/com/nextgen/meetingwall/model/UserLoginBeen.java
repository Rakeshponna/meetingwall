package com.nextgen.meetingwall.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by cas on 31-03-2017.
 */

public class UserLoginBeen {

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @SerializedName("Password")
    private String Password;
    @SerializedName("UserName")
    private String email;
}
