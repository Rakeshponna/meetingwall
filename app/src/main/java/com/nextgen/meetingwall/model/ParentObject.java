package com.nextgen.meetingwall.model;

import com.nextgen.meetingwall.network.json.AgendasListResponce;

import java.util.List;

/**
 * Created by TrangHo on 10-05-2015.
 */
public class ParentObject {
    public AgendasListResponce.AgendasRowsBeen mother;

    public List<ChildObject> childObjects;


    public ParentObject(AgendasListResponce.AgendasRowsBeen agendasListResponce, List<ChildObject> children) {
        this.mother = agendasListResponce;
        this.childObjects = children;
    }
}
