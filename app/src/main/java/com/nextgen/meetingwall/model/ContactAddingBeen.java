package com.nextgen.meetingwall.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.nextgen.meetingwall.ui.activity.ContactsListActivity;

import java.util.ArrayList;

/**
 * Created by admin on 20/01/2017 AD.
 */

public class ContactAddingBeen implements Parcelable {
    private String DesignReview;
    private String ReviewDescription;
    private String Country;
    private String TimeDuration;
    private String HeaderTime;
    private int flag;
    private ArrayList<UserOnlineStatusBean> Images;

    protected ContactAddingBeen(Parcel in) {
        DesignReview = in.readString();
        ReviewDescription = in.readString();
        Country = in.readString();
        TimeDuration = in.readString();
        HeaderTime = in.readString();
        flag = in.readInt();
        Images =in.readArrayList(ContactsListActivity.class.getClassLoader());

;    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(DesignReview);
        dest.writeString(ReviewDescription);
        dest.writeString(Country);
        dest.writeString(TimeDuration);
        dest.writeString(HeaderTime);
        dest.writeInt(flag);
        dest.writeList(Images);

    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ContactAddingBeen> CREATOR = new Creator<ContactAddingBeen>() {
        @Override
        public ContactAddingBeen createFromParcel(Parcel in) {
            return new ContactAddingBeen(in);
        }

        @Override
        public ContactAddingBeen[] newArray(int size) {
            return new ContactAddingBeen[size];
        }
    };

    public Boolean getSelected() {
        return isSelected;
    }

    public void setSelected(Boolean selected) {
        isSelected = selected;
    }

    private Boolean isSelected;


    public void setImages(ArrayList<UserOnlineStatusBean> images) {
        Images = images;
    }
    public ArrayList<UserOnlineStatusBean> getImages() {
        return Images;
    }
    public ContactAddingBeen() {
        this.ReviewDescription = ReviewDescription;
        this.Country = Country;
        this.TimeDuration = TimeDuration;
        this.HeaderTime = HeaderTime;
        this.flag = flag;
        this.Images=Images;
    }



    public String getDesignReview() {
        return DesignReview;
    }
    public void setDesignReview(String id) {
        this. DesignReview=id;
    }

    public String getReviewDescription() {
        return ReviewDescription;
    }
    public void setReviewDescription(String filmName) {
        this.ReviewDescription = filmName;
    }

    public String getCountry() {
        return Country;
    }
    public void setCountry(String country) {
        this.Country = country;
    }

    public String getTimeDuration() {
        return TimeDuration;
    }
    public void setTimeDuration(String population) {
        this. TimeDuration =population;
    }

    public String getHeaderTime() {
        return HeaderTime;
    }
    public void setHeaderTime(String price) {
        this. HeaderTime=price;
    }


    public void setFlag(int flag) {
        this.flag = flag;
    }


    public int getFlag() {
        return this.flag;
    }


}
