package com.nextgen.meetingwall.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by cas on 03-02-2017.
 */

public class UserOnlineStatusBean implements Parcelable {

    private String image;


    public UserOnlineStatusBean(Parcel in) {
        image = in.readString();
        isAvailable = in.readByte() != 0;
        OnlineStatus = in.readByte() != 0;
        UserEPEI = in.readString();
    }

    public static final Creator<UserOnlineStatusBean> CREATOR = new Creator<UserOnlineStatusBean>() {
        @Override
        public UserOnlineStatusBean createFromParcel(Parcel in) {
            return new UserOnlineStatusBean(in);
        }

        @Override
        public UserOnlineStatusBean[] newArray(int size) {
            return new UserOnlineStatusBean[size];
        }
    };

    public UserOnlineStatusBean() {
        this.image = image;

    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    private boolean isAvailable;



    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isOnlineStatus() {
        return OnlineStatus;
    }

    public void setOnlineStatus(boolean onlineStatus) {
        OnlineStatus = onlineStatus;
    }

    private boolean OnlineStatus;


    public String getUserEPEI() {
        return UserEPEI;
    }

    public void setUserEPEI(String userEPEI) {
        UserEPEI = userEPEI;
    }

    private String UserEPEI;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(image);
        dest.writeByte((byte) (isAvailable ? 1 : 0));
        dest.writeByte((byte) (OnlineStatus ? 1 : 0));
        dest.writeString(UserEPEI);
    }
}
