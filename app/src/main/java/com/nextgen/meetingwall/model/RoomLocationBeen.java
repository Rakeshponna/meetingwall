package com.nextgen.meetingwall.model;

/**
 * Created by cas on 23-03-2017.
 */

public class RoomLocationBeen {

    public Boolean getRoomSelected() {
        return isRoomSelected;
    }

    public void setRoomSelected(Boolean roomSelected) {
        isRoomSelected = roomSelected;

    }

    private Boolean isRoomSelected;

    public String getRoomLocation() {
        return RoomLocation;
    }

    public void setRoomLocation(String roomLocation) {
        RoomLocation = roomLocation;
    }

    private String RoomLocation;
}
