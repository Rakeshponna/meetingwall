package com.nextgen.meetingwall.model;

/**
 * Created by cas on 22-03-2017.
 */

public class OthermeetingdetailsBeen {

    public String getMeetingTime() {
        return MeetingTime;
    }

    public void setMeetingTime(String meetingTime) {
        MeetingTime = meetingTime;
    }

    public String getMeetingName() {
        return MeetingName;
    }

    public void setMeetingName(String meetingName) {
        MeetingName = meetingName;
    }


    public String getMeetingLocation() {
        return MeetingLocation;
    }

    public void setMeetingLocation(String meetingLocation) {
        MeetingLocation = meetingLocation;
    }



    public String getMeetingLocationTime() {
        return MeetingLocationTime;
    }

    public void setMeetingLocationTime(String meetingLocationTime) {
        MeetingLocationTime = meetingLocationTime;
    }
    private  String MeetingTime;
    private String MeetingName;
    private String MeetingLocation;
    private String MeetingLocationTime;
}
