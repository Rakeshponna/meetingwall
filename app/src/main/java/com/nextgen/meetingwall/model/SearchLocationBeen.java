package com.nextgen.meetingwall.model;

import java.util.ArrayList;

/**
 * Created by cas on 23-03-2017.
 */

public class SearchLocationBeen {

    public String getCountryLocation() {
        return CountryLocation;
    }

    public void setCountryLocation(String countryLocation) {
        CountryLocation = countryLocation;
    }

    private  String CountryLocation;

    public String getRoomLocation() {
        return RoomLocation;
    }

    public void setRoomLocation(String roomLocation) {
        RoomLocation = roomLocation;
    }

    private String RoomLocation;

    public ArrayList<RoomLocationBeen> getHotelListArray() {
        return HotelListArray;
    }

    public void setHotelListArray(ArrayList<RoomLocationBeen> hotelListArray) {
        HotelListArray = hotelListArray;
    }

    private ArrayList<RoomLocationBeen> HotelListArray;

    public Boolean getSelected() {
        return isSelected;
    }

    public void setSelected(Boolean selected) {
        isSelected = selected;
    }

    private Boolean isSelected;


}
