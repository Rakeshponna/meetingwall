package com.nextgen.meetingwall.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by cas on 04-04-2017.
 */

public class ModuleListMetadataArray {
    public String getDbTableNameString() {
        return DbTableNameString;
    }

    public void setDbTableNameString(String dbTableNameString) {
        DbTableNameString = dbTableNameString;
    }

    @SerializedName("db_table_name")
    private String DbTableNameString;
    @SerializedName("name")
    private String NameString;
    @SerializedName("db_field_name")
    private String DbFieldNameString;
    @SerializedName("type")
    private String TypeString;
    @SerializedName("template")
    private String TemplateString;
    @SerializedName("width")
    private int WidthString;
    @SerializedName("is_hidden")
    private Boolean IsHiddenString;
    @SerializedName("header_name")
    private String HeaderNameString;
    @SerializedName("user_meta_field")
    private int user_meta_field;

    public String getNameString() {
        return NameString;
    }

    public void setNameString(String nameString) {
        NameString = nameString;
    }

    public String getDbFieldNameString() {
        return DbFieldNameString;
    }

    public void setDbFieldNameString(String dbFieldNameString) {
        DbFieldNameString = dbFieldNameString;
    }

    public String getTypeString() {
        return TypeString;
    }

    public void setTypeString(String typeString) {
        TypeString = typeString;
    }

    public String getTemplateString() {
        return TemplateString;
    }

    public void setTemplateString(String templateString) {
        TemplateString = templateString;
    }

    public int getWidthString() {
        return WidthString;
    }

    public void setWidthString(int widthString) {
        WidthString = widthString;
    }

    public Boolean getHiddenString() {
        return IsHiddenString;
    }

    public void setHiddenString(Boolean hiddenString) {
        IsHiddenString = hiddenString;
    }

    public String getHeaderNameString() {
        return HeaderNameString;
    }

    public void setHeaderNameString(String headerNameString) {
        HeaderNameString = headerNameString;
    }

    public int getUser_meta_field() {
        return user_meta_field;
    }

    public void setUser_meta_field(int user_meta_field) {
        this.user_meta_field = user_meta_field;
    }


}
