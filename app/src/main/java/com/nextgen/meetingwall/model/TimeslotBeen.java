package com.nextgen.meetingwall.model;

import java.util.ArrayList;

/**
 * Created by cas on 21-03-2017.
 */

public class TimeslotBeen {

    private String TimeSting;
    private String DateString;
    private String AmString;
    private boolean AmorPm;
    private boolean ShowMeetingDetails;

    private String MeetingTime;
    private String MeetingType;
    private String MeetingLocation;
    private String OtherMeetingNumber;
    private String MeetingStartTime;
    private String MeetingEndTime;

    public Boolean isSelectTime() {
        return selectTime;
    }

    public void setSelectTime(Boolean selectTime) {
        this.selectTime = selectTime;
    }

    private Boolean selectTime;

    private ArrayList<OthermeetingdetailsBeen> Meetings;

    public ArrayList<OthermeetingdetailsBeen> getMeetings() {
        return Meetings;
    }

    public void setMeetings(ArrayList<OthermeetingdetailsBeen> meetings) {
        Meetings = meetings;
    }



    public String getMeetingStartTime() {
        return MeetingStartTime;
    }

    public void setMeetingStartTime(String meetingStartTime) {
        MeetingStartTime = meetingStartTime;
    }

    public String getMeetingEndTime() {
        return MeetingEndTime;
    }

    public void setMeetingEndTime(String meetingEndTime) {
        MeetingEndTime = meetingEndTime;
    }


    public String getOtherMeetingNumber() {
        return OtherMeetingNumber;
    }

    public void setOtherMeetingNumber(String otherMeetingNumber) {
        OtherMeetingNumber = otherMeetingNumber;
    }



    public String getMeetingTime() {
        return MeetingTime;
    }

    public void setMeetingTime(String meetingTime) {
        MeetingTime = meetingTime;
    }

    public String getMeetingType() {
        return MeetingType;
    }

    public void setMeetingType(String meetingType) {
        MeetingType = meetingType;
    }

    public String getMeetingLocation() {
        return MeetingLocation;
    }

    public void setMeetingLocation(String meetingLocation) {
        MeetingLocation = meetingLocation;
    }


    public boolean isShowMeetingDetails() {
        return ShowMeetingDetails;
    }

    public void setShowMeetingDetails(boolean showMeetingDetails) {
        ShowMeetingDetails = showMeetingDetails;
    }



    public boolean isBackground() {
        return IsBackground;
    }

    public void setBackground(boolean background) {
        IsBackground = background;
    }

    private boolean IsBackground;
    public String getTimeSting() {
        return TimeSting;
    }

    public void setTimeSting(String timeSting) {
        TimeSting = timeSting;
    }

    public String getDateString() {
        return DateString;
    }

    public void setDateString(String dateString) {
        DateString = dateString;
    }


    public boolean isAmorPm() {
        return AmorPm;
    }

    public void setAmorPm(boolean amorPm) {
        AmorPm = amorPm;
    }


    public String getAmString() {
        return AmString;
    }

    public void setAmString(String amString) {
        AmString = amString;
    }


}
