package com.nextgen.meetingwall.model;

import java.util.Date;

/**
 * Created by cas on 03-02-2017.
 */

public class MeetingDatesAscendingBean implements Comparable<MeetingDatesAscendingBean>{


    private boolean isdateStatus;

    private java.util.Date dateTime;

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date datetime) {
        this.dateTime = datetime;
    }

    @Override
    public int compareTo(MeetingDatesAscendingBean o) {
        return o.getDateTime().compareTo(getDateTime());
    }


    public boolean isdate() {
        return isdateStatus;
    }

    public void setisdate(boolean dateStatus) {
        isdateStatus = dateStatus;
    }

    public MeetingDatesAscendingBean() {

    }

}
