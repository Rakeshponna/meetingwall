package com.nextgen.meetingwall.model;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by admin on 20/01/2017 AD.
 */

public class MeetingBeen {
    private String DesignReview;
    private String ReviewDescription;
    private String Country;
    private String TimeDuration;
    private String HeaderTime;
    private int flag;
    private ArrayList<UserOnlineStatusBean> Images;
    private ArrayList<MeetingDatesAscendingBean> Dates;

    public boolean isOnclick() {
        return Onclick;
    }

    public void setOnclick(boolean onclick) {
        Onclick = onclick;
    }

    private boolean Onclick;
    public void setFlag(int flag) {
        this.flag = flag;
    }

    public void setImages(ArrayList<UserOnlineStatusBean> images) {
        Images = images;
    }
    public ArrayList<UserOnlineStatusBean> getImages() {
        return Images;
    }
    public void setDates(ArrayList<MeetingDatesAscendingBean> dates) {
        Dates = dates;
    }
    public ArrayList<MeetingDatesAscendingBean> getDates() {
        return Dates;
    }
    public MeetingBeen() {
        this.ReviewDescription = ReviewDescription;
        this.Country = Country;
        this.TimeDuration = TimeDuration;
        this.HeaderTime = HeaderTime;
        this.flag = flag;
    }



    public String getDesignReview() {
        return DesignReview;
    }
    public void setDesignReview(String id) {
        this. DesignReview=id;
    }

    public String getReviewDescription() {
        return ReviewDescription;
    }
    public void setReviewDescription(String filmName) {
        this.ReviewDescription = filmName;
    }

    public String getCountry() {
        return Country;
    }
    public void setCountry(String country) {
        this.Country = country;
    }

    public String getTimeDuration() {
        return TimeDuration;
    }
    public void setTimeDuration(String population) {
        this. TimeDuration =population;
    }

    public String getHeaderTime() {
        return HeaderTime;
    }
    public void setHeaderTime(String price) {
        this. HeaderTime=price;
    }




    public int getFlag() {
        return this.flag;
    }




}
