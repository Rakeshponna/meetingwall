package com.nextgen.meetingwall.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.nextgen.meetingwall.utils.Constants;


public class Storage {

    private final SharedPreferences settings;

    public Storage(Context context )
    {
        settings = PreferenceManager.getDefaultSharedPreferences( context );
    }

    public void saveSecure(String key, String value )
    {
        settings.edit().putString( key, value ).apply();
    }
    public void saveSecureBoolean(String key, boolean value )
    {
        settings.edit().putBoolean( key, value ).apply();
    }

    public String getValue(String key )
    {
        return settings.getString( key, "" );
    }
    public boolean getBooleanValue( String key )
    {
        return settings.getBoolean( key, false );
    }

    public void clearAll()
    {
        settings.edit().
                remove(Constants.IS_USER_ID).
                remove( Constants.IS_USERNAME ).
                remove( Constants.JOBROLL ).
                remove(Constants.IS_USERNAME).

                apply();


    }

}
