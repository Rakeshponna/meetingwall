package com.materialcalendarview.been;

/**
 * Created by cas on 10-03-2017.
 */

public class CalendarBeen {

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;

    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    private int year;
    private int month;
    private int day;

}
