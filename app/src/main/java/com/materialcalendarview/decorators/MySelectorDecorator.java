package com.materialcalendarview.decorators;

import android.app.Activity;
import android.graphics.drawable.Drawable;

import com.materialcalendarview.CalendarDay;
import com.materialcalendarview.DayViewDecorator;
import com.materialcalendarview.DayViewFacade;
import com.nextgen.meetingwall.R;

/**
 * Use a custom selector
 */
public class MySelectorDecorator implements DayViewDecorator {

    private final Drawable drawable;

    public MySelectorDecorator(Activity context) {
        drawable = context.getResources().getDrawable(R.drawable.my_selector);
    }

    @Override
    public boolean shouldDecorate(CalendarDay day) {
        return true;
    }

    @Override
    public void decorate(DayViewFacade view) {
        view.setSelectionDrawable(drawable);
    }
}
